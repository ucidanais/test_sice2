<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    'url_sice' => env('URL_SICE'),
    
    'url_sicedb' => env('URL_SICEDB'),

    'google_api_key' => env('GOOGLE_API_KEY', 'AIzaSyDbtreKJq8y1pQ3J9Dp7qKOIk3e_-a2wFQ'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://sicev2.com.devel'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'America/Guayaquil',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'es',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => env('APP_LOG', 'single'),

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Application Service Providers...
         */
        SICE\Providers\AppServiceProvider::class,
        SICE\Providers\AuthServiceProvider::class,
        SICE\Providers\EventServiceProvider::class,
        SICE\Providers\RouteServiceProvider::class,

        /*
         *   MONGODB
         */
        Jenssegers\Mongodb\MongodbServiceProvider::class,
        Jenssegers\Mongodb\Auth\PasswordResetServiceProvider::class,

        /*
         *  FORMS & HTML
         */
        Collective\Html\HtmlServiceProvider::class,

        /*
         *   MANEJO DE PDFs
         */
        Barryvdh\DomPDF\ServiceProvider::class,

        /*
         *   MANEJO DE ARCHIVOS EXCEL
         */
        Maatwebsite\Excel\ExcelServiceProvider::class,

        /*
         *   MANEJO DE IMAGENES
         */
        Orchestra\Imagine\ImagineServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,

        /*
         *   MANEJO DE SOCIAL LOGIN
         */
        Laravel\Socialite\SocialiteServiceProvider::class,

        /*
         *   CURL
         */
        Ixudra\Curl\CurlServiceProvider::class,

        /*
         *   NOTIFICACIONES PUSH CON FLASHY
         */
        MercurySeries\Flashy\FlashyServiceProvider::class,

        
        /*
         *   GOOGLE MAPS
         */
        Cornford\Googlmapper\MapperServiceProvider::class,

        /*
         *   HERRAMIENTA PARA GENERAR SLUGS
         *
         *  use Cocur\Slugify\Slugify;
         *  $slugify = new Slugify();
         *  echo $slugify->slugify('Hello World!'); // hello-world
         */
        Cocur\Slugify\Bridge\Laravel\SlugifyServiceProvider::class,

        /*
         *   ELOQUENT SINTAX FOR AWS DINAMODB
         */
        BaoPham\DynamoDb\DynamoDbServiceProvider::class,

        /*
         *      Servicio para autenticacion con JWT
         *      Raul Chauvin   
         *      2018/05/02
         */
        SICE\Providers\JwtAuthServiceProvider::class,

        /*
         *      Servicio para manejo de formato de fechas
         *      Raul Chauvin   
         *      2018/05/02
         */
        SICE\Providers\TimeFormatServiceProvider::class,

        /*
         *      Servicio para validacion de identificaciones
         *      Raul Chauvin   
         *      2018/05/02
         */
        SICE\Providers\IdValidationServiceProvider::class,

        /*
         *   IDE HELPER
         */
        Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class,

    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,

        /*
         *   MONGODB
         */
        'Moloquent' => Jenssegers\Mongodb\Model::class,
        
        /*
         *   FORMS & HTML
         */
        'Form'      => Collective\Html\FormFacade::class,
        'Html'      => Collective\Html\HtmlFacade::class,

        /*
         *   MANEJO DE PDFs
         */
        'PDF'       => Barryvdh\DomPDF\Facade::class,


        /*
         *   MANEJO DE ARCHIVOS EXCEL
         */
        'Excel'     => Maatwebsite\Excel\Facades\Excel::class,

        /*
         *   MANEJO DE IMAGENES
         */
        'Imagine' => Orchestra\Imagine\Facade::class,
        'Image' => Intervention\Image\Facades\Image::class,


        /*
         *   MANEJO DE SOCIAL LOGIN
         */
        'Socialite' => Laravel\Socialite\Facades\Socialite::class,

        /*
         *   CURL
         */
        'Curl' => Ixudra\Curl\Facades\Curl::class,

        /*
         *   NOTIFICACIONES PUSH CON FLASHY
         */
        'Flashy' => MercurySeries\Flashy\Flashy::class,

        /*
         *   GOOGLE MAPS
         */
        'Mapper' => Cornford\Googlmapper\Facades\MapperFacade::class,

        /*
         *   HERRAMIENTA PARA GENERAR SLUGS
         *
         *  use Cocur\Slugify\Slugify;
         *  $slugify = new Slugify();
         *  echo $slugify->slugify('Hello World!'); // hello-world
         */
        "Slugify" => Cocur\Slugify\Bridge\Laravel\SlugifyFacade::class,


        /*
         *   ELOQUENT SINTAX FOR AWS DINAMODB
         */
        "DynamoDbModel" => BaoPham\DynamoDb\DynamoDbModel::class,

        /*
         *      Alias para autenticacion con JWT
         *      Raul Chauvin   
         *      2018/05/02
         */
        'JwtAuth' => SICE\Helpers\JwtAuth::class,

        /*
         *      Alias para manejo de formato de fechas
         *      Raul Chauvin   
         *      2018/05/02
         */
        'TimeFormat' => SICE\Helpers\TimeFormat::class,

        /*
         *      Alias para validacion de identificaciones
         *      Raul Chauvin   
         *      2018/05/02
         */
        'IdValidation' => SICE\Helpers\IdValidation::class,

    ],

];
