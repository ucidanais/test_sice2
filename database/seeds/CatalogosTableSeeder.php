<?php

use Illuminate\Database\Seeder;

use SICE\Catalogo;

class CatalogosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // CANALES

		$oCanal = new Catalogo;
        $oCanal->contexto = 'Canales';
        $oCanal->codigo1 = '1';
        $oCanal->descripcion = 'Canal Moderno';

        if($oCanal->save()){

        	$oSubcanal = new Catalogo;
        	$oSubcanal->catalogo_id = $oCanal->id;
	        $oSubcanal->contexto = 'Subcanales';
	        $oSubcanal->codigo1 = '1-01';
	        $oSubcanal->codigo2 = $oCanal->codigo2;
	        $oSubcanal->valor1 = $oCanal->codigo1;
	        $oSubcanal->valor2 = $oCanal->descripcion;
	        $oSubcanal->descripcion = 'Hyper & Super';
	        $oSubcanal->save();

	        $oSubcanal = new Catalogo;
        	$oSubcanal->catalogo_id = $oCanal->id;
	        $oSubcanal->contexto = 'Subcanales';
	        $oSubcanal->codigo1 = '1-02';
	        $oSubcanal->codigo2 = $oCanal->codigo2;
	        $oSubcanal->valor1 = $oCanal->codigo1;
	        $oSubcanal->valor2 = $oCanal->descripcion;
	        $oSubcanal->descripcion = 'Drug & Pharma';
	        $oSubcanal->save();
	        
        }

        $oCanal = new Catalogo;
        $oCanal->contexto = 'Canales';
        $oCanal->codigo1 = '2';
        $oCanal->descripcion = 'Canal Tradicional';
        if($oCanal->save()){
        	
        	$oSubcanal = new Catalogo;
        	$oSubcanal->catalogo_id = $oCanal->id;
	        $oSubcanal->contexto = 'Subcanales';
	        $oSubcanal->codigo1 = '2-01';
	        $oSubcanal->codigo2 = $oCanal->codigo2;
	        $oSubcanal->valor1 = $oCanal->codigo1;
	        $oSubcanal->valor2 = $oCanal->descripcion;
	        $oSubcanal->descripcion = 'Mayoristas';
	        $oSubcanal->save();

        }

        // CADENAS

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '001';
        $oCadena->descripcion = 'Supermaxi';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '002';
        $oCadena->descripcion = 'Megamaxi';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '003';
        $oCadena->descripcion = 'AKI';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '004';
        $oCadena->descripcion = 'Gran AKI';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '005';
        $oCadena->descripcion = 'Super AKI';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '006';
        $oCadena->descripcion = 'Santa María';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '007';
        $oCadena->descripcion = 'Mega Santa María';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '008';
        $oCadena->descripcion = 'TIA';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '009';
        $oCadena->descripcion = 'Super TIA';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '010';
        $oCadena->descripcion = 'Super Ahorro';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '011';
        $oCadena->descripcion = 'Magda';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '012';
        $oCadena->descripcion = 'Coral';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '013';
        $oCadena->descripcion = 'Mi Comisariato';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '014';
        $oCadena->descripcion = 'Mi Comisariato Junior';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '015';
        $oCadena->descripcion = 'Fybeca';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '016';
        $oCadena->descripcion = 'Sana Sana';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '017';
        $oCadena->descripcion = 'Cruz Azul';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '018';
        $oCadena->descripcion = 'Pharmacys';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '019';
        $oCadena->descripcion = 'Medicity';
        $oCadena->save();

        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = '020';
        $oCadena->descripcion = 'Farmacia Económica';
        $oCadena->save();

        // PROVINCIAS
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '1';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'Azuay';

        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Camilo Ponce';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chordeleg';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cuenca';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Pan';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Giron';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Guachapala';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Gualaceo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Nabon';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Oña';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Paute';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pucara';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Fernando';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-13';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Isabel';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-14';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sevilla de Oro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '01-15';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sigsig';
	        $oCiudad->save();
        }

        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '2';
        $oProvincia->codigo2 = '03';
        $oProvincia->descripcion = 'Bolivar';

        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Caluma';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chillanes';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chimbo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Echeandia';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Guaranda';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Las Naves';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '02-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Miguel';
	        $oCiudad->save();
        }

        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '3';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'Cañar';

        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Azogues';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Biblan';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cañar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Deleg';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'La Troncal';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Suscal';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '03-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Tambo';
	        $oCiudad->save();
        }

        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '4';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Carchi';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Bolivar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Espejo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Mira';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Montufar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Pedro de Huaca';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '04-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Tulcan';
	        $oCiudad->save();
        }

        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '6';
        $oProvincia->codigo2 = '03';
        $oProvincia->descripcion = 'Chimborazo';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Alausi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chambo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chunchi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Colta';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cumanda';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Guamote';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Guano';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Papallacta';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Penipe';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '06-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Riobamba';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '5';
        $oProvincia->codigo2 = '03';
        $oProvincia->descripcion = 'Cotopaxi';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'La Mana';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Latacunga';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pangua';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pujili';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Salcedo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Saquisili';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '05-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sigchos';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '7';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'El Oro';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Arenillas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Atahualpa';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Balsas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chilla';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Guabo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Huaquillas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Las Lajas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Machala';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Marcabeli';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pasaje';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Piñas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Portovelo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-13';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Rosa';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '07-14';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Zaruma';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '8';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Esmeraldas';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Atacames';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Eloy Alfaro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Esmeraldas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Muisne';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quininde';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Rio Verde';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '08-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Lorenzo';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '20';
        $oProvincia->codigo2 = '05';
        $oProvincia->descripcion = 'Galapagos';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '20-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Isabela';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '20-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Cristobal';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '20-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Cruz';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '9';
        $oProvincia->codigo2 = '04';
        $oProvincia->descripcion = 'Guayas';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Alfredo Baquerizo Moreno (Jujan)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Balao';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Balzar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Colimes';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Daule';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Duran';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Triunfo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Empalme';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'General Antonio Elizalde (Bucay)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'General Villamil (Playas)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Guayaquil';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Isidro Ayora';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-13';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Lomas de Sargentillo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-14';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Marcelino Maridueña';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-15';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Milagro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-16';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Naranjal';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-17';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Naranjito';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-18';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Nobol (Narcisa de Jesus)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-19';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Palestina';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-20';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pedro Carbo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-21';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Samborondon';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-22';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Lucia';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-23';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Simon Bolivar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-24';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Urbina Jado (Salitre)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '09-25';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Yaguachi';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '10';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Imbabura';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Atuntaqui (Antonio Ante)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cotacachi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Ibarra';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Otavalo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pimampiro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '10-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Urcuqui';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '11';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'Loja';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Calvas (Cariamanga)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Catamayo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Celica';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chaguarpamba';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Espindola';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Gonzanama';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Loja';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Macara';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Olmedo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Paltas (Catacocha)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pindal';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Puyango (Alamor)';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-13';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quilanga';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-14';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Saraguro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-15';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sozoranga';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '11-16';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Zapotillo';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '12';
        $oProvincia->codigo2 = '05';
        $oProvincia->descripcion = 'Los Rios';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Baba';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Babahoyo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Buena Fe';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Mocache';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Montalvo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Palenque';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pueblo Viejo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quevedo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Urdaneta';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Valencia';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Ventanas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '12-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Vinces';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '13';
        $oProvincia->codigo2 = '05';
        $oProvincia->descripcion = 'Manabi';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = '24 de Mayo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Bolivar';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chone';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Carmen';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Flavio Alfaro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Jama';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Jaramillo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Jipijapa';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Junin';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Manta';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Montecristi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Olmedo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-13';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pajan';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-14';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pichincha';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-15';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Puerto Lopez';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-16';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pedernales';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-17';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Portoviejo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-18';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Rocafuerte';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-19';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Vicente';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-20';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Ana';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-21';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sucre';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '13-22';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Tosagua';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '14';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'Morona Santiago';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Gualaquiza';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Huamboya';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Limon-Indanza';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Logroño';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Morona';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pablo VI';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Palora';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Juan Bosco';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santiago';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-10';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sucua';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-11';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Taisha';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '14-12';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Twintza';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '15';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Napo';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '15-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Archidona';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '15-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Carlos J. Arosemena Tola';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '15-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Chaco';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '15-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quijos';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '15-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Tena';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '22';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Orellana';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '22-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Aguarico';
	        $oCiudad->save();

	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '22-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'La Joya de los Sachas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '22-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Loreto';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '22-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Orellana';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '16';
        $oProvincia->codigo2 = '03';
        $oProvincia->descripcion = 'Pastaza';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '16-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Arajuno';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '16-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Mera';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '16-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pastaza';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '16-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Clara';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '17';
        $oProvincia->codigo2 = '02';
        $oProvincia->descripcion = 'Pichincha';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cayambe';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Mejia';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pedro Moncayo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Pedro Vicente Maldonado';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Puerto Quito';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quito';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Rumiñahui';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '17-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Miguel de los Bancos';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '24';
        $oProvincia->codigo2 = '04';
        $oProvincia->descripcion = 'Santa Elena';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '24-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'La Libertad';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '24-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Salinas';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '24-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santa Elena';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '23';
        $oProvincia->codigo2 = '02';
        $oProvincia->descripcion = 'Santo Domingo de los Tsáchilas';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '23-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santo Domingo de los Tsáchilas';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '21';
        $oProvincia->codigo2 = '06';
        $oProvincia->descripcion = 'Sucumbios';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cascales';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cuyabeno';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Gonzalo Pizarro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Lago Agrio';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Putumayo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Shushufindi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '21-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Sucumbios';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '18';
        $oProvincia->codigo2 = '03';
        $oProvincia->descripcion = 'Tungurahua';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Ambato';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Baños de Agua Santa';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Cevallos';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Mocha';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Patate';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Quero';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'San Pedro de Pelileo';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Santiago de Pillaro';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '18-09';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Tisaleo';
	        $oCiudad->save();
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = '19';
        $oProvincia->codigo2 = '07';
        $oProvincia->descripcion = 'Zamora Chinchipe';
        
        if($oProvincia->save()){
        	$oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-01';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Centinela del Condor';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-02';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Chinchipe';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-03';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'El Pangui';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-04';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Nangaritza';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-05';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Palanda';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-06';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Yacuambi';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-07';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Yantzaza';
	        $oCiudad->save();
	        
	        $oCiudad = new Catalogo;
	        $oCiudad->catalogo_id = $oProvincia->id;
	        $oCiudad->contexto = 'Ciudades';
	        $oCiudad->codigo1 = '19-08';
	        $oCiudad->codigo2 = $oProvincia->codigo2;
	        $oCiudad->valor1 = $oProvincia->codigo1;
	        $oCiudad->valor2 = $oProvincia->descripcion;
	        $oCiudad->descripcion = 'Zamora';
	        $oCiudad->save();
        }
    }
}
