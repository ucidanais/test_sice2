<?php

use Illuminate\Database\Seeder;

use SICE\Usuario;

/**
* Clase para cargar datos iniciales en la tabla de usuarios del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @AutenticacionAutorizacion
*/

class UsuariosTableSeeder extends Seeder
{
    /**
     * Metodo para ejecutar el llenado de la tabla de usuarios.
     *
     * @return void
     */
    public function run()
    {
        $objUsuario = new Usuario;
        $objUsuario->nombre = 'Administrador';
        $objUsuario->apellido = 'SICE';
        $objUsuario->email = 'admin@gosice.com';
        $objUsuario->password = '12345678';
        $objUsuario->tipo = 'S';
        $objUsuario->estado = 1;
        $objUsuario->save();
    }
}
