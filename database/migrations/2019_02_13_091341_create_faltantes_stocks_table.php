<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateFaltantesStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::create('faltantes_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('local_id');
            $table->string('cliente_proyecto_id');
            $table->string('familia_producto_id');
            $table->string('categoria_producto_id');
            $table->string('producto_id');
            $table->string('detalle');
            $table->string('ruta_id');

            $table->timestamps();
        });
        $bar->advance();

        $bar->finish();
        print("\n");
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::drop('faltantes_stocks');
        $bar->advance();

        $bar->finish();
        print("\n");
        */
    }
}

