<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreatePresentacionproductosFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('presentacion_productos', function ($table) {
            $table->boolean('es_competencia')->default(0)->after('contexto');
            $table->uuid('presentacion_producto_id')->nullable()->after('familia_producto_id');
            $table->foreign('presentacion_producto_id')->references('id')->on('presentacion_productos');

        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('presentacion_productos', function ($table) {
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropColumn(['es_competencia', 'presentacion_producto_id']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
