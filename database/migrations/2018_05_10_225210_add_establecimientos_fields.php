<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddEstablecimientosFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('establecimientos', function ($table) {
            $table->string('creado_por', 200)->nullable()->after('nombre_barrio');
            $table->string('ultima_actualizacion', 200)->nullable()->after('creado_por');
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('establecimientos', function ($table) {
            $table->dropColumn(['creado_por', 'ultima_actualizacion']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
