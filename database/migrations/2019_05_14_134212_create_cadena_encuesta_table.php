<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateCadenaEncuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        if( ! Schema::hasTable('cadena_encuesta')){
            Schema::create('cadena_encuesta', function(Blueprint $table){
                $table->uuid('cadena_id')->nullable();
                $table->uuid('encuesta_id');
                $table->timestamps();
                $table->primary(['cadena_id', 'encuesta_id']);
                $table->foreign('cadena_id')->references('id')->on('catalogos');
                $table->foreign('encuesta_id')->references('id')->on('encuestas');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::dropIfExists('cadena_encuesta');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
