<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
* Clase para construir las tablas del sistema en la base de datos
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/17
*/

class SiceDB extends Migration
{
    /**
     * Ejecuta las migraciones en la base de datos.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/17
     *
     * @return void
     */
    public function up()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 25);
        $bar->start();

        // TABLA catalogos
        if(!Schema::hasTable('catalogos')){
            Schema::create('catalogos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('contexto',200);
                $table->string('tipo_dato',128)->nullable();
                $table->string('codigo1',255)->nullable();
                $table->string('codigo2',255)->nullable();
                $table->text('valor1')->nullable();
                $table->text('valor2')->nullable();
                $table->text('descripcion')->nullable();
                $table->text('coordenadas_poligono')->nullable();
                $table->uuid('catalogo_id')->nullable();
                $table->timestamps();
                $table->primary('id');
                $table->foreign('catalogo_id')->references('id')->on('catalogos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA proyectos
        if(!Schema::hasTable('proyectos')){
            Schema::create('proyectos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo',16)->unique();
                $table->string('nombre',128)->unique();
                $table->boolean('estado');
                $table->timestamps();
                $table->primary('id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA clientes
        if(!Schema::hasTable('clientes')){
            Schema::create('clientes', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo',16)->unique();
                $table->string('nombre',128)->unique();
                $table->string('logotipo',128)->nullable();
                $table->boolean('estado');
                $table->timestamps();
                $table->primary('id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA parametro_clientes
        if(!Schema::hasTable('parametro_clientes')){
            Schema::create('parametro_clientes', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo',300)->nullable();
                $table->string('parametro',300);
                $table->string('valor_cadena',300)->nullable();
                $table->double('valor_numero',18,2)->nullable();
                $table->dateTime('valor_fecha')->nullable();
                $table->text('observaciones')->nullable();
                $table->date('vigencia_inicio')->nullable();
                $table->date('vigencia_fin')->nullable();
                $table->uuid('cliente_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA parametro_proyectos
        if(!Schema::hasTable('parametro_proyectos')){
            Schema::create('parametro_proyectos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo',300)->nullable();
                $table->string('parametro',300);
                $table->string('valor_cadena',300)->nullable();
                $table->double('valor_numero',18,2)->nullable();
                $table->dateTime('valor_fecha')->nullable();
                $table->text('observaciones')->nullable();
                $table->date('vigencia_inicio')->nullable();
                $table->date('vigencia_fin')->nullable();
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA cliente_proyectos
        if(!Schema::hasTable('cliente_proyectos')){
            Schema::create('cliente_proyectos', function(Blueprint $table){
                $table->uuid('id');
                $table->boolean('estado');
                $table->text('descripcion')->nullable();
                $table->boolean('tomar_stock');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA equipo_trabajo
        if(!Schema::hasTable('equipo_trabajo')){
            Schema::create('equipo_trabajo', function(Blueprint $table){
                $table->uuid('id');
                $table->string('cargo',128);
                $table->string('nombre',128);
                $table->string('apellido',128);
                $table->enum('tipo', array('E','C'));  // Empleado, Cliente
                $table->boolean('lider_proyecto')->nullable();
                $table->uuid('usuario_id');
                $table->uuid('usuario_reporta_id')->nullable();
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('usuario_id')->references('id')->on('usuarios');
                $table->foreign('usuario_reporta_id')->references('id')->on('usuarios');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA encuestas
        if(!Schema::hasTable('encuestas')){
            Schema::create('encuestas', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->enum('tipo', array('N','CE'));  // Normal, Clasificacion de Establecimiento
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA preguntas
        if(!Schema::hasTable('preguntas')){
            Schema::create('preguntas', function(Blueprint $table){
                $table->uuid('id');
                $table->text('pregunta');
                $table->string('tipo_dato', 128);
                $table->boolean('opcion_multiple');
                $table->text('opciones')->nullable();
                $table->integer('orden');
                $table->integer('ponderacion')->nullable();
                $table->uuid('encuesta_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('encuesta_id')->references('id')->on('encuestas');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA familia_productos
        if(!Schema::hasTable('familia_productos')){
            Schema::create('familia_productos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA categoria_productos
        if(!Schema::hasTable('categoria_productos')){
            Schema::create('categoria_productos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->uuid('familia_producto_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('familia_producto_id')->references('id')->on('familia_productos');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA presentacion_productos
        if(!Schema::hasTable('presentacion_productos')){
            Schema::create('presentacion_productos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->double('precio',18,2)->nullable();
                $table->string('contexto',128)->nullable();
                $table->uuid('categoria_producto_id');
                $table->uuid('familia_producto_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('categoria_producto_id')->references('id')->on('categoria_productos');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA producto_cadena
        if(!Schema::hasTable('producto_cadena')){
            Schema::create('producto_cadena', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo_cadena',255);
                $table->string('nombre_cadena',200);
                $table->uuid('presentacion_producto_id');
                $table->uuid('categoria_producto_id');
                $table->uuid('familia_producto_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('presentacion_producto_id')->references('id')->on('presentacion_productos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        // TABLA establecimientos
        if(!Schema::hasTable('establecimientos')){
            Schema::create('establecimientos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('codigo',200);
                $table->string('nombre',128);
                $table->string('direccion_manzana',128)->nullable();
                $table->string('direccion_calle_principal',128)->nullable();
                $table->string('direccion_numero',128)->nullable();
                $table->string('direccion_transversal',128)->nullable();
                $table->string('direccion_referencia',128)->nullable();
                $table->string('administrador',128)->nullable();
                $table->string('telefonos_contacto',128)->nullable();
                $table->string('email_contacto',128)->nullable();
                $table->string('foto',128)->nullable();
                $table->string('geolocalizacion',200)->nullable();
                $table->uuid('uuid_cadena')->nullable();
                $table->string('codigo_cadena',255)->nullable();
                $table->string('nombre_cadena',200)->nullable();
                $table->uuid('uuid_canal');
                $table->string('codigo_canal',255);
                $table->string('nombre_canal',200);
                $table->uuid('uuid_subcanal');
                $table->string('codigo_subcanal',255);
                $table->string('nombre_subcanal',200);
                $table->uuid('uuid_tipo_negocio')->nullable();
                $table->string('codigo_tipo_negocio',255)->nullable();
                $table->string('nombre_tipo_negocio',200)->nullable();
                $table->uuid('uuid_provincia');
                $table->string('codigo_provincia',255);
                $table->string('nombre_provincia',200);
                $table->uuid('uuid_ciudad');
                $table->string('codigo_ciudad',255);
                $table->string('nombre_ciudad',200);
                $table->uuid('uuid_zona')->nullable();
                $table->string('codigo_zona',255)->nullable();
                $table->string('nombre_zona',200)->nullable();
                $table->uuid('uuid_parroquia')->nullable();
                $table->string('codigo_parroquia',255)->nullable();
                $table->string('nombre_parroquia',200)->nullable();
                $table->uuid('uuid_barrio')->nullable();
                $table->string('codigo_barrio',255)->nullable();
                $table->string('nombre_barrio',200)->nullable();
                $table->timestamps();
                $table->primary('id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        // TABLA cliente_establecimientos
        if(!Schema::hasTable('cliente_establecimientos')){
            Schema::create('cliente_establecimientos', function(Blueprint $table){
                $table->uuid('id');
                $table->boolean('en_ruta')->default(0);
                $table->boolean('estado')->default(1);
                $table->uuid('establecimiento_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        
        // TABLA clasificacion_establecimientos
        if(!Schema::hasTable('clasificacion_establecimientos')){
            Schema::create('clasificacion_establecimientos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('clasificacion',128);
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->uuid('establecimiento_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade')->onUpdate('cascade');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();
        


        // TABLA rutas
        if(!Schema::hasTable('rutas')){
            Schema::create('rutas', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->boolean('lunes')->default(0);
                $table->boolean('martes')->default(0);
                $table->boolean('miercoles')->default(0);
                $table->boolean('jueves')->default(0);
                $table->boolean('viernes')->default(0);
                $table->boolean('sabado')->default(0);
                $table->boolean('domingo')->default(0);
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->uuid('equipo_trabajo_id');
                $table->uuid('usuario_id');
                $table->timestamps();
                $table->softDeletes();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->foreign('equipo_trabajo_id')->references('id')->on('equipo_trabajo');
                $table->foreign('usuario_id')->references('id')->on('usuarios');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla establecimiento_ruta
        if(!Schema::hasTable('establecimiento_ruta')){
            Schema::create('establecimiento_ruta', function(Blueprint $table){
                $table->uuid('establecimiento_id');
                $table->uuid('ruta_id');
                $table->integer('orden_visita')->default(0);
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('ruta_id')->references('id')->on('rutas')->onDelete('cascade')->onUpdate('cascade');
                $table->primary('establecimiento_id', 'ruta_id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA grupo_productos
        if(!Schema::hasTable('grupo_productos')){
            Schema::create('grupo_productos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128);
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla grupo_presentacion
        if(!Schema::hasTable('grupo_presentacion')){
            Schema::create('grupo_presentacion', function(Blueprint $table){
                $table->uuid('grupo_id');
                $table->uuid('presentacion_id');
                $table->foreign('grupo_id')->references('id')->on('grupo_productos')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('presentacion_id')->references('id')->on('presentacion_productos')->onDelete('cascade')->onUpdate('cascade');
                $table->primary('grupo_id', 'presentacion_id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        // Tabla hojas_ruta
        if(!Schema::hasTable('hojas_ruta')){
            Schema::create('hojas_ruta', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre');
                $table->date('fecha');
                $table->integer('visitas_por_hacer')->default(0);
                $table->integer('visitas_realizadas')->default(0);
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->uuid('equipo_trabajo_id');
                $table->uuid('usuario_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->foreign('equipo_trabajo_id')->references('id')->on('equipo_trabajo');
                $table->foreign('usuario_id')->references('id')->on('usuarios');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla detalle_rutas
        if(!Schema::hasTable('detalle_rutas')){
            Schema::create('detalle_rutas', function(Blueprint $table){
                $table->uuid('id');
                $table->boolean('estado')->default(0);
                $table->text('observaciones')->nullable();
                $table->date('fecha_visita')->nullable();
                $table->integer('orden_visita')->default(0);
                $table->time('hora_entrada')->nullable();
                $table->time('hora_salida')->nullable();
                $table->string('ubicacion', 200)->nullable();
                $table->uuid('hoja_ruta_id');
                $table->uuid('establecimiento_id');
                $table->uuid('grupo_producto_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('hoja_ruta_id')->references('id')->on('hojas_ruta');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
                $table->foreign('grupo_producto_id')->references('id')->on('grupo_productos');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        // Tabla novedades
        if(!Schema::hasTable('novedades')){
            Schema::create('novedades', function(Blueprint $table){
                $table->uuid('id');
                $table->string('tipo_novedad', 200)->nullable();
                $table->text('detalle')->nullable();
                $table->string('producto', 200)->nullable();
                $table->string('competencia')->nullable();
                $table->string('cliente')->nullable();
                $table->string('cadena')->nullable();
                $table->string('referente_a')->nullable();
                $table->string('rotacion')->nullable();
                $table->decimal('pvp', 18,2)->default(0.00);
                $table->decimal('precio_afiliado', 18,2)->default(0.00);
                $table->decimal('precio_no_afiliado',18,2)->default(0.00);
                $table->string('fabricante')->nullable();
                $table->string('distribuidor')->nullable();
                $table->text('gestion')->nullable();
                $table->uuid('establecimiento_id');
                $table->uuid('detalle_ruta_id');
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
                $table->foreign('detalle_ruta_id')->references('id')->on('detalle_rutas');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla fotos
        if(!Schema::hasTable('fotos')){
            Schema::create('fotos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('imagen', 200)->nullable();
                $table->string('momento_foto', 200)->nullable();
                $table->uuid('novedad_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('novedad_id')->references('id')->on('novedades');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla informacion_detalles
        if(!Schema::hasTable('informacion_detalles')){
            Schema::create('informacion_detalles', function(Blueprint $table){
                $table->uuid('id');
                $table->text('valor')->nullable();
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->uuid('establecimiento_id');
                $table->uuid('familia_producto_id');
                $table->uuid('categoria_producto_id');
                $table->uuid('presentacion_producto_id');
                $table->uuid('detalle_ruta_id');

                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
                $table->foreign('familia_producto_id')->references('id')->on('familia_productos');
                $table->foreign('categoria_producto_id')->references('id')->on('categoria_productos');
                $table->foreign('presentacion_producto_id')->references('id')->on('presentacion_productos');
                $table->foreign('detalle_ruta_id')->references('id')->on('detalle_rutas');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();


        $bar->finish();
        print("\n");
    }

    /**
     * Reversa las migraciones de la base de datos.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/17
     *
     * @return void
     */
    public function down()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 25);
        $bar->start();

        Schema::dropIfExists('informacion_detalles');
        $bar->advance();
        Schema::dropIfExists('fotos');
        $bar->advance();
        Schema::dropIfExists('novedades');
        $bar->advance();
        Schema::dropIfExists('detalle_rutas');
        $bar->advance();
        Schema::dropIfExists('hojas_ruta');
        $bar->advance();
        Schema::dropIfExists('grupo_presentacion');
        $bar->advance();
        Schema::dropIfExists('grupo_productos');
        $bar->advance();
        Schema::dropIfExists('establecimiento_ruta');
        $bar->advance();
        Schema::dropIfExists('rutas');
        $bar->advance();
        Schema::dropIfExists('clasificacion_establecimientos');
        $bar->advance();
        Schema::dropIfExists('cliente_establecimientos');
        $bar->advance();
        Schema::dropIfExists('establecimientos');
        $bar->advance();
        Schema::dropIfExists('producto_cadena');
        $bar->advance();
        Schema::dropIfExists('presentacion_productos');
        $bar->advance();
        Schema::dropIfExists('categoria_productos');
        $bar->advance();
        Schema::dropIfExists('familia_productos');
        $bar->advance();
        Schema::dropIfExists('preguntas');
        $bar->advance();
        Schema::dropIfExists('encuestas');
        $bar->advance();
        Schema::dropIfExists('equipo_trabajo');
        $bar->advance();
        Schema::dropIfExists('cliente_proyectos');
        $bar->advance();
        Schema::dropIfExists('parametro_proyectos');
        $bar->advance();
        Schema::dropIfExists('parametro_clientes');
        $bar->advance();
        Schema::dropIfExists('clientes');
        $bar->advance();
        Schema::dropIfExists('proyectos');
        $bar->advance();
        Schema::dropIfExists('catalogos');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
