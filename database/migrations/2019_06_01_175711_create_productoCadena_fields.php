<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateProductoCadenaFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 2);
        $bar->start();

        if(Schema::hasTable('producto_cadena')){
            Schema::table('producto_cadena', function ($table) {
                $table->string('clasificacion_establecimiento', 128)->nullable()->after('nombre_cadena');

            });
        }
        $bar->advance();

        if(Schema::hasTable('cliente_proyectos')){
            Schema::table('cliente_proyectos', function ($table) {
                $table->text('clasificaciones')->nullable()->after('tomar_stock');

            });
        }
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 2);
        $bar->start();

        if(Schema::hasTable('producto_cadena')){
            Schema::table('producto_cadena', function ($table) {
                $table->dropColumn(['clasificacion_establecimiento']);
            });
        }
        $bar->advance();

        if(Schema::hasTable('cliente_proyectos')){
            Schema::table('cliente_proyectos', function ($table) {
                $table->dropColumn(['clasificaciones']);
            });
        }
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
