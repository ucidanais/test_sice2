<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateDetalleRutaView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        DB::unprepared('
            CREATE VIEW `vw_detalle_rutas` AS
                SELECT 
                    detalle_rutas.*, 
                    establecimientos.codigo, 
                    establecimientos.nombre, 
                    establecimientos.direccion_manzana,
                    establecimientos.direccion_calle_principal,
                    establecimientos.direccion_numero,
                    establecimientos.direccion_transversal,
                    establecimientos.direccion_referencia,
                    establecimientos.geolocalizacion,
                    establecimientos.uuid_cadena,
                    establecimientos.nombre_cadena,
                    establecimientos.uuid_canal,
                    establecimientos.nombre_canal,
                    establecimientos.uuid_subcanal,
                    establecimientos.nombre_subcanal,
                    establecimientos.uuid_provincia,
                    establecimientos.nombre_provincia,
                    establecimientos.uuid_ciudad,
                    establecimientos.nombre_ciudad
                FROM detalle_rutas 
                JOIN establecimientos
                ON detalle_rutas.establecimiento_id = establecimientos.id;
        ');
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        DB::unprepared('DROP VIEW IF EXISTS `vw_detalle_rutas`');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
