<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreatePollPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        if( ! Schema::hasTable('encuesta_fotos')){
            Schema::create('encuesta_fotos', function(Blueprint $table){
                $table->uuid('id');
                $table->text('foto');
                $table->text('descripcion')->nullable();
                $table->uuid('cliente_proyecto_id');
                $table->uuid('cliente_id');
                $table->uuid('proyecto_id');
                $table->uuid('establecimiento_id')->nullable();
                $table->uuid('canal_id')->nullable();
                $table->uuid('subcanal_id')->nullable();
                $table->uuid('cadena_id')->nullable();
                $table->uuid('provincia_id')->nullable();
                $table->uuid('ciudad_id')->nullable();
                $table->uuid('encuesta_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('cliente_proyecto_id')->references('id')->on('cliente_proyectos');
                $table->foreign('cliente_id')->references('id')->on('clientes');
                $table->foreign('proyecto_id')->references('id')->on('proyectos');
                $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
                $table->foreign('canal_id')->references('id')->on('catalogos');
                $table->foreign('subcanal_id')->references('id')->on('catalogos');
                $table->foreign('cadena_id')->references('id')->on('catalogos');
                $table->foreign('provincia_id')->references('id')->on('catalogos');
                $table->foreign('ciudad_id')->references('id')->on('catalogos');
                $table->foreign('encuesta_id')->references('id')->on('encuestas');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::dropIfExists('encuesta_fotos');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
