<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateNovedadesFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('novedades', function ($table) {
            $table->uuid('canal_id')->nullable()->after('establecimiento_id');
            $table->uuid('subcanal_id')->nullable()->after('canal_id');
            $table->uuid('cadena_id')->nullable()->after('subcanal_id');
            $table->uuid('provincia_id')->nullable()->after('cadena_id');
            $table->uuid('ciudad_id')->nullable()->after('provincia_id');
            $table->foreign('canal_id')->references('id')->on('catalogos');
            $table->foreign('subcanal_id')->references('id')->on('catalogos');
            $table->foreign('cadena_id')->references('id')->on('catalogos');
            $table->foreign('provincia_id')->references('id')->on('catalogos');
            $table->foreign('ciudad_id')->references('id')->on('catalogos');
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('novedades', function ($table) {
            $table->dropForeign(['canal_id']);
            $table->dropForeign(['subcanal_id']);
            $table->dropForeign(['cadena_id']);
            $table->dropForeign(['provincia_id']);
            $table->dropForeign(['ciudad_id']);
            $table->dropColumn(['canal_id', 'subcanal_id', 'cadena_id', 'provincia_id', 'ciudad_id']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
