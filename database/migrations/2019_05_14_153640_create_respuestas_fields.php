<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateRespuestasFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('respuesta_encuestas', function ($table) {
            $table->uuid('usuario_id')->nullable()->after('pregunta_id');
            $table->foreign('usuario_id')->references('id')->on('usuarios');

        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('respuesta_encuestas', function ($table) {
            $table->dropForeign(['usuario_id']);
            $table->dropColumn(['usuario_id']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
