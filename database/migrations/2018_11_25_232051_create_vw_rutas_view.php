<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateVwRutasView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        DB::unprepared('
            CREATE VIEW `vw_hojas_ruta` AS
                SELECT 
                detalle_rutas.hoja_ruta_id,
                hojas_ruta.nombre as nombre_hoja_ruta,
                hojas_ruta.fecha,
                hojas_ruta.visitas_por_hacer,
                hojas_ruta.visitas_realizadas,
                hojas_ruta.cliente_proyecto_id,
                hojas_ruta.cliente_id,
                hojas_ruta.proyecto_id,
                hojas_ruta.equipo_trabajo_id,
                hojas_ruta.usuario_id,
                hojas_ruta.ruta_id,
                detalle_rutas.estado,
                detalle_rutas.observaciones,
                detalle_rutas.orden_visita,
                detalle_rutas.hora_entrada,
                detalle_rutas.hora_salida,
                detalle_rutas.ubicacion,
                detalle_rutas.grupo_producto_id, 
                detalle_rutas.created_at,
                detalle_rutas.establecimiento_id,
                establecimientos.codigo, 
                establecimientos.nombre as nombre_establecimiento, 
                establecimientos.direccion_manzana,
                establecimientos.direccion_calle_principal,
                establecimientos.direccion_numero,
                establecimientos.direccion_transversal,
                establecimientos.direccion_referencia,
                establecimientos.geolocalizacion,
                establecimientos.uuid_cadena,
                establecimientos.nombre_cadena,
                establecimientos.uuid_canal,
                establecimientos.nombre_canal,
                establecimientos.uuid_subcanal,
                establecimientos.nombre_subcanal,
                establecimientos.uuid_provincia,
                establecimientos.nombre_provincia,
                establecimientos.uuid_ciudad,
                establecimientos.nombre_ciudad
            FROM hojas_ruta 
            JOIN detalle_rutas
            ON hojas_ruta.id = detalle_rutas.hoja_ruta_id
            JOIN establecimientos
            ON detalle_rutas.establecimiento_id = establecimientos.id;
        ');
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        DB::unprepared('DROP VIEW IF EXISTS `vw_hojas_ruta`');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
