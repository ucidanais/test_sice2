<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
* Clase para construir las tablas del modulo de Autenticacion/Autorizacion en la base de datos
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/15
* @AutenticacionAutorizacion
*/

class AutenticacionAutorizacion extends Migration
{
    /**
     * Ejecuta las migraciones en la base de datos.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/17
     *
     * @return void
     */
    public function up()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 5);
        $bar->start();

        // TABLA password_resets
        if(!Schema::hasTable('password_resets')){
            Schema::create('password_resets', function (Blueprint $table) {
                $table->string('email')->index();
                $table->string('token')->index();
                $table->timestamp('created_at');
            });
        }
        $bar->advance();

        // TABLA usuarios
        if(!Schema::hasTable('usuarios')){
            Schema::create('usuarios', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128)->nullable();
                $table->string('apellido', 128)->nullable();
                $table->string('email', 128)->unique();
                $table->string('password', 255);
                $table->enum('tipo', array('S','A','B','E'));  // Superadministrador, Administrador, Back Office, Estandar
                $table->boolean('estado')->default(0);
                $table->string('avatar',128)->nullable();
                $table->uuid('cliente_id')->nullable(); // id de Cliente
                $table->timestamps();
                $table->rememberToken()->nullable();
                $table->index('cliente_id');
                $table->primary('id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA modulos
        if(!Schema::hasTable('modulos')){
            Schema::create('modulos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128)->unique();
                $table->timestamps();
                $table->primary('id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // TABLA permisos
        if(!Schema::hasTable('permisos')){
            Schema::create('permisos', function(Blueprint $table){
                $table->uuid('id');
                $table->string('nombre',128)->unique();
                $table->string('path',255)->unique();
                $table->uuid('modulo_id');
                $table->timestamps();
                $table->primary('id');
                $table->foreign('modulo_id')->references('id')->on('modulos');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        // Tabla permiso_usuario
        if(!Schema::hasTable('permiso_usuario')){
            Schema::create('permiso_usuario', function(Blueprint $table){
                $table->uuid('permiso_id');
                $table->uuid('usuario_id');
                $table->foreign('permiso_id')->references('id')->on('permisos');
                $table->foreign('usuario_id')->references('id')->on('usuarios');
                $table->primary('permiso_id', 'usuario_id');
                $table->engine = 'InnoDB';
            });
        }
        $bar->advance();

        $bar->finish();
        print("\n");

    }

    /**
     * Reversa las migraciones de la base de datos.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/17
     *
     * @return void
     */
    public function down()
    {

        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 5);
        $bar->start();

        Schema::dropIfExists('permiso_usuario');
        $bar->advance();
        Schema::dropIfExists('permisos');
        $bar->advance();
        Schema::dropIfExists('modulos');
        $bar->advance();
        Schema::dropIfExists('usuarios');
        $bar->advance();
        Schema::dropIfExists('password_resets');
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
