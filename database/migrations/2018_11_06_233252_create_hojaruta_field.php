<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateHojarutaField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('hojas_ruta', function ($table) {
            $table->uuid('ruta_id')->nullable()->after('usuario_id');
            $table->foreign('ruta_id')->references('id')->on('rutas');
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('hojas_ruta', function ($table) {
            $table->dropForeign(['ruta_id']);
            $table->dropColumn(['ruta_id']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
