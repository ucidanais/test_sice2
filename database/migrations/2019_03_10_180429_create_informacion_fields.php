<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreateInformacionFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 2);
        $bar->start();

        Schema::dropIfExists('faltantes_stocks');
        $bar->advance();
        
        Schema::table('informacion_detalles', function ($table) {
            $table->string('tipo')->nullable()->after('id');
            $table->boolean('es_competencia')->default(0)->after('tipo');
            $table->string('producto_competencia')->nullable()->after('es_competencia'); // Producto de la competencia
            $table->integer('stock')->default(0)->after('valor');
            $table->decimal('pvp', 18,2)->default(0.00)->after('stock');
            $table->decimal('precio_afiliado', 18,2)->default(0.00)->after('pvp');
            $table->decimal('precio_no_afiliado',18,2)->default(0.00)->after('precio_afiliado');
            $table->uuid('canal_id')->nullable()->after('establecimiento_id');
            $table->uuid('subcanal_id')->nullable()->after('canal_id');
            $table->uuid('cadena_id')->nullable()->after('subcanal_id');
            $table->uuid('provincia_id')->nullable()->after('cadena_id');
            $table->uuid('ciudad_id')->nullable()->after('provincia_id');
            $table->foreign('canal_id')->references('id')->on('catalogos');
            $table->foreign('subcanal_id')->references('id')->on('catalogos');
            $table->foreign('cadena_id')->references('id')->on('catalogos');
            $table->foreign('provincia_id')->references('id')->on('catalogos');
            $table->foreign('ciudad_id')->references('id')->on('catalogos');

        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $output = new ConsoleOutput();
        $bar = new ProgressBar($output, 1);
        $bar->start();

        Schema::table('informacion_detalles', function ($table) {
            $table->dropForeign(['canal_id']);
            $table->dropForeign(['subcanal_id']);
            $table->dropForeign(['cadena_id']);
            $table->dropForeign(['provincia_id']);
            $table->dropForeign(['ciudad_id']);
            $table->dropColumn(['tipo', 'es_competencia', 'producto_competencia', 'stock', 'pvp', 'precio_afiliado', 'precio_no_afiliado', 'canal_id', 'subcanal_id', 'cadena_id', 'provincia_id', 'ciudad_id']);
        });
        $bar->advance();

        $bar->finish();
        print("\n");
    }
}
