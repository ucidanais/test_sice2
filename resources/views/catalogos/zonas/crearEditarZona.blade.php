@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Zonas</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/zonas/lista-de-zonas'))
                    <a href="{!! URL::to('catalogos/zonas/lista-de-zonas/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Zonas
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($oZona))
                      Editar Zona
                    @else
                      Nueva Zona
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oZona))
                    {!! Form::model($oZona, ['action' => ['ZonasController@update', $oZona->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'catalogos/zonas/agregar-zona/'.$ciudadId, 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif

                    <p>Formulario de creacion de una nueva zona en el sistema</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Provincia <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('provincia_id', ['' => 'Seleccione una provincia'] + $listaProvincias->toArray(), $provinciaId, array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required', 'onchange' => 'javascript: cargarCiudades();', 'id' => 'provincia_id')) !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ciudad <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="catalogo_id" id="catalogo_id">
                          <option value=''>Seleccione una ciudad</option>
                        </select>
                      </div>
                    </div>

                    

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('descripcion', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre de la zona', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="coordenadas_poligono">Coordenadas del Pológono <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::textarea('coordenadas_poligono', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el las coordenadas']) 
                        !!}
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Zona', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')
  <script type="text/javascript">

    @if(isset($oZona))
      cargarCiudades();
    @endif
    function cargarCiudades(){
      var idProvincia = $("#provincia_id").val();
      if(idProvincia != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-ciudades') !!}/"+idProvincia,function(ciudades){
          var ciudadCmb = $("#catalogo_id");
          ciudadCmb.empty();
          ciudadCmb.append("<option value=''>Seleccione una ciudad</option>");
          $.each(ciudades,function(index,ciudad){
            if(ciudad.id == {!! $ciudadId ? $ciudadId : 0 !!}){
              ciudadCmb.append("<option value='" + ciudad.id + "' selected='selected'>" + ciudad.descripcion + "</option>");
            }else{
              ciudadCmb.append("<option value='" + ciudad.id + "'>" + ciudad.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ningun establecimiento");
        var ciudadCmb = $("#catalogo_id");
        ciudadCmb.empty();
        ciudadCmb.append("<option value=''>Seleccione una ciudad</option>");
      }
    }

    // initialize the validator function
    //validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
@endsection