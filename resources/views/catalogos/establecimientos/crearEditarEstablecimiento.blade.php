@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Establecimientos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/lista-de-establecimientos'))
                    <a href="{!! URL::to('catalogos/establecimientos/lista-de-establecimientos/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Establecimientos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/importar-establecimiento'))
                    <a href="{!! URL::to('catalogos/establecimientos/importar-establecimientos/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-import"></i> Importar Establecimientos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($oEstablecimiento))
                      Editar Establecimiento
                    @else
                      Nuevo Establecimiento
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oEstablecimiento))
                    {!! Form::model($oEstablecimiento, ['action' => ['EstablecimientosController@update', $oEstablecimiento->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left', 'files' => true]) !!}
                  @else
                    {!! Form::open(['url' => 'catalogos/establecimientos/agregar-establecimiento', 'class' => 'form-horizontal form-label-left', 'method' => 'post', 'files' => true]) !!}
                  @endif

                    <p>Formulario de creacion de un nuevo establecimiento en el sistema</p>

                    <div class="">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active">
                            <a href="#tabDatosEstablecimiento" id="datosEstablecimientoTab" role="tab" data-toggle="tab" aria-expanded="true">
                              Datos del Establecimiento
                            </a>
                          </li>
                          <li role="presentation" class="">
                            <a href="#tabDatosClasificacion" role="tab" id="datosClasificacionTab" data-toggle="tab" aria-expanded="false">
                              Datos de Clasificacion
                            </a>
                          </li>
                          <li role="presentation" class="">
                            <a href="#tabDatosUbicacion" role="tab" id="datosUbicacionTab" data-toggle="tab" aria-expanded="false">
                              Datos de Ubicacion
                            </a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tabDatosEstablecimiento" aria-labelledby="datosEstablecimientoTab">

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('nombre', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del establecimiento', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Propietario / Administrador
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('administrador', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del propietario/administrador del establecimiento']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Telefono(s) de Contacto
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('telefonos_contacto', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el o los telefonos de contacto del establecimiento']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">E-mail de Contacto
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::email('email_contacto', null, 
                                  ["class" => "email form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el e-mail de contacto del establecimiento','id'=>'email_contacto']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Fotografia</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::file('foto', ['class' => 'form-control col-md-7 col-xs-12']) !!}
                              </div>
                            </div>

                            @if(isset($oEstablecimiento))
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  @if($oEstablecimiento->foto != '')
                                    <img src="{!! asset('resources/fotosEstablecimientos/'.$oEstablecimiento->foto) !!}" width="200" />
                                  @endif
                                </div>
                              </div>
                            @endif

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::select('estado', ['1' => 'Activo', '0' => 'Inactivo'], old('estado'), array('class' => 'required form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                              </div>
                            </div>

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tabDatosClasificacion" aria-labelledby="datosClasificacionTab">
                            
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cadena <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="required form-control col-md-7 col-xs-12" name="id_cadena" id="id_cadena">
                                  <option value=''>Seleccione una cadena</option>
                                  @if( ! $listaCadenas->isEmpty() )
                                    @foreach($listaCadenas as $cadena)
                                      @if($idCadena == $cadena->id)
                                        <option selected="selected" value='{!! $cadena->id !!}'>{!! $cadena->descripcion !!}</option>
                                      @else
                                        <option value='{!! $cadena->id !!}'>{!! $cadena->descripcion !!}</option>
                                      @endif
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Canal <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="required form-control col-md-7 col-xs-12" name="id_canal" id="id_canal" onchange="javascript: cargarSubcanales();">
                                  <option value=''>Seleccione un canal</option>
                                  @if(!$listaCanales->isEmpty())
                                    @foreach($listaCanales as $canal)
                                      @if($idCanal == $canal->id)
                                        <option selected="selected" value='{!! $canal->id !!}'>{!! $canal->descripcion !!}</option>
                                      @else
                                        <option value='{!! $canal->id !!}'>{!! $canal->descripcion !!}</option>
                                      @endif
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subcanal <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="required form-control" name="id_subcanal" id="id_subcanal" onchange="javascript: cargarTiposNegocio();">
                                  <option value=''>Seleccione un subcanal</option>
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tipo de Negocio
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="id_tipo_negocio" id="id_tipo_negocio">
                                  <option value=''>Seleccione un tipo de negocio</option>
                                </select>
                              </div>
                            </div>

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tabDatosUbicacion" aria-labelledby="datosUbicacionTab">
                            
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Provincia <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="required form-control col-md-7 col-xs-12" name="id_provincia" id="id_provincia" onchange="javascript: cargarCiudades();">
                                  <option value=''>Seleccione una provincia</option>
                                  @if(!$listaProvincias->isEmpty())
                                    @foreach($listaProvincias as $provincia)
                                      @if($idProvincia == $provincia->id)
                                        <option selected="selected" value='{!! $provincia->id !!}'>{!! $provincia->descripcion !!}</option>
                                      @else
                                        <option value='{!! $provincia->id !!}'>{!! $provincia->descripcion !!}</option>
                                      @endif
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Ciudad <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="required form-control" name="id_ciudad" id="id_ciudad" onchange="javascript: cargarZonas();">
                                  <option value=''>Seleccione una ciudad</option>
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Zona
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="id_zona" id="id_zona" onchange="javascript: cargarParroquias();">
                                  <option value=''>Seleccione una zona</option>
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Parroquia
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="id_parroquia" id="id_parroquia" onchange="javascript: cargarBarrios();">
                                  <option value=''>Seleccione una parroquia</option>
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Barrio
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="id_barrio" id="id_barrio">
                                  <option value=''>Seleccione un barrio</option>
                                </select>
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-12 col-sm-12 col-xs-12" for="direccion" style="text-align: left;">Dirección</label>
                              
                              <div class="col-md-12 col-sm-12 col-xs-12">&nbsp;</div>

                              <div class="col-md-4 col-sm-4 col-xs-12">
                                {!! Form::text('direccion_calle_principal', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Calle principal']) 
                                !!}
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12">
                                {!! Form::text('direccion_numero', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'No.']) 
                                !!}
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-12">
                                {!! Form::text('direccion_transversal', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Transversal']) 
                                !!}
                              </div>
                              <div class="col-md-3 col-sm-3 col-xs-12">
                                {!! Form::text('direccion_manzana', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Manzana']) 
                                !!}
                              </div>

                              <div class="col-md-12 col-sm-12 col-xs-12">&nbsp;</div>

                              <div class="col-md-12 col-sm-12 col-xs-12">
                                {!! Form::text('direccion_referencia', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese una referencia a la dirección del establecimiento']) 
                                !!}
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Establecimiento', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')

  <script type="text/javascript">
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });


    @if(isset($oEstablecimiento))
      cargarSubcanales();
      cargarCiudades();
    @endif
    
    function cargarSubcanales(){
      var idCanal = $("#id_canal").val();
      if(idCanal != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-subcanales') !!}/"+idCanal,function(subcanales){
          var subcanalCmb = $("#id_subcanal");
          subcanalCmb.empty();
          subcanalCmb.append("<option value=''>Seleccione un subcanal</option>");
          $.each(subcanales,function(index,subcanal){
            if(subcanal.id == "{!! $idSubcanal ? $idSubcanal : 0 !!}"){
              subcanalCmb.append("<option value='" + subcanal.id + "' selected='selected'>" + subcanal.descripcion + "</option>");
              cargarTiposNegocio();
            }else{
              subcanalCmb.append("<option value='" + subcanal.id + "'>" + subcanal.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ningun canal");
        var subcanalCmb = $("#id_subcanal");
        subcanalCmb.empty();
        subcanalCmb.append("<option value=''>Seleccione un subcanal</option>");
      }
    }

    function cargarTiposNegocio(){
      var idSubcanal = $("#id_subcanal").val();
      if(idSubcanal != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-tipos-de-negocio') !!}/"+idSubcanal,function(tiposNegocio){
          var tipoNegocioCmb = $("#id_tipo_negocio");
          tipoNegocioCmb.empty();
          tipoNegocioCmb.append("<option value=''>Seleccione un tipo de negocio</option>");
          $.each(tiposNegocio,function(index,tipoNegocio){
            if(tipoNegocio.id == "{!! $idTipoNegocio ? $idTipoNegocio : 0 !!}"){
              tipoNegocioCmb.append("<option value='" + tipoNegocio.id + "' selected='selected'>" + tipoNegocio.descripcion + "</option>");
            }else{
              tipoNegocioCmb.append("<option value='" + tipoNegocio.id + "'>" + tipoNegocio.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ningun subcanal");
        var tipoNegocioCmb = $("#id_tipo_negocio");
        tipoNegocioCmb.empty();
        tipoNegocioCmb.append("<option value=''>Seleccione un tipo de negocio</option>");
      }
    }

    function cargarCiudades(){
      var idProvincia = $("#id_provincia").val();
      if(idProvincia != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-ciudades') !!}/"+idProvincia,function(ciudades){
          var ciudadCmb = $("#id_ciudad");
          ciudadCmb.empty();
          ciudadCmb.append("<option value=''>Seleccione una ciudad</option>");
          $.each(ciudades,function(index,ciudad){
            if(ciudad.id == "{!! $idCiudad ? $idCiudad : 0 !!}"){
              ciudadCmb.append("<option value='" + ciudad.id + "' selected='selected'>" + ciudad.descripcion + "</option>");
              cargarZonas();
            }else{
              ciudadCmb.append("<option value='" + ciudad.id + "'>" + ciudad.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ninguna provincia");
        var ciudadCmb = $("#id_ciudad");
        ciudadCmb.empty();
        ciudadCmb.append("<option value=''>Seleccione una ciudad</option>");
      }
    }

    function cargarZonas(){
      var idCiudad = $("#id_ciudad").val();
      if(idCiudad != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-zonas') !!}/"+idCiudad,function(zonas){
          var zonaCmb = $("#id_zona");
          zonaCmb.empty();
          zonaCmb.append("<option value=''>Seleccione una zona</option>");
          $.each(zonas,function(index,zona){
            if(zona.id == "{!! $idZona ? $idZona : 0 !!}"){
              zonaCmb.append("<option value='" + zona.id + "' selected='selected'>" + zona.descripcion + "</option>");
              cargarParroquias();
            }else{
              zonaCmb.append("<option value='" + zona.id + "'>" + zona.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ninguna ciudad");
        var zonaCmb = $("#id_zona");
        zonaCmb.empty();
        zonaCmb.append("<option value=''>Seleccione una zona</option>");
      }
    }

    function cargarParroquias(){
      var idZona = $("#id_zona").val();
      if(idZona != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-parroquias') !!}/"+idZona,function(parroquias){
          var parroquiaCmb = $("#id_parroquia");
          parroquiaCmb.empty();
          parroquiaCmb.append("<option value=''>Seleccione una parroquia</option>");
          $.each(parroquias,function(index,parroquia){
            if(parroquia.id == "{!! $idParroquia ? $idParroquia : 0 !!}"){
              parroquiaCmb.append("<option value='" + parroquia.id + "' selected='selected'>" + parroquia.descripcion + "</option>");
              cargarBarrios();
            }else{
              parroquiaCmb.append("<option value='" + parroquia.id + "'>" + parroquia.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ninguna zona");
        var parroquiaCmb = $("#id_parroquia");
        parroquiaCmb.empty();
        parroquiaCmb.append("<option value=''>Seleccione una parroquia</option>");
      }
    }

    function cargarBarrios(){
      var idParroquia = $("#id_parroquia").val();
      if(idParroquia != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-barrios') !!}/"+idParroquia,function(barrios){
          var barrioCmb = $("#id_barrio");
          barrioCmb.empty();
          barrioCmb.append("<option value=''>Seleccione un barrio</option>");
          $.each(barrios,function(index,barrio){
            if(barrio.id == "{!! $idBarrio ? $idBarrio : 0 !!}"){
              barrioCmb.append("<option value='" + barrio.id + "' selected='selected'>" + barrio.descripcion + "</option>");
            }else{
              barrioCmb.append("<option value='" + barrio.id + "'>" + barrio.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ningun sector");
        var barrioCmb = $("#id_barrio");
        barrioCmb.empty();
        barrioCmb.append("<option value=''>Seleccione un barrio</option>");
      }
    }

  
  </script>
@endsection