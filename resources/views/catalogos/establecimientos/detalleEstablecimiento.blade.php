@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle de PDV</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/lista-de-establecimientos'))
                    <a href="{!! route('catalogos.establecimientos.list') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Ver lista
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/editar-establecimiento'))
                    <a href="{!! route('catalogos.establecimientos.edit', [$oEstablecimiento->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-pencil"></i> Editar
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Detalle del PDV "{!! $oEstablecimiento->nombre !!}" </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra el detalle del establecimiento {!! $oEstablecimiento->nombre !!}</p>

                  <div class="">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active">
                              <a href="#tab_info" id="informacion-general-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                Información General
                              </a>
                          </li>
                          
                          <li role="presentation" class="">
                              <a href="#tab_novedades" role="tab" id="novedades-tab" data-toggle="tab" aria-expanded="false">
                                Novedades
                              </a>
                          </li>

                          <li role="presentation" class="">
                              <a href="#tab_rutas" role="tab" id="rutas-tab" data-toggle="tab" aria-expanded="false">
                                Rutas
                              </a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_info" aria-labelledby="informacion-general-tab">
                              
                                  
                                  
                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>FOTO: </strong>
                                    </div>
                                    <div class="col-md-10 col-sm-6 col-xs-6">
                                      @if($oEstablecimiento->foto != '')
                                          <img src="{!! asset('resources/fotosEstablecimientos/'.$oEstablecimiento->foto) !!}" width="300" class="img-responsive" />
                                      @else
                                          <img src="{!! asset('resources/images/default-thumbnail.png') !!}" />
                                      @endif      
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>CODIGO: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->codigo !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>NOMBRE: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>PROVINCIA: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_provincia !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>CIUDAD: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_ciudad !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>ZONA: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_zona !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>PARROQUIA: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_parroquia !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>BARRIO: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_barrio !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>DIRECCION: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      @if($oEstablecimiento->direccion_manzana)
                                        Manzana: {!! $oEstablecimiento->direccion_manzana !!} :: 
                                      @endif
                                      @if($oEstablecimiento->direccion_calle_principal)
                                        {!! $oEstablecimiento->direccion_calle_principal !!}
                                      @endif
                                      @if($oEstablecimiento->direccion_numero)
                                        {!! $oEstablecimiento->direccion_numero !!}
                                      @endif
                                      @if($oEstablecimiento->direccion_transversal)
                                        y {!! $oEstablecimiento->direccion_transversal !!}
                                      @endif
                                      @if($oEstablecimiento->direccion_referencia)
                                        <br />REFERENCIA: {!! $oEstablecimiento->direccion_referencia !!}
                                      @endif
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>CANAL: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_canal !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>SUBCANAL: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_subcanal !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>TIPO DE NEGOCIO: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_tipo_negocio !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>CADENA: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->nombre_cadena !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />


                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>NOMBRE DEL ADMINISTRADOR: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->administrador !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>TELEFONOS: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->telefonos_contacto !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>EMAIL: </strong>
                                    </div>
                                    <div class="col-md-10 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->email_contacto !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>CREADO </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! SICE\Helpers\TimeFormat::dateLongFormat($oEstablecimiento->created_at) !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>POR: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->creado_por !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                                  <div class="row">
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>ULTIMA ACTUALIZACION </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! SICE\Helpers\TimeFormat::dateLongFormat($oEstablecimiento->updated_at) !!}
                                    </div>
                                    <div class="col-md-2 col-sm-6 col-xs-6">
                                      <strong>POR: </strong>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-6">
                                      {!! $oEstablecimiento->ultima_actualizacion !!}
                                    </div>
                                  </div>
                                  <div class="clearfix"></div><br />

                              <hr />
                          </div>

                          <div role="tabpanel" class="tab-pane fade in" id="tab_novedades" aria-labelledby="novedades-tab">
                            <h5>ULTIMAS NOVEDADES</h5><hr />
                            @if( ! $aNovedades->isEmpty())

                            @else
                              <div class="alert alert-info">Aun no se han registrado novedades para "{!! $oEstablecimiento->nombre !!}".</div>
                            @endif
                            <hr />
                          </div>

                          <div role="tabpanel" class="tab-pane fade in" id="tab_rutas" aria-labelledby="rutas-tab">
                            <h5>RUTAS EN LAS QUE SE VISITA</h5>
                            @if( ! $aRutas->isEmpty())
                                
                            @else
                              <div class="alert alert-info">Aun no se ha asignado "{!! $oEstablecimiento->nombre !!}" a ninguna ruta en el sistema.</div>
                            @endif
                            <hr />
                          </div>
                        </div>
                      </div>
                    </div>

                  <h4>GEOREFERENCIA</h4>

                  <div class="form-group " style="height: 500px;">
                      {!! Mapper::render() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
