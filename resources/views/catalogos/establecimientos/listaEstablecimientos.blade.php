@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Establecimientos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/agregar-establecimiento'))
                    <a href="{!! URL::to('catalogos/establecimientos/agregar-establecimiento/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Establecimiento
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/importar-establecimiento'))
                    <a href="{!! URL::to('catalogos/establecimientos/importar-establecimientos/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-import"></i> Importar Establecimientos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Establecimientos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['url' => 'catalogos/establecimientos/lista-de-establecimientos', 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Nombre / Codigo / Cadena / Actividad de Consumidor / Canal / Subcanal / Provincia / Ciudad']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de establecimientos generados en el sistema</p>

                  @if( ! $listaEstablecimientos->isEmpty())
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Direccion </th>
                          <th class="column-title">Provincia / Ciudad </th>
                          <th class="column-title">Canal </th>
                          <th class="column-title">Subcanal </th>
                          <th class="column-title">Cadena </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaEstablecimientos as $establecimiento)
                          <tr class="even pointer">
                            <td class=" ">{!! $establecimiento->nombre !!}</td>
                            <td class=" ">{!! $establecimiento->direccion_calle_principal !!} {!! $establecimiento->direccion_numero !!} {!! $establecimiento->direccion_transversal !!} / {!! $establecimiento->direccion_referencia !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_provincia !!} / {!! $establecimiento->nombre_ciudad !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_canal !!} - {!! $establecimiento->nombre_canal !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_subcanal !!} - {!! $establecimiento->nombre_subcanal !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_cadena !!} - {!! $establecimiento->nombre_cadena !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/detalle-de-establecimiento'))
                                  <a href="{!! URL::to('catalogos/establecimientos/detalle-de-establecimiento/'.$establecimiento->id) !!}" class="btn btn-primary btn-xs" title="Ver Detalle del Establecimiento">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/editar-establecimiento'))
                                  <a href="{!! URL::to('catalogos/establecimientos/editar-establecimiento/'.$establecimiento->id) !!}" class="btn btn-info btn-xs" title="Editar Establecimiento">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/editar-establecimiento'))
                                  <a href="{!! URL::to('catalogos/establecimientos/eliminar-establecimiento/'.$establecimiento->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Establecimiento">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen establecimientos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
