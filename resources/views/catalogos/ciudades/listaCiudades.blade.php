@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Ciudades</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/ciudades/agregar-ciudad'))
                    <a href="{!! URL::to('catalogos/ciudades/agregar-ciudad/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Ciudad
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Ciudades</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['url' => 'catalogos/ciudades/lista-de-ciudades/'.$iProvinciaId, 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Codigo / Descripcion']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de ciudades generadas en el sistema</p>

                  @if(!$listaCiudades->isEmpty())
                    <div align="center"> {!! $listaCiudades->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Provincia </th>
                          <th class="column-title">Ciudad </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaCiudades as $ciudad)
                          <tr class="even pointer">
                            <td class=" ">{!! $ciudad->catalogoPadre->descripcion !!}</td>
                            <td class=" ">{!! $ciudad->descripcion !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('catalogos/zonas/lista-de-zonas'))
                                  <a href="{!! URL::to('catalogos/zonas/lista-de-zonas/'.$ciudad->id) !!}" class="btn btn-primary btn-xs" title="Zonas de la Ciudad">
                                    <i class="glyphicon glyphicon-th-list"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaCiudades->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen ciudades en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
