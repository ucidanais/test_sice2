@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Tipos de Negocio</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio'))
                    <a href="{!! URL::to('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Tipos de Negocio
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($oTipoNegocio))
                      Editar Tipo de Negocio
                    @else
                      Nuevo Tipo de Negocio
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oTipoNegocio))
                    {!! Form::model($oTipoNegocio, ['action' => ['TiposNegocioController@update', $oTipoNegocio->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'catalogos/tipos-de-negocio/agregar-tipo-de-negocio/'.$subcanalId, 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif

                    <p>Formulario de creacion de un nuevo tipo de negocio en el sistema</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Canal <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('canal_id', ['' => 'Seleccione un canal'] + $listaCanales->toArray(), $canalId, array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required', 'onchange' => 'javascript: cargarSubanales();', 'id' => 'canal_id')) !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Subcanal <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" name="catalogo_id" id="catalogo_id">
                          <option value=''>Seleccione un subcanal</option>
                        </select>
                      </div>
                    </div>

                    

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('descripcion', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del tipo de negocio', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Tipo de Negocio', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')
  <script type="text/javascript">

    @if(isset($oSubcanal))
      cargarCanales();
    @endif
    function cargarSubanales(){
      var idCanal = $("#canal_id").val();
      if(idCanal != ''){
        $.get("{!! url('api/v1/catalogos/lista-de-subcanales') !!}/"+idCanal,function(subcanales){
          var subcanalCmb = $("#catalogo_id");
          subcanalCmb.empty();
          subcanalCmb.append("<option value=''>Seleccione un subcanal</option>");
          $.each(subcanales,function(index,subcanal){
            if(subcanal.id == {!! $subcanalId ? $subcanalId : 0 !!}){
              subcanalCmb.append("<option value='" + subcanal.id + "' selected='selected'>" + subcanal.descripcion + "</option>");
            }else{
              subcanalCmb.append("<option value='" + subcanal.id + "'>" + subcanal.descripcion + "</option>");
            }
          });
        });
      }else{
        console.log("No se ha seleccionado ningun canal");
        var subcanalCmb = $("#catalogo_id");
        subcanalCmb.empty();
        subcanalCmb.append("<option value=''>Seleccione un subcanal</option>");
      }
    }

    // initialize the validator function
    //validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
@endsection