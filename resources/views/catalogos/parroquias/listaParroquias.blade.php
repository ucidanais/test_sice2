@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Parroquias</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/parroquias/agregar-parroquia'))
                    <a href="{!! URL::to('catalogos/parroquias/agregar-parroquia/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Parroquia
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Parroquias</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['url' => 'catalogos/parroquias/lista-de-parroquias/'.$iZonaId, 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Codigo / Descripcion']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de parroquias generadas en el sistema</p>

                  @if(!$listaParroquias->isEmpty())
                    <div align="center"> {!! $listaParroquias->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Provincia </th>
                          <th class="column-title">Ciudad </th>
                          <th class="column-title">Zona </th>
                          <th class="column-title">Parroquia </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaParroquias as $parroquia)
                          <tr class="even pointer">
                            <td class=" ">{!! $parroquia->catalogoPadre->catalogoPadre->catalogoPadre->descripcion !!}</td>
                            <td class=" ">{!! $parroquia->catalogoPadre->catalogoPadre->descripcion !!}</td>
                            <td class=" ">{!! $parroquia->catalogoPadre->descripcion !!}</td>
                            <td class=" ">{!! $parroquia->descripcion !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('catalogos/barrios/lista-de-barrios'))
                                  <a href="{!! URL::to('catalogos/barrios/lista-de-barrios/'.$parroquia->id) !!}" class="btn btn-primary btn-xs" title="Barrios de la Parroquia">
                                    <i class="glyphicon glyphicon-th-list"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaParroquias->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen parroquias en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
