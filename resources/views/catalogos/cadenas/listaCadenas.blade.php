@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Cadenas</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('catalogos/cadenas/agregar-cadena'))
                    <a href="{!! URL::to('catalogos/cadenas/agregar-cadena/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Cadena
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Cadenas</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['url' => 'catalogos/cadenas/lista-de-cadenas', 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Codigo / Descripcion']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de cadenas generadas en el sistema</p>

                  @if(!$listaCadenas->isEmpty())
                    <div align="center"> {!! $listaCadenas->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Descripcion </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaCadenas as $cadena)
                          <tr class="even pointer">
                            <td class=" ">{!! $cadena->descripcion !!}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaCadenas->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen cadenas en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
