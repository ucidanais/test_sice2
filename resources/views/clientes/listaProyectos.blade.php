@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>{!! $oCliente->nombre !!}</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                
              </div>
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Proyectos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <p>A continuación se muestra el listado de proyectos que nos encontramos gestionando para usted.</p>

                  <!-- start project list -->
                  <table class="table table-striped projects">
                    <thead>
                      <tr>
                        <th style="width: 1%">#</th>
                        <th style="width: 20%">Nombre del Proyecto</th>
                        <th>Líder(es) de Proyecto</th>
                        <th>Tipo de Proyecto</th>
                        <th>Status</th>
                        <th style="width: 20%">Acciones</th>
                      </tr>
                    </thead>
                    @if( ! $listaProyectos->isEmpty())
                      @php
                        $loop = 0;
                      @endphp
                      @foreach($listaProyectos as $oClienteProyecto)
                        @php
                          $loop++;
                          $aLideres = SICE\EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('lider_proyecto', 1)->get();
                        @endphp
                        <tbody>
                          <tr {!! $oClienteProyecto->estado == 0 ? 'class="danger"' : '' !!}>
                            <td>{!! $loop !!}</td>
                            <td>
                              <a>{!! $oClienteProyecto->descripcion !!}</a><br />
                              <small>Creado: {!! SICE\Helpers\TimeFormat::dateLongFormat($oClienteProyecto->created_at) !!}</small>
                            </td>
                            <td>
                              @if( ! $aLideres->isEmpty())
                                <ul class="list-inline">
                                  @foreach($aLideres as $oLider)
                                    <li>
                                      @if($oLider->usuario->avatar)
                                        <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" class="avatar" alt="{!! $oLider->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $oLider->nombre_completo_cargo !!}">
                                      @else
                                        <img src="{!! asset('resources/images/user.png') !!}" class="avatar" alt="{!! $oLider->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $oLider->nombre_completo_cargo !!}">
                                      @endif
                                    </li>
                                  @endforeach
                                </ul>
                              @endif
                            </td>
                            <td>
                                {!! $oClienteProyecto->proyecto->nombre !!}
                            </td>
                            <td>
                              @if($oClienteProyecto->estado == 1)
                                <button type="button" class="btn btn-success btn-xs">Activo</button>
                              @else
                                <button type="button" class="btn btn-danger btn-xs">Inactivo</button>
                              @endif
                            </td>
                            <td>
                              <a href="{!! route('clientes.proyectos.show', [$oClienteProyecto->id]) !!}" class="btn btn-primary">
                                  <i class="glyphicon glyphicon-eye-open"></i> Ver detalle 
                              </a>
                            </td>
                          </tr>
                        </tbody>
                      @endforeach
                    @else
                      <tbody>
                        <tr>
                          <td colspan="6">
                            <div class="alert alert-info">
                              Aún no se han asignado proyectos
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    @endif

                  </table>
                  <!-- end project list -->

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection