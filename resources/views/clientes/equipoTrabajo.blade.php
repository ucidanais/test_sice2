@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>{!! $oClienteProyecto->cliente->nombre !!} :: {!! $oClienteProyecto->proyecto->nombre !!}</h3>
            </div>
            <div class="title_right">
              <div class="col-md-9 col-sm-9 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                  <a href="{!! route('clientes.proyectos.establecimientos.mapa', [$oClienteProyecto->id]) !!}" class="btn btn-primary">
                    <i class="fa fa-building"></i> Pisos de Venta
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h4><i class="fa fa-laptop"></i>  {!! $oClienteProyecto->descripcion !!}</h4>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">

                    <div class="row">
                      
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2><i class="fa fa-group"></i> Equipo de Trabajo </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">


                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_lider" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Líder(es)</a></li>
                                <li role="presentation" class=""><a href="#tab_empleados" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Colaboradores</a></li>
                              </ul>
                              <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_lider" aria-labelledby="home-tab">
                                    @if( ! $aLideresProyecto->isEmpty())
                                        @foreach($aLideresProyecto as $oLider)
                                          <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInDown">
                                            <div class="well profile_view">
                                              <div class="col-sm-12">
                                                <h4 class="brief"><i>{!! $oLider->cargo !!}</i></h4>
                                                <div class="left col-xs-7">
                                                  <h2>{!! $oLider->nombre_completo !!}</h2>
                                                  <ul class="list-unstyled">
                                                    <li><i class="fa fa-envelope"></i> {!! $oLider->usuario->email !!} </li>
                                                  </ul>
                                                </div>
                                                <div class="right col-xs-5 text-center">
                                                  @if($oLider->usuario->avatar)
                                                    <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" class="img-circle img-responsive" alt="{!! $oLider->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $oLider->nombre_completo_cargo !!}">
                                                  @else
                                                    <img src="{!! asset('resources/images/user.png') !!}" class="img-circle img-responsive" alt="{!! $oLider->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $oLider->nombre_completo_cargo !!}">
                                                  @endif
                                                </div>
                                              </div>
                                              <div class="col-xs-12 bottom text-center">
                                                <div class="col-xs-12 col-sm-6 emphasis">
                                                  <!--
                                                  <button type="button" class="btn btn-primary btn-xs"> 
                                                    <i class="fa fa-user"></i> Ver perfil 
                                                  </button>
                                                  -->
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-info" role="alert">
                                            No se ha asignado un líder de proyecto aún
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_empleados" aria-labelledby="profile-tab">
                                  @if(!$aEmpleados->isEmpty())
                                    @foreach($aEmpleados as $equipo)
                                      <div class="col-md-6 col-sm-6 col-xs-12 animated fadeInDown">
                                        <div class="well profile_view">
                                          <div class="col-sm-12">
                                            <h4 class="brief"><i>{!! $equipo->cargo !!}</i></h4>
                                            <div class="left col-xs-7">
                                              <h2>{!! $equipo->nombre_completo !!}</h2>
                                              <ul class="list-unstyled">
                                                <li><i class="fa fa-envelope"></i> {!! $equipo->usuario->email !!} </li>
                                                @if($equipo->usuario_reporta_id)
                                                  <li>
                                                      <i class="fa fa-user"></i> 
                                                      <strong>REPORTA A:</strong> <br />
                                                      {!! $equipo->usuarioReporta->nombre !!} {!! $equipo->usuarioReporta->apellido !!}
                                                  </li>
                                                @endif
                                              </ul>
                                            </div>
                                            <div class="right col-xs-5 text-center">
                                              @if($equipo->usuario->avatar)
                                                <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" class="img-circle img-responsive" alt="{!! $equipo->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $equipo->nombre_completo_cargo !!}">
                                              @else
                                                <img src="{!! asset('resources/images/user.png') !!}" class="img-circle img-responsive" alt="{!! $equipo->nombre_completo_cargo !!}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{!! $equipo->nombre_completo_cargo !!}">
                                              @endif
                                            </div>
                                          </div>
                                          <div class="col-xs-12 bottom text-center">
                                            <div class="col-xs-12 col-sm-6 emphasis">
                                              <!--
                                                <button type="button" class="btn btn-primary btn-xs"> 
                                                  <i class="fa fa-user"></i> Ver perfil 
                                                </button>
                                              -->
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    @endforeach
                                  @else
                                    <div class="alert alert-info" role="alert">
                                        No se han asignado usuarios al equipo de trabajo
                                    </div>
                                  @endif
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  
                      <div class="clearfix"></div>

                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
