<!-- start project-detail sidebar -->
  <div class="col-md-3 col-sm-3 col-xs-12">

    <section class="panel">

      <div class="x_title">
        <h2> Descripcion del Proyecto</h2>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">

        <h3 class="green"><i class="fa fa-laptop"></i> {!! isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->descripcion : $oClienteProyecto->descripcion !!}</h3>
        
        <div class="project_detail">
          <p class="title">CLIENTE</p>
          <p>{!! isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->cliente->nombre : $oClienteProyecto->cliente->nombre !!}</p>
          
          <p class="title">LIDER(ES) DE PROYECTO</p>
          @if( ! $aLideresProyecto->isEmpty())
              @foreach($aLideresProyecto as $oLiderProyecto)
                {!! $oLiderProyecto->nombre_completo !!}<br />
                <small>{!! $oLiderProyecto->cargo !!}</small><hr />
              @endforeach
          @endif
        </div>

        <br />
          <h5>Informacion del Proyecto</h5>
          <ul class="list-unstyled project_files">
            <li>
              <a href="{!! route('clientes.proyectos.establecimientos.mapa', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-building-o"></i> PDV
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.equipo-trabajo', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-group"></i> Colaboradores
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.reporte-productividad', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-line-chart"></i> Productividad
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.reporte-visibilidad', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-image"></i> Visibilidad
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.reporte-rendimiento-rutas', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-road"></i> Rendimiento de Rutas
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.reporte-timing', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-clock-o"></i> Tiempos
              </a>
            </li>
            <li>
              <a href="{!! route('clientes.proyectos.reporte-faltantes-stock', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
                <i class="fa fa-list-alt"></i> Faltantes de Stock
              </a>
            </li>
          </ul>
          
      </div>

    </section>

  </div>
  <!-- end project-detail sidebar -->