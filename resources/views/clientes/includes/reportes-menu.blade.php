<a class="btn btn-app" href="{!! route('clientes.proyectos.establecimientos.mapa', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-building-o"></i> Puntos de Venta
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.equipo-trabajo', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-group"></i> Colaboradores
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.reporte-productividad', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-line-chart"></i> Reporte de Productividad
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.reporte-visibilidad', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-image"></i> Reporte de Visibilidad
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.reporte-rendimiento-rutas', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-road"></i> Rendimiento de Rutas
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.reporte-timing', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-clock-o"></i> Reporte de Tiempos
</a>

<a class="btn btn-app" href="{!! route('clientes.proyectos.reporte-faltantes-stock', [isset($oClienteProyectoSelected) ? $oClienteProyectoSelected->id : $oClienteProyecto->id]) !!}">
    <i class="fa fa-list-alt"></i> Faltantes de Stock
</a>