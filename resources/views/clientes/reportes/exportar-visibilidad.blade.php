<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <title>Reporte de Visibilidad</title>
    </head>
  
    <body>
        <h1>REPORTE DE VISIBILIDAD</h1>
        <h2>{!! $oClienteProyecto->descripcion !!}</h2>
        <p>Se imprimen resultados de {!! $listaNovedades->count() !!} locales en este reporte</p>
        <p>
            <strong>Se muestran locales de acuerdo a:</strong><br /><br />
            @if($filtros['provincias'])
                <strong>Provincias: </strong> {!! $filtros['provincias'] !!}<br />
            @endif
            @if($filtros['ciudades'])
                <strong>Ciudades: </strong> {!! $filtros['ciudades'] !!}<br />
            @endif
            @if($filtros['canales'])
                <strong>Canales: </strong> {!! $filtros['canales'] !!}<br />
            @endif
            @if($filtros['subcanales'])
                <strong>Subcanales: </strong> {!! $filtros['subcanales'] !!}<br />
            @endif
            @if($filtros['cadenas'])
                <strong>Cadenas: </strong> {!! $filtros['cadenas'] !!}<br />
            @endif
            <strong>Desde el: </strong> {!! $filtros['desde'] !!}<br />
            <strong>Hasta el: </strong> {!! $filtros['hasta'] !!}<br />
        </p>
        <table cellspacing="5" cellpadding="5">
            @if( ! $listaNovedades->isEmpty())
                @foreach($listaNovedades as $oNovedad)
                    <tr>
                        <td colspan="2">
                            <h3>
                                {!! $oNovedad->establecimiento->nombre !!}
                                <?php 
                                    $oClasificacion = SICE\ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oNovedad->establecimiento->id)->first();
                                ?>
                            </h3>
                            <p> 
                                <strong> Provincia: </strong>{!! $oNovedad->provincia_id ? $oNovedad->provincia->descripcion : '' !!}<br />
                                <strong> Ciudad: </strong>{!! $oNovedad->ciudad_id ? $oNovedad->ciudad->descripcion : '' !!}<br />
                                <strong> Canal: </strong>{!! $oNovedad->canal_id ? $oNovedad->canal->descripcion : '' !!}<br />
                                <strong> Subcanal: </strong>{!! $oNovedad->subcanal_id ? $oNovedad->subcanal->descripcion : '' !!}<br />
                                <strong> Cadena: </strong>{!! $oNovedad->cadena_id ? $oNovedad->cadenaObject->descripcion : '' !!}<br />
                                @if(is_object($oClasificacion))
                                    <strong>Clasificación: </strong>{{ $oClasificacion->clasificacion }}<br />
                                @else
                                    <strong>Clasificación: </strong><br />
                                @endif
                                @if($oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuario_reporta_id)
                                    <strong>SUPERVISOR: </strong> {{ $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuarioReporta->nombre }} {{ $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuarioReporta->apellido }}<br />
                                @endif
                                <strong> {!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->cargo !!}: </strong>{!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->nombre_completo !!}<br />
                                <strong> Fecha: </strong>{!! TimeFormat::dateShortFormat($oNovedad->created_at) !!}<br />
                                <strong> Numero de Fotos: </strong> {!! $oNovedad->fotos()->count() !!}
                                @if($oNovedad->detalleRuta->observaciones || $oNovedad->detalle)
                                    <br /><br />
                                    <strong>OBSERVACIONES: </strong> 
                                    {!! $oNovedad->detalleRuta->observaciones !!}<br />
                                    {!! $oNovedad->detalle !!}
                                @endif
                            </p>
                        </td>
                    </tr>
                    @php
                        $par = 0;           
                    @endphp
                    @foreach($oNovedad->fotos as $oFoto)
                        @if($par == 0)
                            <tr>
                        @endif
                            <td align="center">
                                <img style="width: 300px;" src="{!! storage_path($oFoto->imagen) !!}" alt="image" />
                                <br /><br />
                                <strong>Momento de la Foto: </strong>{!! $oFoto->momento_foto !!}<br />
                            </td>
                        @if($par == 1)
                            </tr>
                        @endif
                        @php
                            $par = $par == 0 ? 1 : 0;
                        @endphp
                    @endforeach
                    @if($par == 1)
                        </tr>
                    @endif
                @endforeach
            @endif
        </table>
    </body>
</html>