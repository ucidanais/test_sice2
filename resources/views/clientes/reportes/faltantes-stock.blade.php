@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('resources/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@endsection
    

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!} :: Reporte de Faltantes de Stock</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">

                      @include('clientes.includes.reportes-menu')

                      <h2>FILTROS</h2>

                      {!! Form::open(['route' => ['clientes.proyectos.reporte-faltantes-stock', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                    <div class="row">
                      
                      <div class="col-md-6 col-sm-4 col-xs-6">
                        
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Provincia(s)</strong></div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="aProvincias[]" id="aProvincias" class="select2_multiple form-control" multiple="multiple">
                              <option value="">Todas</option>
                              @if( ! $listaProvincias->isEmpty())
                                @foreach($listaProvincias as $oProvincia)
                                  <option {!! in_array($oProvincia->id, $aProvinciasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oProvincia->id !!}">{!! $oProvincia->descripcion !!}</option>
                                @endforeach
                              @endif
                            </select>
                          </div>
                        </div>

                      </div>
                      <div class="col-md-6 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Ciudades(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCiudades[]" id="aCiudades" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todas</option>
                                @if( ! $listaCiudades->isEmpty())
                                  @foreach($listaCiudades as $oCiudad)
                                    <option {!! in_array($oCiudad->id, $aCiudadesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCiudad->id !!}">{!! $oCiudad->descripcion !!} ({!! $oCiudad->catalogoPadre->descripcion !!})</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Cadena(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCadenas[]" id="aCadenas" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todas</option>
                                @if( ! $listaCadenas->isEmpty())
                                  @foreach($listaCadenas as $oCadena)
                                    <option {!! in_array($oCadena->id, $aCadenasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCadena->id !!}">{!! $oCadena->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Canales(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCanales[]" id="aCanales" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todos</option>
                                @if( ! $listaCanales->isEmpty())
                                  @foreach($listaCanales as $oCanal)
                                    <option {!! in_array($oCanal->id, $aCanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCanal->id !!}">{!! $oCanal->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Subcanales(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aSubcanales[]" id="aSubcanales" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todos</option>
                                @if( ! $listaSubcanales->isEmpty())
                                  @foreach($listaSubcanales as $oSubcanal)
                                    <option {!! in_array($oSubcanal->id, $aSubcanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oSubcanal->id !!}">{!! $oSubcanal->descripcion !!} ({!! $oSubcanal->catalogoPadre->descripcion !!})</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                    </div>

                    <div class="row">
                      
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <label for="desde">Desde</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerDesde'>
                              {!! Form::text('desde', $sDesdeFiltro, ['class' => 'form-control', 'id' => 'desde', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <label for="hasta">Hasta</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerHasta'>
                              {!! Form::text('hasta', $sHastaFiltro, ['class' => 'form-control', 'id' => 'hasta', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Tipo de reporte</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="tipo" id="tipo" class="select2 form-control">
                                @if( ! $listaTipos->isEmpty())
                                  @foreach($listaTipos as $oTipo)
                                    <option {!! ($oTipo->tipo == $sTipoFiltro) ? 'selected="selected"' : '' !!} value="{!! $oTipo->tipo !!}">{!! $oTipo->tipo !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>
                      </div>

                    </div>


                    <div class="row">
                      <div class="col-sm-6">
                          <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                      </div>
                    </div>
                  {!! Form::close() !!}

                      
                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
  <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>
  <script src="{!! asset('resources/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2({
          placeholder: "Seleccione el tipo de reporte",
          allowClear: true
        });

        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });

        $('#datetimepickerDesde').datetimepicker({
          format: 'YYYY-MM-DD'
        });
    
        $('#datetimepickerHasta').datetimepicker({
          format: 'YYYY-MM-DD',
          useCurrent: false
        });
    });


  </script>

@endsection
