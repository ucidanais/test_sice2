@extends('layouts.sice')

@section('customcss')
<link href="{!! asset('resources/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!} :: Reporte de Timing</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">
                    
                      @include('clientes.includes.reportes-menu')

                      <div class="clearfix"></div>

                      <h2>FILTROS</h2>
                    
                    {!! Form::open(['route' => ['clientes.proyectos.reporte-timing', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                        <div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-6">
                              <label for="desde">Desde</label>
                              <div class="form-group">
                              <div class='input-group date' id='datetimepickerDesde'>
                                  {!! Form::text('desde', $sDesdeFiltro, ['class' => 'form-control', 'id' => 'desde', 'placeholder' => 'aaaa-mm-dd']) !!}
                                  <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                              </div>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-6">
                              <label for="hasta">Hasta</label>
                              <div class="form-group">
                              <div class='input-group date' id='datetimepickerHasta'>
                                  {!! Form::text('hasta', $sHastaFiltro, ['class' => 'form-control', 'id' => 'hasta', 'placeholder' => 'aaaa-mm-dd']) !!}
                                  <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                              </div>
                          </div>
                          </div>
                          
                          <div class="row">
                          <div class="col-sm-6">
                              <button class="btn btn-primary" id="filtrar" name="filtrar" value="Filtrar" type="submit" value="filtrar"><i class="fa fa-search"></i> Filtrar</button>
                          </div>
                          <div class="col-sm-6">
                                  <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                          </div>
                        </div>
                    {!! Form::close() !!}  

                  <div class="clearfix"></div>


                  <div class="row">
                    <div class="col-md-6">
                        <h4>
                            TOTAL HORAS<br />
                            <small>
                                DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                <br />
                                HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                            </small>
                        </h4>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Horas Efectivas</td>
                                    <td>
                                        @php
                                          $totalHoras = $tiempoTotalEfectivo + $tiempoTotalMuerto;  
                                          $percent = $totalHoras > 0 ? ($tiempoTotalEfectivo*100) / $totalHoras : 0;
                                        @endphp
                                        ({!! $tiempoTotalEfectivo !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Horas Muertas
                                    </td>
                                    <td>
                                        @php
                                            $percent = $totalHoras > 0 ? ($tiempoTotalMuerto*100) / $totalHoras : 0;
                                        @endphp
                                        ({!! $tiempoTotalMuerto !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>TOTAL</strong>
                                    </td>
                                    <td>
                                        <strong>({!! $totalHoras !!}) 100 %</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div id="chartTotalHoras" style="width: 100%; height: 200px;"></div>
                    </div>
                  </div>
                  <div class="clearfix"></div>



                  <div class="row">
                      <div class="col-md-12">
                          <h4>
                              HORAS POR MERCADERISTA<br />
                              <small>
                                  DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                  <br />
                                  HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                              </small>
                          </h4>
                            @if(is_array($tiemposMercaderistas) && count($tiemposMercaderistas))
                              @foreach($tiemposMercaderistas as $aHorasMercaderistaArray)
                                <table class="table">
                                  <tr>
                                      <td><strong>SUPERVISOR</strong></td>
                                      <td><strong>MERCADERISTA</strong></td>
                                      <td><strong>HORAS EFECTIVAS</strong></td>
                                      <td><strong>HORAS MUERTAS</strong></td>
                                      <td><strong>HORAS TOTAL</strong></td>
                                  </tr>
                                  @foreach($aHorasMercaderistaArray as $aHorasMercaderista)
                                  <tr>
                                      <td>{!! $aHorasMercaderista['supervisor'] !!}</td>
                                      <td>{!! $aHorasMercaderista['mercaderista'] !!}</td>
                                      <td>
                                          @php
                                                $totalMercaderista = $aHorasMercaderista['horasEfectivas'] + $aHorasMercaderista['horasMuertas'];
                                              $percent = $totalMercaderista > 0 ? ($aHorasMercaderista['horasEfectivas']*100) / $totalMercaderista : 0;
                                          @endphp
                                          ({!! $aHorasMercaderista['horasEfectivas'] !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                      </td>
                                      <td>
                                          @php
                                              $percent = $totalMercaderista > 0 ? ($aHorasMercaderista['horasMuertas']*100) / $totalMercaderista : 0;
                                          @endphp
                                          ({!! $aHorasMercaderista['horasMuertas'] !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %  
                                      </td>
                                      <td>
                                          @php
                                              $percent = $totalHoras > 0 ? ($totalMercaderista*100) / $totalHoras : 0;
                                          @endphp
                                          ({!! $totalMercaderista !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                      </td>
                                  </tr>
                                  @endforeach
                                  <tr>
                                    <td><strong>TOTAL</strong></td>
                                    @php
                                       $totalGeneral = $tiempoTotalEfectivo + $tiempoTotalMuerto;
                                    @endphp
                                    <td><strong>({!! $tiempoTotalEfectivo !!}) 100 %</strong></td>
                                    <td><strong>({!! $tiempoTotalMuerto !!}) 100 %</strong></td>

                                    <td><strong>({!! $totalGeneral !!}) 100 %</strong></td>
                                  </tr>
                              @endforeach
                            @endif
                        </table>
                      </div>
                      <div class="col-md-12">
                          <div id="chartHorasMercaderista" style="width: 100%; height: 1000px;"></div>
                      </div>
                    </div>
                    <div class="clearfix"></div>


                  <div class="row">
                    <div class="col-md-12">
                        <h4>
                            HORAS POR PROVINCIA<br />
                            <small>
                                DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                <br />
                                HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                            </small>
                        </h4>
                        <table class="table">
                            <tr>
                                <td><strong>PROVINCIA</strong></td>
                                <td><strong>HORAS EFECTIVAS</strong></td>
                                <td><strong>HORAS MUERTAS</strong></td>
                            </tr>
                          @if(is_array($tiemposProvincia) && count($tiemposProvincia))
                            @foreach($tiemposProvincia as $nombreProvincia => $aHoras)
                                <tr>
                                    <td>{!! $nombreProvincia !!}</td>
                                    <td>
                                        @php
                                            $percent = $totalHoras > 0 ? ($aHoras['horasEfectivas']*100) / $totalHoras : 0;
                                        @endphp
                                        ({!! $aHoras['horasEfectivas'] !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                    </td>
                                    <td>
                                        @php
                                            $percent = $totalHoras > 0 ? ($aHoras['horasMuertas']*100) / $totalHoras : 0;
                                        @endphp
                                        ({!! $aHoras['horasMuertas'] !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %  
                                    </td>
                                </tr>
                            @endforeach
                          @endif
                          <tr>
                              <td><strong>TOTAL</strong></td>
                              <td><strong>({!! $tiempoTotalEfectivo !!}) 100 %</strong></td>
                              <td><strong>({!! $tiempoTotalMuerto !!}) 100 %</strong></td>
                          </tr>
                      </table>
                    </div>
                    <div class="col-md-12">
                        <div id="chartHorasProvincia" style="width: 100%; height: 500px;"></div>
                    </div>
                  </div>
                  <div class="clearfix"></div>



                  <div class="row">
                      <div class="col-md-12">
                          <h4>
                              HORAS EFECTIVAS POR CANAL<br />
                              <small>
                                  DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                  <br />
                                  HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                              </small>
                          </h4>
                          <table class="table">
                              <tr>
                                  <td><strong>CANAL</strong></td>
                                  <td><strong>HORAS EFECTIVAS</strong></td>
                              </tr>
                            @if(is_array($tiemposCanal) && count($tiemposCanal))
                              @foreach($tiemposCanal as $nombreCanal => $numHorasCanal)
                                  <tr>
                                      <td>{!! $nombreCanal !!}</td>
                                      <td>
                                          @php
                                              $percent = $tiempoTotalEfectivo > 0 ? ($numHorasCanal*100) / $tiempoTotalEfectivo : 0;
                                          @endphp
                                          ({!! $numHorasCanal !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                      </td>
                                  </tr>
                              @endforeach
                            @endif
                            <tr>
                                <td><strong>TOTAL</strong></td>
                                <td><strong>({!! $tiempoTotalEfectivo !!}) 100 %</strong></td>
                            </tr>
                        </table>
                      </div>
                      <div class="col-md-12">
                          <div id="chartHorasCanal" style="width: 100%; height: 500px;"></div>
                      </div>
                    </div>
                    <div class="clearfix"></div>



                    <div class="row">
                        <div class="col-md-12">
                            <h4>
                                HORAS EFECTIVAS POR SUBCANAL<br />
                                <small>
                                    DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                    <br />
                                    HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                </small>
                            </h4>
                            <table class="table">
                                <tr>
                                    <td><strong>SUBCANAL</strong></td>
                                    <td><strong>HORAS EFECTIVAS</strong></td>
                                </tr>
                              @if(is_array($tiemposSubcanal) && count($tiemposSubcanal))
                                @foreach($tiemposSubcanal as $nombreSubcanal => $numHorasSubcanal)
                                    <tr>
                                        <td>{!! $nombreSubcanal !!}</td>
                                        <td>
                                            @php
                                                $percent = $tiempoTotalEfectivo > 0 ? ($numHorasSubcanal*100) / $tiempoTotalEfectivo : 0;
                                            @endphp
                                            ({!! $numHorasSubcanal !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                        </td>
                                    </tr>
                                @endforeach
                              @endif
                              <tr>
                                  <td><strong>TOTAL</strong></td>
                                  <td><strong>({!! $tiempoTotalEfectivo !!}) 100 %</strong></td>
                              </tr>
                          </table>
                        </div>
                        <div class="col-md-12">
                            <div id="chartHorasSubcanal" style="width: 100%; height: 500px;"></div>
                        </div>
                      </div>
                      <div class="clearfix"></div>




                      <div class="row">
                          <div class="col-md-12">
                              <h4>
                                  HORAS EFECTIVAS POR CADENA<br />
                                  <small>
                                      DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                      <br />
                                      HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                  </small>
                              </h4>
                              <table class="table">
                                  <tr>
                                      <td><strong>CADENA</strong></td>
                                      <td><strong>HORAS EFECTIVAS</strong></td>
                                  </tr>
                                @if(is_array($tiemposCadena) && count($tiemposCadena))
                                  @foreach($tiemposCadena as $nombreCadena => $numHorasCadena)
                                      <tr>
                                          <td>{!! $nombreCadena !!}</td>
                                          <td>
                                              @php
                                                  $percent = $tiempoTotalEfectivo > 0 ? ($numHorasCadena*100) / $tiempoTotalEfectivo : 0;
                                              @endphp
                                              ({!! $numHorasCadena !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                          </td>
                                      </tr>
                                  @endforeach
                                @endif
                                <tr>
                                    <td><strong>TOTAL</strong></td>
                                    <td><strong>({!! $tiempoTotalEfectivo !!}) 100 %</strong></td>
                                </tr>
                            </table>
                          </div>
                          <div class="col-md-12">
                              <div id="chartHorasCadena" style="width: 100%; height: 500px;"></div>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                  

                    

                </div>

                @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
    <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset('resources/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script type="text/javascript">
      
      $(document).ready(function(){
        $('#datetimepickerDesde').datetimepicker({
          format: 'YYYY-MM-DD'
        });
    
        $('#datetimepickerHasta').datetimepicker({
          format: 'YYYY-MM-DD',
          useCurrent: false
        });
      });

      function drawChartTiming(divId, tiempoEfectivo, tiempoMuerto) {
          
        var data = google.visualization.arrayToDataTable([
            ['Tiempo', 'Minutos'],
            ['Tiempo Efectivo', tiempoEfectivo],
            ['Tiempo Muerto',  tiempoMuerto]
          ]);

          var options = {
            title: 'Tiempo Efectivo vs Tiempo Muerto (en minutos)'
          };

          var chart = new google.visualization.PieChart(document.getElementById(divId));
          chart.draw(data, options);
      }

      google.charts.load('current', {'packages':['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChartHorasTotales);
      google.charts.setOnLoadCallback(drawChartHorasMercaderista);
      google.charts.setOnLoadCallback(drawChartHorasProvincias);
      google.charts.setOnLoadCallback(drawChartHorasCanal);
      google.charts.setOnLoadCallback(drawChartHorasSubcanal);
      google.charts.setOnLoadCallback(drawChartHorasCadena);

      function drawChartHorasTotales() {

        var data = google.visualization.arrayToDataTable([
          ['Horas', 'Numero de Horas'],
          ['Efectivas',     {!! $tiempoTotalEfectivo !!}],
          ['Muertas',    {!! $tiempoTotalMuerto !!}]
        ]);

        var options = {
          title: 'TOTAL HORAS'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chartTotalHoras'));

        chart.draw(data, options);
      }

      function drawChartHorasMercaderista() {

        var data = google.visualization.arrayToDataTable([
          ['Mercaderista', 'Horas Efectivas', 'Horas Muertas'],
          @if(is_array($tiemposMercaderistas) && count($tiemposMercaderistas))
            @php
                $sData = '';   
            @endphp
            @foreach($tiemposMercaderistas as $aHorasMercaderistaArray)
              @foreach($aHorasMercaderistaArray as $aHorasMercaderista)
                @php
                  $sData .= "['SUPERVISOR: ".$aHorasMercaderista['supervisor']." / MERCADERISTA: ".$aHorasMercaderista['mercaderista']."', ".$aHorasMercaderista['horasEfectivas'].", ".$aHorasMercaderista['horasMuertas']."],";
                @endphp
              @endforeach
            @endforeach
            @php
                $sData = substr($sData,0,-1);
            @endphp
            {!! $sData !!}
          @endif
        ]);

        var options = {
          chart: {
            title: 'HORAS POR MERCADERISTA',
            subtitle: 'Total de horas efectivas y horas muertas por mercaderista del proyecto',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('chartHorasMercaderista'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
      
      
      function drawChartHorasProvincias() {

        var data = google.visualization.arrayToDataTable([
          ['Provincia', 'Horas Efectivas', 'Horas Muertas'],
          @if(is_array($tiemposProvincia) && count($tiemposProvincia))
            @php
                $sData = '';   
            @endphp
            @foreach($tiemposProvincia as $nombreProvincia => $aHoras)
                @php
                  $sData .= "['".$nombreProvincia."', ".$aHoras['horasEfectivas'].", ".$aHoras['horasMuertas']."],";
                @endphp
            @endforeach
            @php
                $sData = substr($sData,0,-1);
            @endphp
            {!! $sData !!}
          @endif
        ]);

        var options = {
          chart: {
            title: 'HORAS POR PROVINCIA',
            subtitle: 'Total de horas efectivas y horas muertas por provincia',
          },
          bars: 'horizontal' // Required for Material Bar Charts.
        };

        var chart = new google.charts.Bar(document.getElementById('chartHorasProvincia'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }


      function drawChartHorasCanal() {

        var data = google.visualization.arrayToDataTable([
          ['Canal', 'Horas Efectivas'],
          @if(is_array($tiemposCanal) && count($tiemposCanal))
            @php
                $sData = '';   
            @endphp
            @foreach($tiemposCanal as $nombreCanal => $numHorasCanal)
                @php
                  $sData .= "['".$nombreCanal."', ".$numHorasCanal."],";
                @endphp
            @endforeach
            @php
                $sData = substr($sData,0,-1);
            @endphp
            {!! $sData !!}
          @endif
        ]);

        var options = {
          chart: {
            title: 'HORAS EFECTIVAS POR CANAL',
            subtitle: 'Total de horas efectivas por canal',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('chartHorasCanal'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }



      function drawChartHorasSubcanal() {

        var data = google.visualization.arrayToDataTable([
          ['Subcanal', 'Horas Efectivas'],
          @if(is_array($tiemposSubcanal) && count($tiemposSubcanal))
            @php
                $sData = '';   
            @endphp
            @foreach($tiemposSubcanal as $nombreSubcanal => $numHorasSubcanal)
                @php
                  $sData .= "['".$nombreSubcanal."', ".$numHorasSubcanal."],";
                @endphp
            @endforeach
            @php
                $sData = substr($sData,0,-1);
            @endphp
            {!! $sData !!}
          @endif
        ]);

        var options = {
          chart: {
            title: 'HORAS EFECTIVAS POR SUBCANAL',
            subtitle: 'Total de horas efectivas por subcanal',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('chartHorasSubcanal'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }



      function drawChartHorasCadena() {

        var data = google.visualization.arrayToDataTable([
          ['Cadena', 'Horas Efectivas'],
          @if(is_array($tiemposCadena) && count($tiemposCadena))
            @php
                $sData = '';   
            @endphp
            @foreach($tiemposCadena as $nombreCadena => $numHorasCadena)
                @php
                  $sData .= "['".$nombreCadena."', ".$numHorasCadena."],";
                @endphp
            @endforeach
            @php
                $sData = substr($sData,0,-1);
            @endphp
            {!! $sData !!}
          @endif
        ]);

        var options = {
          chart: {
            title: 'HORAS EFECTIVAS POR CADENA',
            subtitle: 'Total de horas efectivas por cadena',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('chartHorasCadena'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

    </script>
@endsection
