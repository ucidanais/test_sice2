@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!} :: Reporte de Productividad</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">

                        @include('clientes.includes.reportes-menu')

                        <h2>FILTROS</h2>
                        {!! Form::open(['route' => ['clientes.proyectos.reporte-productividad', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                            <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label for="desde">Desde</label>
                                <div class="form-group">
                                <div class='input-group date' id='datetimepickerDesde'>
                                    {!! Form::text('desde', $sDesdeFiltro, ['class' => 'form-control', 'id' => 'desde', 'placeholder' => 'aaaa-mm-dd']) !!}
                                    <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <label for="hasta">Hasta</label>
                                <div class="form-group">
                                <div class='input-group date' id='datetimepickerHasta'>
                                    {!! Form::text('hasta', $sHastaFiltro, ['class' => 'form-control', 'id' => 'hasta', 'placeholder' => 'aaaa-mm-dd']) !!}
                                    <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                                </div>
                            </div>
                            </div>
                            
                            <div class="row">
                            <div class="col-sm-6">
                                <button class="btn btn-primary" id="filtrar" name="filtrar" value="Filtrar" type="submit" value="filtrar"><i class="fa fa-search"></i> Filtrar</button>
                            </div>
                            <div class="col-sm-6">
                                    <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                            </div>
                            </div>
                        {!! Form::close() !!}
                        
                        <div class="clearfix"></div>
                        <br /><br />

                      <div class="row">
                        <div class="col-md-6">
                            <h4>
                                RESUMEN DE VISITAS A REALIZARSE<br />
                                <small>
                                    DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                    <br />
                                    HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                </small>
                            </h4>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Visitas Realizadas</td>
                                        <td>
                                            @php
                                                $percent = $totalVisitas > 0 ? ($iVisitasMes*100) / $totalVisitas : 0;
                                            @endphp
                                            ({!! $iVisitasMes !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Visitas Faltantes
                                        </td>
                                        <td>
                                            @php
                                                $percent = $totalVisitas > 0 ? ($iNoVisitasMes*100) / $totalVisitas : 0;
                                            @endphp
                                            ({!! $iNoVisitasMes !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>TOTAL</strong>
                                        </td>
                                        <td>
                                            <strong>({!! $totalVisitas !!}) 100 %</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div id="chartResumen" style="width: 100%; height: 200px;"></div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="row">
                        <div class="col-md-6">
                            @if( ! $aBySubcanal->isEmpty())
                                <h4>
                                    VISITAS EFECTIVAS POR CANAL<br />
                                    <small>
                                        DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                        <br />
                                        HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                    </small>
                                </h4>
                                <table class="table">
                                    @foreach($aBySubcanal as $subcanal)
                                        <tr>
                                            <td>{!! $subcanal->nombre !!}</td>
                                            <td>
                                                @php
                                                    $percent = $totalVisitasView > 0 ? ($subcanal->num*100) / $totalVisitasView : 0;
                                                @endphp
                                                ({!! $subcanal->num !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td>
                                            <strong>({!! $totalVisitasView !!}) 100 %</strong>
                                        </td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div id="chartSubcanal" style="width: 100%; height: 200px;"></div>
                        </div>
                      </div>


                      <div class="clearfix"></div>
                      <div class="row">
                        <div class="col-md-6">
                            @if( ! $aByCadena->isEmpty())
                                <h4>
                                    VISITAS EFECTIVAS POR CADENA<br />
                                    <small>
                                        DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                        <br />
                                        HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                    </small>
                                </h4>
                                <table class="table">
                                    @foreach($aByCadena as $cadena)
                                        <tr>
                                            <td>{!! $cadena->nombre !!}</td>
                                            <td>
                                                @php
                                                    $percent = $iTotalCadenas > 0 ? ($cadena->num*100) / $iTotalCadenas : 0;
                                                @endphp
                                                ({!! $cadena->num !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td><strong>TOTAL</strong></td>
                                        <td>
                                            <strong>({!! $iTotalCadenas !!}) 100 %</strong>
                                        </td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div id="chartCadenas" style="width: 100%; height: 200px;"></div>
                        </div>
                      </div>
                        
                      <div class="clearfix"></div>
                    
                      <div class="row">
                            <div class="col-md-6">
                                @if( ! $aByProvincias->isEmpty())
                                    <h4>
                                        VISITAS EFECTIVAS POR PROVINCIA<br />
                                        <small>
                                            DESDE {!! TimeFormat::dateShortFormat($sDesdeFiltro) !!}
                                            <br />
                                            HASTA {!! TimeFormat::dateShortFormat($sHastaFiltro) !!}
                                        </small>
                                    </h4>
                                    <table class="table">
                                        @foreach($aByProvincias as $provincia)
                                            <tr>
                                                <td>{!! $provincia->nombre !!}</td>
                                                <td>
                                                    @php
                                                        $percent = $totalVisitasView > 0 ? ($provincia->num*100) / $totalVisitasView : 0;
                                                    @endphp
                                                    ({!! $provincia->num !!}) {!! round($percent, 2, PHP_ROUND_HALF_EVEN); !!} %
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td><strong>TOTAL</strong></td>
                                            <td>
                                                <strong>({!! $totalVisitasView !!}) 100 %</strong>
                                            </td>
                                        </tr>
                                    </table>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div id="chartProvincias" style="width: 100%; height: 200px;"></div>
                            </div>
                      </div>


                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
    <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>
    <script src="{!! asset('resources/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $('#datetimepickerDesde').datetimepicker({
              format: 'YYYY-MM-DD'
            });
        
            $('#datetimepickerHasta').datetimepicker({
              format: 'YYYY-MM-DD',
              useCurrent: false
            });
        });

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChartResumen);
      google.charts.setOnLoadCallback(drawChartCadenas);
      google.charts.setOnLoadCallback(drawChartSubcanal);
      google.charts.setOnLoadCallback(drawChartProvincias);

      @php
        $elem1 = '';
        $elem2 = '';
      @endphp

      function drawChartResumen() {

        var data = google.visualization.arrayToDataTable([
          ['Estado', 'Cantidad'],
          ['Visitados',     {!! $iVisitasMes !!}],
          ['No Visitados',    {!! $iNoVisitasMes !!}]
        ]);

        var options = {
          title: 'Resumen de Visitas'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chartResumen'));

        chart.draw(data, options);
      }

      function drawChartSubcanal() {

        var data = google.visualization.arrayToDataTable([
          ['Subcanal', 'Cantidad'],
          @if( ! $aBySubcanal->isEmpty())
            @foreach($aBySubcanal as $subcanal)
                @php
                    $elem1 .= "['$subcanal->nombre', $subcanal->num],";
                @endphp
            @endforeach
            @php
                $elem1 = substr($elem1,0,-1);
            @endphp
            {!! $elem1 !!}
          @endif
        ]);

        var options = {
          title: 'Visitas por Canal'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chartSubcanal'));

        chart.draw(data, options);
      }


      function drawChartCadenas() {

        var data = google.visualization.arrayToDataTable([
          ['Cadena', 'Cantidad'],
          @if( ! $aByCadena->isEmpty())
            @foreach($aByCadena as $cadena)
                @php
                    $elem1 .= "['$cadena->nombre', $cadena->num],";
                @endphp
            @endforeach
            @php
                $elem1 = substr($elem1,0,-1);
            @endphp
            {!! $elem1 !!}
          @endif
        ]);

        var options = {
          title: 'Visitas por Cadena'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chartCadena'));
        chart.draw(data, options);
      }


      function drawChartProvincias() {

        var data = google.visualization.arrayToDataTable([
          ['Provincia', 'Cantidad'],
          @if( ! $aByProvincias->isEmpty())
            @foreach($aByProvincias as $provincia)
                @php
                    $elem2 .= "['$provincia->nombre', $provincia->num],";
                @endphp
            @endforeach
            @php
                $elem2 = substr($elem2,0,-1);
            @endphp
            {!! $elem2 !!}
          @endif
        ]);

        var options = {
          title: 'Visitas por Provincia'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chartProvincias'));

        chart.draw(data, options);
      }
    </script>
@endsection
