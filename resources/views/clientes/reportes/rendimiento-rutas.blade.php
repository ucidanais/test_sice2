@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!} :: Reporte de Timing</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">
                    
                      @include('clientes.includes.reportes-menu')

                      <div class="clearfix"></div>

                      <h2>FILTROS</h2>
                    
                    {!! Form::open(['route' => ['clientes.proyectos.reporte-rendimiento-rutas', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                      <div class="row">
                        
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <strong>Seleccione el miembro del equipo</strong>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="equipoTrabajo" id="equipoTrabajo" class="select2 form-control custom-select" onchange="javascript: cargarRutas();">
                                @if( ! $aMercaderistas->isEmpty())
                                  @foreach($aMercaderistas as $oMercaderista)
                                    <option {!! $equipoTrabajoFiltro == $oMercaderista->id ? 'selected="selected"' : '' !!} value="{!! $oMercaderista->id !!}">{!! $oMercaderista->nombre !!} {!! $oMercaderista->apellido !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <strong>Seleccione la ruta a verificar</strong>
                              </div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="ruta" id="ruta" class="select2 form-control custom-select">

                                </select>
                              </div>
                            </div>
  
                          </div>

                      </div>
                      
                      <div class="row">
                        <div class="col-sm-6">
                          <button class="btn btn-primary" id="filtrar" name="filtrar" value="Filtrar" type="submit" value="filtrar"><i class="fa fa-search"></i> Filtrar</button>
                        </div>
                        <div class="col-sm-6">
                          <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                        </div>
                      </div>
                  {!! Form::close() !!}  

                  <div class="clearfix"></div>
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @php
                                $aHojasRutaTiempo = [];   
                              @endphp
                            @if( ! $listaHojasRuta->isEmpty())
                              <h3>Analisis de las ultimas 5 visitas a la ruta "{!! $oRuta->nombre !!}"</h3><hr />
                              @foreach($listaHojasRuta as $oHojaRuta)
                                <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                  
                                  <thead>
                                    
                                    <tr class="headings">
                                      <th class="column-title" colspan="5">
                                        {!! TimeFormat::dateLongFormat($oHojaRuta->created_at) !!}
                                      </th>
                                    </tr>

                                    <tr class="headings">
                                      <th class="column-title" colspan="5">
                                        <strong>MERCADERISTA: </strong>{{ $oHojaRuta->equipoTrabajo->nombre_completo }}<br />
                                        @if($oHojaRuta->equipoTrabajo->usuario_reporta_id)
                                          <strong>SUPERVISOR: </strong>{{ $oHojaRuta->equipoTrabajo->usuarioReporta->nombre }} {{ $oHojaRuta->equipoTrabajo->usuarioReporta->apellido }}
                                        @endif
                                      </th>
                                    </tr>

                                    <tr class="headings">
                                      <th class="column-title">PDV </th>
                                      <th class="column-title">Hora Entrada </th>
                                      <th class="column-title">Hora Salida </th>
                                      <th class="column-title">Tiempo Efectivo </th>
                                      <th class="column-title">Tiempo muerto </th>
                                    </tr>

                                  </thead>

                                  <tbody>
                                    @php
                                      $tiempoTotalTrabajo = 0;
                                      $tiempoTotalMuerto = 0;
                                      $aDetallesRuta = $oHojaRuta->detallesRuta()->whereNotNull('hora_entrada')->whereNotNull('hora_salida')->orderBy('hora_entrada', 'asc')->get();
                                    @endphp
                                    @foreach ($aDetallesRuta as $key => $oDetalleRuta)
                                      @php
                                        if($key == 0){
                                          $horaAux = '';
                                        }else{
                                          $keyAnt = $key-1;
                                          $horaAux = $aDetallesRuta[$keyAnt]->hora_salida;
                                        }
                                        $tiempoTrabajo = TimeFormat::timeElapsed($oDetalleRuta->hora_entrada, $oDetalleRuta->hora_salida);
                                        $tiempoMuerto = TimeFormat::timeElapsed($horaAux, $oDetalleRuta->hora_entrada);
                                        $tiempoTotalTrabajo += $tiempoTrabajo;
                                        $tiempoTotalMuerto += $tiempoMuerto;
                                      @endphp
                                      <tr class="even pointer">
                                        <td>{!! $oDetalleRuta->establecimiento->nombre !!}</td>
                                        <td>{!! $oDetalleRuta->hora_entrada !!}</td>
                                        <td>{!! $oDetalleRuta->hora_salida !!}</td>
                                        <td>{!! TimeFormat::convertMinutes($tiempoTrabajo) !!}</td>
                                        <td>{!! TimeFormat::convertMinutes($tiempoMuerto) !!}</td>
                                      </tr>
                                    @endforeach
                                  </tbody>
                                  <tfoot>
                                      <tr class="headings">
                                        <th class="column-title" colspan="3">TOTAL</th>
                                        <th class="column-title">{!! TimeFormat::convertMinutes($tiempoTotalTrabajo) !!}</th>
                                        <th class="column-title">{!! TimeFormat::convertMinutes($tiempoTotalMuerto) !!}</th>
                                      </tr>
                                  </tfoot>
                                </table>

                                <div id="chartTiming_{!! $oHojaRuta->id !!}" style="width: 100%; height: 300px;"></div>
                                @php
                                  $aHojasRutaTiempo[] = [
                                      'idHojaRuta' => $oHojaRuta->id,
                                      'tiempoEfectivo' => $tiempoTotalTrabajo,
                                      'tiempoMuerto' => $tiempoTotalMuerto,
                                  ];   
                                @endphp
                              @endforeach
                            @else
                              <div class="alert alert-info">Aun no se ha generado informacion acerca de est aruta.</div>
                            @endif
                        </div>
                    </div>

                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
    <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script type="text/javascript">
      
      google.charts.load('current', {'packages':['corechart']});

      $(document).ready(function(){
          $(".select2").select2();
          cargarRutas();

          timeout = setInterval(function () {
              if (google.visualization != undefined) {
                @if(count($aHojasRutaTiempo))
                  @foreach ($aHojasRutaTiempo as $hojaRutaData)
                      google.charts.setOnLoadCallback(drawChartTiming('chartTiming_{!! $hojaRutaData['idHojaRuta'] !!}', {!! $hojaRutaData['tiempoEfectivo'] !!}, {!! $hojaRutaData['tiempoMuerto'] !!}));
                  @endforeach
                @endif
                clearInterval(timeout);
              }
          }, 300);
      });

      function cargarRutas(){
        var idEquipoTrabajo = $("#equipoTrabajo").val();
        var idClienteProyecto = '{!! $oClienteProyecto->id !!}';
        if(idEquipoTrabajo != ''){
          $.get("{!! route('api.v1.rutas.listJSON') !!}/"+idClienteProyecto+'/'+idEquipoTrabajo,function(response){
            var rutasCmb = $("#ruta");
            rutasCmb.empty();
            $.each(response.listaRutas,function(index,ruta){
              if(ruta.UUID == '{!! $rutaFiltro ? $rutaFiltro : '' !!}'){
                rutasCmb.append("<option value='" + ruta.UUID + "' selected='selected'>" + ruta.Nombre + "</option>");
                $('#ruta').val(ruta.UUID).trigger('change.select2');
              }else{
                rutasCmb.append("<option value='" + ruta.UUID + "'>" + ruta.Nombre + "</option>");
              }
            });
          });
        }else{
          var rutasCmb = $("#ruta");
          rutasCmb.empty();
        }
      }

      function drawChartTiming(divId, tiempoEfectivo, tiempoMuerto) {
          
        var data = google.visualization.arrayToDataTable([
            ['Tiempo', 'Minutos'],
            ['Tiempo Efectivo', tiempoEfectivo],
            ['Tiempo Muerto',  tiempoMuerto]
          ]);

          var options = {
            title: 'Tiempo Efectivo vs Tiempo Muerto (en minutos)'
          };

          var chart = new google.visualization.PieChart(document.getElementById(divId));
          chart.draw(data, options);
      }

    </script>
@endsection
