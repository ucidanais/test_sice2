@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('resources/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@endsection
    

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!} :: Reporte de Visibilidad</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">

                      @include('clientes.includes.reportes-menu')

                      <h2>FILTROS</h2>

                      {!! Form::open(['route' => ['clientes.proyectos.reporte-visibilidad', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                    <div class="row">
                      
                      <div class="col-md-6 col-sm-4 col-xs-6">
                        
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Provincia(s)</strong></div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="aProvincias[]" id="aProvincias" class="select2_multiple form-control" multiple="multiple">
                              <option value="">Todas</option>
                              @if( ! $listaProvincias->isEmpty())
                                @foreach($listaProvincias as $oProvincia)
                                  <option {!! in_array($oProvincia->id, $aProvinciasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oProvincia->id !!}">{!! $oProvincia->descripcion !!}</option>
                                @endforeach
                              @endif
                            </select>
                          </div>
                        </div>

                      </div>
                      <div class="col-md-6 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Ciudades(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCiudades[]" id="aCiudades" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todas</option>
                                @if( ! $listaCiudades->isEmpty())
                                  @foreach($listaCiudades as $oCiudad)
                                    <option {!! in_array($oCiudad->id, $aCiudadesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCiudad->id !!}">{!! $oCiudad->descripcion !!} ({!! $oCiudad->catalogoPadre->descripcion !!})</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Cadena(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCadenas[]" id="aCadenas" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todas</option>
                                @if( ! $listaCadenas->isEmpty())
                                  @foreach($listaCadenas as $oCadena)
                                    <option {!! in_array($oCadena->id, $aCadenasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCadena->id !!}">{!! $oCadena->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Canales(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aCanales[]" id="aCanales" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todos</option>
                                @if( ! $listaCanales->isEmpty())
                                  @foreach($listaCanales as $oCanal)
                                    <option {!! in_array($oCanal->id, $aCanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCanal->id !!}">{!! $oCanal->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Subcanales(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aSubcanales[]" id="aSubcanales" class="select2_multiple form-control" multiple="multiple">
                                <option value="">Todos</option>
                                @if( ! $listaSubcanales->isEmpty())
                                  @foreach($listaSubcanales as $oSubcanal)
                                    <option {!! in_array($oSubcanal->id, $aSubcanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oSubcanal->id !!}">{!! $oSubcanal->descripcion !!} ({!! $oSubcanal->catalogoPadre->descripcion !!})</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <label for="desde">Desde</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerDesde'>
                              {!! Form::text('desde', $sDesdeFiltro, ['class' => 'form-control', 'id' => 'desde', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-6">
                        <label for="hasta">Hasta</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerHasta'>
                              {!! Form::text('hasta', $sHastaFiltro, ['class' => 'form-control', 'id' => 'hasta', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <button class="btn btn-primary" id="filtrar" name="filtrar" value="Filtrar" type="submit" value="filtrar"><i class="fa fa-search"></i> Filtrar</button>
                      </div>
                      <div class="col-sm-6">
                          <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                      </div>
                    </div>
                  {!! Form::close() !!}

                      @if( ! $listaNovedades->isEmpty())
                          @foreach($listaNovedades as $oNovedad)
                              <hr />
                              <h2>
                                {!! $oNovedad->establecimiento->nombre !!} <br />
                                <?php 
                                    $oClasificacion = SICE\ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oNovedad->establecimiento->id)->first();
                                ?>
                                <small> 
                                  <strong> Provincia: </strong>{!! $oNovedad->provincia_id ? $oNovedad->provincia->descripcion : '' !!}<br />
                                  <strong> Ciudad: </strong>{!! $oNovedad->ciudad_id ? $oNovedad->ciudad->descripcion : '' !!}<br />
                                  <strong> Canal: </strong>{!! $oNovedad->canal_id ? $oNovedad->canal->descripcion : '' !!}<br />
                                  <strong> Subcanal: </strong>{!! $oNovedad->subcanal_id ? $oNovedad->subcanal->descripcion : '' !!}<br />
                                  <strong> Cadena: </strong>{!! $oNovedad->cadena_id ? $oNovedad->cadenaObject->descripcion : '' !!}<br />
                                  <strong>Clasificación: </strong>{{ is_object($oClasificacion) ? $oClasificacion->clasificacion : '' }}<br />
                                  @if($oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuario_reporta_id)
                                    <strong>SUPERVISOR: </strong> {{ $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuarioReporta->nombre }} {{ $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->usuarioReporta->apellido }}
                                  @endif
                                  <strong> {!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->cargo !!}: </strong>{!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->nombre_completo !!}<br />
                                  @if($oNovedad->detalleRuta->observaciones || $oNovedad->detalle)
                                    <br />
                                    <strong>OBSERVACIONES: </strong> 
                                    {!! $oNovedad->detalleRuta->observaciones !!}<br />
                                    {!! $oNovedad->detalle !!}
                                  @endif
                                </small>
                              </h2>
                              <div class="row">
                                @foreach($oNovedad->fotos as $oFoto)
                                  <div class="col-md-6">
                                    <div class="thumbnail">
                                      <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="{!! \Storage::disk('public')->url($oFoto->imagen) !!}" alt="image" />
                                        <div class="mask">
                                          <p>{!! $oFoto->momento_foto !!}</p>
                                        </div>
                                      </div>
                                      <div class="caption">
                                        <p>
                                          {!! $oNovedad->detalle !!}<br />
                                          {!! TimeFormat::dateShortFormat($oFoto->created_at) !!}<br />
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                @endforeach
                              </div>
                          @endforeach
                      @else
                        <div class="alert alert-info">No se encontro informacion con los filtros seleccionados.</div>
                      @endif
                      {!! $listaNovedades->links() !!}
                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
  <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>
  <script src="{!! asset('resources/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });

        $('#datetimepickerDesde').datetimepicker({
          format: 'YYYY-MM-DD'
        });
    
        $('#datetimepickerHasta').datetimepicker({
          format: 'YYYY-MM-DD',
        });
    });


  </script>

@endsection
