@extends('layouts.sice')

@section('customcss')
  
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Detalle del Proyecto <small> {!! $oClienteProyecto->proyecto->nombre !!} </small> </h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  <a href="{!! route('clientes.proyectos.list') !!}" class="btn btn-primary">
                    <i class="fa fa-tasks"></i> Proyectos
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{!! $oClienteProyecto->descripcion !!}</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">
                      <ul class="stats-overview">
                        <li>
                          <span class="name"> Colaboradores </span>
                          <span class="value text-success"> {!! $iColaboradores !!} </span>
                        </li>
                        <li>
                          <span class="name"> PDV En Ruta </span>
                          <span class="value text-success"> {!! $iPDV !!} </span>
                        </li>
                        <li class="hidden-phone">
                          <span class="name"> Visitas Por Semana </span>
                          <span class="value text-success"> {!! $iVisitas !!} </span>
                        </li>
                      </ul>
                      <br />

                      @include('clientes.includes.reportes-menu')

                      <div id="dashboard" style="width: 100%; height: 500px;"></div>
                    
                      <div class="clearfix"></div>
                      
                      <h2>Ultimas visitas realizadas</h2>
                      <div class="clearfix"></div>
                          
                      @if( ! $aNovedades->isEmpty() )
                        <ul class="list-unstyled timeline">
                          @foreach($aNovedades as $oNovedad)
                            <li>
                              <div class="block">
                                <div class="tags">
                                  <a href="" class="tag">
                                    <span>{!! date('d', strtotime($oNovedad->created_at)) !!} {!! date('M', strtotime($oNovedad->created_at)) !!}</span>
                                  </a>
                                </div>
                                <div class="block_content">
                                  <h2 class="title">
                                    <a>{!! $oNovedad->establecimiento->nombre !!}</a>
                                  </h2>
                                  <div class="byline">
                                    <span>{!! TimeFormat::LongTimeFilter($oNovedad->created_at) !!}</span> por <a>{!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->nombre !!} {!! $oNovedad->detalleRuta->hojaRuta->equipoTrabajo->apellido !!}</a>
                                  </div>
                                  <p class="excerpt">
                                      {!! $oNovedad->establecimiento->nombre_canal !!} / 
                                      {!! $oNovedad->establecimiento->nombre_subcanal !!} / 
                                      {!! $oNovedad->establecimiento->nombre_cadena !!} / 
                                      {!! $oNovedad->establecimiento->nombre_provincia !!} / 
                                      {!! $oNovedad->establecimiento->nombre_ciudad !!}
                                      @if($oNovedad->detalleRuta->observaciones != '')
                                        <br /><br />
                                        <strong>Observaciones: </strong>{!! $oNovedad->detalleRuta->observaciones !!}
                                      @endif
                                  </p>
                                </div>
                              </div>
                            </li>
                          @endforeach
                        </ul>
                      @endif
                  </div>

                  @include('clientes.includes.project-navbar')

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Estado', 'Cantidad'],
          ['Visitados',     {!! $iVisitasMes !!}],
          ['No Visitados',    {!! $iNoVisitasMes !!}]
        ]);

        var options = {
          title: 'Resumen de Visitas del mes en curso'
        };

        var chart = new google.visualization.PieChart(document.getElementById('dashboard'));

        chart.draw(data, options);
      }
    </script>
@endsection
