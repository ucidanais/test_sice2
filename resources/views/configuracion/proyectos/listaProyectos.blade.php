@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Proyectos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos/agregar-proyecto'))
                    <a href="{!! URL::to('configuracion/proyectos/agregar-proyecto/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Proyecto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Proyectos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de proyectos generados en el sistema</p>

                  @if(!$listaProyectos->isEmpty())
                    <div align="center"> {!! $listaProyectos->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Codigo </th>
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Estado </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaProyectos as $proyecto)
                          <tr class="even pointer">
                            <td class=" ">{!! $proyecto->codigo !!}</td>
                            <td class=" ">{!! $proyecto->nombre !!}</td>
                            <td class=" ">
                              @if($proyecto->estado == 1)
                                <span class="label label-success">Activo</span>
                              @else
                                <span class="label label-danger">Inactivo</span>
                              @endif
                            </td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos/cambiar-estado-proyecto'))
                                  @if($proyecto->estado == 1)
                                    <a href="{!! URL::to('configuracion/proyectos/cambiar-estado-proyecto/'.$proyecto->id) !!}" class="btn btn-success btn-xs" title="Inhabilitar Proyecto">
                                      <i class="glyphicon glyphicon-ok"></i>
                                    </a>
                                  @else
                                    <a href="{!! URL::to('configuracion/proyectos/cambiar-estado-proyecto/'.$proyecto->id) !!}" class="btn btn-danger btn-xs" title="Habilitar Proyecto">
                                      <i class="glyphicon glyphicon-remove"></i>
                                    </a>
                                  @endif
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos/editar-proyecto'))
                                  <a href="{!! URL::to('configuracion/proyectos/editar-proyecto/'.$proyecto->id) !!}" class="btn btn-info btn-xs" title="Editar Proyecto">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos/eliminar-proyecto'))
                                  <a href="{!! URL::to('configuracion/proyectos/eliminar-proyecto/'.$proyecto->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Proyecto">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaProyectos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen proyectos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
