@extends('layouts.sice')

@section('customcss')
  <!-- Custom styling plus plugins -->
  <link href="{!! asset('resources/css/custom.css') !!}" rel="stylesheet">
  <link href="{!! asset('resources/css/icheck/flat/green.css') !!}" rel="stylesheet">

  <link href="{!! asset('resources/js/datatables/jquery.dataTables.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/buttons.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/fixedHeader.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/responsive.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/scroller.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
          <div class="row">

            <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Equipo de Trabajo <br /><small>{!! $oClienteProyecto->descripcion !!}</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    @if(!$aEquipoTrabajo->isEmpty())
                      
                      <table id="teamMembers" class="table table-striped dt-responsive">
                        <thead>
                          <tr>
                            <th>Avatar</th>
                            <th>Detalle</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>


                        <tbody>
                          @foreach($aEquipoTrabajo as $equipoTrabajo)
                            <tr>
                              <td>
                                @if($equipoTrabajo->usuario->avatar != '')
                                  <img src="{!! asset('resources/images/avatares/'.$equipoTrabajo->usuario->avatar) !!}" width="25" />
                                @else
                                  <img src="{!! asset('resources/images/user.png') !!}" width="25" />
                                @endif
                              </td>
                              <td>
                                <span>
                                    <span><strong>{!! $equipoTrabajo->nombre !!} {!! $equipoTrabajo->apellido !!}</strong></span><br />
                                </span>
                                <span class="message">
                                    @if($equipoTrabajo->lider_proyecto)
                                      <strong>Líder de Proyecto</strong><br />
                                    @endif
                                    <strong>Cargo: </strong> {!! $equipoTrabajo->cargo !!}<br />
                                    @if($equipoTrabajo->usuario_reporta_id)
                                      <strong>Reporta a:</strong>{!! $equipoTrabajo->usuarioReporta->nombre !!} {!! $equipoTrabajo->usuarioReporta->apellido !!}<br />
                                    @endif
                                    @if($equipoTrabajo->tipo == 'E')
                                      Usuario de Go Trade
                                    @else
                                      Usuario de {!! $equipoTrabajo->cliente->nombre !!}
                                    @endif
                                </span>
                              </td>
                              <td>
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="javascript: updateTeamMember('{{ route('configuracion.proyectos-por-cliente.equipos-trabajo.actualizar', [$equipoTrabajo->id]) }}', '{{ $equipoTrabajo->id }}', '{{ $equipoTrabajo->cargo }}', '{{ $equipoTrabajo->nombre }}', '{{ $equipoTrabajo->apellido }}', '{{ $equipoTrabajo->tipo }}', '{{ $equipoTrabajo->lider_proyecto }}', '{{ $equipoTrabajo->usuario_id }}', '{{ $equipoTrabajo->usuario_reporta_id }}');" title="Asignar al proyecto">
                                    <i class="fa fa-edit"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-danger" onclick="javascript: location.href='{!! url('configuracion/proyectos-por-cliente/quitar-colaborador-del-equipo-de-trabajo/'.$equipoTrabajo->id) !!}'" title="Quitar Colaborador del Proyecto"><i class="fa fa-trash"></i></button>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    @else
                      <div class="alert alert-info" role="alert">
                        Aún no se han asignado colaboradores para el equipo de trabajo de este proyecto.
                      </div>
                    @endif
                  </div>
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Usuarios <br /><small>{!! $oClienteProyecto->descripcion !!}</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_goTrade" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Usuarios Go</a></li>
                          <li role="presentation" class=""><a href="#tab_cliente" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Usuarios Cliente</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_goTrade" aria-labelledby="home-tab">
                              @if(!$aUsuariosGo->isEmpty())
                                <table id="goUsers" class="table table-striped dt-responsive">
                                  <thead>
                                    <tr>
                                      <th>Avatar</th>
                                      <th>Detalle</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($aUsuariosGo as $usuario)
                                      <?php
                                          $isTeamMember = SICE\EquipoTrabajo::where('usuario_id', $usuario->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->where('cliente_id', $oCliente->id)->where('proyecto_id', $oProyecto->id)->first();
                                          $tipoUsuarioEquipo = ($usuario->tipo == 'E' && $usuario->cliente_id != '') ? 'C' : 'E';
                                      ?>
                                      @if( ! is_object($isTeamMember))
                                        <tr>
                                          <td>
                                            <span class="image">
                                                @if($usuario->avatar != '')
                                                  <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" width="25" />
                                                @else
                                                  <img src="{!! asset('resources/images/user.png') !!}" width="25" />
                                                @endif
                                            </span>
                                          </td>
                                          <td>
                                            <span>
                                                  <span>
                                                    <strong>{!! $usuario->nombre !!} {!! $usuario->apellido !!}</strong>
                                                  </span>
                                              </span>
                                              <br />
                                              <span class="message">
                                                  <strong>Tipo de Usuario: </strong> 
                                                  @if($usuario->tipo == 'S')
                                                      Super Administrador
                                                  @endif
                                                  @if($usuario->tipo == 'A')
                                                      Administrador
                                                  @endif
                                                  @if($usuario->tipo == 'B')
                                                      Back Office
                                                  @endif
                                                  @if($usuario->tipo == 'E')
                                                      Usuario Estandar
                                                  @endif
                                                  <br />
                                                  <strong>Email: </strong> {!! $usuario->email !!}
                                              </span>
                                          </td>
                                          <td>
                                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="javascript: asignaUsuario('{!! $usuario->id !!}','{!! $tipoUsuarioEquipo !!}','{!! $usuario->nombre !!}','{!! $usuario->apellido !!}')" title="Asignar al proyecto"><i class="fa fa-sign-in"></i></button>
                                          </td>
                                        </tr>
                                      @endif
                                    @endforeach
                                  </tbody>
                                </table>
                              @else
                                <div class="alert alert-info" role="alert">
                                    No existen usuarios de Go Trade disponibles para poder asignarlos al proyecto
                                </div>
                              @endif
                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_cliente" aria-labelledby="profile-tab">
                              @if(!$aUsuariosCliente->isEmpty())
                                <table id="clientUsers" class="table table-striped dt-responsive">
                                  <thead>
                                    <tr>
                                      <th>Avatar</th>
                                      <th>Detalle</th>
                                      <th>Acciones</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($aUsuariosCliente as $usuario)
                                      <?php
                                          $isTeamMember = SICE\EquipoTrabajo::where('usuario_id', $usuario->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->where('cliente_id', $oCliente->id)->where('proyecto_id', $oProyecto->id)->first();
                                          $tipoUsuarioEquipo = ($usuario->tipo == 'E' && $usuario->cliente_id != '') ? 'C' : 'E';
                                      ?>
                                      @if( ! is_object($isTeamMember))
                                        <tr>
                                          <td>
                                            <span class="image">
                                                @if($usuario->avatar != '')
                                                  <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" width="25" />
                                                @else
                                                  <img src="{!! asset('resources/images/user.png') !!}" width="25" />
                                                @endif
                                            </span>
                                          </td>
                                          <td>
                                            <span>
                                              <strong>{!! $usuario->nombre !!} {!! $usuario->apellido !!}</strong>
                                            </span><br />
                                            <br />
                                            <span class="message">
                                                <strong>Tipo de Usuario: </strong> 
                                                @if($usuario->tipo == 'S')
                                                    Super Administrador
                                                @endif
                                                @if($usuario->tipo == 'A')
                                                    Administrador
                                                @endif
                                                @if($usuario->tipo == 'B')
                                                    Back Office
                                                @endif
                                                @if($usuario->tipo == 'E')
                                                    Usuario Estandar
                                                @endif
                                                <br />
                                                <strong>Email: </strong> {!! $usuario->email !!}
                                            </span>
                                          </td>
                                          <td>
                                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="javascript: asignaUsuario('{!! $usuario->id !!}','{!! $tipoUsuarioEquipo !!}','{!! $usuario->nombre !!}','{!! $usuario->apellido !!}')" title="Asignar al proyecto"><i class="fa fa-sign-in"></i></button>
                                          </td>
                                        </tr>
                                      @endif
                                    @endforeach
                                  </tbody>
                                </table>
                              @else
                                <div class="alert alert-info" role="alert">
                                    No existen usuarios de {!! $oClienteProyecto->cliente->nombre !!} disponibles para poder asignarlos al proyecto
                                </div>
                              @endif
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>

        </div>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" id="asignaUsuario">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">Asignar usuario al proyecto</h4>
        </div>
        {!! Form::open(['url' => 'configuracion/proyectos-por-cliente/asignar-usuario-al-equipo-de-trabajo/'.$oClienteProyecto->id, 'class' => 'form-horizontal form-label-left', 'method' => 'post', 'id' => 'AsignarUsuarioProyecto', 'name' => 'AsignarUsuarioProyecto']) !!}

            <input type="hidden" name="usuario_id" id="usuario_id" value="" />
            <input type="hidden" name="tipo" id="tipo" value="" />
            <input type="hidden" name="cliente_proyecto_id" id="cliente_proyecto_id" value="{!! $oClienteProyecto->id !!}" />
            <input type="hidden" name="cliente_id" id="cliente_id" value="{!! $oClienteProyecto->cliente_id !!}" />
            <input type="hidden" name="proyecto_id" id="proyecto_id" value="{!! $oClienteProyecto->proyecto_id !!}" />

            <div class="modal-body">

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cliente</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::text('nombreCliente', $oClienteProyecto->cliente->nombre, 
                        ["class" => "form-control col-md-9 col-xs-12", "disabled" => "disabled"]) 
                    !!}
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::text('nombreProyecto', $oClienteProyecto->proyecto->nombre, 
                    ["class" => "form-control col-md-9 col-xs-12", "disabled" => "disabled"]) 
                  !!}
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::text('nombre', null, 
                    ["class" => "form-control col-md-9 col-xs-12", 'placeholder'=>'Ingrese el nombre del colaborador', 'required' => 'required', "readonly" => "readonly", 'id' => 'nombre']) 
                  !!}
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Apellido <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::text('apellido', null, 
                    ["class" => "form-control col-md-9 col-xs-12", 'placeholder'=>'Ingrese el apellido del colaborador', 'required' => 'required', "readonly" => "readonly", 'id' => 'apellido']) 
                  !!}
                </div>
              </div>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cargo <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! Form::select('cargo', ['Gerente de Proyecto' => 'Gerente de Proyecto', 'Jefe de Proyecto' => 'Jefe de Proyecto', 'Supervisor' => 'Supervisor', 'Mercaderista/Impulsadora' => 'Mercaderista/Impulsadora', 'Auditor' => 'Auditor', 'Cliente' => 'Cliente'], null, 
                    ["class" => "form-control col-md-9 col-xs-12", 'placeholder'=>'Seleccione el cargo del colaborador en el proyecto', 'required' => 'required', 'id' => 'cargo']) 
                  !!}
                </div>
              </div>
                
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Lider de Proyecto <span class="required">*</span></label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {!! 
                    Form::select('lider_proyecto', [0 => 'NO', 1 => 'SI'], 0, array('class' => 'form-control col-md-9 col-xs-12', 'id' => 'lider_proyecto')) 
                  !!}
                </div>
              </div>
                
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Reporta a</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="usuario_reporta_id" id="usuario_reporta_id" class="select2_single form-control" tabindex="-1">
                        <option selected="selected" value="">Seleccione un Usuario</option>
                        @if(!$aUsuariosGo->isEmpty())
                            @foreach($aUsuariosGo as $usuarioReporta)
                                <?php
                                  $isTeamMember = SICE\EquipoTrabajo::where('usuario_id', $usuario->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->where('cliente_id', $oCliente->id)->where('proyecto_id', $oProyecto->id)->first();
                                ?>
                                @if(is_object($isTeamMember))
                                  <option value="{!! $usuarioReporta->id !!}">{!! $usuarioReporta->nombre !!} {!! $usuarioReporta->apellido !!}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              {!! Form::submit('Asignar Usuario', ["class" => "btn btn-primary", "id" => "btnAsignaUsuario"]) !!}

              <div style='display: none' id="spinnerReg" align="center">
                <div style="width:16px;height:11px;">
                  <img src="{{ asset('resources/images/loading.gif') }}" width="16" />
                </div>
              </div>

              <div style='display: none' role="alert" class="alert alert-danger alert-dismissible fade in" id="errorMsg"></div>
              <div style='display: none' role="alert" class="alert alert-success alert-dismissible fade in" id="successMsg"></div>

            </div>
        {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection

@section('customjs')
    <!-- Datatables-->
    <script src="{!! asset('resources/js/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.bootstrap.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/jszip.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/vfs_fonts.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.fixedHeader.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.keyTable.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/responsive.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.scroller.min.js') !!}"></script>
    <script src="{!! asset('resources/js/pace/pace.min.js') !!}"></script>


    <script type="text/javascript">

      $('#teamMembers').dataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip'
      });

      $('#goUsers').dataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip'
      });

      $('#clientUsers').dataTable({
        "dom": '<"pull-left"f><"pull-right"l>tip'
      });

      //$(".select2_single").select2();



      $('#AsignarUsuarioProyecto').submit(function () {
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            beforeSend: function(){
                $('#spinnerReg').show();
            },
            complete: function(data){
                
            },
            success: function (data) {
                $('#spinnerReg').hide();
                $('#errorMsg').hide().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>');
                $('#successMsg').hide().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>');
                if(data.success == false){
                    var errores = '';
                    for(var datos in data.message){
                        errores += '<span class="fs10">' + data.message[datos] + '</span><br />';
                    }
                    if(errores != ''){
                        $('#errorMsg').show().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+errores);
                    }
                }else{
                    $('#successMsg').show().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+data.message);
                    location.reload();
                }
            },
            error: function(errors){
                $('#spinnerReg').hide();
                $('#successMsg').hide().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>');
                $('#errorMsg').show().html('<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+errors);
            }
        });
       return false;
    });
    
    function asignaUsuario(idUsuario, tipoUsuario, nombreUsuario, apellidoUsuario){
        $('#usuario_id').val(idUsuario);
        $('#tipo').val(tipoUsuario);
        $('#nombre').val(nombreUsuario);
        $('#apellido').val(apellidoUsuario);
        $('#myModalLabel').html('Asignar usuario al proyecto');
        $('#btnAsignaUsuario').val('Asignar Usuario');
        if(tipoUsuario == 'C'){
            $('#lider_proyecto').val(0);
            $('#lider_proyecto').attr('disabled','disabled');
            $('#usuario_reporta_id').val('');
            $('#usuario_reporta_id').attr('disabled','disabled');
        }else{
            $('#lider_proyecto').val(0);
            $('#lider_proyecto').removeAttr('disabled');
            $('#usuario_reporta_id').val('');
            $('#usuario_reporta_id').removeAttr('disabled');
        }
        //$(".select2_single").select2();
    }




    function updateTeamMember(
                                urlUpdate, 
                                idEquipoTrabajo, 
                                cargo, 
                                nombre, 
                                apellido, 
                                tipo, 
                                liderProyecto, 
                                usuarioId, 
                                usuarioReportaId
                              ){
        $('#myModalLabel').html('Actualizar Información del Team Member');
        $('#btnAsignaUsuario').val('Actualizar Team Member');
        $('#AsignarUsuarioProyecto').attr('action', urlUpdate);
        $('#usuario_id').val(usuarioId);
        $("#usuario_reporta_id option[value='"+ usuarioReportaId +"']").attr("selected",true);
        $('#tipo').val(tipo);
        $('#nombre').val(nombre);
        $('#apellido').val(apellido);
        $("#cargo option[value='"+ cargo +"']").attr("selected",true);
        if(tipo == 'C'){
            $('#lider_proyecto').val(0);
            $('#lider_proyecto').attr('disabled','disabled');
            $('#usuario_reporta_id').val('');
            $('#usuario_reporta_id').attr('disabled','disabled');
        }else{
            $("#lider_proyecto option[value='"+ liderProyecto +"']").attr("selected",true);
            $('#lider_proyecto').removeAttr('disabled');
            $("#usuario_reporta_id option[value='"+ usuarioReportaId +"']").attr("selected",true);
            $('#usuario_reporta_id').removeAttr('disabled');
        }
        //$(".select2_single").select2();
    }



    </script>
@endsection