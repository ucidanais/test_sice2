@extends('layouts.sice')

@section('customcss')
  <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>PDV del cliente "{!! $oClienteProyecto->cliente->nombre !!}" en el proyecto "{!! $oClienteProyecto->proyecto->nombre !!}"</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/lista-de-establecimientos'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Ver lista
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/exportar-establecimientos'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.exportExcel', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-download"></i> Exportar Excel
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/seleccionar-establecimiento'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.lista.asignar', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-plus"></i> Agregar Establecimientos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de PDV de "{!! $oClienteProyecto->descripcion !!}" en MAPA <small>Total de PDV ({!! $numEstablecimientos !!})</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.mapa', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                      <div class="row">
                        
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Buscar Por:</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              {!! Form::text('sValorBusqueda', null, 
                                  ["class" => "form-control col-md-12 col-sm-12 col-xs-12", 'placeholder'=>'ID / NOMBRE / CODIGO / DIRECCION / CANAL / SUBCANAL']) 
                                !!}
                            </div>
                          </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Provincia(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aProvincias[]" id="aProvincias" class="select2_multiple form-control" multiple="multiple">
                                @if( ! $listaProvincias->isEmpty())
                                  @foreach($listaProvincias as $oProvincia)
                                    <option {!! in_array($oProvincia->id, $aProvinciasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oProvincia->id !!}">{!! $oProvincia->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Ciudades(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCiudades[]" id="aCiudades" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCiudades->isEmpty())
                                    @foreach($listaCiudades as $oCiudad)
                                      <option {!! in_array($oCiudad->id, $aCiudadesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCiudad->id !!}">{!! $oCiudad->descripcion !!} ({!! $oCiudad->catalogoPadre->descripcion !!})</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>

                        
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Cadena(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCadenas[]" id="aCadenas" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCadenas->isEmpty())
                                    @foreach($listaCadenas as $oCadena)
                                      <option {!! in_array($oCadena->id, $aCadenasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCadena->id !!}">{!! $oCadena->descripcion !!}</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Canales(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCanales[]" id="aCanales" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCanales->isEmpty())
                                    @foreach($listaCanales as $oCanal)
                                      <option {!! in_array($oCanal->id, $aCanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCanal->id !!}">{!! $oCanal->descripcion !!}</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Subcanales(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aSubcanales[]" id="aSubcanales" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaSubcanales->isEmpty())
                                    @foreach($listaSubcanales as $oSubcanal)
                                      <option {!! in_array($oSubcanal->id, $aSubcanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oSubcanal->id !!}">{!! $oSubcanal->descripcion !!} ({!! $oSubcanal->catalogoPadre->descripcion !!})</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                          {!! Form::submit('Filtrar', ["class" => "btn btn-primary"]) !!}
                      </div>
                    {!! Form::close() !!}
                </div>

                <div class="x_content">
                    <hr />
                    <p>Se encontraron "{!! $numEstablecimientos !!}" resultado(s) de la busqueda con los filtros seleccionados.</p>
                    <hr />
                    <img src="http://maps.google.com/mapfiles/ms/micons/green-dot.png" width="16"> ESTA EN RUTA | 
                    <img src="http://maps.google.com/mapfiles/ms/micons/red-dot.png" width="16"> NO ESTA EN RUTA
                  <hr />

                  <div style="height: 600px; width: 100%;">
                        <div id="map-canvas-0" style="width: 100%; height: 100%; margin: 0; padding: 0; position: relative; overflow: hidden;">
                    </div>
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection


@section('customjs')
  
  <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3.31&region=GB&language=en-gb&key={!! config('app.google_api_key') !!}&libraries=places"></script>
  
  <script type="text/javascript" src="//googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"></script>
  <script>

    $(document).ready(function() {
        
        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });
        initialize();
    });


    var maps = [];

    function initialize() {
      var bounds = new google.maps.LatLngBounds();
      var infowindow = new google.maps.InfoWindow();
      var position = new google.maps.LatLng(-0.207193, -78.4593905);

      var mapOptions_0 = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI:  false ,
            scrollwheel:  true ,
            fullscreenControl:  true ,
      };

      var map_0 = new google.maps.Map(document.getElementById('map-canvas-0'), mapOptions_0);
      map_0.setTilt(0);

      var markers = [];
      var infowindows = [];
      var shapes = [];

      @if( ! $listaEstablecimientos->isEmpty())
        @php
          $key = 0;
        @endphp
        @foreach($listaEstablecimientos as $oEstablecimiento)
          @php
            if($oEstablecimiento->geolocalizacion){
            $coordenadas = explode(',', $oEstablecimiento->geolocalizacion);
            if($oEstablecimiento->foto != ''){
                $foto = asset('resources/fotosEstablecimientos/'.$oEstablecimiento->foto);
            }else{
                $foto = asset('resources/images/default-thumbnail.png');
            }

            $enRuta = $oEstablecimiento->en_ruta ? '<span class="label label-success">SI</span>' : '<span class="label label-danger">NO</span>';

            if($oEstablecimiento->en_ruta){
              $icon = 'http://maps.google.com/mapfiles/ms/micons/green-dot.png';
            }else{
              $icon = 'http://maps.google.com/mapfiles/ms/micons/red-dot.png';
            }
            
            $content = '<div class="row">';
                $content .= '<div class="col-md-6">';
                    $content .= '<img src="'.$foto.'" width="250" class="img-responsive" alt="'.$oEstablecimiento->nombre.'" />';
                $content .= '</div>';
                $content .= '<div class="col-md-6">';
                    $content .= '<h2>'.$oEstablecimiento->nombre.'</h2><hr />';
                    $content .= '<p><strong>CODIGO: </strong>'.$oEstablecimiento->codigo.'</p>';
                    $content .= '<p>';
                    if($oEstablecimiento->nombre_canal)
                        $content .= $oEstablecimiento->nombre_canal;

                    if($oEstablecimiento->nombre_subcanal)
                        $content .= ' / '.$oEstablecimiento->nombre_subcanal;

                    if($oEstablecimiento->nombre_tipo_negocio)
                        $content .= ' / '.$oEstablecimiento->nombre_tipo_negocio;

                    if($oEstablecimiento->nombre_cadena)
                        $content .= ' / '.$oEstablecimiento->nombre_cadena;
                    $content .= '</p>';
                    $content .= '<p>';
                        $content .= '<strong>DIRECCION</strong><br />';
                        if($oEstablecimiento->direccion_manzana)
                            $content .= 'MANZANA: '.$oEstablecimiento->direccion_manzana.' / ';

                        if($oEstablecimiento->direccion_calle_principal)
                            $content .= $oEstablecimiento->direccion_calle_principal;

                        if($oEstablecimiento->direccion_numero)
                            $content .= ' '.$oEstablecimiento->direccion_numero;

                        if($oEstablecimiento->direccion_transversal)
                            $content .= ' y '.$oEstablecimiento->direccion_transversal;

                        if($oEstablecimiento->direccion_referencia)
                            $content .= ' / REFERENCIA: '.$oEstablecimiento->direccion_referencia;

                    $content .= '</p>';
                    $content .= '<p>';
                        if($oEstablecimiento->nombre_provincia)
                            $content .= $oEstablecimiento->nombre_provincia;

                        if($oEstablecimiento->nombre_ciudad)
                            $content .= ' / '.$oEstablecimiento->nombre_ciudad;

                        if($oEstablecimiento->nombre_zona)
                            $content .= ' / '.$oEstablecimiento->nombre_zona;

                        if($oEstablecimiento->nombre_parroquia)
                            $content .= ' / '.$oEstablecimiento->nombre_parroquia;

                        if($oEstablecimiento->nombre_barrio)
                            $content .= ' / '.$oEstablecimiento->nombre_barrio;

                    $content .= '</p>';
                    $content .= '<p><strong>EN RUTA: </strong>'.$enRuta.'</p>';
                $content .= '</div>';
            $content .= '</div>';
          @endphp
          var markerPosition_{!! $key !!} = new google.maps.LatLng({!! $coordenadas[0] !!}, {!! $coordenadas[1] !!});
          var marker_{!! $key !!} = new google.maps.Marker({
                                                position: markerPosition_{!! $key !!},
                                                title: "{!! $oEstablecimiento->nombre !!}",
                                                label: "",
                                                animation: '',
                                                icon: "{!! $icon !!}"
                                              });

          bounds.extend(marker_{!! $key !!}.position);
          marker_{!! $key !!}.setMap(map_0);
          markers.push(marker_{!! $key !!});
      
          var infowindow_{!! $key !!} = new google.maps.InfoWindow({
            content: "{!! addslashes($content) !!}"
          });

          infowindow_{!! $key !!}.setOptions({ maxWidth: 700 });

          google.maps.event.addListener(marker_{!! $key !!}, 'click', function() {
              infowindow_{!! $key !!}.open(map_0, marker_{!! $key !!});
          });

          infowindows.push(infowindow_{!! $key !!});
          @php
            $key++;
          }
          @endphp
        @endforeach
      @endif

      var idleListener = google.maps.event.addListenerOnce(map_0, "idle", function () {
                                                              map_0.setZoom(12);
                                                              map_0.fitBounds(bounds);
                                                          });
      var map = map_0;
      
      maps.push({
        key: 0,
        markers: markers,
        infowindows: infowindows,
        map: map_0,
        shapes: shapes
      });
    }
  </script>

@endsection
