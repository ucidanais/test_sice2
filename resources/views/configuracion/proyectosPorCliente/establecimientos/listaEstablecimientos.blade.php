@extends('layouts.sice')

@section('customcss')
  <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>PDV del cliente "{!! $oClienteProyecto->cliente->nombre !!}" en el proyecto "{!! $oClienteProyecto->proyecto->nombre !!}"</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/mapa-de-establecimientos'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.mapa', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Ver Mapa
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/exportar-establecimientos'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.exportExcel', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-download"></i> Exportar Excel
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/seleccionar-establecimiento'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.lista.asignar', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-plus"></i> Agregar Establecimientos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de PDV de "{!! $oClienteProyecto->descripcion !!}" <small>Total de PDV ({!! $numEstablecimientos !!})</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.lista', $oClienteProyecto->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                      <div class="row">
                        
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Buscar Por:</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              {!! Form::text('sValorBusqueda', old('sValorBusqueda'), 
                                  ["class" => "form-control col-md-12 col-sm-12 col-xs-12", 'placeholder'=>'ID / NOMBRE / CODIGO / DIRECCION / CANAL / SUBCANAL']) 
                                !!}
                            </div>
                          </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Provincia(s)</strong></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="aProvincias[]" id="aProvincias" class="select2_multiple form-control" multiple="multiple">
                                @if( ! $listaProvincias->isEmpty())
                                  @foreach($listaProvincias as $oProvincia)
                                    <option {!! in_array($oProvincia->id, $aProvinciasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oProvincia->id !!}">{!! $oProvincia->descripcion !!}</option>
                                  @endforeach
                                @endif
                              </select>
                            </div>
                          </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Ciudades(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCiudades[]" id="aCiudades" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCiudades->isEmpty())
                                    @foreach($listaCiudades as $oCiudad)
                                      <option {!! in_array($oCiudad->id, $aCiudadesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCiudad->id !!}">{!! $oCiudad->descripcion !!} ({!! $oCiudad->catalogoPadre->descripcion !!})</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>

                        
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione la(s) Cadena(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCadenas[]" id="aCadenas" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCadenas->isEmpty())
                                    @foreach($listaCadenas as $oCadena)
                                      <option {!! in_array($oCadena->id, $aCadenasFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCadena->id !!}">{!! $oCadena->descripcion !!}</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Canales(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aCanales[]" id="aCanales" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaCanales->isEmpty())
                                    @foreach($listaCanales as $oCanal)
                                      <option {!! in_array($oCanal->id, $aCanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oCanal->id !!}">{!! $oCanal->descripcion !!}</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                          
                            <div class="form-group">
                              <div class="col-md-12 col-sm-12 col-xs-12"><strong>Seleccione lo(s) Subcanales(s)</strong></div>
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <select name="aSubcanales[]" id="aSubcanales" class="select2_multiple form-control" multiple="multiple">
                                  @if( ! $listaSubcanales->isEmpty())
                                    @foreach($listaSubcanales as $oSubcanal)
                                      <option {!! in_array($oSubcanal->id, $aSubcanalesFiltro) ? 'selected="selected"' : '' !!} value="{!! $oSubcanal->id !!}">{!! $oSubcanal->descripcion !!} ({!! $oSubcanal->catalogoPadre->descripcion !!})</option>
                                    @endforeach
                                  @endif
                                </select>
                              </div>
                            </div>

                        </div>
                      </div>
                      
                      <div class="col-sm-6">
                          {!! Form::submit('Filtrar', ["class" => "btn btn-primary"]) !!}
                      </div>
                    {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de establecimientos asignados a este proyecto</p>

                  @if( ! $listaEstablecimientos->isEmpty())
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                    
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">ID </th>
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Direccion </th>
                          <th class="column-title">Provincia / Ciudad </th>
                          <th class="column-title">Canal </th>
                          <th class="column-title">Subcanal </th>
                          <th class="column-title">Cadena </th>
                          <th class="column-title">Estado </th>
                          <th class="column-title">En Ruta </th>
                          <th class="column-title no-link last" width="150"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaEstablecimientos as $establecimiento)
                          <tr class="even pointer">
                            <td class=" ">{!! $establecimiento->id !!}</td>
                            <td class=" ">{!! $establecimiento->nombre !!}</td>
                            <td class=" ">
                              @if($establecimiento->direccion_manzana)
                                Manzana: {!! $establecimiento->direccion_manzana !!} / 
                              @endif
                              {!! $establecimiento->direccion_calle_principal !!} 
                              @if($establecimiento->direccion_numero)
                                {!! $establecimiento->direccion_numero !!} 
                              @endif
                              @if($establecimiento->direccion_transversal)
                                y {!! $establecimiento->direccion_transversal !!} 
                              @endif
                              @if($establecimiento->direccion_referencia)
                                <br /> <strong>Referencia:</strong> {!! $establecimiento->direccion_referencia !!}
                              @endif
                            </td>
                            <td class=" ">
                                {!! $establecimiento->nombre_provincia !!} / {!! $establecimiento->nombre_ciudad !!}
                            </td>
                            <td class=" ">{!! $establecimiento->nombre_canal !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_subcanal !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_cadena !!}</td>
                            <td class=" ">
                              @if($establecimiento->estado == 1)
                                <span class="label label-success">Activo</span>
                              @else
                                <span class="label label-danger">Inactivo</span>
                              @endif
                            </td>
                            <td class=" ">
                              @if($establecimiento->en_ruta == 1)
                                <span class="label label-success">SI</span>
                              @else
                                <span class="label label-danger">NO</span>
                              @endif
                            </td>
                            <td class="last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/quitar-establecimiento'))
                                  {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.quitar', $oClienteProyecto->id, $establecimiento->id], 'class' => 'form-horizontal form-label-left', 'method' => 'delete']) !!}
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/detalle-establecimiento'))
                                  <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.detalle', [$oClienteProyecto->id, $establecimiento->id]) !!}" class="btn btn-info btn-xs" title="Ver Detalle del Establecimiento '{!! $establecimiento->nombre !!}'">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/editar-establecimiento'))
                                  <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.editar', [$oClienteProyecto->id, $establecimiento->id]) !!}" class="btn btn-primary btn-xs" title="Editar el Establecimiento '{!! $establecimiento->nombre !!}'">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/quitar-establecimiento'))
                                    <button type="submit" class="btn btn-xs btn-danger" title="Quitar establecimiento '{!! $establecimiento->nombre !!}' del proyecto '{!! $oClienteProyecto->descripcion !!}'">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                  {!! Form::close() !!}
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen establecimientos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection


@section('customjs')
  
  <script type="text/javascript">

    $(document).ready(function() {
        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });
    });
  </script>

@endsection
