@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Lista General de Establecimientos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/lista-de-establecimientos'))
                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.mapa', [$oClienteProyecto->id]) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Establecimientos del Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Establecimientos</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.lista.asignar', $oClienteProyecto->id], 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Nombre / Codigo / Cadena / Actividad de Consumidor / Canal / Subcanal / Provincia / Ciudad']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de establecimientos del sistema excepto los que se encuentran asignados al proyecto "{!! $oClienteProyecto->descripcion !!}" del cliente "{!! $oCliente->nombre !!}"</p>

                  @if(!$listaEstablecimientos->isEmpty())
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Direccion </th>
                          <th class="column-title">Provincia / Ciudad </th>
                          <th class="column-title">Canal </th>
                          <th class="column-title">Subcanal </th>
                          <th class="column-title">Cadena </th>
                          <th class="column-title">Es parte del proyecto </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaEstablecimientos as $establecimiento)
                          @if(is_null($establecimiento->cliente_proyecto_id))
                            <tr class="even pointer">
                          @else
                            @if($establecimiento->cliente_proyecto_id == $oClienteProyecto->id)
                              <tr class="even pointer success">
                            @else
                              <tr class="even pointer">
                            @endif
                          @endif
                            <td class=" ">{!! $establecimiento->nombre !!}</td>
                            <td class=" ">{!! $establecimiento->direccion_calle_principal !!} {!! $establecimiento->direccion_numero !!} {!! $establecimiento->direccion_transversal !!} / {!! $establecimiento->direccion_referencia !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_provincia !!} / {!! $establecimiento->nombre_ciudad !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_canal !!} - {!! $establecimiento->nombre_canal !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_subcanal !!} - {!! $establecimiento->nombre_subcanal !!}</td>
                            <td class=" ">{!! $establecimiento->codigo_cadena !!} - {!! $establecimiento->nombre_cadena !!}</td>
                            <td class=" ">
                              @if(is_null($establecimiento->cliente_proyecto_id))
                                  <span class="label label-warning">NO</span> 
                              @else
                                @if($establecimiento->cliente_proyecto_id == $oClienteProyecto->id)
                                  <span class="label label-success">SI</span>
                                @else
                                  <span class="label label-warning">NO</span>
                                @endif
                              @endif
                            </td>
                            <td class=" last">
                              <small>
                                @if(is_null($establecimiento->cliente_proyecto_id))
                                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/asignar-establecimiento'))
                                    <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.asignar', [$oClienteProyecto->id, $establecimiento->id]) !!}" class="btn btn-success btn-xs" title="Agregar establecimiento '{!! $establecimiento->nombre !!}' al proyecto '{!! $oClienteProyecto->descripcion !!}'">
                                      <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                  @endif
                                @else
                                  @if($establecimiento->cliente_proyecto_id == $oClienteProyecto->id)
                                    @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/quitar-establecimiento'))
                                        {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.quitar', $oClienteProyecto->id, $establecimiento->id], 'class' => 'form-horizontal form-label-left', 'method' => 'delete']) !!}
                                          <button type="submit" class="btn btn-xs btn-danger" title="Quitar establecimiento '{!! $establecimiento->nombre !!}' del proyecto '{!! $oClienteProyecto->descripcion !!}'">
                                              <i class="glyphicon glyphicon-trash"></i>
                                          </button>
                                        {!! Form::close() !!}

                                  @endif
                                  @else
                                    @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/asignar-establecimiento'))
                                      <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.asignar', [$oClienteProyecto->id, $establecimiento->id]) !!}" class="btn btn-success btn-xs" title="Agregar establecimiento al proyecto">
                                        <i class="glyphicon glyphicon-plus"></i>
                                      </a>
                                    @endif
                                  @endif
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaEstablecimientos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen establecimientos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
