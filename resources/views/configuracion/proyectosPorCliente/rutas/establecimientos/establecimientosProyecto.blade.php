@extends('layouts.sice')

@section('customcss')
  <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
  <!-- Custom styling plus plugins -->
  <link href="{!! asset('resources/css/custom.css') !!}" rel="stylesheet">
  <link href="{!! asset('resources/css/icheck/flat/green.css') !!}" rel="stylesheet">

  <link href="{!! asset('resources/js/datatables/jquery.dataTables.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/buttons.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/fixedHeader.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/responsive.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/scroller.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_right">
              <h3>PDV del proyecto "{!! $oClienteProyecto->descripcion !!}" para el cliente "{!! $oCliente->nombre !!}"</h3>
            </div>
          </div>
          @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
            <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
              <i class="fa fa-eye"></i> Detalle de Proyecto
            </a>
          @endif
          @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos'))
            <a href="{!! URL::to('configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos/'.$oRuta->id) !!}" class="btn btn-primary btn-sm">
              <i class="fa fa-th-list"></i> Volver a la Ruta
            </a>
          @endif
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de PDV del proyecto "{!! $oClienteProyecto->descripcion !!}"</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de establecimientos asignados a este proyecto</p>

                  @if( ! $aEstablecimientos->isEmpty())
                    
                    <table id="listaEstablecimientos" class="table table-striped dt-responsive">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Direccion </th>
                          <th class="column-title">Provincia / Ciudad </th>
                          <th class="column-title">Canal </th>
                          <th class="column-title">Subcanal </th>
                          <th class="column-title">Cadena </th>
                          <th class="column-title">En Ruta </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($aEstablecimientos as $oClienteEstablecimiento)
                          @php
                            $establecimiento = $oClienteEstablecimiento->establecimiento;
                            $enRuta = $oRuta->establecimientos()->where('id', $establecimiento->id)->first();
                          @endphp
                          <tr class="even pointer">
                            <td class=" ">{!! $establecimiento->nombre !!}</td>
                            <td class=" ">
                              @if($establecimiento->direccion_manzana)
                                Manzana: {!! $establecimiento->direccion_manzana !!} / 
                              @endif
                              {!! $establecimiento->direccion_calle_principal !!} 
                              @if($establecimiento->direccion_numero)
                                {!! $establecimiento->direccion_numero !!} 
                              @endif
                              @if($establecimiento->direccion_transversal)
                                y {!! $establecimiento->direccion_transversal !!} 
                              @endif
                              @if($establecimiento->direccion_referencia)
                                <br /> <strong>Referencia:</strong> {!! $establecimiento->direccion_referencia !!}
                              @endif
                            </td>
                            <td class=" ">
                                {!! $establecimiento->nombre_provincia !!} / {!! $establecimiento->nombre_ciudad !!}
                            </td>
                            <td class=" ">{!! $establecimiento->nombre_canal !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_subcanal !!}</td>
                            <td class=" ">{!! $establecimiento->nombre_cadena !!}</td>
                            <td class=" ">
                              @if(is_object($enRuta))
                                <span class="label label-success">SI</span>
                              @else
                                <span class="label label-danger">NO</span>
                              @endif
                            </td>
                            <td class="last">
                              <small>
                                @if(is_object($enRuta))
                                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/quitar-establecimiento'))
                                    {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.delete', $oRuta->id, $establecimiento->id], 'class' => 'form-horizontal form-label-left', 'method' => 'delete']) !!}
                                      <button type="submit" class="btn btn-xs btn-danger" title="Quitar establecimiento '{!! $establecimiento->nombre !!}' de la ruta '{!! $oRuta->nombre !!}'">
                                          <i class="glyphicon glyphicon-trash"></i>
                                      </button>
                                    {!! Form::close() !!}
                                  @endif
                                @else
                                  <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos.store', [$oRuta->id, $establecimiento->id]) !!}" class="btn btn-xs btn-success" title="Agregar establecimiento {!! $establecimiento->nombre !!} a la ruta {!! $oRuta->nombre !!}">
                                      <i class="glyphicon glyphicon-plus"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @else
                    <div class="alert alert-info">Aun no existen establecimientos en el sistema.</div>
                  @endif

                  <hr />
                    <p>A continuación los establecimientos de la ruta puntillados en el mapa.</p>
                    <p>
                      <img src="http://maps.google.com/mapfiles/ms/micons/green-dot.png" alt="En Ruta" /> Está en ruta | 
                      <img src="http://maps.google.com/mapfiles/ms/micons/red-dot.png" alt="En Ruta" /> No está en ruta
                    </p>
                  <hr />

                  <div style="height: 600px; width: 100%;">
                    <div id="map-canvas-0" style="width: 100%; height: 100%; margin: 0; padding: 0; position: relative; overflow: hidden;">
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection


@section('customjs')
  <!-- Datatables-->
    <script src="{!! asset('resources/js/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.bootstrap.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/jszip.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/vfs_fonts.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.fixedHeader.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.keyTable.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/responsive.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.scroller.min.js') !!}"></script>
    <script src="{!! asset('resources/js/pace/pace.min.js') !!}"></script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3.31&region=GB&language=en-gb&key={!! config('app.google_api_key') !!}&libraries=places"></script>
    <script type="text/javascript" src="//googlemaps.github.io/js-marker-clusterer/src/markerclusterer.js"></script>

  <script type="text/javascript">

    $(document).ready(function() {
        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });

        $('#listaEstablecimientos').dataTable();
        initialize();
    });

    var maps = [];

    function initialize() {
      var bounds = new google.maps.LatLngBounds();
      var infowindow = new google.maps.InfoWindow();
      var position = new google.maps.LatLng(-0.207193, -78.4593905);

      var mapOptions_0 = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI:  false ,
            scrollwheel:  true ,
            fullscreenControl:  true ,
      };

      var map_0 = new google.maps.Map(document.getElementById('map-canvas-0'), mapOptions_0);
      map_0.setTilt(0);

      var markers = [];
      var infowindows = [];
      var shapes = [];

      @if( ! $aEstablecimientos->isEmpty())
        @php
          $key = 0;
        @endphp
        @foreach($aEstablecimientos as $oClienteEstablecimiento)
          @php
            
            $oEstablecimiento = $oClienteEstablecimiento->establecimiento;
            if($oEstablecimiento->geolocalizacion){
              $coordenadas = explode(',', $oEstablecimiento->geolocalizacion);
              if($oEstablecimiento->foto != ''){
                  $foto = asset('resources/fotosEstablecimientos/'.$oEstablecimiento->foto);
              }else{
                  $foto = asset('resources/images/default-thumbnail.png');
              }

              $estaEnRuta = $oRuta->establecimientos()->where('id', $oEstablecimiento->id)->first();
              if(is_object($estaEnRuta)){
                $enRuta = '<span class="label label-success">SI</span>';
                $icon = 'http://maps.google.com/mapfiles/ms/micons/green-dot.png';
              }else{
                $enRuta = '<span class="label label-danger">NO</span>';
                $icon = 'http://maps.google.com/mapfiles/ms/micons/red-dot.png';
              }
              
              $content = '<div class="row">';
                  $content .= '<div class="col-md-6">';
                      $content .= '<img src="'.$foto.'" width="250" class="img-responsive" alt="'.$oEstablecimiento->nombre.'" />';
                  $content .= '</div>';
                  $content .= '<div class="col-md-6">';
                      $content .= '<h2>'.$oEstablecimiento->nombre.'</h2><hr />';
                      $content .= '<p><strong>CODIGO: </strong>'.$oEstablecimiento->codigo.'</p>';
                      $content .= '<p>';
                      if($oEstablecimiento->nombre_canal)
                          $content .= $oEstablecimiento->nombre_canal;

                      if($oEstablecimiento->nombre_subcanal)
                          $content .= ' / '.$oEstablecimiento->nombre_subcanal;

                      if($oEstablecimiento->nombre_tipo_negocio)
                          $content .= ' / '.$oEstablecimiento->nombre_tipo_negocio;

                      if($oEstablecimiento->nombre_cadena)
                          $content .= ' / '.$oEstablecimiento->nombre_cadena;
                      $content .= '</p>';
                      $content .= '<p>';
                          $content .= '<strong>DIRECCION</strong><br />';
                          if($oEstablecimiento->direccion_manzana)
                              $content .= 'MANZANA: '.$oEstablecimiento->direccion_manzana.' / ';

                          if($oEstablecimiento->direccion_calle_principal)
                              $content .= $oEstablecimiento->direccion_calle_principal;

                          if($oEstablecimiento->direccion_numero)
                              $content .= ' '.$oEstablecimiento->direccion_numero;

                          if($oEstablecimiento->direccion_transversal)
                              $content .= ' y '.$oEstablecimiento->direccion_transversal;

                          if($oEstablecimiento->direccion_referencia)
                              $content .= ' / REFERENCIA: '.$oEstablecimiento->direccion_referencia;

                      $content .= '</p>';
                      $content .= '<p>';
                          if($oEstablecimiento->nombre_provincia)
                              $content .= $oEstablecimiento->nombre_provincia;

                          if($oEstablecimiento->nombre_ciudad)
                              $content .= ' / '.$oEstablecimiento->nombre_ciudad;

                          if($oEstablecimiento->nombre_zona)
                              $content .= ' / '.$oEstablecimiento->nombre_zona;

                          if($oEstablecimiento->nombre_parroquia)
                              $content .= ' / '.$oEstablecimiento->nombre_parroquia;

                          if($oEstablecimiento->nombre_barrio)
                              $content .= ' / '.$oEstablecimiento->nombre_barrio;

                      $content .= '</p>';
                      $content .= '<p>';
                        $content .= '<strong>En Ruta:</strong> '.$enRuta;
                      $content .= '</p>';
                      if(is_object($estaEnRuta)){
                          if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/quitar-establecimiento')){
                            $content .= '<p>';
                            $content .= Form::open(['route' => ['configuracion.proyectos-por-cliente.establecimientos.delete', $oRuta->id, $establecimiento->id], 'class' => 'form-horizontal form-label-left', 'method' => 'delete']);
                              $content .= '<button type="submit" class="btn btn-xs btn-danger" title="Quitar establecimiento '.$oEstablecimiento->nombre.' de la ruta '.$oRuta->nombre.'"><i class="glyphicon glyphicon-trash"></i> Quitar de la ruta '.$oRuta->nombre.'</button>';
                            $content .= Form::close();
                            $content .= '</p>';
                          }
                      }else{
                          $content .= '<a href="'.route('configuracion.proyectos-por-cliente.establecimientos.store', [$oRuta->id, $establecimiento->id]).'" class="btn btn-xs btn-success" title="Agregar establecimiento '.$oEstablecimiento->nombre.' a la ruta '.$oRuta->nombre.'"><i class="glyphicon glyphicon-plus"></i> Agregar a la ruta '.$oRuta->nombre.'</a>';
                      }
                  $content .= '</div>';
              $content .= '</div>';
          @endphp
          var markerPosition_{!! $key !!} = new google.maps.LatLng({!! $coordenadas[0] !!}, {!! $coordenadas[1] !!});
          var marker_{!! $key !!} = new google.maps.Marker({
                                                position: markerPosition_{!! $key !!},
                                                title: "{!! $oEstablecimiento->nombre !!}",
                                                label: "",
                                                animation: '',
                                                icon: "{!! $icon !!}"
                                              });

          bounds.extend(marker_{!! $key !!}.position);
          marker_{!! $key !!}.setMap(map_0);
          markers.push(marker_{!! $key !!});
      
          var infowindow_{!! $key !!} = new google.maps.InfoWindow({
            content: "{!! addslashes($content) !!}"
          });

          infowindow_{!! $key !!}.setOptions({ maxWidth: 700 });

          google.maps.event.addListener(marker_{!! $key !!}, 'click', function() {
              infowindow_{!! $key !!}.open(map_0, marker_{!! $key !!});
          });

          infowindows.push(infowindow_{!! $key !!});
          @php
            $key++;
          }
          @endphp
        @endforeach
      @endif

      var idleListener = google.maps.event.addListenerOnce(map_0, "idle", function () {
                                                              map_0.setZoom(12);
                                                              map_0.fitBounds(bounds);
                                                          });
      var map = map_0;
      
      maps.push({
        key: 0,
        markers: markers,
        infowindows: infowindows,
        map: map_0,
        shapes: shapes
      });
    }
  </script>

@endsection
