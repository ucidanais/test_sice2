@extends('layouts.sice')

@section('customcss')
  <link href="{!! asset('resources/js/datatables/jquery.dataTables.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/buttons.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/fixedHeader.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/responsive.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
  <link href="{!! asset('resources/js/datatables/scroller.bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/agregar-ruta'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/rutas/agregar-ruta/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Ruta
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Rutas</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de rutas generadas para este proyecto</p>

                  @if(!$aRutas->isEmpty())
                    
                    <table id="rutas" class="table table-striped dt-responsive">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Colaborador asignado </th>
                          <th class="column-title">Proyecto </th>
                          <th class="column-title">Cliente </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($aRutas as $ruta)
                          <tr class="even pointer">
                            <td class=" ">{!! $ruta->nombre !!}</td>
                            <td class=" ">{!! $ruta->equipoTrabajo->nombreCompleto !!}</td>
                            <td class=" ">{!! $ruta->clienteProyecto->descripcion !!}</td>
                            <td class=" ">{!! $ruta->cliente->nombre !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/editar-ruta'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/rutas/editar-ruta/'.$oClienteProyecto->id.'/'.$ruta->id) !!}" class="btn btn-info btn-xs" title="Editar Ruta">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/eliminar-ruta'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/rutas/eliminar-ruta/'.$oClienteProyecto->id.'/'.$ruta->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Ruta">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos'))
                                  <a href="{!! route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list', [$ruta->id]) !!}" class="btn btn-primary btn-xs" title="Lista de Establecimientos">
                                    <i class="fa fa-tasks"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    
                  @else
                    <div class="alert alert-info">Aun no existen rutas para este proyecto en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
    <!-- Datatables-->
    <script src="{!! asset('resources/js/datatables/jquery.dataTables.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.bootstrap.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.buttons.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/jszip.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/pdfmake.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/vfs_fonts.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.html5.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/buttons.print.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.fixedHeader.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.keyTable.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.responsive.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/responsive.bootstrap.min.js') !!}"></script>
    <script src="{!! asset('resources/js/datatables/dataTables.scroller.min.js') !!}"></script>
    <script src="{!! asset('resources/js/pace/pace.min.js') !!}"></script>


    <script type="text/javascript">

      $('#rutas').dataTable();


    </script>
@endsection
