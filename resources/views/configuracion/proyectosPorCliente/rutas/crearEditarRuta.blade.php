@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/lista-de-rutas'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Rutas
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  @if($oRuta)
                    <h2>Editar Ruta</h2>
                  @else
                    <h2>Crear Ruta</h2>
                  @endif
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                  @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oRuta))
                    {!! Form::model($oRuta, ['action' => ['RutasController@update', $oRuta->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'configuracion/proyectos-por-cliente/rutas/agregar-ruta/'.$oClienteProyecto->id, 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif
                    {!! Form::hidden('cliente_proyecto_id', $oClienteProyecto->id) !!}
                    {!! Form::hidden('proyecto_id', $oProyecto->id) !!}
                    {!! Form::hidden('cliente_id', $oCliente->id) !!}

                    <p>Formulario de creacion de una nueva ruta para el proyecto "{!! $oProyecto->nombre !!}" del cliente "{!! $oCliente->nombre !!}"</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oClienteProyecto->descripcion !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Colaborador <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="equipo_trabajo_id" id="equipo_trabajo_id" class="form-control col-md-7 col-xs-12" required="required">
                          <option value="">Seleccione un Colaborador</option>
                          @if(!$aEquipoTrabajo->isEmpty())
                            @foreach($aEquipoTrabajo as $equipoTrabajo)
                              <option value="{!! $equipoTrabajo->id !!}" {!! $equipoTrabajoId == $equipoTrabajo->id ? 'selected="selected"' : '' !!}>{!! $equipoTrabajo->nombre_completo_cargo !!}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre de la ruta <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('nombre', old('nombre'), 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre de la ruta', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>
                    <hr />
                    
                    <div class="form-group">
                      <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                          Que días de la semana se visitará esta ruta
                      </label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <div class="checkbox">
                          @if(isset($oRuta))
                            {{ Form::checkbox('lunes',1,$oRuta->lunes == 1 ? true : false,array('class' => 'flat')) }} Lunes<br />
                            {{ Form::checkbox('martes',1,$oRuta->martes == 1 ? true : false,array('class' => 'flat')) }} Martes<br />
                            {{ Form::checkbox('miercoles',1,$oRuta->miercoles == 1 ? true : false,array('class' => 'flat')) }} Miercoles<br />
                            {{ Form::checkbox('jueves',1,$oRuta->jueves == 1 ? true : false,array('class' => 'flat')) }} Jueves<br />
                            {{ Form::checkbox('viernes',1,$oRuta->viernes == 1 ? true : false,array('class' => 'flat')) }} Viernes<br />
                            {{ Form::checkbox('sabado',1,$oRuta->sabado == 1 ? true : false,array('class' => 'flat')) }} Sabado<br />
                            {{ Form::checkbox('domingo',1,$oRuta->domingo == 1 ? true : false,array('class' => 'flat')) }} Domingo
                          @else
                            {{ Form::checkbox('lunes',1,false,array('class' => 'flat')) }} Lunes<br />
                            {{ Form::checkbox('martes',1,false,array('class' => 'flat')) }} Martes<br />
                            {{ Form::checkbox('miercoles',1,false,array('class' => 'flat')) }} Miercoles<br />
                            {{ Form::checkbox('jueves',1,false,array('class' => 'flat')) }} Jueves<br />
                            {{ Form::checkbox('viernes',1,false,array('class' => 'flat')) }} Viernes<br />
                            {{ Form::checkbox('sabado',1,false,array('class' => 'flat')) }} Sabado<br />
                            {{ Form::checkbox('domingo',1,false,array('class' => 'flat')) }} Domingo
                          @endif
                        </div>
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Ruta', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
