@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/familias-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Familias de Producto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto/'.$oFamiliaProducto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Categoria de Producto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Categorias de Producto</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de categorias de producto generadas para este proyecto</p>

                  @if(!$aCategoriasProducto->isEmpty())
                    
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Familia </th>
                          <th class="column-title">Proyecto </th>
                          <th class="column-title">Cliente </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($aCategoriasProducto as $categoriaProducto)
                          <tr class="even pointer">
                            <td class=" ">{!! $categoriaProducto->nombre !!}</td>
                            <td class=" ">{!! $categoriaProducto->familiaProducto->nombre !!}</td>
                            <td class=" ">{!! $categoriaProducto->proyecto->nombre !!}</td>
                            <td class=" ">{!! $categoriaProducto->cliente->nombre !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto/'.$oFamiliaProducto->id.'/'.$categoriaProducto->id) !!}" class="btn btn-info btn-xs" title="Editar Categoria de Producto">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-categoria-de-producto'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/eliminar-categoria-de-producto/'.$oFamiliaProducto->id.'/'.$categoriaProducto->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Categoria de Producto">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$categoriaProducto->id) !!}" class="btn btn-primary btn-xs" title="Lista de Presentaciones de Producto">
                                    <i class="fa fa-tasks"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    
                  @else
                    <div class="alert alert-info">Aun no existen familias de producto para este proyecto en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
