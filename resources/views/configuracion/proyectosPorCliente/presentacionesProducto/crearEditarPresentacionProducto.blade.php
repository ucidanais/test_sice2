@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/familias-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Familias de Producto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/categorias-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Categorias de Producto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de  Presentaciones de Productos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  @if($oPresentacionProducto)
                    <h2>Editar Presentacion de Productos</h2>
                  @else
                    <h2>Crear Presentacion de Productos</h2>
                  @endif
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                  @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(is_object($oPresentacionProducto))
                    {!! Form::model($oPresentacionProducto, ['action' => ['PresentacionesProductosController@update', $oPresentacionProducto->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto/'.$oCategoriaProducto->id, 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif
                    {!! Form::hidden('categoria_producto_id', $oCategoriaProducto->id) !!}
                    {!! Form::hidden('familia_producto_id', $oFamiliaProducto->id) !!}
                    {!! Form::hidden('proyecto_id', $oProyecto->id) !!}
                    {!! Form::hidden('cliente_id', $oCliente->id) !!}

                    <p>Formulario de creacion de una nueva presentacion de productos para el proyecto "{!! $oProyecto->nombre !!}" del cliente "{!! $oCliente->nombre !!}"</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oFamiliaProducto->clienteProyecto->descripcion !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Familia de Producto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oFamiliaProducto->nombre !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Categoria de Producto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oCategoriaProducto->nombre !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="es_competencia">
                        Es un producto de la competencia?
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('es_competencia', [0 => 'NO', 1 => 'SI'], old('es_competencia'), ['id' => 'es_competencia', 'class' => 'form-control col-md-7 col-xs-12', 'required' => 'required', 'onchange' => 'javascript: muestraListaProductos();']) !!}
                      </div>
                    </div>

                    <div class="item form-group" id="listaProductosDiv">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="presentacion_producto_id">
                        Es competencia de:
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="presentacion_producto_id" id="presentacion_producto_id" class="form-control col-md-7 col-xs-12">
                              <option value="">Seleccione el Producto</option>
                              @if( ! $listaPresentacionesProducto->isEmpty())
                                @foreach($listaPresentacionesProducto as $oPresentacionProductoMarca)
                                  @if(is_object($oPresentacionProducto))
                                    @if($oPresentacionProducto->presentacion_producto_id == $oPresentacionProductoMarca->id)
                                      <option value="{!! $oPresentacionProductoMarca->id !!}" selected="selected">
                                        {!! $oPresentacionProductoMarca->categoriaProducto->familiaProducto->nombre !!} - {!! $oPresentacionProductoMarca->categoriaProducto->nombre !!} - {!! $oPresentacionProductoMarca->nombre !!}
                                      </option>
                                    @else
                                      <option value="{!! $oPresentacionProductoMarca->id !!}">
                                        {!! $oPresentacionProductoMarca->categoriaProducto->familiaProducto->nombre !!} - {!! $oPresentacionProductoMarca->categoriaProducto->nombre !!} - {!! $oPresentacionProductoMarca->nombre !!}
                                      </option>
                                    @endif
                                  @else
                                      <option value="{!! $oPresentacionProductoMarca->id !!}">
                                        {!! $oPresentacionProductoMarca->categoriaProducto->familiaProducto->nombre !!} - {!! $oPresentacionProductoMarca->categoriaProducto->nombre !!} - {!! $oPresentacionProductoMarca->nombre !!}
                                      </option>
                                  @endif
                                @endforeach
                              @endif
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('nombre', old('nombre'), 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre de la presentación de producto', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Precio Sugerido
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('precio', old('precio'), 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el precio de la presentacion de producto']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Contexto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('contexto', old('contexto'), 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el contexto de la presentacion de producto']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="grupos">Grupos de Producto</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="grupos[]" id="grupos" class="select2_multiple form-control col-md-7 col-xs-12" multiple="multiple" required="required">
                            @if( ! $listaGruposProducto->isEmpty())
                              @foreach($listaGruposProducto as $oGrupoProducto)
                                  @php
                                    $exists = is_object($oPresentacionProducto) ? $oPresentacionProducto->gruposProducto->contains($oGrupoProducto->id) : false;
                                  @endphp
                                  <option value="{{ $oGrupoProducto->id }}" {{ $exists ? 'selected="selected"' : '' }}>{{ $oGrupoProducto->nombre }}</option>
                              @endforeach
                            @endif
                        </select> 
                      </div>
                    </div>

                    <div class="x_title">
                      <h2>Cadenas</h2>
                      <div class="clearfix"></div>
                    </div>
                    <p>Seleccione la o las cadenas en las cuales se encuentra ésta presentación de producto.</p>

                    @if( ! $listaCadenas->isEmpty())
                      <?php
                        $aClasificaciones = explode(',', $oClienteProyecto->clasificaciones);
                      ?>
                      <div class="form-group">
                        @foreach($listaCadenas as $oCadena)
                          @if(is_array($aClasificaciones))
                            @foreach($aClasificaciones as $clasificacion)
                              <?php
                                $activo = false;
                                if(is_object($oPresentacionProducto)){
                                  $oProductoCadena = SICE\ProductoCadena::where('codigo_cadena', $oCadena->id)->where('clasificacion_establecimiento', $clasificacion)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->first();
                                  if(is_object($oProductoCadena)){
                                    $activo = true;
                                  }
                                }
                              ?>
                              <div class="checkbox">
                                <label>
                                  {{ Form::checkbox('cadenas[]', $oCadena->id.'|'.$clasificacion, $activo,array('class' => 'flat')) }} {{ $oCadena->descripcion }} ({{ $clasificacion }})
                                </label>
                              </div>
                            @endforeach
                          @else
                              <?php
                                $activo = false;
                                if(is_object($oPresentacionProducto)){
                                  $oProductoCadena = SICE\ProductoCadena::where('codigo_cadena', $oCadena->id)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->first();
                                  if(is_object($oProductoCadena)){
                                    $activo = true;
                                  }
                                }
                              ?>
                              <div class="checkbox">
                                <label>
                                  {{ Form::checkbox('cadenas[]',$oCadena->id,$activo,array('class' => 'flat')) }} {{ $oCadena->descripcion }}
                                </label>
                              </div>
                          @endif
                        @endforeach
                      </div>
                    @else
                      <div class="alert alert-info">Aun no existen cadenas en el sistema.</div>
                    @endif


                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Presentacion de Producto', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
  <script type="text/javascript">

    $(".select2_multiple").select2({
        placeholder: "Seleccione los grupos de producto",
        allowClear: true
    });

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);

    $(document).ready(function(){
        muestraListaProductos();
    });


    function muestraListaProductos(){
        var esCompetencia = $('#es_competencia').val();
        $("#presentacion_producto_id").val("");
        if(esCompetencia == 1){
          $('#listaProductosDiv').show();
        }else{
          $('#listaProductosDiv').hide();
        }
    }
  </script>
@endsection
