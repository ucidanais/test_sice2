@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/familias-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oFamiliaProducto->cliente_proyecto_id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Familias de Producto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/categorias-de-productos'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Categorias de Producto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto/'.$oCategoriaProducto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Presentacion de Producto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Presentaciones de Producto</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de presentaciones de producto generadas para este proyecto</p>

                  @if(!$aPresentacionesProducto->isEmpty())
                    
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Categoria </th>
                          <th class="column-title">Familia </th>
                          <th class="column-title">Es de la Competencia? </th>
                          <th class="column-title">Competencia de </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($aPresentacionesProducto as $presentacionProducto)
                          <tr class="even pointer">
                            <td class=" ">{!! $presentacionProducto->nombre !!}</td>
                            <td class=" ">{!! $oCategoriaProducto->nombre !!}</td>
                            <td class=" ">{!! $oFamiliaProducto->nombre !!}</td>
                            <td class=" ">{!! $presentacionProducto->es_competencia == 1 ? 'SI' : 'NO' !!}</td>
                            <td class=" ">{!! $presentacionProducto->presentacion_producto_id ? $presentacionProducto->presentacionProductoPadre->nombre : '' !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto/'.$oCategoriaProducto->id.'/'.$presentacionProducto->id) !!}" class="btn btn-info btn-xs" title="Editar Presentacion de Producto">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-presentacion-de-producto'))
                                  <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/eliminar-presentacion-de-producto/'.$oCategoriaProducto->id.'/'.$presentacionProducto->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Presentacion de Producto">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    
                  @else
                    <div class="alert alert-info">Aun no existen familias de producto para este proyecto en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection