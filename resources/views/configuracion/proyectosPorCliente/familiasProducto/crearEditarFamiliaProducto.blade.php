@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Familia de Producto
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  @if($oFamiliaProducto)
                    <h2>Editar Familia de Productos</h2>
                  @else
                    <h2>Crear Familia de Productos</h2>
                  @endif
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                  @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oFamiliaProducto))
                    {!! Form::model($oFamiliaProducto, ['action' => ['FamiliasProductosController@update', $oFamiliaProducto->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto/'.$oClienteProyecto->id, 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif
                    {!! Form::hidden('cliente_proyecto_id', $oClienteProyecto->id) !!}
                    {!! Form::hidden('proyecto_id', $oProyecto->id) !!}
                    {!! Form::hidden('cliente_id', $oCliente->id) !!}

                    <p>Formulario de creacion de una nueva familia de productos para el proyecto "{!! $oProyecto->nombre !!}" del cliente "{!! $oCliente->nombre !!}"</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oClienteProyecto->descripcion !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('nombre', old('nombre'), 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre de la familia de producto', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Familia de Producto', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
