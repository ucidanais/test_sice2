@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Ver proyectos por cliente
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Proyectos por Cliente</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra el formulario para edicion del proyecto.</p>

                  @if($errors->has())
                    <div role="alert" class="alert alert-danger alert-dismissible fade in">
                      <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                        <span aria-hidden="true">×</span>
                      </button>
                      @foreach ($errors->all('<p>:message</p>') as $message)
                        {!! $message !!}
                      @endforeach
                    </div>
                  @endif

                  {!! Form::model($objClienteProyecto, ['action' => ['ClienteProyectosController@update', $objClienteProyecto->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}

                    {{ Form::hidden('id', $objClienteProyecto->id) }}
                    
                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Cliente <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $objCliente->nombre !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $objProyecto->nombre !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Descripcion <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('descripcion', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese la descripcion de este proyecto', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('estado', ['1' => 'Activo', '0' => 'Inactivo'], old('estado'), array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tomar Stock <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('tomar_stock', ['1' => 'SI', '0' => 'NO'], old('tomar_stock'), array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                      </div>
                    </div>


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Clasificación que tendrán los Establecimientos <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::textarea('clasificaciones', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese las diferentes clasificaciones que tendran los establecimientos en este proyecto separadas por una coma (,). Por Ejemplo:  AAA,AA,A,B,C', 'required' => 'required', 'id' => 'clasificaciones']) 
                        !!}
                      </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Proyecto', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
