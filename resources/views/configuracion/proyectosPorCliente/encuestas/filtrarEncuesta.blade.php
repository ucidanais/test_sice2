@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('resources/assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/agregar-encuesta'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/encuestas/lista-de-encuestas/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Lista de Encuestas
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Reporte de Encuestas</h2>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-12 col-sm-12 col-xs-12">

                      <h2>FILTROS</h2>

                      {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.encuestas.descarga', $oClienteProyecto->id, $oEncuesta->id], 'class' => 'form-horizontal style-form', 'method' => 'post']) !!}
                    

                    <div class="row">
                      
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <label for="tipoReporte">Tipo de Reporte</label>
                        <div class="form-group">
                          {!! Form::select('tipoReporte', ['fotos' => 'Fotos', 'excel' => 'Excel de Datos'], null, ['id' => 'tipoReporte', 'class' => 'form-control']) !!}
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <label for="desde">Desde</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerDesde'>
                              {!! Form::text('desde', null, ['class' => 'form-control', 'id' => 'desde', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <label for="hasta">Hasta</label>
                        <div class="form-group">
                          <div class='input-group date' id='datetimepickerHasta'>
                              {!! Form::text('hasta', null, ['class' => 'form-control', 'id' => 'hasta', 'placeholder' => 'aaaa-mm-dd']) !!}
                              <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                        </div>
                      </div>
                      
                      
                    </div>


                    <div class="row">
                      <div class="col-sm-6">
                          <button class="btn btn-primary" id="exportar" name="exportar" value="Exportar" type="submit" value="exportar"><i class="fa fa-file-pdf-o"></i> Exportar</button>
                      </div>
                    </div>
                  {!! Form::close() !!}

                      
                  </div>

                  

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
  <script src="{!! asset('resources/assets/moment/min/moment.min.js') !!}"></script>
  <script src="{!! asset('resources/assets/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2({
          placeholder: "Seleccione el tipo de reporte",
          allowClear: true
        });

        $(".select2_multiple").select2({
          placeholder: "Por favor seleccione los filtros",
          allowClear: true
        });

        $('#datetimepickerDesde').datetimepicker({
          format: 'YYYY-MM-DD'
        });
    
        $('#datetimepickerHasta').datetimepicker({
          format: 'YYYY-MM-DD',
          useCurrent: false
        });
        
        $("#datetimepickerDesde").on("dp.change", function(e) {
            $('#datetimepickerHasta').data("DateTimePicker").minDate(e.date);
        });
        
        $("#datetimepickerHasta").on("dp.change", function(e) {
            $('#datetimepickerDesde').data("DateTimePicker").maxDate(e.date);
        });
    });


  </script>

@endsection
