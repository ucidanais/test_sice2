<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <title>{{ $oEncuesta->nombre }}</title>
    </head>
  
    <body>
        <h1>{{ $oEncuesta->nombre }}</h1>
        <h2>{!! $oProyectoCliente->descripcion !!}</h2>
        <p>Se imprimen resultados de {!! $reporte->count() !!} fotos en este reporte</p>
        <p>
            <strong>Se muestran locales de acuerdo a:</strong><br /><br />
            <strong>Desde el: </strong> {!! $filtros['desde'] !!}<br />
            <strong>Hasta el: </strong> {!! $filtros['hasta'] !!}<br />
        </p>
        <table cellspacing="5" cellpadding="5">
            @if( ! $reporte->isEmpty())
                @foreach($reporte as $oEncuestaFoto)
                    <tr>
                        <td>
                            <h3>
                                {!! $oEncuestaFoto->establecimiento_id ? $oEncuestaFoto->establecimiento->nombre : '' !!}
                            </h3>
                            <p> 
                                <strong> Provincia: </strong>{!! $oEncuestaFoto->provincia_id ? $oEncuestaFoto->provincia->descripcion : '' !!}<br />
                                <strong> Ciudad: </strong>{!! $oEncuestaFoto->ciudad_id ? $oEncuestaFoto->ciudad->descripcion : '' !!}<br />
                                <strong> Canal: </strong>{!! $oEncuestaFoto->canal_id ? $oEncuestaFoto->canal->descripcion : '' !!}<br />
                                <strong> Subcanal: </strong>{!! $oEncuestaFoto->subcanal_id ? $oEncuestaFoto->subcanal->descripcion : '' !!}<br />
                                <strong> Cadena: </strong>{!! $oEncuestaFoto->cadena_id ? $oEncuestaFoto->cadenaObject->descripcion : '' !!}<br />
                                <strong> Fecha: </strong>{!! TimeFormat::dateShortFormat($oEncuestaFoto->created_at) !!}<br />
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <img style="width: 300px;" src="{!! storage_path($oEncuestaFoto->foto) !!}" alt="image" />
                            <br /><br />
                            <strong>Descripcion: </strong>{!! $oEncuestaFoto->descripcion !!}<br />
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </body>
</html>