@extends('layouts.sice')

@section('customcss')
    <link href="{!! asset('resources/css/select/select2.min.css') !!}" rel="stylesheet">
@endsection

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-eye"></i> Detalle de Proyecto
                    </a>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/lista-de-encuestas'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/encuestas/lista-de-encuestas/'.$oClienteProyecto->id) !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Encuestas
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  @if($oEncuesta)
                    <h2>Editar Encuesta</h2>
                  @else
                    <h2>Crear Encuesta</h2>
                  @endif
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">
                  @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oEncuesta))
                    {!! Form::model($oEncuesta, ['route' => ['configuracion.proyectos-por-cliente.encuestas.update', $oClienteProyecto->id, $oEncuesta->id], 'method' => 'PUT', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['route' => ['configuracion.proyectos-por-cliente.encuestas.store', $oClienteProyecto->id], 'class' => 'form-horizontal form-label-left', 'method' => 'POST']) !!}
                  @endif
                    {!! Form::hidden('cliente_proyecto_id', $oClienteProyecto->id) !!}
                    {!! Form::hidden('proyecto_id', $oProyecto->id) !!}
                    {!! Form::hidden('cliente_id', $oCliente->id) !!}

                    <p>Formulario de creacion de una nueva encuesta para el proyecto "{!! $oProyecto->nombre !!}" del cliente "{!! $oCliente->nombre !!}"</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Proyecto
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! $oClienteProyecto->descripcion !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tipo de encuesta <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('tipo', ['N' => 'Mercaderistas', 'S' => 'Supervisores'], old('tipo'), ["class" => "select2 form-control col-md-7 col-xs-12 required", 'placeholder'=>'Seleccione el tipo de encuesta', 'required' => 'required', 'id' => 'tipo', 'onchange' => 'javascript: mostrarOcultarCadenas(); ']) !!}
                      </div>
                    </div>

                    <div class="item form-group" id="cadenasDiv">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cadenas">Cadenas</label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="cadenas[]" id="cadenas" class="select2_multiple form-control col-md-7 col-xs-12" multiple="multiple">
                            @if( ! $aCadenas->isEmpty())
                              @foreach($aCadenas as $oCadena)
                                  @php
                                    $exists = is_object($oEncuesta) ? $oEncuesta->cadenas->contains($oCadena->id) : false;
                                  @endphp
                                  <option value="{{ $oCadena->id }}" {{ $exists ? 'selected="selected"' : '' }}>{{ $oCadena->descripcion }}</option>
                              @endforeach
                            @endif
                        </select> 
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre de la encuesta <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('nombre', old('nombre'), ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre de la encuesta', 'required' => 'required']) !!}
                      </div>
                    </div>
                    <hr />


                    <h4>PREGUNTAS</h4>
                    @php
                      $nextPregunta = 0;
                    @endphp
                    <div id="listaPreguntasDiv"> 
                      @if(is_object($oEncuesta))
                        @php
                          $aPreguntas = $oEncuesta->preguntas()->orderBy('orden', 'asc')->get();
                        @endphp

                        @if( ! $aPreguntas->isEmpty())
                          @foreach($aPreguntas as $oPregunta)
                            @php
                              $nextPregunta++; 
                            @endphp
                            
                            <div id="preguntaDiv_{{ $nextPregunta }}">
                              {{ Form::hidden('pregunta_id[]', $oPregunta->id) }}
                              <div class="row" class="item form-group">
                                <div class="col-md-12">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orden">
                                    Orden de la Pregunta: <span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" name="orden[]" id="orden_{{ $nextPregunta }}" class="form-control col-md-7 col-xs-12" placeholder="Ingrese el orden en el que debe aparecer la pregunta" required="required" value="{{ $oPregunta->orden }}" />
                                  </div>
                                </div>
                              </div>
                              <hr />

                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pregunta">
                                  Texto de la Pregunta <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input type="text" name="pregunta[]" id="pregunta_{{ $nextPregunta }}" class="form-control col-md-7 col-xs-12" placeholder="Ingrese el texto de la pregunta" required="required" value="{{ $oPregunta->pregunta }}" />
                                </div>
                              </div>
                              <hr />

                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo_dato">
                                  Tipo de Pregunta <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <select name="tipo_dato[]" id="tipo_dato_{{ $nextPregunta }}" class="form-control col-md-7 col-xs-12" required="required">
                                    <option value="">Seleccione el tipo de pregunta</option>
                                    <option value="I" {{ $oPregunta->tipo_dato == 'I' ? 'selected="selected"' : '' }}>Número Entero</option>
                                    <option value="F" {{ $oPregunta->tipo_dato == 'F' ? 'selected="selected"' : '' }}>Número Decimal</option>
                                    <option value="P" {{ $oPregunta->tipo_dato == 'P' ? 'selected="selected"' : '' }}>Porcentaje</option>
                                    <option value="M" {{ $oPregunta->tipo_dato == 'M' ? 'selected="selected"' : '' }}>Moneda</option>
                                    <option value="T" {{ $oPregunta->tipo_dato == 'T' ? 'selected="selected"' : '' }}>Texto abierto</option>
                                    <option value="O" {{ $oPregunta->tipo_dato == 'O' ? 'selected="selected"' : '' }}>Opción Múltiple (única respuesta)</option>
                                    <option value="OM" {{ $oPregunta->tipo_dato == 'OM' ? 'selected="selected"' : '' }}>Opción Múltiple (más de 1 respuesta)</option>
                                  </select>
                                </div>
                              </div>
                              <hr />

                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="opciones">
                                  Opciones
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <textarea name="opciones[]" id="opciones_{{ $nextPregunta }}" class="form-control col-md-7 col-xs-12" placeholder="Ingrese las opciones separadas por una coma (,)">{{ implode(',',$oPregunta->opciones )}}</textarea>
                                </div>
                              </div>
                              <hr />

                              <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                  <a onclick="javascript: deletePregunta('{{ route('configuracion.proyectos-por-cliente.encuestas.delete-pregunta', [$oPregunta->id]) }}', {{ $nextPregunta }});" title="Eliminar Pregunta" class="btn btn-sm btn-danger">
                                    <i class="fa fa-trash"></i>
                                  </a> 
                                </div>
                              </div>

                            </div>
                            <div class="ln_solid"></div>

                          @endforeach
                        @endif

                      @endif
                    </div>

                    
                    <div class="form-group">
                      <div class="col-md-6">
                        <a onclick="javascript: addPregunta();" class="btn btn-success">
                          <i class="fa fa-plus"></i> Agregar Pregunta
                        </a> 
                      </div>
                    </div>

                    
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Encuesta', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection


@section('customjs')
  <script type="text/javascript">
    $(".select2_multiple").select2({
        placeholder: "Seleccione las cadenas",
        allowClear: true
    });

    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    mostrarOcultarCadenas();

    function mostrarOcultarCadenas(){
       var tipo = $('#tipo').val();

       if(tipo == 'N'){
          $('#cadenasDiv').show();
          $('#cadenas').removeAttr('disabled');
       }else if(tipo == 'S'){
          $('#cadenasDiv').hide();
          $('#cadenas').val('');
          $('#cadenas').attr('disabled', 'disabled');
       }
    }

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);

    var nextPregunta = {{ $nextPregunta }};

    function addPregunta(){
        nextPregunta++;
        var row = '<div id="preguntaDiv_'+nextPregunta+'"><div class="row" class="item form-group"><div class="col-md-12"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="orden">Orden de la Pregunta: <span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12"><input type="number" name="orden[]" id="orden_'+nextPregunta+'" class="form-control col-md-7 col-xs-12" placeholder="Ingrese el orden en el que debe aparecer la pregunta" required="required" value="'+nextPregunta+'" /></div></div></div><hr /><div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="pregunta">Texto de la Pregunta <span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12"><input type="text" name="pregunta[]" id="pregunta_'+nextPregunta+'" class="form-control col-md-7 col-xs-12" placeholder="Ingrese el texto de la pregunta" required="required" value="" /></div></div><hr /><div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo_dato">Tipo de Pregunta <span class="required">*</span></label><div class="col-md-6 col-sm-6 col-xs-12"><select name="tipo_dato[]" id="tipo_dato_'+nextPregunta+'" class="form-control col-md-7 col-xs-12" required="required"><option value="" selected="selected">Seleccione el tipo de pregunta</option><option value="I">Número Entero</option><option value="F">Número Decimal</option><option value="P">Porcentaje</option><option value="M">Moneda</option><option value="T">Texto abierto</option><option value="O">Opción Múltiple (única respuesta)</option><option value="OM">Opción Múltiple (más de 1 respuesta)</option></select></div></div><hr /><div class="item form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="opciones">Opciones</label><div class="col-md-6 col-sm-6 col-xs-12"><textarea name="opciones[]" id="opciones_'+nextPregunta+'" class="form-control col-md-7 col-xs-12" placeholder="Ingrese las opciones separadas por una coma (,)"></textarea></div></div><hr /><div class="form-group"><div class="col-md-6 col-md-offset-3"><a onclick="javascript: $(\'#preguntaDiv_'+nextPregunta+'\').remove();" title="Eliminar Pregunta" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Eliminar Pregunta</a> </div></div></div><div class="ln_solid"></div>';
        $("#listaPreguntasDiv").append(row);
    }

    function deletePregunta(urlDeletePregunta, nextPregunta){
        $.ajax({
            type: 'GET',
            url: urlDeletePregunta,
            dataType: 'json',
            beforeSend: function(){
            },
            success: function (data) {
                $('#preguntaDiv_'+nextPregunta).remove();
                alert(data.message);
            },
            error: function (data) {
                alert('ERROR: Ups!. Parece que por el momento el sistema no esta disponible, por favor intentelo nuevamente luego de unos minutos');
            }
        });    
    }

  </script>
@endsection
