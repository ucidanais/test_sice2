@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Proyectos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            <div class="col-md-12">
              <div class="x_panel">
                <div class="x_title">
                  <h3><i class="fa fa-laptop"></i>  {!! $oClienteProyecto->descripcion !!}</h3>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <div class="col-md-9 col-sm-9 col-xs-12">


                    <div class="row">
                      
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.equipos-trabajo.list', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-group"></i>
                            </div>
                            <h3 class="top75">Equipo</h3>
                            <p>Equipo de trabajo para el proyecto.</p>
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.grupos-de-productos.list', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-cubes"></i>
                            </div>
                            <h3 class="top75">G. Productos</h3>
                            <p>Administración de Grupos de productos.</p>
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.familias-de-productos.list', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-cubes"></i>
                            </div>
                            <h3 class="top75">Productos</h3>
                            <p>Administración de Portafolio de productos.</p>
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.rutas.list', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-road"></i>
                            </div>
                            <h3 class="top75">Rutas</h3>
                            <p>Administración de Rutas del proyecto.</p>
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-building"></i>
                            </div>
                            <h3 class="top75">PDV</h3>
                            <p>Administración de Pisos de venta del proyecto.</p>
                          </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 cursorPointer" onclick="javascript: location.href = '{!! route('configuracion.proyectos-por-cliente.encuestas.list', [$oClienteProyecto->id]) !!}'">
                          <div class="tile-stats">
                            <div class="icon">
                              <i class="fa fa-check"></i>
                            </div>
                            <h3 class="top75">Encuestas</h3>
                            <p>Administración de Encuestas del proyecto.</p>
                          </div>
                        </div>
                      
                    </div>
                    <hr />

                    <div id="mainb"></div>

                    <div>
                      
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2><i class="fa fa-group"></i> Equipo de Trabajo </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">


                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_lider" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Líder</a></li>
                                <li role="presentation" class=""><a href="#tab_empleados" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Empleados</a></li>
                                <li role="presentation" class=""><a href="#tab_clientes" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Clientes</a></li>
                              </ul>
                              <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_lider" aria-labelledby="home-tab">
                                    @if($oLiderProyecto)
                                        <strong>{!! $oLiderProyecto->nombre !!} {!! $oLiderProyecto->apellido !!}</strong>
                                    @else
                                        <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                            <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                            No se ha asignado un líder de proyecto aún
                                        </div>
                                    @endif
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_empleados" aria-labelledby="profile-tab">
                                  @if(!$aEmpleados->isEmpty())
                                        <table class="table table-striped">
                                            <thead>
                                              <tr>
                                                <th>Nombre</th>
                                                <th>Reporta a</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($aEmpleados as $equipo)
                                              <tr>
                                                <td>
                                                  @if($equipo->lider_proyecto)
                                                    <strong>
                                                      {!! $equipo->nombre !!} {!! $equipo->apellido !!} <br />
                                                      <small>{!! $equipo->cargo !!}</small>
                                                    </strong>
                                                  @else
                                                    {!! $equipo->nombre !!} {!! $equipo->apellido !!} <br />
                                                    <small>{!! $equipo->cargo !!}</small>
                                                  @endif
                                                </td>
                                                <td>
                                                    @if($equipo->usuario_reporta_id)
                                                        {!! $equipo->usuarioReporta->nombre !!} {!! $equipo->usuarioReporta->apellido !!}
                                                    @else
                                                        &nbsp;
                                                    @endif
                                                </td>
                                              </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                  @else
                                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                        <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                        No se han asignado usuarios al equipo de trabajo
                                    </div>
                                  @endif
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_clientes" aria-labelledby="profile-tab">
                                  @if(!$aClientes->isEmpty())
                                    <table class="table table-striped">
                                        <thead>
                                          <tr>
                                            <th>Nombre</th>
                                            <th>Cargo</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($aClientes as $equipo)
                                          <tr>
                                            <td>{!! $equipo->nombre !!} {!! $equipo->apellido !!}</td>
                                            <td>{!! $equipo->cargo !!}</td>
                                          </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                  @else
                                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                        <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                        No se han asignado usuarios al equipo de trabajo
                                    </div>
                                  @endif
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2><i class="fa fa-cubes"></i> Productos </h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab1" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_familias" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Familias</a></li>
                                <li role="presentation" class=""><a href="#tab_grupos" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Grupos</a></li>
                              </ul>
                              <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_familias" aria-labelledby="home-tab">
                                  @if(!$aFamiliasProductos->isEmpty())
                                    <table class="table table-striped">
                                        <thead>
                                          <tr>
                                            <th>Nombre</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($aFamiliasProductos as $familiaProducto)
                                          <tr>
                                            <td>{!! $familiaProducto->nombre !!}</td>
                                          </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                  @else
                                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                        <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                        Aún no se han asignado productos del portafolio del cliente a este proyecto
                                    </div>
                                  @endif
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_grupos" aria-labelledby="profile-tab">
                                  @if(!$aGruposProductos->isEmpty())
                                    <table class="table table-striped">
                                        <thead>
                                          <tr>
                                            <th>Nombre</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($aGruposProductos as $grupoProducto)
                                          <tr>
                                            <td>{!! $grupoProducto->nombre !!}</td>
                                          </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                  @else
                                    <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                        <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                        Aún no se han generado los grupos de productos para este proyecto
                                    </div>
                                  @endif
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2><i class="fa fa-road"></i> Rutas </h2>
                            <div class="clearfix"></div>
                          </div>
                            @if(!$aRutas->isEmpty())
                              <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th>Detalle</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  @foreach($aRutas as $ruta)
                                    <tr>
                                      <td>
                                        <strong>{!! $ruta->nombre !!}</strong><br />
                                        <strong>Usuario Asignado: </strong>{!! $ruta->EquipoTrabajo->nombre !!} {!! $ruta->EquipoTrabajo->apellido !!}<br />
                                        <strong>Cliente: </strong>{!! $ruta->cliente->nonbre !!}
                                        <strong>Proyecto: </strong>{!! $ruta->proyecto->nonbre !!}
                                      </td>
                                    </tr>
                                  @endforeach
                                  </tbody>
                              </table>
                            @else
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                                Aún no se han generado las rutas para este proyecto
                            </div>
                            @endif
                        </div>
                      </div>


                  </div>

                  <!-- start project-detail sidebar -->
                  <div class="col-md-3 col-sm-3 col-xs-12">

                    <section class="panel">

                      <div class="x_title">
                        <h3> Resumen</h3>
                        <div class="clearfix"></div>
                      </div>
                      <div class="panel-body">
                        <div class="project_detail">
                          <p class="title">CLIENTE</p>
                          <p>{!! $oClienteProyecto->cliente->nombre !!}</p>
                          
                          <p class="title">PROYECTO</p>
                          <p>{!! $oClienteProyecto->proyecto->nombre !!}</p>
                          
                          <p class="title">LIDER DE PROYECTO</p>
                          @if($oLiderProyecto)
                            {!! $oLiderProyecto->nombre !!} {!! $oLiderProyecto->apellido !!}
                          @else
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                              <button class="close" aria-label="Close" type="button" data-dismiss="alert"><span aria-hidden="true">×</span></button>
                              No se ha asignado un líder de proyecto aún
                            </div>
                          @endif
                        </div>
                          
                      </div>

                    </section>

                  </div>
                  <!-- end project-detail sidebar -->

                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
