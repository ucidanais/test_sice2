@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Configuracion de Proyecto por cliente</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-proyecto'))
                    <a href="{!! URL::to('configuracion/proyectos-por-cliente/listar-por-cliente/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Listar por Cliente
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Proyectos por Cliente</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  @if(!$listaProyectos->isEmpty())
                    <div align="center"> {!! $listaProyectos->render() !!}</div>
                    @foreach($listaProyectos as $proyecto)
                      <h1>{!! $proyecto->nombre !!}</h1>
                      <div class="title_right">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                          <div class="input-group">
                            @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/generar-proyecto'))
                              <a href="{!! URL::to('configuracion/proyectos-por-cliente/generar-proyecto/proyecto/'.$proyecto->id) !!}" class="btn btn-primary btn-sm">
                                <i class="glyphicon glyphicon-plus"></i> Generar nuevo Proyecto
                              </a>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      @if($proyecto->clientesProyecto()->count())
                        <table class="table table-striped responsive-utilities jambo_table bulk_action">
                          <thead>
                            <tr class="headings">
                              <th class="column-title">Cliente </th>
                              <th class="column-title">Descripcion </th>
                              <th class="column-title">Estado </th>
                              <th class="column-title">Se debe Tomar Stock </th>
                              <th class="column-title no-link last"><span class="nobr">Acciones</span>
                              </th>
                              <th class="bulk-actions" colspan="7">
                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($proyecto->clientesProyecto as $clienteProyecto)
                              @php
                                $verificaClienteProyecto = SICE\ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')->where('cliente_proyectos.id', $clienteProyecto->id)->where('equipo_trabajo.usuario_id', Auth::user()->id)->count();
                              @endphp
                              @if($verificaClienteProyecto || Auth::user()->tipo == 'S')
                                <tr {{ $clienteProyecto->estado == 0 ? 'class=danger' : '' }}>
                                  <td>{!! $clienteProyecto->cliente->nombre !!}</td>
                                  <td>{!! $clienteProyecto->descripcion !!}</td>
                                  <td>{!! $clienteProyecto->estado == 1 ? 'ACTIVO' : 'INACTIVO' !!}</td>
                                  <td>{!! $clienteProyecto->tomar_stock !!}</td>
                                  <td>
                                    @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/cambiar-estado'))
                                      @if($clienteProyecto->estado == 1)
                                        <a href="{!! URL::to('configuracion/proyectos-por-cliente/cambiar-estado/'.$clienteProyecto->id) !!}" class="btn btn-success btn-xs" title="Inhabilitar Proyecto">
                                          <i class="glyphicon glyphicon-ok"></i>
                                        </a>
                                      @else
                                        <a href="{!! URL::to('configuracion/proyectos-por-cliente/cambiar-estado/'.$clienteProyecto->id) !!}" class="btn btn-danger btn-xs" title="Habilitar Proyecto">
                                          <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                      @endif
                                    @endif
                                    @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/editar-proyecto-por-cliente'))
                                      <a href="{!! URL::to('configuracion/proyectos-por-cliente/editar-proyecto-por-cliente/'.$clienteProyecto->id) !!}" class="btn btn-info btn-xs" title="Editar Configuracion del proyecto">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                      </a>
                                    @endif
                                    @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente'))
                                      <a href="{!! URL::to('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente/'.$clienteProyecto->id) !!}" class="btn btn-primary btn-xs" title="Detalle de Proyecto">
                                        <i class="glyphicon glyphicon-search"></i>
                                      </a>
                                    @endif
                                  </td>
                                </tr>
                              @endif
                            @endforeach
                          </tbody>
                        </table>
                      @endif
                      <hr />
                    @endforeach
                    <div align="center"> {!! $listaProyectos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen clientes en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
