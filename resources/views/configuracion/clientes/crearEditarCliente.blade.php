@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Clientes</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('configuracion/clientes/lista-de-clientes'))
                    <a href="{!! URL::to('configuracion/clientes/lista-de-clientes/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Clientes
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($oCliente))
                      Editar Cliente
                    @else
                      Nuevo Cliente
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($oCliente))
                    {!! Form::model($oCliente, ['action' => ['ClientesController@update', $oCliente->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left', 'files' => true]) !!}
                  @else
                    {!! Form::open(['url' => 'configuracion/clientes/agregar-cliente', 'class' => 'form-horizontal form-label-left', 'method' => 'post', 'files' => true]) !!}
                  @endif

                    <p>Formulario de creacion de un nuevo cliente en el sistema</p>

                    <div class="">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active">
                            <a href="#tab_content1" id="datos-cliente-tab" role="tab" data-toggle="tab" aria-expanded="true">
                              Datos del Cliente
                            </a>
                          </li>
                          <li role="presentation" class="">
                            <a href="#tab_content2" role="tab" id="vigencia-licencia-tab" data-toggle="tab" aria-expanded="false">
                              Vigencia de Licencia
                            </a>
                          </li>
                          <li role="presentation" class="">
                            <a href="#tab_content3" role="tab" id="numero-usuarios-tab" data-toggle="tab" aria-expanded="false">
                              Numero de Usuarios
                            </a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="datos-cliente-tab">
                            
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Codigo <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('codigo', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el codigo del cliente', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('nombre', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del cliente', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Logotipo</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::file('logotipo', ['class' => 'form-control col-md-7 col-xs-12']) !!}
                              </div>
                            </div>

                            @if(isset($oCliente))
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  @if($oCliente->logotipo != '')
                                    <img src="{!! asset('resources/logosClientes/'.$oCliente->logotipo) !!}" width="200" />
                                  @endif
                                </div>
                              </div>
                            @endif

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::select('estado', ['1' => 'Activo', '0' => 'Inactivo'], old('estado'), array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                              </div>
                            </div>

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="vigencia-licencia-tab">
                            @if(isset($oLicencia))
                              <input type="hidden" name="idLicencia" value="{!! $oLicencia->id !!}" />
                            @endif
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Desde</label>
                              <div class="col-md-3 col-sm-3 col-xs-12">
                                {!! Form::text('desde', $desde, 
                                  ["class" => "form-control col-md-3 col-xs-12", 'placeholder'=>'Licencia valida desde...', 'id'=>'desde']) 
                                !!}
                              </div>
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Hasta</label>
                              <div class="col-md-3 col-sm-3 col-xs-12">
                                {!! Form::text('hasta', $hasta, 
                                  ["class" => "form-control col-md-3 col-xs-12", 'placeholder'=>'Licencia valida hasta...', 'id'=>'hasta']) 
                                !!}
                              </div>
                            </div>

                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="numero-usuarios-tab">
                            @if(isset($oNumUsuarios))
                              <input type="hidden" name="idNumUsuarios" value="{!! $oNumUsuarios->id !!}" />
                            @endif
                            
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Numero maximo de usuarios habilitados para el cliente <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('numero_usuarios', $numUsuarios, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el numero maximo de usuarios para este cliente', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Cliente', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')

  <script type="text/javascript">
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    

    $(document).ready(function(){

      var options = {
        startDate: moment(),
        minDate: moment(),
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: false,
        /*
        ranges: {
          'Hoy': [moment(), moment()],
          'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Ultimos 7 dias': [moment().subtract(6, 'days'), moment()],
          'Ultimos 30 dias': [moment().subtract(29, 'days'), moment()],
          'Este mes': [moment().startOf('month'), moment().endOf('month')],
          'Ultimo mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        calender_style: "picker_4",
        */
        //opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        dateFormat: "YYYY-MM-DD",
        locale: {
          applyLabel: 'Seleccionar',
          cancelLabel: 'Limpiar',
          fromLabel: 'Desde',
          toLabel: 'Hasta',
          //customRangeLabel: 'Personalizado',
          daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          firstDay: 1
        }
      };

      $('#desde').daterangepicker({
        startDate: moment(),
        minDate: moment(),
        singleDatePicker: true,
        format: 'YYYY-MM-DD',
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        locale: {
          daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          firstDay: 1
        }
      }, function(start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
      });

      $('#hasta').daterangepicker({
        startDate: moment(),
        minDate: moment(),
        singleDatePicker: true,
        format: 'YYYY-MM-DD',
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        locale: {
          daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          firstDay: 1
        }
      }, function(start, end, label) {
        //console.log(start.toISOString(), end.toISOString(), label);
      });

    });
  </script>
@endsection