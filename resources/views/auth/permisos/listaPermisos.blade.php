@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Permisos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('seguridades/permisos/agregar-permiso'))
                    <a href="{!! URL::to('seguridades/permisos/agregar-permiso/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Permiso
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Permisos <small>del sistema</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de permisos generados en el sistema</p>

                  @if(!$listaPermisos->isEmpty())
                    <div align="center"> {!! $listaPermisos->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Modulo </th>
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Path </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaPermisos as $permiso)
                          <tr class="even pointer">
                            <td class=" ">{!! $permiso->modulo->nombre !!}</td>
                            <td class=" ">{!! $permiso->nombre !!}</td>
                            <td class=" ">{!! $permiso->path !!}</td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('seguridades/permisos/editar-permiso'))
                                  <a href="{!! URL::to('seguridades/permisos/editar-permiso/'.$permiso->id) !!}" class="btn btn-info btn-xs" title="Editar Permiso">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('seguridades/permisos/eliminar-permiso'))
                                  <a href="{!! URL::to('seguridades/permisos/eliminar-permiso/'.$permiso->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Permiso">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaPermisos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen permisos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
