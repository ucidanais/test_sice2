@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Modulos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('seguridades/modulos/agregar-modulo'))
                    <a href="{!! URL::to('seguridades/modulos/agregar-modulo/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Modulo
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Modulos <small>del sistema</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de modulos generados en el sistema</p>

                  @if(!$listaModulos->isEmpty())
                    <div align="center"> {!! $listaModulos->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Tipo </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaModulos as $modulo)
                          <tr class="even pointer">
                            <td class=" ">{!! $modulo->nombre !!}</td>
                            <td class=" ">
                              @if($modulo->tipo == 'A')
                                <span class="label label-primary">Administrador</span>
                              @else
                                <span class="label label-default">Estándar</span>
                              @endif
                            </td>
                            <td class=" last">
                              <small>
                                @if(SICE\Usuario::verificaPermiso('seguridades/modulos/editar-modulo'))
                                  <a href="{!! URL::to('seguridades/modulos/editar-modulo/'.$modulo->id) !!}" class="btn btn-info btn-xs" title="Editar Modulo">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                  </a>
                                @endif
                                @if(SICE\Usuario::verificaPermiso('seguridades/modulos/eliminar-modulo'))
                                  <a href="{!! URL::to('seguridades/modulos/eliminar-modulo/'.$modulo->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Modulo">
                                    <i class="glyphicon glyphicon-trash"></i>
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaModulos->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen modulos en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection

@section('customjs')
  <script type="text/javascript">
    
  </script>
@endsection