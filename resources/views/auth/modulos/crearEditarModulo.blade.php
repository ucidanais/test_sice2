@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Modulos</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('seguridades/modulos/lista-de-modulos'))
                    <a href="{!! URL::to('seguridades/modulos/lista-de-modulos/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Modulos
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($objModulo))
                      Editar Modulo
                    @else
                      Nuevo Modulo
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($objModulo))
                    {!! Form::model($objModulo, ['action' => ['ModulosController@update', $objModulo->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left']) !!}
                  @else
                    {!! Form::open(['url' => 'seguridades/modulos/agregar-modulo', 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
                  @endif

                    <p>Formulario de creacion de un nuevo modulo en el sistema</p>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::text('nombre', null, 
                          ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del módulo', 'required' => 'required']) 
                        !!}
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tipo <span class="required">*</span>
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::select('tipo', ['A' => 'Administrador', 'E' => 'Estandar'], null, array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required', 'placeholder' => 'Seleccione el tipo de módulo')) !!}
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Modulo', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')
  <script type="text/javascript">
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
@endsection