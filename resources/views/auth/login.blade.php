@extends('layouts.login')

@section('content')

<div class="">

    <a class="hiddenanchor" id="toregister"></a>
    <a class="hiddenanchor" id="tologin"></a>

    <div id="wrapper">

      <div id="login" class="animate form">
          <center><div class="row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-10">

                  </div>
                  <div class="col-sm-1"></div>
              </div>
          </center>
        <section class="login_content">

            <img src="{!! asset('resources/images/logo_siceMe.png')!!}" alt="" style="width: 400px; padding:02px 70px 5px 5px">
          <form class="form-login" role="form" method="POST" action="{{ url('/login') }}">
          {!! csrf_field() !!}

            <h1>SICE :: Login</h1>
            @if (Session::has('mensajeLogin'))
              <div class="alert alert-danger">{{ Session::get('mensajeLogin') }}</div>
            @endif

            @if (Session::has('mensajePermisos'))
                <div class="alert alert-danger">{{ Session::get('mensajePermisos') }}</div>
            @endif
            <div>
              <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail" autofocus>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>
            <div>
              <input type="password" class="form-control" placeholder="Password" name="password">
              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div>
              <button type="submit" class="btn btn-default submit">Entrar</button>
              <a class="reset_pass" href="#">olvid&oacute; su clave?</a>
            </div>
            <div class="clearfix"></div>
            <div class="separator">
              <div class="clearfix"></div>
              <br />
              <div>
                <p>©{!! date('Y') !!} Todos los derechos reservados. Go Trade</p>
              </div>
            </div>
          </form>
          <!-- form -->
        </section>
        <!-- content -->
      </div>
    </div>
  </div>

@endsection