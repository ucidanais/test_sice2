@extends('layouts.sice')

@section('content')
  <div class="">

          <div class="page-title">
            <div class="title_left">
              <h3>Usuarios</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/agregar-usuario'))
                    <a href="{!! URL::to('seguridades/usuarios/agregar-usuario/') !!}" class="btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i> Agregar Usuario
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Lista de Usuarios <small>del sistema</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  {!! Form::open(['url' => 'seguridades/usuarios/lista-de-usuarios', 'class' => 'form-horizontal style-form']) !!}
                    <div class="col-sm-10">
                        {!! Form::text('valor_busqueda', null, ["class" => "form-control", 'placeholder'=>'Buscar por ID / Nombre / Apellido / Email']) !!}
                    </div>
                    <div class="col-sm-2">
                        {!! Form::submit('Buscar', ["class" => "btn btn-primary"]) !!}
                    </div>
                  {!! Form::close() !!}
                </div>

                <div class="x_content">

                  <p>A continuacion se muestra la lista de usuarios generados en el sistema</p>


                  @if(!$listaUsuarios->isEmpty() && (Auth::user()->tipo == 'S' || Auth::user()->tipo == 'A'))
                    <div align="center"> {!! $listaUsuarios->render() !!}</div>
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                      <thead>
                        <tr class="headings">
                          <th class="column-title"></th>
                          <th class="column-title">Nombre </th>
                          <th class="column-title">Email </th>
                          <th class="column-title">Tipo </th>
                          <th class="column-title">Estado </th>
                          <th class="column-title no-link last"><span class="nobr">Acciones</span>
                          </th>
                          <th class="bulk-actions" colspan="7">
                            <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($listaUsuarios as $usuario)
                          <tr class="even pointer">
                              <td class=" ">
                                  @if($usuario->avatar != '')
                                    <img src="{!! asset('resources/images/avatares/'.$usuario->avatar) !!}" width="25" />
                                  @else
                                    <img src="{!! asset('resources/images/avatar.png') !!}" width="25" />
                                  @endif
                              </td>
                            <td class=" ">{!! $usuario->nombre !!} {!! $usuario->apellido !!}</td>
                            <td class=" ">{!! $usuario->email !!}</td>
                            <td class=" ">
                              @if($usuario->tipo == 'S')
                                <span class="label label-success">Super Administrador</span>
                              @endif
                              @if($usuario->tipo == 'A')
                                <span class="label label-primary">Administrador</span>
                              @endif
                              @if($usuario->tipo == 'B')
                                <span class="label label-info">Back Office</span>
                              @endif
                              @if($usuario->tipo == 'E')
                                <span class="label label-warning">Usuario Estandar</span>
                              @endif
                            </td>
                            <td class=" ">
                              @if($usuario->estado == 1)
                                <span class="label label-success">Activo</span>
                              @else
                                <span class="label label-danger">Inactivo</span>
                              @endif
                            </td>
                            <td class=" last">
                              <small>
                                {{-- 
                                      En primer lugar se verifica si el usuario que se esta listando es el mismo que esta navegando en 
                                      el sistema, en cuyo caso se muestra unicamente el boton para llevar al usuario hacia la pagina
                                      donde puede editar su perfil.

                                      El super usuario es el usuario con ID = 1, este usuario podra cambiar la informacion o eliminar
                                      a cualquier usuario del sistema excepto a si mismo, en ese caso unicamente puede ingresar a su 
                                      perfil.

                                      Los usuarios Super Administradores no pueden ser eliminados o cambiar su informacion por otros
                                      usuarios Super Administradores, unicamente el super usuario puede hacerlo.

                                      Los usuarios Super Administradores unicamente puedes cambiar informacion o eliminar a usuarios 
                                      Administradores, Back Office y Estandar pero no con otros super administradores.

                                      Los usuarios Administradores no pueden cambiar informacion o eliminar a usuarios Super Administradores o Administradores, unicamente a usuarios Back Office y Estandar.

                                      Todas estas reglas se aplican en el siguiente bloque de codigo para mostrar o no los botones
                                      para editar o eliminar usuarios.
                                --}}
                                @if($usuario->id != Auth::user()->id) 
                                  @if(Auth::user()->id == 1)
                                    @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/cambiar-estado-usuario'))
                                      @if($usuario->estado == 1)
                                        <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-success btn-xs" title="Inhabilitar Usuario">
                                          <i class="glyphicon glyphicon-ok"></i>
                                        </a>
                                      @else
                                        <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Habilitar Usuario">
                                          <i class="glyphicon glyphicon-remove"></i>
                                        </a>
                                      @endif
                                    @endif
                                    @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/editar-usuario'))
                                      <a href="{!! URL::to('seguridades/usuarios/editar-usuario/'.$usuario->id) !!}" class="btn btn-info btn-xs" title="Editar Usuario">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                      </a>
                                    @endif
                                    @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/eliminar-usuario'))
                                      <a href="{!! URL::to('seguridades/usuarios/eliminar-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Usuario">
                                        <i class="glyphicon glyphicon-trash"></i>
                                      </a>
                                    @endif
                                  @else
                                    @if($usuario->tipo != 'S')
                                      @if($usuario->tipo == 'A')
                                        @if(Auth::user()->tipo == 'S')
                                          @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/cambiar-estado-usuario'))
                                            @if($usuario->estado == 1)
                                              <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-success btn-xs" title="Inhabilitar Usuario">
                                                <i class="glyphicon glyphicon-ok"></i>
                                              </a>
                                            @else
                                              <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Habilitar Usuario">
                                                <i class="glyphicon glyphicon-remove"></i>
                                              </a>
                                            @endif
                                          @endif
                                          @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/editar-usuario'))
                                            <a href="{!! URL::to('seguridades/usuarios/editar-usuario/'.$usuario->id) !!}" class="btn btn-info btn-xs" title="Editar Usuario">
                                              <i class="glyphicon glyphicon-pencil"></i>
                                            </a>
                                          @endif
                                          @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/eliminar-usuario'))
                                            <a href="{!! URL::to('seguridades/usuarios/eliminar-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Usuario">
                                              <i class="glyphicon glyphicon-trash"></i>
                                            </a>
                                          @endif
                                        @endif
                                      @else
                                        @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/cambiar-estado-usuario'))
                                          @if($usuario->estado == 1)
                                            <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-success btn-xs" title="Inhabilitar Usuario">
                                              <i class="glyphicon glyphicon-ok"></i>
                                            </a>
                                          @else
                                            <a href="{!! URL::to('seguridades/usuarios/cambiar-estado-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Habilitar Usuario">
                                              <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                          @endif
                                        @endif
                                        @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/editar-usuario'))
                                          <a href="{!! URL::to('seguridades/usuarios/editar-usuario/'.$usuario->id) !!}" class="btn btn-info btn-xs" title="Editar Usuario">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                          </a>
                                        @endif
                                        @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/eliminar-usuario'))
                                          <a href="{!! URL::to('seguridades/usuarios/eliminar-usuario/'.$usuario->id) !!}" class="btn btn-danger btn-xs" title="Eliminar Usuario">
                                            <i class="glyphicon glyphicon-trash"></i>
                                          </a>
                                        @endif
                                      @endif
                                    @endif
                                  @endif
                                @else
                                  <a href="{!! URL::to('mi-perfil') !!}" class="btn btn-info btn-xs" title="Editar Mis Datos">
                                    <i class="glyphicon glyphicon-pencil"></i> Mi Perfil
                                  </a>
                                @endif
                              </small>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <div align="center"> {!! $listaUsuarios->render() !!}</div>
                  @else
                    <div class="alert alert-info">Aun no existen usuarios en el sistema.</div>
                  @endif
                </div>
              </div>
            </div>
          </div>

        </div>
@endsection
