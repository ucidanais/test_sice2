@extends('layouts.sice')

@section('content')
  <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Usuarios</h3>
            </div>
            <div class="title_right">
              <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                  @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/lista-de-usuarios'))
                    <a href="{!! URL::to('seguridades/usuarios/lista-de-usuarios/') !!}" class="btn btn-primary btn-sm">
                      <i class="fa fa-tasks"></i> Lista de Usuarios
                    </a>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            @if (Session::has('message'))
              {!! Session::get('message') !!}
            @endif
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>
                    @if(isset($objUsuario))
                      Editar Usuario
                    @else
                      Nuevo Usuario
                    @endif
                  </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                @if($errors->has())
                  <div role="alert" class="alert alert-danger alert-dismissible fade in">
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                      <span aria-hidden="true">×</span>
                    </button>
                    @foreach ($errors->all('<p>:message</p>') as $message)
                      {!! $message !!}
                    @endforeach
                  </div>
                @endif

                  @if(isset($objUsuario))
                    {!! Form::model($objUsuario, ['action' => ['UsuariosController@update', $objUsuario->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left', 'files' => true]) !!}
                  @else
                    {!! Form::open(['url' => 'seguridades/usuarios/agregar-usuario', 'class' => 'form-horizontal form-label-left', 'method' => 'post', 'files' => true]) !!}
                  @endif

                    <p>Formulario de creacion de un nuevo usuario en el sistema</p>

                    <div class="">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="datos-usuario-tab" role="tab" data-toggle="tab" aria-expanded="true">Datos del Usuario</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="roles-tab" data-toggle="tab" aria-expanded="false">Roles</a>
                          </li>
                          
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="datos-usuario-tab">
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nombre <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('nombre', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el nombre del usuario', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Apellido <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::text('apellido', null, 
                                  ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el apellido del usuario', 'required' => 'required']) 
                                !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">E-mail <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                @if(!isset($objUsuario))
                                  {!! Form::text('email', null, 
                                    ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el email del usuario', 'required' => 'required']) 
                                  !!}
                                @else
                                  {!! $objUsuario->email !!}
                                @endif
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                Password 
                                @if(!isset($objUsuario))
                                  <span class="required">*</span>
                                @endif
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                @if(isset($objUsuario))
                                  {!! Form::password('password', 
                                    ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el password del usuario', 'data-validate-length' => '6,12']) 
                                  !!}
                                @else
                                  {!! Form::password('password', 
                                    ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Ingrese el password del usuario', 'required' => 'required', 'data-validate-length' => '6,12']) 
                                  !!}
                                @endif
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                Confirmar Password 
                                @if(!isset($objUsuario))
                                  <span class="required">*</span>
                                @endif
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                @if(isset($objUsuario))
                                  {!! Form::password('password_confirmation', 
                                    ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Confirme el password del usuario', 'data-validate-linked' => 'password']) 
                                  !!}
                                @else
                                  {!! Form::password('password_confirmation', 
                                    ["class" => "form-control col-md-7 col-xs-12", 'placeholder'=>'Confirme el password del usuario', 'required' => 'required', 'data-validate-linked' => 'password']) 
                                  !!}
                                @endif
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Estado <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::select('estado', ['1' => 'Activo', '0' => 'Inactivo'], old('estado'), array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tipo de Usuario <span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::select('tipo', ['E' => 'Usuario Estandar','B' => 'Back Office','A' => 'Administrador','S' => 'Super Administrador'], old('tipo'), array('class' => 'form-control col-md-7 col-xs-12', 'required' => 'required')) !!}
                              </div>
                            </div>
                              
                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="avatar">Avatar</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::file('avatar', ['class' => 'form-control col-md-7 col-xs-12']) !!}
                              </div>
                            </div>

                            @if(isset($oUsuario))
                              <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="avatar"></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  @if($oUsuario->avatar != '')
                                    <img src="{!! asset('resources/images/avatares/'.$oUsuario->avatar) !!}" width="100" />
                                  @else
                                    <img src="{!! asset('resources/images/avatar.png') !!}" width="100" />
                                  @endif
                                </div>
                              </div>
                            @endif


                          </div>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="roles-tab">
                            <br /><br />
                            @foreach($listaModulos as $modulo)
                              <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                  {{ $modulo->nombre }}
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                  @foreach($modulo->permisos as $permiso)
                                      <div class="checkbox">
                                        <label>
                                          @if(isset($objUsuario))
                                            @if($objUsuario->verificaPermiso($permiso->path, $objUsuario->id))
                                              {{ Form::checkbox('permisos[]',$permiso->id,true,array('class' => 'flat')) }} {{ $permiso->nombre }}
                                            @else
                                              {{ Form::checkbox('permisos[]',$permiso->id,false,array('class' => 'flat')) }} {{ $permiso->nombre }}
                                            @endif
                                          @else
                                            {{ Form::checkbox('permisos[]',$permiso->id,false,array('class' => 'flat')) }} {{ $permiso->nombre }}
                                          @endif
                                        </label>
                                      </div>
                                  @endforeach
                                </div>
                              </div>
                              <hr />
                            @endforeach
                          </div>
                        </div>
                      </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        {!! Form::submit('Guardar Usuario', ["class" => "btn btn-success"]) !!}
                      </div>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

@section('customjs')
  <script type="text/javascript">
    // initialize the validator function
    validator.message['date'] = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
      .on('blur', 'input[required], input.optional, select.required', validator.checkField)
      .on('change', 'select.required', validator.checkField)
      .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required')
      .on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

    // bind the validation to the form submit event
    //$('#send').click('submit');//.prop('disabled', true);

    $('form').submit(function(e) {
      e.preventDefault();
      var submit = true;
      // evaluate the form using generic validaing
      if (!validator.checkAll($(this))) {
        submit = false;
      }

      if (submit)
        this.submit();
      return false;
    });

    /* FOR DEMO ONLY */
    $('#vfields').change(function() {
      $('form').toggleClass('mode2');
    }).prop('checked', false);

    $('#alerts').change(function() {
      validator.defaults.alerts = (this.checked) ? false : true;
      if (this.checked)
        $('form .alert').remove();
    }).prop('checked', false);
  </script>
@endsection