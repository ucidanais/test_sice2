  
  <script src="{!! asset('resources/assets/bootstrap/dist/js/bootstrap.min.js') !!}"></script>

  <!-- gauge js -->
  
  <script src="{!! asset('resources/js/nicescroll/jquery.nicescroll.min.js') !!}"></script>
  <!-- icheck -->
  <script src="{!! asset('resources/js/icheck/icheck.min.js') !!}"></script>

  <!-- chart js -->
  <script src="{!! asset('resources/js/moment.min.js') !!}"></script>
  <script src="{!! asset('resources/js/chartjs/chart.min.js') !!}"></script>

  <!-- flot js -->
  <!--[if lte IE 8]><script type="text/javascript" src="{!! asset('resources/js/excanvas.min.js') !!}"></script><![endif]-->
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.pie.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.orderBars.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.time.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/date.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.spline.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.stack.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/curvedLines.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/flot/jquery.flot.resize.js') !!}"></script>

  <!-- worldmap -->
  <script type="text/javascript" src="{!! asset('resources/js/maps/jquery-jvectormap-2.0.3.min.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/maps/gdp-data.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/maps/jquery-jvectormap-world-mill-en.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('resources/js/maps/jquery-jvectormap-us-aea-en.js') !!}"></script>
  <!-- pace -->
  <script src="{!! asset('resources/js/pace/pace.min.js') !!}"></script>
  <!-- skycons -->
  <script src="{!! asset('resources/js/skycons/skycons.min.js') !!}"></script>

  <script src="{!! asset('resources/js/validator/validator.js') !!}"></script>
  
  <script src="{!! asset('resources/js/select/select2.full.js') !!}"></script>
  
  <script src="{!! asset('resources/js/custom.js') !!}"></script>