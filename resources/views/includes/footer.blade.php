<!-- footer content -->

        <footer>
          <div class="copyright-info">
            <p class="pull-right">SICE - Powered by <strong><a href="http://www.gotrade.com.ec" target="_blank">GO TRADE</a></strong>. ©{!! date('Y') !!} Todos los derechos reservados.  
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->