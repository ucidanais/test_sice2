      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;padding: 0px 0px 0px 40px">
            <a href="{!! route('home') !!}" class="site_title"><img src="{!! asset('resources/images/logo_sice.png')!!}" alt="" style="width: 50%"></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile" style="padding-bottom: 110px;">
            <div class="profile_pic">
              @if(Auth::user()->avatar)
                <img src="{!! asset('resources/images/avatares/'.Auth::user()->avatar) !!}" alt="{!! Auth::user()->nombre !!} {!! Auth::user()->apellido !!}" class="img-circle profile_img">
              @else
                <img src="{!! asset('resources/images/user.png') !!}" alt="{!! Auth::user()->nombre !!} {!! Auth::user()->apellido !!}" class="img-circle profile_img">
              @endif
            </div>
            <div class="profile_info">
              <span>Bienvenido,</span>
              <h2>{!! Auth::user()->nombre !!} {!! Auth::user()->apellido !!}</h2>
            </div>
          </div>
          <!-- /menu prile quick info -->

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>NAVEGACION</h3>
              <ul class="nav side-menu">
                <li><a href="{!! url('/') !!}"><i class="fa fa-home"></i> Home </a></li>
                <li><a href="{!! route('profile') !!}"><i class="fa fa-child"></i> Mi Perfil </a></li>
                <li><a href="{!! route('logout') !!}"><i class="fa fa-sign-out"></i> Salir </a></li>
              </ul>
            </div>

            @if(Auth::user()->tipo == 'S' || Auth::user()->tipo == 'A' || Auth::user()->tipo == 'B')
              <div class="menu_section">
                <h3>PARAMETRIZACION</h3>
                <ul class="nav side-menu">
                      @if(SICE\Usuario::verificaPermiso('catalogos/provincias/lista-de-provincias'))
                        <li>
                          <a href="{!! url('catalogos/provincias/lista-de-provincias') !!}">
                            <i class="fa fa-map-marker"></i> Provincias
                          </a>
                        </li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/ciudades/lista-de-ciudades'))
                        <li><a href="{!! url('catalogos/ciudades/lista-de-ciudades') !!}"><i class="fa fa-map-marker"></i> Ciudades</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/zonas/lista-de-zonas'))
                        <li><a href="{!! url('catalogos/zonas/lista-de-zonas') !!}"><i class="fa fa-map-marker"></i> Zonas</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/parroquias/lista-de-parroquias'))
                        <li><a href="{!! url('catalogos/parroquias/lista-de-parroquias') !!}"><i class="fa fa-map-marker"></i> Parroquias</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/barrios/lista-de-barrios'))
                        <li><a href="{!! url('catalogos/barrios/lista-de-barrios') !!}"><i class="fa fa-map-marker"></i> Barrios</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/cadenas/lista-de-cadenas'))
                        <li><a href="{!! url('catalogos/cadenas/lista-de-cadenas') !!}"><i class="fa fa-map-marker"></i> Cadenas</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/canales/lista-de-canales'))
                        <li><a href="{!! url('catalogos/canales/lista-de-canales') !!}"><i class="fa fa-map-marker"></i> Canales</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/subcanales/lista-de-subcanales'))
                        <li><a href="{!! url('catalogos/subcanales/lista-de-subcanales') !!}"><i class="fa fa-map-marker"></i> Subcanales</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio'))
                        <li><a href="{!! url('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio') !!}"><i class="fa fa-map-marker"></i> Tipos de Negocio</a></li>
                      @endif
                      @if(SICE\Usuario::verificaPermiso('catalogos/establecimientos/lista-de-establecimientos'))
                        <li><a href="{!! url('catalogos/establecimientos/lista-de-establecimientos') !!}"><i class="fa fa-building-o"></i> Establecimientos</a></li> 
                      @endif
                </ul>
              </div>

              <div class="menu_section">
                <h3>PROYECTOS</h3>
                <ul class="nav side-menu">
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos/lista-de-proyectos'))
                    <li><a href="{!! url('configuracion/proyectos/lista-de-proyectos') !!}"><i class="fa fa-bar-chart"></i> Proyectos</a></li>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/clientes/lista-de-clientes'))
                    <li><a href="{!! url('configuracion/clientes/lista-de-clientes') !!}"><i class="fa fa-briefcase"></i> Clientes</a></li>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente'))
                    <li>
                        <a href="{!! url('configuracion/proyectos-por-cliente/listar-por-cliente') !!}">
                          <i class="fa fa-check-square-o"></i> Listar Proyectos por Cliente
                        </a>
                    </li>
                  @endif
                  @if(SICE\Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-proyecto'))
                    <li>
                        <a href="{!! url('configuracion/proyectos-por-cliente/listar-por-proyecto') !!}">
                          <i class="fa fa-check-square-o"></i> Listar Clientes por Proyecto
                        </a>
                    </li>
                  @endif
                </ul>
              </div>
            @endif
            
            @if(Auth::user()->tipo == 'S' || Auth::user()->tipo == 'A')
              <div class="menu_section">
                <h3>Autenticacion / Autorizacion</h3>
                <ul class="nav side-menu">
                  @if(Auth::user()->tipo == 'S')
                    <li><a  href="{!! url('seguridades/modulos') !!}"><i class="fa fa-cubes"></i> Modulos</a></li>
                    <li><a  href="{!! url('seguridades/permisos') !!}"><i class="fa fa-lock"></i> Permisos</a></li>
                  @endif

                  @if(SICE\Usuario::verificaPermiso('seguridades/usuarios/lista-de-usuarios'))
                    <li><a  href="{!! url('seguridades/usuarios') !!}"><i class="fa fa-group"></i> Usuarios</a></li>
                  @endif
                </ul>
              </div>
            @else
              @if(SICE\Usuario::verificaCliente())
                @if(SICE\Usuario::verificaPermiso('clientes/usuarios/lista-de-usuarios'))
                  <div class="menu_section">
                    <h3>Autenticacion / Autorizacion</h3>
                    <ul class="nav side-menu">
                      <li>
                        <a href="{!! route('clientes.usuarios.list') !!}">
                            <i class="fa fa-group"></i> Usuarios
                        </a>
                      </li>
                    </ul>
                  </div>
                @endif
              @endif
            @endif

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Inicio" href="{!! route('home') !!}">
              <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Mi Perfil" href="{!! route('profile') !!}">
              <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Salir" href="{!! route('logout') !!}">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>