<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>SICE :: Login</title>

  <!-- Bootstrap core CSS -->

  <link href="{!! asset('resources/css/bootstrap.min.css') !!}" rel="stylesheet">

  <link href="{!! asset('resources/fonts/css/font-awesome.min.css') !!}" rel="stylesheet">
  <link href="{!! asset('resources/css/animate.min.css') !!}" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="{!! asset('resources/css/custom.css') !!}" rel="stylesheet">
  <link href="{!! asset('resources/css/icheck/flat/green.css') !!}" rel="stylesheet">


  <script type="text/javascript" src="{!! asset('resources/js/jquery.min.js') !!}"></script>


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

  @yield('content')

  @include('flashy::message')

</body>

</html>