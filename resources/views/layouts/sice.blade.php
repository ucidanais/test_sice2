<!DOCTYPE html>
<html lang="es">
  @include('includes.head')
  
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('includes.sidebar')
        @include('includes.top')
        <!-- page content -->
        <div class="right_col" role="main">
          @yield('content')
          @include('includes.footer')
        </div>
        <!-- /page content -->
      </div>
    </div>
    @include('includes.notificaciones')
    @include('includes.scripts')
    @yield('customjs')
    <!-- /datepicker -->
    <!-- /footer content -->
    @include('flashy::message')
  </body>
</html>
