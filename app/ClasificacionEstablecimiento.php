<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Clasificacion de Establecimientos por proyecto de cada cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
*/

class ClasificacionEstablecimiento extends Model
{

    use UuidModelTrait;

    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'clasificacion_establecimientos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'clasificacion',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'establecimiento_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // ClasificacionEstablecimiento __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // ClasificacionEstablecimiento __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente');
    }

    // ClasificacionEstablecimiento __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto');
    }

    // ClasificacionEstablecimiento __belongs_to__ Establecimiento
    public function establecimiento() {
        return $this->belongsTo('SICE\Establecimiento');
    }

    

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/

    public function scopeByClasificacion($sQuery, $sClasificacion){
        return $sQuery->whereClasificacion($sClasificacion);
    }

    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

}
