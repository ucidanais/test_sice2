<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de preguntas de las encuestas
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class Pregunta extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'preguntas';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'pregunta',
        'tipo_dato',
        /*
            I  (Numero Entero),
            F  (Numero Flotante),
            P  (Porcentaje),
            M  (Moneda),
            T  (Texto),
            O  (Seleccion de Opciones),
            OM (Seleccion Multiple)
            O2 (Seleccion Multiple con doble respuesta)
        */
        'opcion_multiple',
        'opciones',
        'orden',
        'ponderacion',
        'encuesta_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];
    protected $casts = [
        'opciones' => 'array',
    ];
    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Pregunta __belongs_to__ Encuesta
    public function encuesta() {
        return $this->belongsTo('SICE\Encuesta');
    }

    // Pregunta __has_many__ RespuestaEncuesta
    public function respuestaEncuestas() {
        return $this->hasMany('SICE\RespuestaEncuesta');
    }

    /**
     * Método que genera una estructura JSON para las opciones ingresadas para esta pregunta
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/05/20
     *
     * @param array opciones
     *
     */
    public function setOpcionesAttribute($opciones){
        $this->attributes['opciones'] = json_encode($opciones);
    }

    /**
     * Método que convierte la estructura JSON en un array para devolverlo al momento de obtener el registro desde el modelo
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/05/20
     *
     * @param string opciones
     *
     */
    public function getOpcionesAttribute($opciones){
        return json_decode($opciones);
    }
}
