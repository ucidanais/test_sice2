<?php

namespace SICE\Console\Commands;

use Illuminate\Console\Command;

use SICE\HojaRuta;
use SICE\DetalleRuta;

class RefreshVisitsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sice:refresh-visits';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que actualiza las visitas realizadas en una hoja de ruta de acuerdo a la informacion de detalle de ruta.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $aHojasRuta = HojaRuta::where('visitas_por_hacer', '>', 0)->get();
        if( ! $aHojasRuta->isEmpty()){
            foreach($aHojasRuta as $oHojaRuta){
                $oHojaRuta->visitas_realizadas = DetalleRuta::where('hoja_ruta_id', $oHojaRuta->id)->where('estado', 1)->count();
                $oHojaRuta->save();
                $this->comment('Visitas realizadas de la Hoja de Ruta '.$oHojaRuta->nombre.' actualizadas');
            }
        }else{
            $this->info('Nada que actualizar');
        }
    }
}
