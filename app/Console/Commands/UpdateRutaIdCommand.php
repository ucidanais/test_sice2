<?php

namespace SICE\Console\Commands;

use Illuminate\Console\Command;

use SICE\Ruta;
use SICE\HojaRuta;

class UpdateRutaIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sice:update-hoja-ruta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Coloca el foreign key ruta_id en todas las hojas de ruta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $aRutas = Ruta::whereNull('deleted_at')->get();
        if( ! $aRutas->isEmpty()){
            foreach($aRutas as $oRuta){
                HojaRuta::where('nombre', 'like', $oRuta->nombre.'%')->update(['ruta_id' => $oRuta->id]);
                $this->comment('Hojas de Ruta pertenecientes a la Ruta '.$oRuta->nombre.' actualizadas');
            }
        }
    }
}
