<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Equipos de trabajo para Proyectos por Cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class EquipoTrabajo extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'equipo_trabajo';

    protected $appends = array('nombre_completo', 'nombre_completo_cargo');

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'cargo',
        'nombre',
        'apellido',
        'tipo',
        'lider_proyecto',
        'usuario_id',
        'usuario_reporta_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // EquipoTrabajo __belongs_to__ Usuario
    public function usuario() {
        return $this->belongsTo('SICE\Usuario');
    }

    // EquipoTrabajo __belongs_to__ Usuario
    public function usuarioReporta() {
        return $this->belongsTo('SICE\Usuario','usuario_reporta_id');
    }

    // EquipoTrabajo __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // EquipoTrabajo __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // EquipoTrabajo __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // EquipoTrabajo __has_many__ Ruta
    public function rutas() {
        return $this->hasMany('SICE\Ruta','equipo_trabajo_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
        EJ:
        $aEquipoTrabajoPorCliente = EquipoTrabajo::empleado()->byCliente($idCliente)->get();
    **************************************************************************************************/
    public function scopeByCargo($sQuery, $sCargo){
        return $sQuery->whereCargo($sCargo);
    }

    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

    public function scopeEmpleado($sQuery){
        return $sQuery->whereTipo('E');
    }

    public function scopeCliente($sQuery){
        return $sQuery->whereTipo('C');
    }
    
    public function scopeLider($sQuery){
        return $sQuery->whereLiderProyecto(1);
    }


    public function getNombreCompletoAttribute(){
        return  $this->nombre." ".$this->apellido;
    }

    public function getNombreCompletoCargoAttribute(){
        return  $this->nombre." ".$this->apellido." / ".$this->cargo;
    }
}
