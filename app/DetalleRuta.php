<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Detalles de Hojas de Ruta en proyectos de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
* @Operacion
* @Gestion
* @Reportes
*/

class DetalleRuta extends Model
{

    use UuidModelTrait;
    
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'detalle_rutas';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'estado',
        'observaciones',
        'fecha_visita',
        'orden_visita',
        'hora_entrada',
        'hora_salida',
        'ubicacion',
        'establecimiento_id',
        'grupo_producto_id',
        'hoja_ruta_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/29
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // DetalleRuta __belongs_to__ HojaRuta
    public function hojaRuta() {
        return $this->belongsTo('SICE\HojaRuta', 'hoja_ruta_id');
    }

    // DetalleRuta __belongs_to__ Establecimiento
    public function establecimiento() {
        return $this->belongsTo('SICE\Establecimiento', 'establecimiento_id');
    }

    // DetalleRuta __belongs_to__ GrupoProducto
    public function grupoProducto() {
        return $this->belongsTo('SICE\GrupoProducto', 'grupo_producto_id');
    }

    // DetalleRuta __belongs_to__ ClienteProyecto
    public function clienteProyecto(){
        return $this->belongsTo('SICE\ClienteProyecto', 'cliente_proyecto_id');
    }

    // DetalleRuta __belongs_to__ Cliente
    public function cliente(){
        return $this->belongsTo('SICE\Cliente', 'cliente_id');
    }

    // DetalleRuta __belongs_to__ Proyecto
    public function proyecto(){
        return $this->belongsTo('SICE\Proyecto', 'proyecto_id');
    }

    // DetalleRuta __has_many__ Novedades
    public function novedades() {
        return $this->hasMany('SICE\Novedad', 'detalle_ruta_id');
    }

    // DetalleRuta __has_many__ InformacionDetalle
    public function informacionesDetalle() {
        return $this->hasMany('SICE\InformacionDetalle', 'detalle_ruta_id');
    }

    // DetalleRuta __has_many_through__ Foto
    public function fotos() {
        return $this->hasManyThrough('SICE\Novedad','SICE\Foto','detalle_ruta_id', 'novedad_id','id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/
    public function scopeVisitados($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeNoVisitados($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeDesde($sQuery, $dFecha){
        return $sQuery->where('created_at', '>=', $dFecha);   
    }

    public function scopeHasta($sQuery, $dFecha){
        return $sQuery->where('created_at', '<=', $dFecha);
    }
}
