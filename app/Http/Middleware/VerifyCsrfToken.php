<?php

namespace SICE\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    	'api/v1/login',
        'api/v1/catalogos/agregar-catalogo',
        'api/v1/catalogos/actualizar-catalogo/*',
        'api/v1/establecimientos/agregar-establecimiento',
        'api/v1/establecimientos/actualizar-establecimiento/*',
        'api/v1/rutas/generar-hoja-de-ruta',
        'api/v1/rutas/actualizar-detalle/*',
        'api/v1/novedades/agregar-novedad',
        'api/v1/novedades/actualizar-novedad/*',
        'api/v1/novedades/agregar-foto/*',
        'api/v1/novedades/eliminar-foto/*',
        'api/v1/msl/*',
        'api/v1/encuestas/guardar-encuesta',
        'api/v1/fotos/guardar',
        'api/v1/fotos/editar/*'
    ];
}
