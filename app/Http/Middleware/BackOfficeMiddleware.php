<?php

namespace SICE\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BackOfficeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->tipo != 'S' && Auth::user()->tipo != 'A' && Auth::user()->tipo != 'B') {
            return redirect()->guest('home');
        }
        return $next($request);
    }
}
