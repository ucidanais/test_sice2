<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class UsuarioUpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"     => "required",
            "apellido"   => "required",
            "password"   => "between:6,12|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/",
            "tipo"       => "",
            "estado"     => "required",
            "cliente_id" => "",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'    => 'Por favor ingrese el nombre del usuario',
            'apellido.required'  => 'Por favor ingrese el apellido del usuario',
            'password.between'   => 'La clave debe tener entre 6 y 12 caracteres',
            'password.confirmed' => 'La clave y su confirmación no coinciden',
            'password.regex'     => 'La clave debe tener al menos una letra mayúscula, una minúscula y un número',
            'estado.required'    => 'Por favor seleccione el estado del usuario'
        ];
    }
}
