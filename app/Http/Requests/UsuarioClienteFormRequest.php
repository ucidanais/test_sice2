<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class UsuarioClienteFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"     => "required",
            "apellido"   => "required",
            "email"      => "required|email|unique:usuarios,email",
            "password"   => "required|between:6,12|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/",
            "tipo"       => "",
            "estado"     => "required",
            "cliente_id" => "required",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required'    => 'Por favor ingrese el nombre del usuario',
            'apellido.required'  => 'Por favor ingrese el apellido del usuario',
            'email.required'     => 'Por favor ingrese una dirección de email para el usuario',
            'email.email'        => 'Por favor ingrese una dirección de email válida',
            'email.unique'       => 'El email que usted a ingresado ya existe en nuestra base de datos',
            'password.required'  => 'Por favor ingrese una clave de acceso',
            'password.between'   => 'La clave debe tener entre 6 y 12 caracteres',
            'password.confirmed' => 'La clave y su confirmación no coinciden',
            'password.regex'     => 'La clave debe tener al menos una letra mayúscula, una minúscula y un número',
            'estado.required'    => 'Por favor seleccione el estado del usuario',
            'cliente_id.required'    => 'Por favor seleccione el cliente al que pertenece el usuario',
        ];
    }
}
