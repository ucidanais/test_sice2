<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ClienteFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "codigo"=>"required",
            "nombre"=>"required",
            "logotipo"=>"image",
            "estado"=>"required",
        ];
    }

    public function messages()
    {
        return [
            'codigo.required' => 'Por favor ingrese el codigo del cliente',
            'nombre.required' => 'Por favor ingrese el nombre del cliente',
            'logotipo.image' => 'Por favor seleccione una imagen en formato JPG, PNG o GIF para colocarla como logotipo del cliente',
            'estado.required' => 'Por favor seleccione el estado del cliente',
        ];
    }
}
