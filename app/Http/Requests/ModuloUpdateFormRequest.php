<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ModuloUpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"=>"required|unique:modulos,nombre,".$this->iId,
            "tipo"=>"required",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre del módulo',
            'nombre.unique' => 'Ya existe un módulo con ese nombre',
            'tipo.required' => 'Por favor seleccione el tipo de módulo',
        ];
    }
}
