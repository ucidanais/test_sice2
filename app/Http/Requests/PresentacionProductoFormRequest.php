<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class PresentacionProductoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
            "precio" => "numeric",
            "contexto" => "",
            "familia_producto_id" => "",
            "categoria_producto_id" => "required",
            "cliente_id" => "required",
            "proyecto_id" => "required",
            "cadenas" => "",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre de la presentación del producto',
            'precio.numeric' => 'Por favor ingrese un valor numerico valido para el precio del producto',
            'categoria_producto_id.required' => 'Por favor seleccione una categoria de producto',
            'cliente_id.required' => 'Por favor seleccione un cliente',
            'proyecto_id.required' => 'Por favor seleccione un proyecto',
        ];
    }
}
