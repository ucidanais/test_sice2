<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class EncuestaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
            "cliente_proyecto_id" => "required|exists:cliente_proyectos,id,estado,1",
            "cliente_id" => "required|exists:clientes,id",
            "proyecto_id" => "required|exists:proyectos,id",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre de la encuesta',
            'cliente_proyecto_id.required' => 'Por favor seleccione un proyecto por cliente',
            'cliente_id.required' => 'Por favor seleccione un cliente',
            'proyecto_id.required' => 'Por favor seleccione un proyecto',
        ];
    }
}
