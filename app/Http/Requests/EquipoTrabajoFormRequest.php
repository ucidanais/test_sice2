<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class EquipoTrabajoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cargo" => "required",
            "nombre" => "required",
            "apellido" => "required",
            "tipo" => "required",
            "lider_proyecto" => "sometimes|required",
            "usuario_id" => "required",
            "usuario_reporta_id" => "",
            "cliente_proyecto_id" => "required",
            "cliente_id" => "required",
            "proyecto_id" => "required",
        ];
    }

    public function messages()
    {
        return [
            'cargo.required' => 'Por favor ingrese el cargo del colaborador',
            'nombre.required' => 'Por favor ingrese el nombre del colaborador',
            'apellido.required' => 'Por favor ingrese el apellido del colaborador',
            'tipo.required' => 'Por favor seleccione si el colaborador es parte de la nomina de la compania o del cliente',
            'lider_proyecto.required' => 'Por favor indique si el colaborador es o no el lider del proyecto',
            'usuario_id.required' => 'Por favor seleccione el usuario que formará parte de este equipo de trabajo',
            'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto configurado por cliente para el cual se asigna este colaborador',
            'cliente_id.required' => 'Por favor seleccione el cliente para el cual se esta configurando este equipo de trabajo',
            'proyecto_id.required' => 'Por favor seleccione el proyecto para el cual se esta configurando este equipo de trabajo',
        ];
    }
}
