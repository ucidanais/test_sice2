<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class EstablecimientoImportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "listaEstablecimientos" => "file|mimes:xls,xlsx,csv",
        ];
    }

    public function messages()
    {
        return [
            'listaEstablecimientos.file' => 'Hubo un error al momento de cargar el archivo',
            'listaEstablecimientos.mimes' => 'Unicamnte se admiten archivos de tipo XLS, XLSX o CSV',
        ];
    }
}
