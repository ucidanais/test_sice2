<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ZonaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "descripcion" => "required",
            "coordenadas_poligono" => "",
            "catalogo_id" => "required",
        ];
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'Por favor ingrese el nombre de la zona',
            'catalogo_id'          => 'Por favor seleccione la ciudad a la que pertenece esta zona',
        ];
    }
}
