<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class GrupoProductoCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre del grupo de productos',
        ];
    }
}
