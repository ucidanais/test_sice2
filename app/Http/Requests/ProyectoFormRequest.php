<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ProyectoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "codigo" => "required",
            "nombre" => "required",
            "estado" => "required",
        ];
    }

    public function messages()
    {
        return [
            'codigo.required' => 'Por favor ingrese el codigo del proyecto',
            'nombre.required' => 'Por favor ingrese el nombre del proyecto',
            'estado.required' => 'Por favor seleccione el estado del proyecto',
        ];
    }
}
