<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ClienteProyectoUpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "estado" => "required",
            "descripcion" => "required|unique:cliente_proyectos,descripcion,".$this->get('id'),
            "tomar_stock" => "required",
        ];
    }

    public function messages()
    {
        return [
            'estado.required' => 'Por favor seleccione el estado de este proyecto para el cliente',
            'descripcion.required' => 'Por favor ingrese la descripcion de este proyecto para el cliente seleccionado',
            'descripcion.unique' => 'Ya existe un proyecto configurado para un cliente con esa descripcion',
            'tomar_stock.required' => 'Por favor indique si para este proyecto se debe tomar el stock de los productos en los PDV',
        ];
    }
}
