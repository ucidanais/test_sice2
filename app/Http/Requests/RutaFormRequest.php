<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class RutaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
            "lunes" => "in:0,1",
            "martes" => "in:0,1",
            "miercoles" => "in:0,1",
            "jueves" => "in:0,1",
            "viernes" => "in:0,1",
            "sabado" => "in:0,1",
            "domingo" => "in:0,1",
            "cliente_proyecto_id" => "required",
            "cliente_id" => "required",
            "proyecto_id" => "required",
            "equipo_trabajo_id" => "required",
            "usuario_id" => "",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre de la ruta',
            'lunes.in' => 'Por favor seleccione SI(1) o NO(0)',
            'martes.in' => 'Por favor seleccione SI(1) o NO(0)',
            'miercoles.in' => 'Por favor seleccione SI(1) o NO(0)',
            'jueves.in' => 'Por favor seleccione SI(1) o NO(0)',
            'viernes.in' => 'Por favor seleccione SI(1) o NO(0)',
            'sabado.in' => 'Por favor seleccione SI(1) o NO(0)',
            'domingo.in' => 'Por favor seleccione SI(1) o NO(0)',
            'cliente_proyecto_id.required' => 'Por favor seleccione un proyecto por cliente',
            'cliente_id.required' => 'Por favor seleccione un cliente',
            'proyecto_id.required' => 'Por favor seleccione un proyecto',
            'equipo_trabajo_id.required' => 'Por favor seleccione una persona del equipo de trabajo',
        ];
    }
}
