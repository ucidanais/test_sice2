<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class LoginFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email"=>"required|email",
            "password"=>"required"
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Por favor ingrese su email',
            'email.email' => 'Por favor ingrese una email correcto',
            'password.required' => 'Por favor ingrese su clave de acceso',
        ];
    }
}
