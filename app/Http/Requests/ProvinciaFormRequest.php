<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class ProvinciaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "descripcion"=>"required",
            "coordenadas_poligono" => "",
        ];
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'Por favor ingrese el nombre de la provincia',
        ];
    }
}
