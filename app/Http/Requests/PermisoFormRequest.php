<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class PermisoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "modulo_id"=>"required",
            "nombre"=>"required",
            "path"=>"required",
        ];
    }

    public function messages()
    {
        return [
            'modulo_id.required' => 'Por favor seleccione un módulo',
            'nombre.required' => 'Por favor ingrese el nombre del permiso',
            'path.required' => 'Por favor ingrese el path del permiso',
        ];
    }
}
