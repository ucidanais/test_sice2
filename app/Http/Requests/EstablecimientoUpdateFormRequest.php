<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class EstablecimientoUpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
            "direccion_manzana" => "",
            "direccion_calle_principal" => "required",
            "direccion_numero" => "required",
            "direccion_transversal" => "required",
            "direccion_referencia" => "required",
            "administrador" => "",
            "telefonos_contacto" => "",
            "email_contacto" => "email",
            "foto" => "image",
            "geolocalizacion" => "",
            "id_cadena" => "",
            "id_canal" => "required",
            "id_subcanal" => "required",
            "id_tipo_negocio" => "",
            "id_provincia" => "required",
            "id_ciudad" => "required",
            "id_zona" => "",
            "id_parroquia" => "",
            "id_barrio" => "",
            "estado" => "required",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre del establecimiento',
            'direccion_calle_principal.required' => 'Por favor ingrese la calle principal del establecimiento',
            'direccion_numero.required' => 'Por favor ingrese la numeracion de la dirección del establecimiento',
            'direccion_transversal.required' => 'Por favor ingrese la transversal del establecimiento',
            'direccion_referencia.required' => 'Por favor ingrese una referencia de la dirección del establecimiento',
            'email_contacto.email' => 'Por favor ingrese una dirección de email correcta',
            'foto.image' => 'Por favor ingrese una imagen valida para la foto del establecimiento',
            'id_canal.required' => 'Por favor seleccione un canal para el establecimiento',
            'id_subcanal.required' => 'Por favor seleccione un subcanal para el establecimiento',
            'id_provincia.required' => 'Por favor seleccione una provincia',
            'id_ciudad.required' => 'Por favor seleccione una ciudad',
            'estado.required' => 'Por favor seleccione el estado del establecimiento',
        ];
    }
}
