<?php

namespace SICE\Http\Requests;

use SICE\Http\Requests\Request;

class FamiliaProductoFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            "nombre" => "required",
            "cliente_proyecto_id" => "required",
            "cliente_id" => "required",
            "proyecto_id" => "required",
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese el nombre de la familia de productos',
            'cliente_proyecto_id.required' => 'Por favor seleccione un proyecto por cliente',
            'cliente_id.required' => 'Por favor seleccione un cliente',
            'proyecto_id.required' => 'Por favor seleccione un proyecto',
        ];
    }
}
