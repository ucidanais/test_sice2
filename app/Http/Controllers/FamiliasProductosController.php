<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\FamiliaProducto;
use SICE\ClienteProyecto;
use SICE\Usuario;
use SICE\Http\Requests\FamiliaProductoFormRequest;

/**
* Clase para administración de familias de producto en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/03/20
* @Configuracion
*/

class FamiliasProductosController extends Controller
{
    /**
    * Metodo para mostrar la lista de familias de producto del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/familias-de-productos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/familias-de-productos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $aFamiliasProducto = $oProyectoCliente->familiasProducto;
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'aFamiliasProducto' => $aFamiliasProducto,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.familiasProducto.listaFamiliasProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de familias de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de creacion de una familia de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function create($iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'oFamiliaProducto' => null,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.familiasProducto.crearEditarFamiliaProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos una familia de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto
    * @method POST
    * @param  SICE\Http\Requests\FamiliaProductoFormRequest $familiaProductoFormRequest
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function store(FamiliaProductoFormRequest $familiaProductoFormRequest, $iClienteProyectoId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-familia-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $oFamiliaProducto = new FamiliaProducto;
                $oFamiliaProducto->nombre = $familiaProductoFormRequest->input('nombre');
                $oFamiliaProducto->cliente_proyecto_id = $familiaProductoFormRequest->input('cliente_proyecto_id');
                $oFamiliaProducto->cliente_id = $familiaProductoFormRequest->input('cliente_id');
                $oFamiliaProducto->proyecto_id = $familiaProductoFormRequest->input('proyecto_id');
                if($oFamiliaProducto->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto '.$oFamiliaProducto->nombre.' agregara exitosamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto '.$oFamiliaProducto->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                }
                return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de edición de una familia de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-familia-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function edit($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-familia-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oFamiliaProducto = FamiliaProducto::find($iId);
                    if($oFamiliaProducto){
                        $aData = [
                            'oClienteProyecto' => $oProyectoCliente,
                            'oCliente' => $oProyectoCliente->cliente,
                            'oProyecto' => $oProyectoCliente->proyecto,
                            'oFamiliaProducto' => $oFamiliaProducto,
                            'menuActivo' => 'configuracion',
                            'titulo' => 'SICE :: Configurar Proyecto por cliente',
                        ];
                        return view("configuracion.proyectosPorCliente.familiasProducto.crearEditarFamiliaProducto", $aData);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos los cambios realizados familia de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-familia-de-producto
    * @method POST
    * @param  SICE\Http\Requests\FamiliaProductoFormRequest $familiaProductoFormRequest
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function update(FamiliaProductoFormRequest $familiaProductoFormRequest, $iId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-familia-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $iClienteProyectoId = $familiaProductoFormRequest->input('cliente_proyecto_id');

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oFamiliaProducto = FamiliaProducto::find($iId);
                    if($oFamiliaProducto){
                        $oFamiliaProducto->nombre = $familiaProductoFormRequest->input('nombre');
                        $oFamiliaProducto->cliente_proyecto_id = $familiaProductoFormRequest->input('cliente_proyecto_id');
                        $oFamiliaProducto->cliente_id = $familiaProductoFormRequest->input('cliente_id');
                        $oFamiliaProducto->proyecto_id = $familiaProductoFormRequest->input('proyecto_id');
                        if($oFamiliaProducto->save()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto '.$oFamiliaProducto->nombre.' agregara exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto '.$oFamiliaProducto->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para eliminar una familia de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/20
    *
    * @route /configuracion/proyectos-por-cliente/productos/eliminar-familia-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function destroy($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-familia-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oFamiliaProducto = FamiliaProducto::find($iId);
                    if($oFamiliaProducto){
                        $aCategorias = $oFamiliaProducto->categoriasProducto;
                        $aCadenas = $oFamiliaProducto->productosCadena;
                        if(!$aCategorias->isEmpty()){
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto "'.$oFamiliaProducto->nombre.'" no puede ser eliminada debido a que tiene categorias de producto asociadas</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);    
                        }
                        if(!$aCadenas->isEmpty()){
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto "'.$oFamiliaProducto->nombre.'" no puede ser eliminada debido a que tiene cadenas de establecimientos asociadas</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);    
                        }
                        
                        if($oFamiliaProducto->delete()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto "'.$oFamiliaProducto->nombre.'" ha sido eliminada exitosamente</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto "'.$oFamiliaProducto->nombre.'" no puede ser eliminada.Por favor intentelo nuevamente luego de unos minutos o contacte al administrador del sistema</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/familias-de-productos/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
