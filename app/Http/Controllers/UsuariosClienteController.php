<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Permiso;
use SICE\Modulo;
use SICE\Usuario;
use SICE\Cliente;
use SICE\Http\Requests\UsuarioClienteFormRequest;
use SICE\Http\Requests\UsuarioClienteUpdateFormRequest;

/**
* Clase para administración de usuarios de un determinado cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/28/02
* @Configuracion
* @Autenticacion/Autorizacion
*/

class UsuariosClienteController extends Controller
{
    /**
    * Metodo para mostrar la lista de usuarios del sistema, tambien permite buscar un usuario en particular
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/28/02
    *
    * @route /configuracion/clientes/usuarios/lista-de-usuarios
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param  int $iClienteId 
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iClienteId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/lista-de-usuarios')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
         		$aData = array(
		                'listaUsuarios' => Usuario::buscarUsuario($request->input('valor_busqueda'), $oCliente->id),
		                'oCliente' => $oCliente,
		                'menuActivo' => 'configuracion',
		                'titulo' => 'Lista de Usuarios',
		        );
		        return view('configuracion.clientes.usuarios.listaUsuarios', $aData);
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo usuario en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/28/02
    *
    * @route /configuracion/clientes/usuarios/agregar-usuario
    * @method GET
    * @param  int $iClienteId
    * @return \Illuminate\Http\Response
    */
    public function create($iClienteId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/agregar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
         		$aData = array(
	                'listaModulos' => Modulo::has('permisos')->get(),
	                'oCliente' => $oCliente,
	                'menuActivo' => 'configuracion',
	                'titulo' => 'Agregar Usuario',
		        );
		        return view('configuracion.clientes.usuarios.crearEditarUsuario', $aData);
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
    * Metodo para guardar un nuevo usuario en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/28/02
    *
    * @route /configuracion/clientes/usuarios/agregar-usuario
    * @method POST
    * @param int $iClienteId
    * @param  \SICE\Http\Requests\UsuarioClienteFormRequest  $usuarioFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(UsuarioClienteFormRequest $usuarioFormRequest, $iClienteId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/agregar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
				$oUsuario = new Usuario;
		        $oUsuario->nombre = $usuarioFormRequest->input('nombre');
		        $oUsuario->apellido = $usuarioFormRequest->input('apellido');
		        $oUsuario->email = $usuarioFormRequest->input('email');
		        $oUsuario->password = $usuarioFormRequest->input('password');
		        $oUsuario->tipo = $usuarioFormRequest->input('tipo');
		        $oUsuario->estado = $usuarioFormRequest->input('estado');
		        $oUsuario->cliente_id = $oCliente->id;
		        
		        if ($usuarioFormRequest->hasFile('avatar')){
		            if ($usuarioFormRequest->file('avatar')->isValid()){
		                $avatar = $usuarioFormRequest->file('avatar');
		                if(!is_dir('./resources/images/avatares')){
		                    mkdir('./resources/images/avatares',777);
		                }
		                $i=0;
		                $info = explode(".",$avatar->getClientOriginalName());
		                $oUsuario->avatar = $avatar->getClientOriginalName();
		                while(file_exists('./resources/images/avatares/'.$oUsuario->avatar)){
		                    $i++;
		                    $oUsuario->avatar= $info[0].$i.".".$info[1];
		                }
		                $usuarioFormRequest->file('avatar')->move('resources/images/avatares',$oUsuario->avatar);
		            }
		        }

		        if($oUsuario->save()){
		            $oUsuario->permisos()->detach();
		            if($usuarioFormRequest->input('permisos')){
		                foreach($usuarioFormRequest->input('permisos') as $key => $val){
		                    // Instanciamos el permiso
		                    $oPermiso = Permiso::find($val);
		                    // insertamos en la tabla pivot en usuario y el permiso
		                    // de esta manera queda asignado el permiso al usuario
		                    $oUsuario->permisos()->save($oPermiso);
		                }
		            }
		            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' creado satisfactoriamente</div>');
		        }else{
		            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser creado</div>');
		        }
		        return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);         		
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }

    }

    /**
    * Metodo para mostrar la informacion de un usuario del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/28/02
    *
    * @route /configuracion/clientes/usuarios/detalle-de-usuario
    * @method POST
    * @param int $iClienteId
    * @param  int $iId
    * @return \Illuminate\Http\Response
    */
    public function show($iClienteId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/detalle-de-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
				       		
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un usuario del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/28/02
     *
     * @route /configuracion/clientes/usuarios/editar-usuario
     * @method GET
     * @param int $iClienteId
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iClienteId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/editar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
            	if($iId){
		            if($iId == \Auth::user()->iId || $iId == 1){
		                return redirect('mi-Perfil');
		            }
		            $oUsuario = Usuario::find($iId);
		            if($oUsuario){
		                $aData = array(
		                    'listaModulos' => Modulo::has('permisos')->get(),
		                    'menuActivo' => 'configuracion',
		                    'oCliente' => $oCliente,
		                    'objUsuario' => $oUsuario,
		                    'titulo' => 'Editar Usuario',
		                );
		                return view('configuracion.clientes.usuarios.crearEditarUsuario', $aData);
		            }else{
		                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El usuario seleccionado no existe</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);  
		            }
		        }else{
		            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder editarlo</div>');
		            return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		        }
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
     * Metodo que actualiza la informacion del usuario en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/28/02
     *
     * @route /configuracion/clientes/usuarios/editar-usuario
     * @method POST
     * @param  \SICE\Http\Requests\UsuarioClienteUpdateFormRequest  $usuarioFormRequest
     * @param  int $iClienteId 
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioClienteUpdateFormRequest $usuarioFormRequest, $iClienteId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/editar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
				if($iId){
		            if($iId == \Auth::user()->iId || $iId == 1){
		                return Redirect::to('mi-perfil');    
		            }
		            $oUsuario = Usuario::find($iId);
		            if($oUsuario){
		                $oUsuario->nombre = $usuarioFormRequest->input('nombre');
		                $oUsuario->apellido = $usuarioFormRequest->input('apellido');
		                if($usuarioFormRequest->input('password')){
		                    $oUsuario->password = $usuarioFormRequest->input('password');
		                }
		                $oUsuario->estado = $usuarioFormRequest->input('estado');
		                $oUsuario->tipo = $usuarioFormRequest->input('tipo');
                        $oUsuario->cliente_id = $oCliente->id;
		                
		                $avatarAnterior = $oUsuario->avatar;
		                if ($usuarioFormRequest->hasFile('avatar')){
		                    if ($usuarioFormRequest->file('avatar')->isValid()){
		                        $avatar = $avatarFormRequest->file('avatar');
		                        if(!is_dir('./resources/images/avatares')){
		                            mkdir('./resources/images/avatares',777);
		                        }
		                        $i=0;
		                        $avatar = explode(".",$avatar->getClientOriginalName());
		                        $oUsuario->avatar= $avatar->getClientOriginalName();
		                        while(file_exists('./resources/images/avatares/'.$oUsuario->avatar)){
		                            $i++;
		                            $oUsuario->avatar = $info[0].$i.".".$info[1];
		                        }
		                        $usuarioFormRequest->file('avatar')->move('resources/images/avatares',$oUsuario->avatar);
		                    }
		                }

		                if($oUsuario->save()){
		                    if($avatarAnterior && ($avatarAnterior != $oUsuario->avatar)){
		                        unlink('./resources/images/avatares/'.$avatarAnterior);
		                    }
		                }

		                $oUsuario->permisos()->detach();
		                if($usuarioFormRequest->input('permisos')){
		                    foreach($usuarioFormRequest->input('permisos') as $key => $val){
		                        // Instanciamos el permiso
		                        $oPermiso = Permiso::find($val);
		                        // insertamos en la tabla pivot en usuario y el permiso
		                        // de esta manera queda asignado el permiso al usuario
		                        $oUsuario->permisos()->save($oPermiso);
		                    }
		                }

		                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' actualizado satisfactoriamente</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		            }else{
		                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		            }
		        }else{
		            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder editarlo</div>');
		            return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		        }				       		
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
        
    }


    /**
     * Metodo que elimina un usuario del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/28/02
     *
     * @route /configuracion/clientes/usuarios/eliminar-usuario
     * @method GET
     * @param  int $iClienteId
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iClienteId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/eliminar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
				if($iId){
		            if($iId == 1){
		                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede eliminar al usuario Super Administrador</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		            }
		            if($iId == \Auth::user()->iId){
		                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede eliminar a usted mismo</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		            }
		            $oUsuario = Usuario::find($iId);
		            if($oUsuario){
		                $aEquipoTrabajo = $oUsuario->equiposTrabajo;
		                $aEquipoTrabajoReporta = $oUsuario->equiposTrabajoReporta;
		                $oUsuario->permisos()->detach();
		                if( ! $aEquipoTrabajo->isEmpty()){
		                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado debido a que esta asignado a uno o mas equipos de trabajo</div>');
		                    return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		                }
		                if( ! $aEquipoTrabajoReporta->isEmpty()){
		                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado debido a que esta asignado a uno o mas equipos de trabajo</div>');
		                    return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		                }
		                if($oUsuario->delete()){
		                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' eliminado satisfactoriamente</div>');
		                    return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		                }else{
		                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado</div>');
		                    return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		                }
		            }else{
		                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
		                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		            }           
		        }else{
		            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder eliminarlo</div>');
		            return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
		        }		       		
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }


    /**
     * Metodo que cambia el estado de un usuario del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/28/02
     *
     * @route /configuracion/clientes/usuarios/cambiar-estado-usuario
     * @method GET
     * @param  int $iClienteId
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function cambiaEstado($iClienteId = '', $iId = '')
    {

        if(!Usuario::verificaPermiso('configuracion/clientes/usuarios/cambiar-estado-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteId){
            $oCliente = Cliente::find($iClienteId);
            if($oCliente){
				if($iId){
            if($iId == 1){
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede cambiar el estado al usuario Super Administrador</div>');
                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
            }
            if($iId == \Auth::user()->iId){
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede cambiar de estado a usted mismo</div>');
                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
            }
            $oUsuario = Usuario::find($iId);
            if($oUsuario){
                $oUsuario->estado = $oUsuario->estado == 1 ? 0 : 1;
                $oUsuario->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del usuario '.$oUsuario->nombre.' / '.$oUsuario->email.' satisfactoriamente</div>');
                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
                return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder cambiar su estado</div>');
            return redirect('configuracion/clientes/usuarios/lista-de-usuarios/'.$oCliente->id);
        }				       		
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }

    }
}
