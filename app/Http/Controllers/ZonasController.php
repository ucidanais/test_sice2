<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\ZonaFormRequest;

/**
* Clase para administración de zonas
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class ZonasController extends Controller
{
    /**
    * Metodo para mostrar la lista de zonas (el metodo acepta tambien el ID de la ciudad para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/zonas/lista-de-zonas
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iCiudadId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iCiudadId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/lista-de-zonas')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaZonas' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Zonas', $iCiudadId),
            'menuActivo' => 'catalogos',
            'iCiudadId' => $iCiudadId,
            'titulo' => 'SICE :: Zonas',
        ];
        return view("catalogos.zonas.listaZonas", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una nueva zona
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/zonas/agregar-zona
    * @method GET
    * @param int $iCiudadId
    * @return \Illuminate\Http\Response
    */
    public function create($iCiudadId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/agregar-zona')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
        $iIdProvincia = 0;
        if($iCiudadId){
            $oCiudad = Catalogo::find($iCiudadId);
            if($oCiudad){
                $iIdProvincia = $oCiudad->catalogo_id;
            }
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'provinciaId' => $iIdProvincia,
            'ciudadId' => $iCiudadId,
            'listaProvincias' => $aListaProvincias,
            'titulo' => 'SICE :: Crear Nueva Zona',
        ];
        return view("catalogos.zonas.crearEditarZona", $aData);
    }

    /**
    * Metodo para guardar una nueva zona
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/zonas/agregar-zona
    * @method POST
    * @param  \SICE\Http\Requests\ZonaFormRequest  $zonaFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ZonaFormRequest $zonaFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/agregar-zona')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oZona = new Catalogo;
        $oCatalogoPadre = Catalogo::find($zonaFormRequest->input('catalogo_id'));
        $oZona->contexto = 'Zonas';
        $oZona->codigo1 = Catalogo::generaCodigo('Zonas', $oCatalogoPadre->id);
        $oZona->codigo2 = $oCatalogoPadre->codigo1;
        $oZona->valor2 = $oCatalogoPadre->descripcion;
        $oZona->descripcion = $zonaFormRequest->input('descripcion');
        $oZona->coordenadas_poligono = $zonaFormRequest->input('coordenadas_poligono');
        $oZona->catalogo_id = $zonaFormRequest->input('catalogo_id');
        
        if($oZona->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$oZona->descripcion.' guardada satisfactoriamente</div>');
            return redirect('catalogos/zonas');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$oZona->descripcion.' no pudo ser guardada</div>');
            return redirect('catalogos/zonas');
        }
        
    }

    /**
     * Metodo que muestra el formulario de edicion de una zona de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/zonas/editar-zona
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/editar-zona')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oZona = Catalogo::find($iId);
            if($oZona){
                $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
                $aData = [
                    'oZona' => $oZona,
                    'listaProvincias' => $aListaProvincias,
                    'provinciaId' => $oZona->catalogoPadre->catalogoPadre->id,
                    'ciudadId' => $oZona->catalogo_id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Zona',
                ];
                return view('catalogos.zonas.crearEditarZona', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La zona seleccionada no existe</div>');
                return redirect('catalogos/zonas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una zona para poder editarla.</div>');
            return redirect('catalogos/zonas');
        }
    }

    /**
     * Metodo que actualiza la informacion de la zona en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/zonas/editar-zona
     * @method POST
     * @param  \SICE\Http\Requests\ZonaFormRequest  $zonaFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ZonaFormRequest $zonaFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/editar-zona')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oZona = Catalogo::find($iId);
            if($oZona){
                $oZona->contexto = 'Zonas';
                $oZona->descripcion = $ciudadFormRequest->input('descripcion');
                $oZona->coordenadas_poligono = $zonaFormRequest->input('coordenadas_poligono');
             
                if($oZona->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$oZona->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$oZona->descripcion.' no pudo ser guardada</div>');
                }
                return redirect('catalogos/zonas');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La zona seleccionada no existe</div>');
                return redirect('catalogos/zonas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una zona para poder actualizar su información</div>');
            return redirect('catalogos/zonas');
        }
    }

    /**
     * Metodo que elimina una zona del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/zonas/eliminar-zona
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/zonas/eliminar-zona')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oZona = Catalogo::find($iId);
            if($oZona){
                $nombre = $oZona->descripcion;
                $aSectores = $oZona->catalogos;
                
                if( ! $aSectores->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$nombre.' no pudo ser eliminada debido a que tiene sectores relacionados</div>');
                    return redirect('catalogos/zonas');
                }

                if($oZona->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$nombre.' eliminada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Zona '.$nombre.' no pudo ser eliminada</div>');
                }
                return redirect('catalogos/zonas');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La zona seleccionada no existe</div>');
                return redirect('catalogos/zonas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una zona para poder eliminarla</div>');
            return redirect('catalogos/zonas');
        }
    }
}
