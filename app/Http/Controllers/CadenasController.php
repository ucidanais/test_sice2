<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\CadenaFormRequest;

/**
* Clase para administración de cadenas
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class CadenasController extends Controller
{
    /**
    * Metodo para mostrar la lista de cadenas 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/cadenas/lista-de-cadenas
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('catalogos/cadenas/lista-de-cadenas')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        $data = [
            'listaCadenas' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Cadenas'),
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Cadenas',
        ];
        return view("catalogos.cadenas.listaCadenas", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una nueva cadena
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/cadenas/agregar-cadena
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('catalogos/cadenas/agregar-cadena')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Crear Nueva Cadena',
        ];
        return view("catalogos.cadenas.crearEditarCadena", $aData);
    }

    
    /**
    * Metodo para guardar una nueva cadena
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/cadenas/agregar-cadena
    * @method POST
    * @param  \SICE\Http\Requests\CadenaFormRequest  $cadenaFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(CadenaFormRequest $cadenaFormRequest)
    {
        
        if(!Usuario::verificaPermiso('catalogos/cadenas/agregar-cadena')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        
        $oCadena = new Catalogo;
        $oCadena->contexto = 'Cadenas';
        $oCadena->codigo1 = Catalogo::generaCodigo('Cadenas', null);
        $oCadena->descripcion = $cadenaFormRequest->input('descripcion');

        if($oCadena->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$oCadena->descripcion.' guardada satisfactoriamente</div>');
            return redirect('catalogos/cadenas');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$oCadena->desripcion.' no pudo ser guardada</div>');
            return redirect('catalogos/cadenas');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de una cadena de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/cadenas/editar-cadena
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/cadenas/editar-cadena')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCadena = Catalogo::find($iId);
            if($oCadena){
                $aData = [
                    'oCadena' => $oCadena,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Cadena',
                ];
                return view('catalogos.cadenas.crearEditarCadena', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La cadena seleccionada no existe</div>');
                return redirect('catalogos/cadenas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una cadena para poder editarla.</div>');
            return redirect('catalogos/cadenas');
        }
    }

    
    /**
     * Metodo que actualiza la informacion de la cadena en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/cadenas/editar-cadena
     * @method POST
     * @param  \SICE\Http\Requests\CadenaFormRequest  $cadenaFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(CadenaFormRequest $cadenaFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/cadenas/editar-cadena')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCadena = Catalogo::find($iId);
            if($oCadena){
                $oCadena->contexto = 'Cadenas';
                $oCadena->descripcion = $cadenaFormRequest->input('descripcion');
             
                if($oCadena->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$oCadena->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$oCadena->descripcion.' no pudo ser guardada</div>');
                }
                return redirect('catalogos/cadenas');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La cadena seleccionada no existe</div>');
                return redirect('catalogos/cadenas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una cadena para poder actualizar su información</div>');
            return redirect('catalogos/cadenas');
        }
    }

    
    /**
     * Metodo que elimina una cadena del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/cadenas/eliminar-cadena
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/cadenas/eliminar-cadena')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCadena = Catalogo::find($iId);
            if($oCadena){
                $nombre = $oCadena->descripcion;
                if($oCadena->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$nombre.' eliminada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cadena '.$nombre.' no pudo ser eliminada</div>');
                }
                return redirect('catalogos/cadenas');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La cadena seleccionada no existe</div>');
                return redirect('catalogos/cadenas');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una cadena para poder eliminarla</div>');
            return redirect('catalogos/cadenas');
        }
    }
}
