<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Cliente;
use SICE\Proyecto;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\EquipoTrabajo;
use SICE\Http\Requests\EquipoTrabajoFormRequest;

/**
* Clase para administración de equipos de trabajo en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/02/20
* @Configuracion
*/

class EquiposTrabajoController extends Controller
{
    /**
     * Metodo que muestra la pantalla de administracion de los equipos de trabajo de los proyectos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/02/20
     *
     * @route /configuracion/proyectos-por-cliente/equipo-de-trabajo
     * @method GET
     * @param  int  $iClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function equipoTrabajo($iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/equipo-de-trabajo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $aEquipoTrabajo = $oProyectoCliente->equiposTrabajo;
                $aUsuariosGo = Usuario::where('estado', 1)->whereNull('cliente_id')->get();
                $aUsuariosCliente = Usuario::where('estado', 1)->where('cliente_id', $oProyectoCliente->cliente_id)->get();
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'aEquipoTrabajo' => $aEquipoTrabajo,
                    'aUsuariosGo' => $aUsuariosGo,
                    'aUsuariosCliente' => $aUsuariosCliente,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.equipoTrabajo", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder cambiar su estado</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
    
    
    /**
     * Metodo que guarda en la base de datos el nuevo integrante del equipo de trabajo
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/02/20
     *
     * @route /configuracion/proyectos-por-cliente/asignar-usuario-al-equipo-de-trabajo
     * @method POST
     * @param  \SICE\Http\Requests\EquipoTrabajoFormRequest  $equipoTrabajoFormRequest
     * @param  int  $iClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function asignarUsuarioProyecto(EquipoTrabajoFormRequest $equipoTrabajoFormRequest, $iClienteProyectoId = ''){
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/asignar-usuario-al-equipo-de-trabajo')){
            return response()->json([
                'success' => false,
                'message' => ['Usted no tiene permisos para navegar en esta seccion del sistema']
            ]);
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $verificaColaborador = EquipoTrabajo::where('usuario_id','=',$equipoTrabajoFormRequest->input('usuario_id'))
                                                    ->where('cliente_proyecto_id','=',$oProyectoCliente->id)
                                                    ->where('cliente_id','=',$oProyectoCliente->cliente_id)
                                                    ->where('proyecto_id','=',$oProyectoCliente->proyecto_id)
                                                    ->count();
                if($verificaColaborador){
                    return response()->json([
                        'success' => false,
                        'message' => ['El usuario '.$equipoTrabajoFormRequest->input('nombre').' '.$equipoTrabajoFormRequest->input('apellido').' no fue asignado al proyecto '.$oProyectoCliente->descripcion.' debido a que ya forma parte de este equipo de trabajo']
                    ]);
                }
                $oEquipoTrabajo = new EquipoTrabajo;
                $oEquipoTrabajo->cargo = $equipoTrabajoFormRequest->input('cargo');
                $oEquipoTrabajo->nombre = $equipoTrabajoFormRequest->input('nombre');
                $oEquipoTrabajo->apellido = $equipoTrabajoFormRequest->input('apellido');
                $oEquipoTrabajo->tipo = $equipoTrabajoFormRequest->input('tipo');
                $oEquipoTrabajo->lider_proyecto = $equipoTrabajoFormRequest->input('lider_proyecto');
                $oEquipoTrabajo->usuario_id = $equipoTrabajoFormRequest->input('usuario_id');
                if($equipoTrabajoFormRequest->input('usuario_reporta_id')){
                    $oEquipoTrabajo->usuario_reporta_id = $equipoTrabajoFormRequest->input('usuario_reporta_id');
                }
                $oEquipoTrabajo->cliente_proyecto_id = $oProyectoCliente->id;
                $oEquipoTrabajo->cliente_id = $oProyectoCliente->cliente_id;
                $oEquipoTrabajo->proyecto_id = $oProyectoCliente->proyecto_id;

                if($oEquipoTrabajo->save()){
                    return response()->json([
                        'success' => true,
                        'message' => ['El colaborador '.$oEquipoTrabajo->nombre.' '.$oEquipoTrabajo->apellido.' ha sido asignado correctamente al proyecto '.$oProyectoCliente->descripcion]
                    ]);
                }else{
                    return response()->json([
                        'success' => true,
                        'message' => ['Lo sentimos, el colaborador '.$oEquipoTrabajo->nombre.' '.$oEquipoTrabajo->apellido.' no pudo ser asignado al proyecto '.$oProyectoCliente->descripcion.'. Por favor intentelo nuevamente luego de unos minutos']
                    ]);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => ['Proyecto seleccionado no existe']
                ]);
            }           
        }else{
            return response()->json([
                'success' => false,
                'message' => ['Por favor seleccione un proyecto para poder asignar su nuevo colaborador']
            ]);
        }
    }
    


    /**
     * Metodo que actualiza en la base de datos el integrante del equipo de trabajo de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/05/05
     *
     * @route /configuracion/proyectos-por-cliente/actualizar-usuario-del-equipo-de-trabajo
     * @method PUT / PATCH
     * @param  \SICE\Http\Requests\EquipoTrabajoFormRequest  $request
     * @param  int  $iEquipoTrabajoId
     * @return \Illuminate\Http\Response
     */
    public function actualizaUsuarioProyecto(EquipoTrabajoFormRequest $request, $iEquipoTrabajoId = ''){
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/actualizar-usuario-del-equipo-de-trabajo')){
            return response()->json([
                'success' => false,
                'message' => ['Usted no tiene permisos para navegar en esta seccion del sistema']
            ]);
        }

        if($iEquipoTrabajoId){
            $oEquipoTrabajo = EquipoTrabajo::find($iEquipoTrabajoId);
            if(is_object($oEquipoTrabajo)){
                
                $oProyectoCliente = $oEquipoTrabajo->clienteProyecto;
                
                $verificaColaborador = EquipoTrabajo::where('usuario_id','=',$request->input('usuario_id'))
                                                    ->where('cliente_proyecto_id','=',$oProyectoCliente->id)
                                                    ->where('cliente_id','=',$oProyectoCliente->cliente_id)
                                                    ->where('proyecto_id','=',$oProyectoCliente->proyecto_id)
                                                    ->count();
                if( ! $verificaColaborador){
                    return response()->json([
                        'success' => false,
                        'message' => ['El usuario '.$request->input('nombre').' '.$request->input('apellido').' no fue actualizado en el proyecto '.$oProyectoCliente->descripcion.' debido a que no forma parte de este equipo de trabajo']
                    ]);
                }
                
                $oEquipoTrabajo->cargo = $request->input('cargo');
                $oEquipoTrabajo->nombre = $request->input('nombre');
                $oEquipoTrabajo->apellido = $request->input('apellido');
                $oEquipoTrabajo->tipo = $request->input('tipo');
                $oEquipoTrabajo->lider_proyecto = $request->input('lider_proyecto');
                $oEquipoTrabajo->usuario_id = $request->input('usuario_id');
                if($request->input('usuario_reporta_id')){
                    $oEquipoTrabajo->usuario_reporta_id = $request->input('usuario_reporta_id');
                }
                $oEquipoTrabajo->cliente_proyecto_id = $oProyectoCliente->id;
                $oEquipoTrabajo->cliente_id = $oProyectoCliente->cliente_id;
                $oEquipoTrabajo->proyecto_id = $oProyectoCliente->proyecto_id;

                if($oEquipoTrabajo->save()){
                    return response()->json([
                        'success' => true,
                        'message' => ['El colaborador '.$oEquipoTrabajo->nombre.' '.$oEquipoTrabajo->apellido.' ha sido actualziado correctamente']
                    ]);
                }else{
                    return response()->json([
                        'success' => true,
                        'message' => ['Lo sentimos, el colaborador '.$oEquipoTrabajo->nombre.' '.$oEquipoTrabajo->apellido.' no pudo ser actualziado. Por favor intentelo nuevamente luego de unos minutos']
                    ]);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => ['El colaborador seleccionado no está asignado a este proyecto']
                ]);
            }           
        }else{
            return response()->json([
                'success' => false,
                'message' => ['Por favor seleccione un colaborador para poder actualizar su información']
            ]);
        }
    }


    /**
     * Metodo que quita un integrante del equipo de trabajo
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/02/20
     *
     * @route /configuracion/proyectos-por-cliente/quitar-colaborador-del-equipo-de-trabajo
     * @method POST
     * @param  int  $iEquipoTrabajoId
     * @return \Illuminate\Http\Response
     */
    public function quitarUsuarioProyecto($iEquipoTrabajoId = ''){
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/quitar-colaborador-del-equipo-de-trabajo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iEquipoTrabajoId){
            $oEquipoTrabajo = EquipoTrabajo::find($iEquipoTrabajoId);
            if($oEquipoTrabajo){
                $hasRutas = $oEquipoTrabajo->rutas;
                if(!$hasRutas->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El colaborador seleccionado tiene rutas asignadas, por lo tanto no podrá ser eliminado de este equipo de trabajo.</div>');
                    return redirect('configuracion/proyectos-por-cliente/equipo-de-trabajo/'.$oEquipoTrabajo->cliente_proyecto_id);    
                }

                if($oEquipoTrabajo->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El colaborador seleccionado ha sido eliminado de este equipo de trabajo.</div>');
                    return redirect('configuracion/proyectos-por-cliente/equipo-de-trabajo/'.$oEquipoTrabajo->cliente_proyecto_id);
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El colaborador seleccionado no pudo ser eliminado de este equipo de trabajo, por favor intentelo nuevamente dentro de unos minutos.</div>');
                    return redirect('configuracion/proyectos-por-cliente/equipo-de-trabajo/'.$oEquipoTrabajo->cliente_proyecto_id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');    
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para cambiar su configuracion</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
