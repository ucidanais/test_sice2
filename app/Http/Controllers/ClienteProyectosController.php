<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Cliente;
use SICE\Proyecto;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\Novedad;
use SICE\FamiliaProducto;
use SICE\Ruta;
use SICE\HojaRuta;
use SICE\EquipoTrabajo;
use SICE\GrupoProducto;
use SICE\Http\Requests\ClienteProyectoFormRequest;
use SICE\Http\Requests\ClienteProyectoUpdateFormRequest;
use SICE\Http\Requests\EquipoTrabajoFormRequest;

/**
* Clase para administración de proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/01/10
* @Configuracion
*/


class ClienteProyectosController extends Controller
{
    /**
    * Metodo para mostrar la lista de clientes para los cuales se han generado o se desean generar nuevos proyectos
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/10
    *
    * @route /configuracion/proyectos-por-cliente/listar-por-cliente
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function clientes(Request $request)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaClientes' => Cliente::buscarCliente($request->input('valor_busqueda'),10),
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Configurar Proyecto por cliente',
        ];
        return view("configuracion.proyectosPorCliente.listaClientes", $aData);
    }

    /**
    * Metodo para mostrar la lista de proyectos y los clientes para los cuales se han realizado estos proyectos o generar nuevos proyectos para un determinado cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/10
    *
    * @route /configuracion/proyectos-por-cliente/listar-por-proyecto
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function proyectos(Request $request)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/listar-por-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaProyectos' => Proyecto::buscarProyecto($request->input('valor_busqueda'),10),
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Configurar Proyecto por cliente',
        ];
        return view("configuracion.proyectosPorCliente.listaProyectos", $aData);
    }

    /**
    * Metodo para mostrar el formulario de generacion de un nuevo proyecto por cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/12
    *
    * @route /configuracion/proyectos-por-cliente/generar-proyecto
    * @method GET
    * @param string $sClienteProyecto
    * @param int $iIdCliProy
    * @return \Illuminate\Http\Response
    */
    public function create($sClienteProyecto = '', $iIdCliProy = ''){
    	if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/generar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($sClienteProyecto == 'cliente'){
        	$objCliente = Cliente::find($iIdCliProy);
        	if($objCliente){
        		$aData = [
	        		'listaProyectos' => Proyecto::lists('nombre','id'),
	        		'objCliente' => $objCliente,
	        		'objProyecto' => null,
	        		'listaClientes' => [],
	        		'menuActivo' => 'configuracion',
	        		'titulo' => 'SICE :: Configurar Proyecto por cliente',
	        	];
        	}else{
        		$aData = [
	        		'listaProyectos' => Proyecto::lists('nombre','id'),
	        		'objCliente' => null,
	        		'objProyecto' => null,
	        		'listaClientes' => Cliente::lists('nombre','id'),
	        		'menuActivo' => 'configuracion',
	        		'titulo' => 'SICE :: Configurar Proyecto por cliente',
	        	];
        	}
        }elseif($sClienteProyecto == 'proyecto'){
        	$objProyecto = Proyecto::find($iIdCliProy);
        	if($objProyecto){
        		$aData = [
	        		'listaProyectos' => [],
	        		'objCliente' => null,
	        		'objProyecto' => $objProyecto,
	        		'listaClientes' => Cliente::lists('nombre','id'),
	        		'menuActivo' => 'configuracion',
	        		'titulo' => 'SICE :: Configurar Proyecto por cliente',
	        	];
        	}else{
        		$aData = [
	        		'listaProyectos' => Proyecto::lists('nombre','id'),
	        		'objCliente' => null,
	        		'objProyecto' => null,
	        		'listaClientes' => Cliente::lists('nombre','id'),
	        		'menuActivo' => 'configuracion',
	        		'titulo' => 'SICE :: Configurar Proyecto por cliente',
	        	];
        	}
        }else{
        	$aData = [
        		'listaProyectos' => Proyecto::lists('nombre','id'),
        		'objCliente' => null,
        		'objProyecto' => null,
        		'listaClientes' => Cliente::lists('nombre','id'),
        		'menuActivo' => 'configuracion',
        		'titulo' => 'SICE :: Configurar Proyecto por cliente',
        	];
        }
        return view("configuracion.proyectosPorCliente.generarProyecto", $aData);
    }


    /**
    * Metodo para almacenar en la Base de datos la configuracion del proyecto con el cliente seleccionados
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/12
    *
    * @route /configuracion/proyectos-por-cliente/generar-proyecto
    * @method POST
    * @param  \SICE\Http\Requests\ClienteProyectoFormRequest  $clienteProyectoFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ClienteProyectoFormRequest $clienteProyectoFormRequest)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/generar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        $oClienteProyecto = new ClienteProyecto;
        $oClienteProyecto->estado = $clienteProyectoFormRequest->input('estado');
        $oClienteProyecto->descripcion = $clienteProyectoFormRequest->input('descripcion');
        $oClienteProyecto->tomar_stock = $clienteProyectoFormRequest->input('tomar_stock');
        $oClienteProyecto->clasificaciones = $clienteProyectoFormRequest->input('clasificaciones');
        $oClienteProyecto->cliente_id = $clienteProyectoFormRequest->input('cliente_id');
        $oClienteProyecto->proyecto_id = $clienteProyectoFormRequest->input('proyecto_id');

        if($oClienteProyecto->save()){
            
            $oGrupoProducto = new GrupoProducto;
            $oGrupoProducto->nombre = $oClienteProyecto->descripcion;
            $oGrupoProducto->cliente_proyecto_id = $oClienteProyecto->id;
            $oGrupoProducto->cliente_id = $oClienteProyecto->cliente_id;
            $oGrupoProducto->proyecto_id = $oClienteProyecto->proyecto_id;
            $oGrupoProducto->save();

        	$objCliente = Cliente::find($oClienteProyecto->cliente_id);
        	$objProyecto = Proyecto::find($oClienteProyecto->proyecto_id);
        	\Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto '.$objProyecto->nombre.' se ha asignado satisfactoriamente al cliente '.$objCliente->nombre.' con la descripcion: '.$oClienteProyecto->descripcion.'</div>');
        }else{
        	\Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se pudo configurar el proyecto con el cliente seleccionados, por favor intentelo nuevamente.</div>');
        }
        return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
    }


    /**
    * Metodo para mostrar el formulario de edicion de un proyecto por cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/13
    *
    * @route /configuracion/proyectos-por-cliente/editar-proyecto-por-cliente
    * @method GET
    * @param int $iIdCliProy
    * @return \Illuminate\Http\Response
    */
    public function edit($iIdCliProy = ''){
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/editar-proyecto-por-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iIdCliProy){
            $objClienteProyecto = ClienteProyecto::find($iIdCliProy);
            if($objClienteProyecto){
                $aData = [
                    'objCliente' => $objClienteProyecto->cliente,
                    'objProyecto' => $objClienteProyecto->proyecto,
                    'objClienteProyecto' => $objClienteProyecto,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.editarProyecto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');    
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para cambiar su configuracion</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }


    /**
    * Metodo para almacenar en la Base de datos la configuracion del proyecto con el cliente seleccionados
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/13
    *
    * @route /configuracion/proyectos-por-cliente/editar-proyecto-por-cliente
    * @method POST
    * @param  \SICE\Http\Requests\ClienteProyectoUpdateFormRequest  $clienteProyectoUpdateFormRequest
    * @return \Illuminate\Http\Response
    */
    public function update(ClienteProyectoUpdateFormRequest $clienteProyectoUpdateFormRequest, $iIdCliProy = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/generar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iIdCliProy){
            $oClienteProyecto = ClienteProyecto::find($iIdCliProy);
            if($oClienteProyecto){
                $oClienteProyecto->estado = $clienteProyectoUpdateFormRequest->input('estado');
                $oClienteProyecto->descripcion = $clienteProyectoUpdateFormRequest->input('descripcion');
                $oClienteProyecto->tomar_stock = $clienteProyectoUpdateFormRequest->input('tomar_stock');
                $oClienteProyecto->clasificaciones = $clienteProyectoUpdateFormRequest->input('clasificaciones');

                if($oClienteProyecto->save()){
                    
                    $oGrupoProducto = GrupoProducto::where('cliente_proyecto_id', $oClienteProyecto->id)->first();
                    if(!$oGrupoProducto){
                        $oGrupoProducto = new GrupoProducto;
                        $oGrupoProducto->cliente_proyecto_id = $oClienteProyecto->id;
                        $oGrupoProducto->cliente_id = $oClienteProyecto->cliente_id;
                        $oGrupoProducto->proyecto_id = $oClienteProyecto->proyecto_id;
                    }
                    $oGrupoProducto->nombre = $oClienteProyecto->descripcion;
                    $oGrupoProducto->save();
                    
                    $objCliente = Cliente::find($oClienteProyecto->cliente_id);
                    $objProyecto = Proyecto::find($oClienteProyecto->proyecto_id);
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto '.$objProyecto->nombre.' se ha asignado satisfactoriamente al cliente '.$objCliente->nombre.' con la descripcion: '.$oClienteProyecto->descripcion.'</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se pudo configurar el proyecto con el cliente seleccionados, por favor intentelo nuevamente.</div>');
                }
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');    
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para cambiar su configuracion</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
        $oClienteProyecto = new ClienteProyecto;
    }


    /**
    * Metodo para mostrar el detalle de un proyecto por cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/15
    *
    * @route /configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente
    * @method GET
    * @param int $iIdCliProy
    * @return \Illuminate\Http\Response
    */
    public function show($iIdCliProy = ''){
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/detalle-proyecto-por-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iIdCliProy){
            $oClienteProyecto = ClienteProyecto::find($iIdCliProy);
            if($oClienteProyecto){
                $oLiderProyecto = $oClienteProyecto->equiposTrabajo()->lider()->first();
                $aEmpleados = $oClienteProyecto->equiposTrabajo()->empleado()->orderBy('lider_proyecto', 'desc')->get();
                $aClientes = $oClienteProyecto->equiposTrabajo()->cliente()->get();
                
                $aData = [
                    'oClienteProyecto' => $oClienteProyecto,
                    'oLiderProyecto' => $oLiderProyecto,
                    'aEmpleados' => $aEmpleados,
                    'aClientes' => $aClientes,
                    'aFamiliasProductos' => $oClienteProyecto->familiasProducto,
                    'aGruposProductos' => $oClienteProyecto->gruposProducto,
                    'aRutas' => $oClienteProyecto->rutas,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.detalleProyecto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');    
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para cambiar su configuracion</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }



    /**
     * Metodo que cambia el estado de un proyecto por cliente del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/01/20
     *
     * @route /configuracion/proyectos-por-cliente/cambiar-estado
     * @method GET
     * @param  int  $iClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado($iClienteProyectoId = '')
    {

        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/cambiar-estado')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $oProyectoCliente->estado = $oProyectoCliente->estado == 1 ? 0 : 1;
                $oProyectoCliente->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del proyecto '.$oProyectoCliente->descripcion.' satisfactoriamente</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder cambiar su estado</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
