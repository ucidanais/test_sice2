<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\CiudadFormRequest;

/**
* Clase para administración de ciudades
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class CiudadesController extends Controller
{
    /**
    * Metodo para mostrar la lista de ciudades (el metodo acepta tambien el ID de la provincia para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/ciudades/lista-de-ciudades
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iProvinciaId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iProvinciaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/ciudades/lista-de-ciudades')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaCiudades' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Ciudades', $iProvinciaId),
            'menuActivo' => 'catalogos',
            'iProvinciaId' => $iProvinciaId,
            'titulo' => 'SICE :: Ciudades',
        ];
        return view("catalogos.ciudades.listaCiudades", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una nueva ciudad
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/ciudades/agregar-ciudad
    * @method GET
    * @param int $iProvinciaId
    * @return \Illuminate\Http\Response
    */
    public function create($iProvinciaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/ciudades/agregar-ciudad')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');

        $aData = [
            'menuActivo' => 'catalogos',
            'provinciaId' => $iProvinciaId,
            'listaProvincias' => $aListaProvincias,
            'titulo' => 'SICE :: Crear Nueva Ciudad',
        ];
        return view("catalogos.ciudades.crearEditarCiudad", $aData);
    }

    
    /**
    * Metodo para guardar una nueva ciudad
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/ciudades/agregar-ciudad
    * @method POST
    * @param  \SICE\Http\Requests\CiudadFormRequest  $ciudadFormRequest
    * @param int $iProvinciaId
    * @return \Illuminate\Http\Response
    */
    public function store(CiudadFormRequest $ciudadFormRequest, $iProvinciaId = '')
    {
    	if(!Usuario::verificaPermiso('catalogos/ciudades/agregar-ciudad')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oCiudad = new Catalogo;
        $oCatalogoPadre = Catalogo::find($barrioFormRequest->input('catalogo_id'));
        $oCiudad->contexto = 'Ciudades';
        $oCiudad->codigo1 = Catalogo::generaCodigo('Ciudades', $catalogoPadre->id);
        $oCiudad->codigo2 = $oCatalogoPadre->codigo1;
        $oCiudad->valor2 = $oCatalogoPadre->descripcion;
        $oCiudad->descripcion = $ciudadFormRequest->input('descripcion');
        $oCiudad->coordenadas_poligono = $ciudadFormRequest->input('coordenadas_poligono');
        $oCiudad->catalogo_id = $oCatalogoPadre->id;
        
        if($oCiudad->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$oCiudad->descripcion.' guardada satisfactoriamente</div>');
            return redirect('catalogos/ciudades');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$oCiudad->descripcion.' no pudo ser guardada</div>');
            return redirect('catalogos/ciudades');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de una ciudad de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/ciudades/editar-ciudad
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/ciudades/editar-ciudad')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCiudad = Catalogo::find($iId);
            if($oCiudad){
            	$aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
                $aData = [
                    'oCiudad' => $oCiudad,
                    'listaProvincias' => $aListaProvincias,
                    'provinciaId' => $oCiudad->catalogo_id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Ciudad',
                ];
                return view('catalogos.ciudades.crearEditarCiudad', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ciudad seleccionada no existe</div>');
                return redirect('catalogos/ciudades');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ciudad para poder editarla.</div>');
            return redirect('catalogos/ciudades');
        }
    }

    
    /**
     * Metodo que actualiza la informacion de la ciudad en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/ciudades/editar-ciudad
     * @method POST
     * @param  \SICE\Http\Requests\CiudadFormRequest  $ciudadFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(CiudadFormRequest $ciudadFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/ciudades/editar-ciudad')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCiudad = Catalogo::find($iId);
            if($oCiudad){
                $oCiudad->contexto = 'Ciudades';
                $oCiudad->descripcion = $ciudadFormRequest->input('descripcion');
                $oCiudad->coordenadas_poligono = $ciudadFormRequest->input('coordenadas_poligono');
             
                if($oCiudad->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$oCiudad->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$oCiudad->descripcion.' no pudo ser guardada</div>');
                }
                return redirect('catalogos/ciudades');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ciudad seleccionada no existe</div>');
                return redirect('catalogos/ciudades');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ciudad para poder actualizar su información</div>');
            return redirect('catalogos/ciudades');
        }
    }

    
    /**
     * Metodo que elimina una ciudad del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/ciudades/eliminar-ciudad
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/ciudades/eliminar-ciudad')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCiudad = Catalogo::find($iId);
            if($oCiudad){
                $nombre = $oCiudad->descripcion;
                $aZonas = $oCiudad->catalogos;
                
                if( ! $aZonas->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$nombre.' no pudo ser eliminada debido a que tiene zonas relacionadas</div>');
                    return redirect('catalogos/ciudades');
                }


                if($oCiudad->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$nombre.' eliminada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ciudad '.$nombre.' no pudo ser eliminada</div>');
                }
                return redirect('catalogos/ciudades');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ciudad seleccionada no existe</div>');
                return redirect('catalogos/ciudades');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ciudad para poder eliminarla</div>');
            return redirect('catalogos/ciudades');
        }
    }
}
