<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;


use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\SectorFormRequest;

/**
* Clase para administración de sectores
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class SectoresController extends Controller
{
    /**
    * Metodo para mostrar la lista de sectores (el metodo acepta tambien el ID de la ciudad para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/sectores/lista-de-sectores
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iZonaId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iZonaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/sectores/lista-de-sectores')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaSectores' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Sectores', $iZonaId),
            'menuActivo' => 'catalogos',
            'iZonaId' => $iZonaId,
            'titulo' => 'SICE :: Sectores',
        ];
        return view("catalogos.sectores.listaSectores", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo sector
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/sectores/agregar-sector
    * @method GET
    * @param int $iZonaId
    * @return \Illuminate\Http\Response
    */
    public function create($iZonaId = '')
    {
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La administración de catalogos se debe realizar en el sistema SICEDB</div>');
        return redirect('home');

        if(!Usuario::verificaPermiso('catalogos/sectores/agregar-sector')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaProvincias = Catalogo::byContexto('Provincias')->lists('descripcion','id');
        $iIdProvincia = 0;
        $iIdCiudad = 0;
        $iIdZona = 0;
        if($iZonaId){
            $oZona = Catalogo::find($iZonaId);
            if($oZona){
                $iIdCiudad = $oZona->catalogoPadre->id;
                $iIdProvincia = $oZona->catalogoPadre->catalogoPadre->id;
            }
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'provinciaId' => $iIdProvincia,
            'ciudadId' => $iIdCiudad,
            'zonaId' => $iZonaId,
            'listaProvincias' => $aListaProvincias,
            'titulo' => 'SICE :: Crear Nuevo Sector',
        ];
        return view("catalogos.sectores.crearEditarSector", $aData);
    }

    /**
    * Metodo para guardar un nuevo sector
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/sectores/agregar-sector
    * @method POST
    * @param  \SICE\Http\Requests\SectorFormRequest  $sectorFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(SectorFormRequest $sectorFormRequest)
    {
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La administración de catalogos se debe realizar en el sistema SICEDB</div>');
        return redirect('home');

        if(!Usuario::verificaPermiso('catalogos/sectores/agregar-sector')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oSector = new Catalogo;
        $oSector->contexto = 'Sectores';
        $oSector->codigo1 = $sectorFormRequest->input('codigo1');
        $oSector->descripcion = $sectorFormRequest->input('descripcion');
        $oSector->coordenadas_poligono = $sectorFormRequest->input('coordenadas_poligono');
        $oSector->catalogo_id = $sectorFormRequest->input('catalogo_id');
        
        if($oSector->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$oSector->descripcion.' guardado satisfactoriamente</div>');
            return redirect('catalogos/sectores');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$oSector->descripcion.' no pudo ser guardado</div>');
            return redirect('catalogos/sectores');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un sector de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/sectores/editar-sector
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La administración de catalogos se debe realizar en el sistema SICEDB</div>');
        return redirect('home');

        if(!Usuario::verificaPermiso('catalogos/sectores/editar-sector')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSector = Catalogo::find($iId);
            if($oSector){
                $aListaProvincias = Catalogo::byContexto('Provincias')->lists('descripcion','id');
                $aData = [
                    'oSector' => $oSector,
                    'listaProvincias' => $aListaProvincias,
                    'provinciaId' => $oSector->catalogoPadre->catalogoPadre->catalogoPadre->id,
                    'ciudadId' => $oSector->catalogoPadre->catalogoPadre->id,
                    'zonaId' => $oSector->catalogoPadre->id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Sector',
                ];
                return view('catalogos.sectores.crearEditarSector', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El sector seleccionado no existe</div>');
                return redirect('catalogos/sectores');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un sector para poder editarlo.</div>');
            return redirect('catalogos/sectores');
        }
    }

    /**
     * Metodo que actualiza la informacion del sector en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/sectores/editar-sector
     * @method POST
     * @param  \SICE\Http\Requests\SectorFormRequest  $sectorFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(SectorFormRequest $sectorFormRequest, $iId = '')
    {
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La administración de catalogos se debe realizar en el sistema SICEDB</div>');
        return redirect('home');

        if(!Usuario::verificaPermiso('catalogos/sectores/editar-sector')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSector = Catalogo::find($iId);
            if($oSector){
                $oSector->contexto = 'Sectores';
                $oSector->codigo1 = $sectorFormRequest->input('codigo1');
                $oSector->descripcion = $sectorFormRequest->input('descripcion');
                $oSector->coordenadas_poligono = $sectorFormRequest->input('coordenadas_poligono');
                $oSector->catalogo_id = $sectorFormRequest->input('catalogo_id');
             
                if($oSector->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$oSector->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$oSector->descripcion.' no pudo ser guardado</div>');
                }
                return redirect('catalogos/sectores');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El sector seleccionado no existe</div>');
                return redirect('catalogos/sectores');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un sector para poder actualizar su información</div>');
            return redirect('catalogos/sectores');
        }
    }

    /**
     * Metodo que elimina un sector del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/sectores/eliminar-sector
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La administración de catalogos se debe realizar en el sistema SICEDB</div>');
        return redirect('home');

        if(!Usuario::verificaPermiso('catalogos/sectores/eliminar-sector')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSector = Catalogo::find($iId);
            if($oSector){
                $nombre = $oSector->descripcion;
                $aBarrios = $oSector->catalogos;
                
                if( ! $aBarrios->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$nombre.' no pudo ser eliminado debido a que tiene barrios relacionados</div>');
                    return redirect('catalogos/sectores');
                }
                
                if($oSector->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sector '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('catalogos/sectores');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El sector seleccionado no existe</div>');
                return redirect('catalogos/sectores');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un sector para poder eliminarlo</div>');
            return redirect('catalogos/sectores');
        }
    }
}
