<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Permiso;
use SICE\Modulo;
use SICE\Usuario;
use SICE\Http\Requests\UsuarioFormRequest;
use SICE\Http\Requests\UsuarioUpdateFormRequest;

/**
* Clase para administración de usuarios del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Autenticacion/Autorizacion
*/

class UsuariosController extends Controller
{
    /**
    * Metodo para mostrar la lista de usuarios del sistema, tambien permite buscar un usuario en particular
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/usuarios/lista-de-usuarios
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/lista-de-usuarios')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = array(
                'listaUsuarios' => Usuario::buscarUsuario($request->input('valor_busqueda')),
                'menuActivo' => 'seguridades',
                'titulo' => 'Lista de Usuarios',
        );
        return view('auth.usuarios.listaUsuarios', $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo usuario en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/usuarios/agregar-usuario
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/agregar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = array(
                'listaModulos' => Modulo::has('permisos')->get(),
                'menuActivo' => 'seguridades',
                'titulo' => 'Agregar Usuario',
        );
        return view('auth.usuarios.crearEditarUsuario', $aData);
    }

    /**
    * Metodo para guardar un nuevo usuario en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/usuarios/agregar-usuario
    * @method POST
    * @param  \SICE\Http\Requests\UsuarioFormRequest  $usuarioFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(UsuarioFormRequest $usuarioFormRequest)
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/agregar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $oUsuario = new Usuario;
        $oUsuario->nombre = $usuarioFormRequest->input('nombre');
        $oUsuario->apellido = $usuarioFormRequest->input('apellido');
        $oUsuario->email = $usuarioFormRequest->input('email');
        $oUsuario->password = $usuarioFormRequest->input('password');
        $oUsuario->tipo = $usuarioFormRequest->input('tipo');
        $oUsuario->estado = $usuarioFormRequest->input('estado');
        
        if ($usuarioFormRequest->hasFile('avatar')){
            if ($usuarioFormRequest->file('avatar')->isValid()){
                $avatar = $usuarioFormRequest->file('avatar');
                if(!is_dir('./resources/images/avatares')){
                    mkdir('./resources/images/avatares',777);
                }
                $i=0;
                $info = explode(".",$avatar->getClientOriginalName());
                $oUsuario->avatar = $avatar->getClientOriginalName();
                while(file_exists('./resources/images/avatares/'.$oUsuario->avatar)){
                    $i++;
                    $oUsuario->avatar= $info[0].$i.".".$info[1];
                }
                $usuarioFormRequest->file('avatar')->move('resources/images/avatares',$oUsuario->avatar);
            }
        }

        if($oUsuario->save()){
            $oUsuario->permisos()->detach();
            if($usuarioFormRequest->input('permisos')){
                foreach($usuarioFormRequest->input('permisos') as $key => $val){
                    // Instanciamos el permiso
                    $oPermiso = Permiso::find($val);
                    // insertamos en la tabla pivot en usuario y el permiso
                    // de esta manera queda asignado el permiso al usuario
                    $oUsuario->permisos()->save($oPermiso);
                }
            }
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' creado satisfactoriamente</div>');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser creado</div>');
        }
        return redirect('seguridades/usuarios');
    }

    /**
    * Metodo para mostrar la informacion de un usuario del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/usuarios/detalle-de-usuario
    * @method POST
    * @param  int $iId
    * @return \Illuminate\Http\Response
    */
    public function show($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/detalle-de-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un usuario del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/usuarios/editar-usuario
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/editar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            if($iId == \Auth::user()->iId){
                return redirect('mi-Perfil');
            }
            $oUsuario = Usuario::find($iId);
            if($oUsuario){
                $aData = array(
                    'listaModulos' => Modulo::has('permisos')->get(),
                    'menuActivo' => 'seguridades',
                    'objUsuario' => $oUsuario,
                    'titulo' => 'Editar Usuario',
                );
                return view('auth.usuarios.crearEditarUsuario', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El usuario seleccionado no existe</div>');
                return redirect('seguridades/usuarios');  
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder editarlo</div>');
            return redirect('seguridades/usuarios');  
        }
    }

    /**
     * Metodo que actualiza la informacion del usuario en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/usuarios/editar-usuario
     * @method POST
     * @param  \SICE\Http\Requests\UsuarioUpdateFormRequest  $usuarioFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioUpdateFormRequest $usuarioFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/editar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        if($iId){
            if($iId == \Auth::user()->iId){
                return Redirect::to('mi-perfil');    
            }
            $oUsuario = Usuario::find($iId);
            if($oUsuario){
                $oUsuario->nombre = $usuarioFormRequest->input('nombre');
                $oUsuario->apellido = $usuarioFormRequest->input('apellido');
                if($usuarioFormRequest->input('password')){
                    $oUsuario->password = $usuarioFormRequest->input('password');
                }
                $oUsuario->estado = $usuarioFormRequest->input('estado');
                $oUsuario->tipo = $usuarioFormRequest->input('tipo');
                
                $avatarAnterior = $oUsuario->avatar;
                if ($usuarioFormRequest->hasFile('avatar')){
                    if ($usuarioFormRequest->file('avatar')->isValid()){
                        $avatar = $avatarFormRequest->file('avatar');
                        if(!is_dir('./resources/images/avatares')){
                            mkdir('./resources/images/avatares',777);
                        }
                        $i=0;
                        $avatar = explode(".",$avatar->getClientOriginalName());
                        $oUsuario->avatar= $avatar->getClientOriginalName();
                        while(file_exists('./resources/images/avatares/'.$oUsuario->avatar)){
                            $i++;
                            $oUsuario->avatar = $info[0].$i.".".$info[1];
                        }
                        $usuarioFormRequest->file('avatar')->move('resources/images/avatares',$oUsuario->avatar);
                    }
                }

                if($oUsuario->save()){
                    if($avatarAnterior && ($avatarAnterior != $oUsuario->avatar)){
                        unlink('./resources/images/avatares/'.$avatarAnterior);
                    }
                }

                $oUsuario->permisos()->detach();
                if($usuarioFormRequest->input('permisos')){
                    foreach($usuarioFormRequest->input('permisos') as $key => $val){
                        // Instanciamos el permiso
                        $oPermiso = Permiso::find($val);
                        // insertamos en la tabla pivot en usuario y el permiso
                        // de esta manera queda asignado el permiso al usuario
                        $oUsuario->permisos()->save($oPermiso);
                    }
                }

                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' actualizado satisfactoriamente</div>');
                return redirect('seguridades/usuarios');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
                return redirect('seguridades/usuarios');  
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder editarlo</div>');
            return redirect('seguridades/usuarios');  
        }
        
    }


    /**
     * Metodo que muestra el formulario de edicion del usuario que esta navegando en el sistema (perfil)
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /mi-perfil
     * @method GET
     * @return \Illuminate\Http\Response
     */
    public function miPerfil(){

        $aData = array(
            'objUsuario' => \Auth::user(),
            'menuActivo' => 'dashboard',
            'titulo' => 'Editar Mis Datos',
        );
        return view('auth.usuarios.editarMisDatos', $aData);
    }


    /**
     * Metodo que actualiza la informacion del usuario que esta navegando en el sistema en la base de datos (guarda datos de perfil)
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /mi-perfil
     * @method POST
     * @param  \SICE\Http\Requests\UsuarioUpdateFormRequest  $usuarioFormRequest
     * @return \Illuminate\Http\Response
     */
    public function miPerfilSave(UsuarioUpdateFormRequest $usuarioFormRequest){

        \Auth::user()->nombre = $usuarioFormRequest->input('nombre');
        \Auth::user()->apellido= $usuarioFormRequest->input('apellido');
        if($usuarioFormRequest->input('password')){
            \Auth::user()->password = $usuarioFormRequest->input('password');
        }

        \Auth::user()->save();

        \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Sus datos han sido actualizados satisfactoriamente</div>');
        return redirect('mi-perfil');
    }



    /**
     * Metodo que elimina un usuario del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/usuarios/eliminar-usuario
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/usuarios/eliminar-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            if($iId == \Auth::user()->iId){
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede eliminar a usted mismo</div>');
                return redirect('seguridades/usuarios');  
            }
            $oUsuario = Usuario::find($iId);
            if($oUsuario){
                
                $aEquipoTrabajo = $oUsuario->equiposTrabajo;
                $aEquipoTrabajoReporta = $oUsuario->equiposTrabajoReporta;
                $aRutas = $oUsuario->rutas;
                $oUsuario->permisos()->detach();
                if( ! $aEquipoTrabajo->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado debido a que esta asignado a uno o mas equipos de trabajo</div>');
                    return redirect('seguridades/usuarios');
                }
                if( ! $aEquipoTrabajoReporta->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado debido a que esta asignado a uno o mas equipos de trabajo</div>');
                    return redirect('seguridades/usuarios');
                }
                if( ! $aRutas->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado debido a que esta asignado a una o más rutas en el sistema.</div>');
                    return redirect('seguridades/usuarios');
                }
                if($oUsuario->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' eliminado satisfactoriamente</div>');
                    return redirect('seguridades/usuarios');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario '.$oUsuario->nombre.' '.$oUsuario->apellido.' / '.$oUsuario->email.' no pudo ser eliminado</div>');
                    return redirect('seguridades/usuarios');
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
                return redirect('seguridades/usuarios');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder eliminarlo</div>');
            return redirect('seguridades/usuarios');
        }
    }


    /**
     * Metodo que cambia el estado de un usuario del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/usuarios/cambiar-estado-usuario
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function cambiaEstado($iId = '')
    {

        if(!Usuario::verificaPermiso('seguridades/usuarios/cambiar-estado-usuario')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            if($iId == 1){
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede cambiar el estado al usuario Super Administrador</div>');
                return redirect('seguridades/usuarios');
            }
            if($iId == \Auth::user()->iId){
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede cambiar de estado a usted mismo</div>');
                return redirect('seguridades/usuarios');  
            }
            $oUsuario = Usuario::find($iId);
            if($oUsuario){
                $oUsuario->estado = $oUsuario->estado == 1 ? 0 : 1;
                $oUsuario->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del usuario '.$oUsuario->nombre.' / '.$oUsuario->email.' satisfactoriamente</div>');
                return redirect('seguridades/usuarios');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usuario seleccionado no existe</div>');
                return redirect('seguridades/usuarios');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un usuario para poder cambiar su estado</div>');
            return redirect('seguridades/usuarios');
        }
    }
}
