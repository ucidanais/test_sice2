<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\TipoNegocioFormRequest;

/**
* Clase para administración de tipos de negocio
* @Autor Raúl Chauvin
* @FechaCreacion  2016/08/08
* @Parametrizacion
*/

class TiposNegocioController extends Controller
{
    /**
    * Metodo para mostrar la lista de tipos de negocio (el metodo acepta tambien el ID del subcanal para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/tipos-de-negocio/lista-de-tipos-de-negocio
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iCanalId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iSubcanalId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaTiposNegocio' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'TiposNegocio', $iSubcanalId),
            'menuActivo' => 'catalogos',
            'iSubcanalId' => $iSubcanalId,
            'titulo' => 'SICE :: Tipos de Negocio',
        ];
        return view("catalogos.tiposNegocio.listaTiposNegocio", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo tipo de negocio
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/tipos-de-negocio/agregar-tipo-de-negocio
    * @method GET
    * @param int $iCanalId
    * @return \Illuminate\Http\Response
    */
    public function create($iSubcanalId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/agregar-tipo-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaCanales = Catalogo::where('contexto', 'Canales')->lists('descripcion','id');
        $iIdCanal = 0;
        if($iSubcanalId){
            $oSubcanal = Catalogo::find($iSubcanalId);
            if($oSubcanal){
                $iIdCanal = $oSubcanal->catalogo_id;
            }
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'canalId' => $iIdCanal,
            'subcanalId' => $iSubcanalId,
            'listaCanales' => $aListaCanales,
            'titulo' => 'SICE :: Crear Nuevo Tipo de Negocio',
        ];
        return view("catalogos.tiposNegocio.crearEditarTipoNegocio", $aData);
    }

    
    /**
    * Metodo para guardar un nuevo tipo de negocio
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/tipos-de-negocio/agregar-tipo-de-negocio
    * @method POST
    * @param  \SICE\Http\Requests\TipoNegocioFormRequest  $tipoNegocioFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(TipoNegocioFormRequest $tipoNegocioFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/agregar-tipo-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oTipoNegocio = new Catalogo;
        $oCatalogoPadre = Catalogo::find($barrioFormRequest->input('catalogo_id'));
        $oTipoNegocio->contexto = 'TiposNegocio';
        $oTipoNegocio->codigo1 = Catalogo::generaCodigo('TiposNegocio', $catalogoPadre->id);
        $oTipoNegocio->codigo2 = $oCatalogoPadre->codigo1;
        $oTipoNegocio->valor2 = $oCatalogoPadre->descripcion;
        $oTipoNegocio->descripcion = $tipoNegocioFormRequest->input('descripcion');
        $oTipoNegocio->catalogo_id = $tipoNegocioFormRequest->input('catalogo_id');
        
        if($oTipoNegocio->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de Negocio '.$oTipoNegocio->descripcion.' guardado satisfactoriamente</div>');
            return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de Negocio '.$oTipoNegocio->descripcion.' no pudo ser guardado</div>');
            return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de un tipo de negocio de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/tipos-de-negocio/editar-tipo-de-negocio
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/editar-tipo-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oTipoNegocio = Catalogo::find($iId);
            if($oTipoNegocio){
                $aListaCanales = Catalogo::where('contexto', 'Canales')->lists('descripcion','id');
                $aData = [
                    'oTipoNegocio' => $oTipoNegocio,
                    'listaCanales' => $aListaCanales,
                    'canalId' => $oTipoNegocio->catalogoPadre->catalogoPadre->id,
                    'subccanalId' => $oTipoNegocio->catalogo_id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Tipo de Negocio',
                ];
                return view('catalogos.tiposNegocio.crearEditarTipoNegocio', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El tipo de negocio seleccionado no existe</div>');
                return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un tipo de negocio para poder editarlo.</div>');
            return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
        }
    }

    
    /**
     * Metodo que actualiza la informacion del tipo de negocio en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/tipos-de-negocio/editar-tipo-de-negocio
     * @method POST
     * @param  \SICE\Http\Requests\TipoNegocioFormRequest  $tipoNegocioFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(TipoNegocioFormRequest $tipoNegocioFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/editar-tipo-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oTipoNegocio = Catalogo::find($iId);
            if($oTipoNegocio){
                $oTipoNegocio->contexto = 'TiposNegocio';
                $oTipoNegocio->descripcion = $tipoNegocioFormRequest->input('descripcion');
             
                if($oTipoNegocio->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de Negocio '.$oTipoNegocio->descripcion.' guardado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de Negocio '.$oTipoNegocio->descripcion.' no pudo ser guardado</div>');
                }
                return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El tipo de negocio seleccionado no existe</div>');
                return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un tipo de negocio para poder actualizar su información</div>');
            return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
        }
    }

    
    /**
     * Metodo que elimina un tipo de negocio del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/tipos-de-negocio/eliminar-tipo-de-negocio
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/tipos-de-negocio/eliminar-tipo-de-negocio')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oTipoNegocio = Catalogo::find($iId);
            if($oTipoNegocio){
                $nombre = $oTipoNegocio->descripcion;

                if($oTipoNegocio->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de negocio '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Tipo de negocio '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El tipo de negocio seleccionado no existe</div>');
                return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un tipo de negocio para poder eliminarlo</div>');
            return redirect('catalogos/tipos-de-negocio/lista-de-tipos-de-negocio');
        }
    }
}
