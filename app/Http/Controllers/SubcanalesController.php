<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\SubcanalFormRequest;

/**
* Clase para administración de subcanales
* @Autor Raúl Chauvin
* @FechaCreacion  2016/08/09
* @Parametrizacion
*/

class SubcanalesController extends Controller
{
    /**
    * Metodo para mostrar la lista de subcanales (el metodo acepta tambien el ID del canal para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/subcanales/lista-de-subcanales
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iCanalId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iCanalId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/lista-de-subcanales')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaSubcanales' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Subcanales', $iCanalId),
            'menuActivo' => 'catalogos',
            'iCanalId' => $iCanalId,
            'titulo' => 'SICE :: Subcanales',
        ];
        return view("catalogos.subcanales.listaSubcanales", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo subcanal
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/subcanales/agregar-subcanal
    * @method GET
    * @param int $iCanalId
    * @return \Illuminate\Http\Response
    */
    public function create($iCanalId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/agregar-subcanal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'canalId' => $iCanalId,
            'listaCanales' => Catalogo::where('contexto', 'Canales')->lists('descripcion','id'),
            'titulo' => 'SICE :: Crear Nuevo Subcanal',
        ];
        return view("catalogos.subcanales.crearEditarSubcanal", $aData);
    }

    
    /**
    * Metodo para guardar una nuevo subcanal
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/subcanales/agregar-subcanal
    * @method POST
    * @param  \SICE\Http\Requests\SubcanalFormRequest  $subcanalFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(SubcanalFormRequest $subcanalFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/agregar-subcanal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oSubcanal = new Catalogo;
        $oCatalogoPadre = Catalogo::find($subcanalFormRequest->input('catalogo_id'));
        $oSubcanal->contexto = 'Subcanales';
        $oSubcanal->codigo1 = Catalogo::generaCodigo('Subcanales', $oCatalogoPadre->id);
        $oSubcanal->codigo2 = $oCatalogoPadre->codigo1;
        $oSubcanal->valor2 = $oCatalogoPadre->descripcion;
        $oSubcanal->descripcion = $subcanalFormRequest->input('descripcion');
        $oSubcanal->catalogo_id = $subcanalFormRequest->input('catalogo_id');
        
        if($oSubcanal->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$oSubcanal->descripcion.' guardado satisfactoriamente</div>');
            return redirect('catalogos/subcanales/lista-de-subcanales');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$oSubcanal->descripcion.' no pudo ser guardado</div>');
            return redirect('catalogos/subcanales/lista-de-subcanales');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de un subcanal de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/subcanales/editar-subcanal
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/editar-subcanal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSubcanal = Catalogo::find($iId);
            if($oSubcanal){
                $aData = [
                    'oSubcanal' => $oSubcanal,
                    'listaCanales' => Catalogo::where('contexto', 'Subcanales')->lists('descripcion','id'),
                    'canalId' => $oSubcanal->catalogo_id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Subcanal',
                ];
                return view('catalogos.subcanales.crearEditarSubcanal', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El subcanal seleccionado no existe</div>');
                return redirect('catalogos/subcanales/lista-de-subcanales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un subcanal para poder editarlo.</div>');
            return redirect('catalogos/subcanales/lista-de-subcanales');
        }
    }

    
    /**
     * Metodo que actualiza la informacion del subcanal en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/subcanales/editar-subcanal
     * @method POST
     * @param  \SICE\Http\Requests\SubcanalFormRequest  $subcanalFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(SubcanalFormRequest $subcanalFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/editar-subcanal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSubcanal = Catalogo::find($iId);
            if($oSubcanal){
                $oSubcanal->contexto = 'Subcanales';
                $oSubcanal->descripcion = $subcanalFormRequest->input('descripcion');
             
                if($oSubcanal->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$oSubcanal->descripcion.' guardado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$oSubcanal->descripcion.' no pudo ser guardado</div>');
                }
                return redirect('catalogos/subcanales/lista-de-subcanales');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El subcanal seleccionado no existe</div>');
                return redirect('catalogos/subcanales/lista-de-subcanales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un subcanal para poder actualizar su información</div>');
            return redirect('catalogos/subcanales/lista-de-subcanales');
        }
    }

    
    /**
     * Metodo que elimina un subcanal del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/subcanales/eliminar-subcanal
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/subcanales/eliminar-subcanal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oSubcanal = Catalogo::find($iId);
            if($oSubcanal){
                $nombre = $oSubcanal->descripcion;
                $aTiposNegocio = $oSubcanal->catalogos;
                
                if( ! $aTiposNegocio->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$nombre.' no puede ser eliminado debido a que tiene tipos de negocio relacionados</div>');
                    return redirect('catalogos/subcanales/lista-de-subcanales');
                }


                if($oSubcanal->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Subcanal '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('catalogos/subcanales/lista-de-subcanales');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El subcanal seleccionado no existe</div>');
                return redirect('catalogos/subcanales/lista-de-subcanales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un subcanal para poder eliminarlo</div>');
            return redirect('catalogos/subcanales/lista-de-subcanales');
        }
    }
}
