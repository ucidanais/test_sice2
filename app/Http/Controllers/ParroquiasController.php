<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;


use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\ParroquiaFormRequest;

/**
* Clase para administración de parroquias
* @Autor Raúl Chauvin
* @FechaCreacion  2016/09/23
* @Parametrizacion
*/

class ParroquiasController extends Controller
{
    /**
    * Metodo para mostrar la lista de parroquias (el metodo acepta tambien el ID de la zona para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/09/23
    *
    * @route /catalogos/parroquias/lista-de-parroquias
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iZonaId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iZonaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/parroquias/lista-de-parroquias')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaParroquias' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Parroquias', $iZonaId),
            'menuActivo' => 'catalogos',
            'iZonaId' => $iZonaId,
            'titulo' => 'SICE :: Parroquias',
        ];
        return view("catalogos.parroquias.listaParroquias", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una nueva Parroquia
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/09/23
    *
    * @route /catalogos/parroquias/agregar-parroquia
    * @method GET
    * @param int $iZonaId
    * @return \Illuminate\Http\Response
    */
    public function create($iZonaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/paroquias/agregar-parroquia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
        $iIdProvincia = 0;
        $iIdCiudad = 0;
        $iIdZona = 0;
        if($iZonaId){
            $oZona = Catalogo::find($iZonaId);
            if($oZona){
                $iIdCiudad = $oZona->catalogoPadre->id;
                $iIdProvincia = $oZona->catalogoPadre->catalogoPadre->id;
            }
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'provinciaId' => $iIdProvincia,
            'ciudadId' => $iIdCiudad,
            'zonaId' => $iZonaId,
            'listaProvincias' => $aListaProvincias,
            'titulo' => 'SICE :: Crear Nueva Parroquia',
        ];
        return view("catalogos.parroquias.crearEditarParroquia", $aData);
    }

    /**
    * Metodo para guardar una nueva parroquia
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/09/23
    *
    * @route /catalogos/parroquias/agregar-parroquia
    * @method POST
    * @param  \SICE\Http\Requests\ParroquiaFormRequest  $parroquiaFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ParroquiaFormRequest $parroquiaFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/parroquias/agregar-paroquia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        

        $oParroquia = new Catalogo;
        $oParroquia->contexto = 'Parroquias';
        $oParroquia->codigo1 = $parroquiaFormRequest->input('codigo1');
        $oParroquia->descripcion = $parroquiaFormRequest->input('descripcion');
        $oParroquia->coordenadas_poligono = $parroquiaFormRequest->input('coordenadas_poligono');
        $oParroquia->catalogo_id = $parroquiaFormRequest->input('catalogo_id');
        
        if($oParroquia->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$oParroquia->descripcion.' guardada satisfactoriamente</div>');
            return redirect('catalogos/parroquias/lista-de-parroquias/'.$oParroquia->catalogo_id);
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$oParroquia->descripcion.' no pudo ser guardada</div>');
            return redirect('catalogos/parroquias/lista-de-parroquias/'.$oParroquia->catalogo_id);
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de una parroquia de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/09/23
     *
     * @route /catalogos/parroquias/editar-parroquia
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/parroquias/editar-parroquia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oParroquia = Catalogo::find($iId);
            if($oParroquia){
                $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
                $aData = [
                    'oParroquia' => $oParroquia,
                    'listaProvincias' => $aListaProvincias,
                    'provinciaId' => $oParroquia->catalogoPadre->catalogoPadre->catalogoPadre->id,
                    'ciudadId' => $oParroquia->catalogoPadre->catalogoPadre->id,
                    'zonaId' => $oParroquia->catalogoPadre->id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Parroquia',
                ];
                return view('catalogos.parroquia.crearEditarParroquia', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La parroquia seleccionada no existe</div>');
                return redirect('catalogos/parroquias/lista-de-parroquias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una paroquia para poder editarla.</div>');
            return redirect('catalogos/parroquias/lista-de-parroquias');
        }
    }

    /**
     * Metodo que actualiza la informacion de la parroquia en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/09/23
     *
     * @route /catalogos/parroquias/editar-parroquia
     * @method POST
     * @param  \SICE\Http\Requests\ParroquiaFormRequest  $parroquiaFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ParroquiaFormRequest $parroquiaFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/parroquias/editar-parroquia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oParroquia = Catalogo::find($iId);
            if($oParroquia){
                $oParroquia->contexto = 'Parroquias';
                $oParroquia->codigo1 = $parroquiaFormRequest->input('codigo1');
                $oParroquia->descripcion = $parroquiaFormRequest->input('descripcion');
                $oParroquia->coordenadas_poligono = $parroquiaFormRequest->input('coordenadas_poligono');
                $oParroquia->catalogo_id = $parroquiaFormRequest->input('catalogo_id');
             
                if($oParroquia->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$oParroquia->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$oParroquia->descripcion.' no pudo ser guardada</div>');
                }
                return redirect('catalogos/parroquias/lista-de-parroquias/'.$oParroquia->catalogo_id);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La parroquia seleccionada no existe</div>');
                return redirect('catalogos/parroquias/lista-de-parroquias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una parroquia para poder actualizar su información</div>');
            return redirect('catalogos/parroquias/lista-de-parroquias');
        }
    }

    /**
     * Metodo que elimina una parroquia del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/09/23
     *
     * @route /catalogos/parroquias/eliminar-parroquia
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/parroquias/eliminar-parroquia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oParroquia = Catalogo::find($iId);
            if($oParroquia){
                $nombre = $oParroquia->descripcion;
                $aBarrios = $oParroquia->catalogos;
                
                if( ! $aBarrios->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$nombre.' no pudo ser eliminada debido a que tiene barrios relacionados</div>');
                    return redirect('catalogos/parroquias/lista-de-parroquias/'.$oParroquia->catalogo_id);
                }
                
                if($oParroquia->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$nombre.' eliminada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Parroquia '.$nombre.' no pudo ser eliminada</div>');
                }
                return redirect('catalogos/parroquias/lista-de-parroquias');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El sector seleccionado no existe</div>');
                return redirect('catalogos/parroquias/lista-de-parroquias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un sector para poder eliminarlo</div>');
            return redirect('catalogos/parroquias/lista-de-parroquias');
        }
    }
}
