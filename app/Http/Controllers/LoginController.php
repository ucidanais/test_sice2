<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Requests\LoginFormRequest;

/**
* Clase para manejo de login en el sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Autenticacion/Autorizacion
*/

class LoginController extends Controller
{
    
	/**
    * Metodo para mostrar el formulario de login del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/29
    *
    * @route /login
    * @method GET
    * @return \Illuminate\Http\Response
    */

    public function login(){
        // Si el usuario tiene su sesion iniciada lo llevo directo al home sin pasar por el formulario de login
        if(\Auth::check()){
            return redirect('home');
        }
        return view('auth.login')->withTitulo('SICE :: Login');
    }

    /**
    * Metodo para validar las credenciales del usuario luego de que ha llenado el formulario de login
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/29
    *
    * @route /login
    * @method POST
    * @param  \SICE\Http\Requests\LoginFormRequest  $loginFormRequest
    * @return \Illuminate\Http\Response
    */
    public function validaLogin(LoginFormRequest $loginFormRequest){
        
        // Guardamos las credenciales en un array
            // este array lleva los valores de nombre de usuario y clave de acceso
            // las llaves deben llamarse email y password, y los campos de la BD deben tambien llevar este nombre
            $credentials = array(
                'email' => $loginFormRequest->input('email'),
                'password' => $loginFormRequest->input('password'),
            );
            $remember = $loginFormRequest->input('remember') ? TRUE : FALSE;

            // Pasamos los datos al metodo attempt para validar al usuario, en caso de que las credenciales sean correctas
            // el usuario será validado en el sistema, se iniciará su sesion y sera redirigido a la pagina de inicio (home)
            // caso contrario sera redirigido a la pantalla de login con el mensaje correspondiente
            if (\Auth::attempt( $credentials, $remember )) {
                if(\Auth::user()->estado == 1){
                    return redirect('home');
                }else{
                    \Auth::logout();
                    return redirect('/')->with('mensajeLogin', 'su cuenta de usuario se encuentra deshabilitada');
                }
            }else{
                return redirect('/')->with('mensajeLogin', 'Su email o password son incorrectos o su cuenta de usuario no existe.');
            }
    }


    /**
    * Metodo para cerrar sesion en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/29
    *
    * @route /salir
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function cerrarSesion(){
        \Auth::logout();
        return redirect('/');
    }
}
