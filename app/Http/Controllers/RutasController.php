<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Ruta;
use SICE\EquipoTrabajo;
use SICE\ClienteProyecto;
use SICE\Usuario;
use SICE\Http\Requests\RutaFormRequest;

/**
* Clase para administración de rutas en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/03/22
* @Configuracion
*/

class RutasController extends Controller
{
    /**
    * Metodo para mostrar la lista de rutas del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/lista-de-rutas
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/lista-de-rutas')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $aRutas = $oProyectoCliente->rutas;
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'aRutas' => $aRutas,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.rutas.listaRutas", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de familias de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar la lista de rutas de una persona del equipo de trabajo de un proyecto especifico
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/11/07
    *
    * @route /configuracion/proyectos-por-cliente/rutas/lista-de-rutas-json
    * @method GET
    * @param  string sEquipoTrabajoId?
    * @param string iClienteProyectoId?
    * @return \Illuminate\Http\Response
    */
    public function listaJSON($iClienteProyectoId = '', $sEquipoTrabajoId = '')
    {
        if( ! $iClienteProyectoId || ! $sEquipoTrabajoId){
            $aResponse = [
                'status' => false,
                'code' => 404,
                'message' => 'Por favor seleccione el proyecto y el miembro del equipo de trabajo para obtener la lista de rutas',
                'listaRutas' => [],
            ];
            return response()->json($aResponse, 200);
        }

        $oClienteProyecto = ClienteProyecto::find($iClienteProyectoId);
        $oEquipoTrabajo = EquipoTrabajo::find($sEquipoTrabajoId);

        if( ! is_object($oClienteProyecto) || ! is_object($oEquipoTrabajo)){
            $aResponse = [
                'status' => false,
                'code' => 404,
                'message' => 'El proyecto o el miembro del equipo de trabajo no existen o no estan disponibles, por favor seleccione otros',
                'listaRutas' => [],
            ];
            return response()->json($aResponse, 200);
        }

        $aRutas = [];
        $listaRutas = Ruta::where('cliente_proyecto_id', $oClienteProyecto->id)->where('equipo_trabajo_id', $oEquipoTrabajo->id)->get();
        if( ! $listaRutas->isEmpty()){
            foreach($listaRutas as $oRuta){
                $oRutaParse = [];
                $listaEstablecimientos = $oRuta->establecimientos;
                $oRutaParse['UUID'] = $oRuta->id;
                $oRutaParse['Nombre'] = $oRuta->nombre;
                $oRutaParse['Lunes'] = $oRuta->lunes == 1 ? true : false;
                $oRutaParse['Martes'] = $oRuta->martes == 1 ? true : false;
                $oRutaParse['Miercoles'] = $oRuta->miercoles == 1 ? true : false;
                $oRutaParse['Jueves'] = $oRuta->jueves == 1 ? true : false;
                $oRutaParse['Viernes'] = $oRuta->viernes == 1 ? true : false;
                $oRutaParse['Sabado'] = $oRuta->sabado == 1 ? true : false;
                $oRutaParse['Domingo'] = $oRuta->domingo == 1 ? true : false;
                $oRutaParse['PDV'] = [];
                if( ! $listaEstablecimientos->isEmpty()){
                    foreach ($listaEstablecimientos as $oEstablecimiento) {
                        $oEstablecimientoParse = [
                            'UUID' => $oEstablecimiento->id,
                            'codigo' => $oEstablecimiento->codigo,
                            'nombre' => $oEstablecimiento->nombre,
                            'direccionManzana' => $oEstablecimiento->direccion_manzana,
                            'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                            'direccionNumero' => $oEstablecimiento->direccion_numero,
                            'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                            'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                            'administrador' => $oEstablecimiento->administrador,
                            'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                            'emailContacto' => $oEstablecimiento->email_contacto,
                            'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                            'uuidCadena' => $oEstablecimiento->uuid_cadena,
                            'nombreCadena' => $oEstablecimiento->nombre_cadena,
                            'uuidCanal' => $oEstablecimiento->uuid_canal,
                            'nombreCanal' => $oEstablecimiento->nombre_canal,
                            'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                            'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                            'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                            'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                            'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                            'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                            'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                            'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                            'uuidZona' => $oEstablecimiento->uuid_zona,
                            'nombreZona' => $oEstablecimiento->nombre_zona,
                            'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                            'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                            'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                            'nombreBarrio' => $oEstablecimiento->nombre_barrio,
                            'creadoPor' => $oEstablecimiento->creado_por,
                            'ultimaActualizacion' => $oEstablecimiento->ultima_actualizacion,
                            'ordenVisita' => $oEstablecimiento->pivot->orden_visita,
                        ];
                        $oRutaParse['PDV'][] = $oEstablecimientoParse;
                    }
                }
                $aRutas[] = $oRutaParse;
            }
        }

        $aResponse = [
            'status' => true,
            'code' => 200,
            'message' => 'Lista de Rutas del Miembro del Equipo: '.$oEquipoTrabajo->nombre.' '.$oEquipoTrabajo->apellido,
            'listaRutas' => $aRutas,
        ];
        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/agregar-ruta
    * @method GET
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function create($iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/agregar-ruta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $aEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oProyectoCliente->id)->empleado()->get();
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'oRuta' => null,
                    'aEquipoTrabajo' => $aEquipoTrabajo,
                    'equipoTrabajoId' => 0,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.rutas.crearEditarRuta", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/agregar-ruta
    * @method POST
    * @param  SICE\Http\Requests\RutaFormRequest $rutaFormRequest
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function store(RutaFormRequest $rutaFormRequest, $iClienteProyectoId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/agregar-ruta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                $oRuta = new Ruta;
                $oRuta->nombre = $rutaFormRequest->input('nombre');
                $oRuta->lunes = $rutaFormRequest->input('lunes', 0);
                $oRuta->martes = $rutaFormRequest->input('martes', 0);
                $oRuta->miercoles = $rutaFormRequest->input('miercoles', 0);
                $oRuta->jueves = $rutaFormRequest->input('jueves', 0);
                $oRuta->viernes = $rutaFormRequest->input('viernes', 0);
                $oRuta->sabado = $rutaFormRequest->input('sabado', 0);
                $oRuta->domingo = $rutaFormRequest->input('domingo', 0);
                $oRuta->cliente_proyecto_id = $rutaFormRequest->input('cliente_proyecto_id');
                $oRuta->cliente_id = $rutaFormRequest->input('cliente_id');
                $oRuta->proyecto_id = $rutaFormRequest->input('proyecto_id');
                $oRuta->equipo_trabajo_id = $rutaFormRequest->input('equipo_trabajo_id');
                $oRuta->usuario_id = EquipoTrabajo::find($rutaFormRequest->input('equipo_trabajo_id'))->usuario_id;
                if($oRuta->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ruta '.$oRuta->nombre.' agregada exitosamente</div>');
                    return redirect('configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos/'.$oRuta->id);
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ruta '.$oRuta->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                    return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de edición de una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/editar-ruta
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function edit($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/editar-ruta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oRuta = Ruta::find($iId);
                    if($oRuta){
                        $aEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oProyectoCliente->id)->where('tipo', 'E')->get();
                        $aData = [
                            'oClienteProyecto' => $oProyectoCliente,
                            'oCliente' => $oProyectoCliente->cliente,
                            'oProyecto' => $oProyectoCliente->proyecto,
                            'oRuta' => $oRuta,
                            'aEquipoTrabajo' => $aEquipoTrabajo,
                            'equipoTrabajoId' => $oRuta->equipo_trabajo_id,
                            'menuActivo' => 'configuracion',
                            'titulo' => 'SICE :: Configurar Proyecto por cliente',
                        ];
                        return view("configuracion.proyectosPorCliente.rutas.crearEditarRuta", $aData);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos los cambios realizados a la ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/editar-ruta
    * @method POST
    * @param  SICE\Http\Requests\RutaFormRequest $rutaFormRequest
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function update(RutaFormRequest $rutaFormRequest, $iId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/editar-ruta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $iClienteProyectoId = $rutaFormRequest->input('cliente_proyecto_id');

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oRuta = Ruta::find($iId);
                    if($oRuta){
                        $oRuta->nombre = $rutaFormRequest->input('nombre');
                        $oRuta->lunes = $rutaFormRequest->input('lunes', 0);
                        $oRuta->martes = $rutaFormRequest->input('martes', 0);
                        $oRuta->miercoles = $rutaFormRequest->input('miercoles', 0);
                        $oRuta->jueves = $rutaFormRequest->input('jueves', 0);
                        $oRuta->viernes = $rutaFormRequest->input('viernes', 0);
                        $oRuta->sabado = $rutaFormRequest->input('sabado', 0);
                        $oRuta->domingo = $rutaFormRequest->input('domingo', 0);
                        $oRuta->cliente_proyecto_id = $rutaFormRequest->input('cliente_proyecto_id');
                        $oRuta->cliente_id = $rutaFormRequest->input('cliente_id');
                        $oRuta->proyecto_id = $rutaFormRequest->input('proyecto_id');
                        $oRuta->equipo_trabajo_id = $rutaFormRequest->input('equipo_trabajo_id');
                        $oRuta->usuario_id = EquipoTrabajo::find($rutaFormRequest->input('equipo_trabajo_id'))->usuario_id;
                        if($oRuta->save()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ruta '.$oRuta->nombre.' actualizada exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Ruta '.$oRuta->nombre.' no pudo ser actualizada, por favor intentelo nuevamente luego de unos minutos.</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para eliminar una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/rutas/eliminar-ruta
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function destroy($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/rutas/eliminar-ruta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
            if($oProyectoCliente){
                if($iId){
                    $oRuta = Ruta::find($iId);
                    if($oRuta){
                        $oRuta->establecimientos()->detach();
                        if($oRuta->delete()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta "'.$oRuta->nombre.'" ha sido eliminada exitosamente</div>');
                            return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta "'.$oRuta->nombre.'" no puede ser eliminada.Por favor intentelo nuevamente luego de unos minutos o contacte al administrador del sistema</div>');
                            return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                        }
                        return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta para poder eliminarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/rutas/lista-de-rutas/'.$oProyectoCliente->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
