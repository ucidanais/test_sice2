<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\PresentacionProducto;
use SICE\ProductoCadena;
use SICE\ClienteProyecto;
use SICE\Establecimiento;
use SICE\Catalogo;

use SICE\Helpers\JwtAuth;
use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

class ProductosController extends Controller
{
    //
    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }

    /**
     * Metodo para ver la lista de grupos de productos por proyecto del cliente
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/05/15
     *
     * @route /api/v1/productos/lista-de-grupos-de-productos
     * @method GET
     * @param  Illuminate\Http\Request  $request
     * @param string  sClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function gruposProducto(Request $request, $sClienteProyectoId = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        
        if( ! $aJwtAuthResponse['auth']){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'listaGruposProducto' => [],
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        if( ! $sClienteProyectoId){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los grupos de productos asociados al mismo.',
                'listaGruposProducto' => [],
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
                'listaGruposProducto' => [],
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        $aGruposProducto = $oClienteProyecto->gruposProducto;

        if( ! $aGruposProducto->isEmpty()){
            $aGruposProductoParse = [];
            foreach($aGruposProducto as $oGrupoProducto){
                $aGruposProductoParse[] = [
                    'UUID' => $oGrupoProducto->id,
                    'Nombre' => $oGrupoProducto->nombre,
                ];
            }
            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de grupos de producto del proyecto.',
                'listaGruposProducto' => $aGruposProductoParse,
                'errors' => null,
            ];
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
                'listaGruposProducto' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
     * Metodo para ver la lista de productos por proyecto del cliente
     * @Autor Julio Silvera
     * @FechaCreacion  2019/01/25
     *
     * @route /api/v1/productos/lista-de-productos
     * @method GET
     * @param  Illuminate\Http\Request  $request
     * @param string  sClienteProyectoId
     * @param string  sCadenaId
     * @param int  iCompetencia
     * @param int  iGrupoProductoId
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $sClienteProyectoId = '', $sCadenaId = '', $iCompetencia = null, $iGrupoProductoId = null, $sClasificacion = null)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
            if($sClienteProyectoId){
                $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
                if(is_object($oClienteProyecto)){
                    $listaProductos = [];
                    if($iCompetencia === null){
                        $aPresentacionProductos = $oClienteProyecto->presentacionProducto;
                    }else{
                        if($iCompetencia == 1){
                            $aPresentacionProductos = $oClienteProyecto->presentacionProducto()->competencia()->get();
                        }else{
                            $aPresentacionProductos = $oClienteProyecto->presentacionProducto()->marca()->get();
                        }
                    }
                    
                    if( ! $aPresentacionProductos->isEmpty()){
                        foreach($aPresentacionProductos as $oPresentacionProducto){
                            
                            if($sClasificacion){
                                $aProductoCadenas = ProductoCadena::where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->where('clasificacion_establecimiento', 'like', '%'.$sClasificacion.'%')->get();
                            }else{
                                $aProductoCadenas = ProductoCadena::where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->get();    
                            }
                            

                            $banCadena = false;
                            $aProductoCadenasParse = [];
                            if( ! $aProductoCadenas->isEmpty()){
                                foreach($aProductoCadenas as $oProductoCadena){
                                    $oCadena = Catalogo::find($oProductoCadena->codigo_cadena);
                                    if($sCadenaId == $oCadena->id){
                                        $banCadena = true;
                                    }
                                    $oProductoCadenaParse = [
                                        'UUID' => $oCadena->id,
                                        'CodigoCadena' => $oCadena->codigo1,
                                        'NombreCadena' => $oCadena->descripcion,
                                    ];
                                    $aProductoCadenasParse[] = $oProductoCadenaParse;
                                }
                            }
                            //cambio Dana
                            if( ! $sCadenaId || ($banCadena === true)){
                                $oPresentacionProductoParse = [
                                    'UUID' => $oPresentacionProducto->id,
                                    'Nombre' => $oPresentacionProducto->nombre,
                                    'Precio' => $oPresentacionProducto->precio,
                                    'Codigo' => $oPresentacionProducto->contexto,
                                    'EsCompetencia' => $oPresentacionProducto->es_competencia == 1 ? 'SI' : 'NO',
                                    'CompetenciaDeUUID' => $oPresentacionProducto->presentacion_producto_id ? $oPresentacionProducto->presentacionProductoPadre->id : '',
                                    'CompetenciaDeNombre' => $oPresentacionProducto->presentacion_producto_id ? $oPresentacionProducto->presentacionProductoPadre->nombre : '',
                                    'UUIDCategoriaProducto' => $oPresentacionProducto->categoria_producto_id,
                                    'CategoriaProducto' => $oPresentacionProducto->categoriaProducto->nombre,
                                    'UUIDFamiliaProducto' => $oPresentacionProducto->familia_producto_id,
                                    'FamiliaProducto' => $oPresentacionProducto->categoriaProducto->familiaProducto->nombre,
                                    'Cadenas' => $aProductoCadenasParse,
                                ];
                                if($iGrupoProductoId){
                                    $exists = $oPresentacionProducto->gruposProducto->contains($iGrupoProductoId);
                                    if($exists){
                                        $listaProductos[] = $oPresentacionProductoParse;
                                    }
                                }else{
                                    $listaProductos[] = $oPresentacionProductoParse;
                                }
                                
                            }
                        }
                    }
                    $aResponse = [
                        'status' => true,
                        'code' => 200,
                        'message' => 'Lista de Productos',
                        'listaProductos' => $listaProductos,
                        'errors' => null,
                    ];
                }else{
                    $aResponse = [
                        'status' => false,
                        'code' => 400,
                        'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
                        'listaProductos' => [],
                        'errors' => null,
                    ];  
                }
            }else{
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
                    'listaProductos' => [],
                    'errors' => null,
                ];
            }
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'listaProductos' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }


    /**
     * Metodo para obtener un producto por su código de barras
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/07/08
     *
     * @route /api/v1/productos/detalle-de-producto
     * @method GET
     * @param  Illuminate\Http\Request  $request
     * @param string  sClienteProyectoId
     * @param string  sProductoCode
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $sClienteProyectoId = '', $sProductoCode = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        
        if( ! $aJwtAuthResponse['auth']){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oProducto' => null,
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        if( ! $sClienteProyectoId){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los grupos de productos asociados al mismo.',
                'oProducto' => null,
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
                'oProducto' => null,
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }


        $oPresentacionProducto = PresentacionProducto::where('contexto', $sProductoCode)->first();

        if( ! is_object($oPresentacionProducto)){
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'No existe una presentación de producto con ese codigo de barras.',
                'oProducto' => null,
                'errors' => null,
            ];
            return response()->json($aResponse, 200);
        }

        $aProductoCadenas = ProductoCadena::where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->get();
        $aProductoCadenasParse = [];
        if( ! $aProductoCadenas->isEmpty()){
            foreach($aProductoCadenas as $oProductoCadena){
                $oCadena = Catalogo::find($oProductoCadena->codigo_cadena);
                $oProductoCadenaParse = [
                    'UUID' => $oCadena->id,
                    'CodigoCadena' => $oCadena->codigo1,
                    'NombreCadena' => $oCadena->descripcion,
                ];
                $aProductoCadenasParse[] = $oProductoCadenaParse;
            }
        }

        $oPresentacionProductoParse = [
            'UUID' => $oPresentacionProducto->id,
            'Nombre' => $oPresentacionProducto->nombre,
            'Precio' => $oPresentacionProducto->precio,
            'Codigo' => $oPresentacionProducto->contexto,
            'EsCompetencia' => $oPresentacionProducto->es_competencia == 1 ? 'SI' : 'NO',
            'CompetenciaDeUUID' => $oPresentacionProducto->presentacion_producto_id ? $oPresentacionProducto->presentacionProductoPadre->id : '',
            'CompetenciaDeNombre' => $oPresentacionProducto->presentacion_producto_id ? $oPresentacionProducto->presentacionProductoPadre->nombre : '',
            'UUIDCategoriaProducto' => $oPresentacionProducto->categoria_producto_id,
            'CategoriaProducto' => $oPresentacionProducto->categoriaProducto->nombre,
            'UUIDFamiliaProducto' => $oPresentacionProducto->familia_producto_id,
            'FamiliaProducto' => $oPresentacionProducto->categoriaProducto->familiaProducto->nombre,
            'Cadenas' => $aProductoCadenasParse,
        ];

        $aResponse = [
            'status' => true,
            'code' => 200,
            'message' => 'Detalle de la presentacion del producto.',
            'oProducto' => $oPresentacionProductoParse,
            'errors' => null,
        ];
        

        return response()->json($aResponse, 200);
    }
}
