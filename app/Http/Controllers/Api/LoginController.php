<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Usuario;

use SICE\Helpers\JwtAuth;

use Auth;

class LoginController extends Controller
{

    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function jwtLogin(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        
        $sEmail = $request->input('email', null);
        $sPassword = $request->input('password', null);
        $getToken = $request->input('getToken', null);

        return response()->json($oJwtAuth->signup($sEmail, $sPassword, $getToken, $_SERVER['REMOTE_ADDR']), 200);

    }
}
