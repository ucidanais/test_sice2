<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Catalogo;

use SICE\Helpers\JwtAuth;

/**
* Clase que genera respuestas JSON y forma parte del API de la aplicacion, en esta clase se podra obtener informacion de los
* catalogos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @API
*/

class CatalogosController extends Controller
{

    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }
    
	/**
    * Metodo para mostrar la lista de catalogos del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/09
    *
    * @route /api/v1/catalogos/lista
    * @method GET
    * @return \Illuminate\Http\Response
    */

    public function listaCatalogos(){
        if(Catalogo::count()){
            return response()->json(Catalogo::all());
        }else{
            return response()->json([]);
        }
    }


    /**
    * Metodo para mostrar la lista de provincias en formato JSON
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-provincias
    * @method GET
    * @return \Illuminate\Http\Response
    */

    public function listaProvincias(){
    	if(Catalogo::where('contexto', 'Provincias')->count()){
            return response()->json(Catalogo::where('contexto', 'Provincias')->get());
        }else{
            return response()->json([]);
        }
    }

    /**
    * Metodo para mostrar la lista de ciudades en formato JSON, se puede pasar tambien el ID de la provincia para filtrar
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-ciudades
    * @method GET
    * @param int $iIdProvincia
    * @return \Illuminate\Http\Response
    */

    public function listaCiudades($iIdProvincia = ''){
    	if($iIdProvincia){
    		if(Catalogo::where('contexto', 'Ciudades')->where('catalogo_id', $iIdProvincia)->count()){
	            return response()->json(Catalogo::where('contexto', 'Ciudades')->where('catalogo_id', $iIdProvincia)->get());
	        }else{
	            return response()->json([]);
	        }
    	}else{
    		if(Catalogo::where('contexto', 'Ciudades')->count()){
	            return response()->json(Catalogo::where('contexto', 'Ciudades')->get());
	        }else{
	            return response()->json([]);
	        }
    	}
    }


    /**
    * Metodo para mostrar la lista de zonas en formato JSON, se puede pasar tambien el ID de la ciudad para filtrar
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-zonas
    * @method GET
    * @param int $iIdCiudad
    * @return \Illuminate\Http\Response
    */

    public function listaZonas($iIdCiudad = ''){
    	if($iIdCiudad){
    		if(Catalogo::where('contexto', 'Zonas')->where('catalogo_id', $iIdCiudad)->count()){
	            return response()->json(Catalogo::where('contexto', 'Zonas')->where('catalogo_id', $iIdCiudad)->get());
	        }else{
	            return response()->json([]);
	        }
    	}else{
    		if(Catalogo::where('contexto', 'Zonas')->count()){
	            return response()->json(Catalogo::where('contexto', 'Zonas')->get());
	        }else{
	            return response()->json([]);
	        }
    	}
    }


    /**
    * Metodo para mostrar la lista de parroquias en formato JSON, se puede pasar tambien el ID de la zona para filtrar
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-parroquias
    * @method GET
    * @param int $iIdZona
    * @return \Illuminate\Http\Response
    */

    public function listaParroquias($iIdZona = ''){
    	if($iIdZona){
    		if(Catalogo::where('contexto', 'Parroquias')->where('catalogo_id', $iIdZona)->count()){
	            return response()->json(Catalogo::where('contexto', 'Parroquias')->where('catalogo_id', $iIdZona)->get());
	        }else{
	            return response()->json([]);
	        }
    	}else{
    		if(Catalogo::where('contexto', 'Parroquias')->count()){
	            return response()->json(Catalogo::where('contexto', 'Parroquias')->get());
	        }else{
	            return response()->json([]);
	        }
    	}
    }


    /**
    * Metodo para mostrar la lista de barrios en formato JSON, se puede pasar tambien el ID de la zona para filtrar
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-barrios
    * @method GET
    * @param int $iIdParroquia
    * @return \Illuminate\Http\Response
    */

    public function listaBarrios($iIdParroquia = ''){
        if($iIdParroquia){
            if(Catalogo::where('contexto', 'Barrios')->where('catalogo_id', $iIdParroquia)->count()){
                return response()->json(Catalogo::where('contexto', 'Barrios')->where('catalogo_id', $iIdParroquia)->get());
            }else{
                return response()->json([]);
            }
        }else{
            if(Catalogo::where('contexto', 'Barrios')->count()){
                return response()->json(Catalogo::where('contexto', 'Barrios')->get());
            }else{
                return response()->json([]);
            }
        }
    }

    /**
    * Metodo para mostrar la lista de cadenas en formato JSON
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-cadenas
    * @method GET
    * @return \Illuminate\Http\Response
    */

    public function listaCadenas(){
        if(Catalogo::where('contexto', 'Cadenas')->count()){
            return response()->json(Catalogo::where('contexto', 'Cadenas')->get());
        }else{
            return response()->json([]);
        }
    }

    /**
    * Metodo para mostrar la lista de canales en formato JSON
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-canales
    * @method GET
    * @return \Illuminate\Http\Response
    */

    public function listaCanales(){
        if(Catalogo::where('contexto', 'Canales')->count()){
            return response()->json(Catalogo::where('contexto', 'Canales')->get());
        }else{
            return response()->json([]);
        }
    }

    /**
    * Metodo para mostrar la lista de canales en formato JSON
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-canales
    * @method GET
    * @param int $iIdActividadConsumidor
    * @return \Illuminate\Http\Response
    */

    public function listaSubcanales($iIdCanal = ''){
        if($iIdCanal){
            if(Catalogo::where('contexto', 'Subcanales')->where('catalogo_id', $iIdCanal)->count()){
                return response()->json(Catalogo::where('contexto', 'Subcanales')->where('catalogo_id', $iIdCanal)->get());
            }else{
                return response()->json([]);
            }
        }else{
            if(Catalogo::where('contexto', 'Subcanales')->count()){
                return response()->json(Catalogo::where('contexto', 'Subcanales')->get());
            }else{
                return response()->json([]);
            }
        }
    }

    /**
    * Metodo para mostrar la lista de tipos de negocio en formato JSON
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/lista-de-subcanales
    * @method GET
    * @param int $iIdCanal
    * @return \Illuminate\Http\Response
    */

    public function listaTiposNegocio($iIdSubanal = ''){
        if($iIdSubanal){
            if(Catalogo::where('contexto', 'TiposNegocio')->where('catalogo_id', $iIdSubanal)->count()){
                return response()->json(Catalogo::where('contexto', 'TiposNegocio')->where('catalogo_id', $iIdSubanal)->get());
            }else{
                return response()->json([]);
            }
        }else{
            if(Catalogo::where('contexto', 'TiposNegocio')->count()){
                return response()->json(Catalogo::where('contexto', 'TiposNegocio')->get());
            }else{
                return response()->json([]);
            }
        }
    }

    /**
    * Metodo para obtener la informacion de un catalogo en particular en formato JSON de acuerdo a su ID
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /api/v1/catalogos/catalogo
    * @method GET
    * @param int $iId
    * @return \Illuminate\Http\Response
    */

    public function obtenerCatalogo($iId = ''){
    	if(!$iId){
            return response()->json([]);
        }
        $oCatalogo = Catalogo::find($iId);
        $oCatalogo->catalogoPadre;
        $oCatalogo->catalogos;
        if(!$oCatalogo){
            return response()->json([]);
        }
        return response()->json($oCatalogo);
    }

    /**
    * Metodo para obtener la informacion de un catalogo en particular en formato JSON de acuerdo a su Contexto y Codigo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/31
    *
    * @route /api/v1/catalogos/catalogo-por-codigo
    * @method GET
    * @param int $sContexto
    * @param int $sCodigo1
    * @return \Illuminate\Http\Response
    */

    public function obtenerCatalogoPorCodigo($sContexto, $sCodigo1 = ''){
        if(!$sCodigo1){
            return response()->json([]);
        }
        $oCatalogo = Catalogo::where('contexto', $sContexto)->where('codigo1', $sCodigo1)->first();
        if(!$oCatalogo){
            return response()->json([]);
        }
        $oCatalogo->catalogoPadre;
        $oCatalogo->catalogos;
        return response()->json($oCatalogo);
    }


    /**
    * Metodo para guardar un nuevo catalogo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/12/14
    *
    * @route /api/v1/catalogos/agregar-catalogo
    * @method POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function guardarCatalogo(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authorization', null), true);

        if($aJwtAuthResponse['auth']){
            $oCatalogo = new Catalogo;
            if($request->input('codigoPadre')){
                $oCatalogoPadre = Catalogo::where('codigo1', $request->input('codigoPadre'))->first();
                $oCatalogo->catalogo_id = $oCatalogoPadre->id;
            }
            $oCatalogo->contexto = $request->input('contexto');
            $oCatalogo->tipo_dato = $request->input('tipo_dato');
            $oCatalogo->codigo1 = $request->input('codigo1');
            $oCatalogo->codigo2 = $request->input('codigo2');
            $oCatalogo->valor1 = $request->input('valor1');
            $oCatalogo->valor2 = $request->input('valor2');
            $oCatalogo->descripcion = $request->input('descripcion');
            $oCatalogo->coordenadas_poligono = $request->input('coordenadas_poligono');
            if($oCatalogo->save()){
                $aResponse = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Catalogo guardado satisfactoriamente',
                    'oCatalogue' => $oCatalogo,
                    'errors' => null,
                ];
            }else{
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'Catalogo no pudo ser guardado',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
            }
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oCatalogue' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para actualizar un catalogo por su Codigo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/12/14
    *
    * @route /api/v1/catalogos/actualizar-catalogo
    * @method POST
    * @param  \Illuminate\Http\Request  $request
    * @param  string $sContexto
    * @param  string $sCodigoCatalogo
    * @return \Illuminate\Http\Response
    */
    public function actualizarCatalogo(Request $request, $sContexto = '', $sCodigoCatalogo = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authorization', null), true);

        if($aJwtAuthResponse['auth']){
            if(!$sCodigoCatalogo){
                return response()->json(['success' => 0, 'mensaje' => 'Por favor seleccione un catalogo para poder actualizarlo.']);
            }
            $oCatalogo = Catalogo::where('contexto', $sContexto)->where('codigo1', $sCodigoCatalogo)->first();
            if(!$oCatalogo){
                return response()->json(['success' => 0, 'mensaje' => 'El catalogo seleccionado no existe.']);
            }
            $oCatalogoPadre = Catalogo::where('codigo1', $request->input('codigoPadre'))->first();
            $oCatalogo->contexto = $request->input('contexto');
            $oCatalogo->tipo_dato = $request->input('tipo_dato');
            $oCatalogo->codigo1 = $request->input('codigo1');
            $oCatalogo->codigo2 = $request->input('codigo2');
            $oCatalogo->valor1 = $request->input('valor1');
            $oCatalogo->valor2 = $request->input('valor2');
            $oCatalogo->descripcion = $request->input('descripcion');
            $oCatalogo->coordenadas_poligono = $request->input('coordenadas_poligono');
            $oCatalogo->catalogo_id = $oCatalogoPadre->id;
            
            if($oCatalogo->save()){
                $aResponse = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Catalogo actualizado satisfactoriamente',
                    'oCatalogue' => $oCatalogo,
                    'errors' => null,
                ];
            }else{
                $aResponse = [
                    'status' => fale,
                    'code' => 400,
                    'message' => 'Catalogo no pudo ser actualizado',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
            }
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oCatalogue' => null,
                'errors' => null,
            ];
        }
        return response()->json($aResponse, 200);
    }


    /**
    * Metodo para eliminar un catalogo por su CODIGO
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/12/14
    *
    * @route /api/v1/catalogos/eliminar-catalogo
    * @method GET
    * @param  string $sContexto
    * @param  string $sCodigoCatalogo
    * @return \Illuminate\Http\Response
    */
    public function eliminarCatalogo($sContexto = '', $sCodigoCatalogo = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authorization', null), true);

        if($aJwtAuthResponse['auth']){
            if(!$sCodigoCatalogo){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'Por favor seleccione un catalogo para poder eliminarlo.',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
                return response()->json($aResponse, 200);
            }
            $oCatalogo = Catalogo::where('contexto', $sContexto)->where('codigo1', $sCodigoCatalogo)->first();
            if(!$oCatalogo){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'El catalogo seleccionado no existe.',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
                return response()->json($aResponse, 200);
            }

            $aCatalogosHijos = $oCatalogo->catalogos;

            if(count($aCatalogosHijos)){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'El catalogo seleccionado no puede ser eliminado ya que tiene catalogos relacionados.',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
                return response()->json($aResponse, 200);
            }

            if($oCatalogo->delete()){
                $aResponse = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Catalogo eliminado satisfactoriamente.',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
            }else{
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'El Catalogo no pudo ser eliminado.',
                    'oCatalogue' => null,
                    'errors' => null,
                ];
            }
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oCatalogue' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }
}
