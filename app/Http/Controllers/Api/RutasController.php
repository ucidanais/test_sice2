<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Catalogo;

use SICE\Proyecto;
use SICE\Cliente;
use SICE\ClienteProyecto;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\PresentacionProducto;
use SICE\ProductoCadena;
use SICE\GrupoProducto;

use SICE\Establecimiento;
use SICE\ClienteEstablecimiento;
use SICE\ClasificacionEstablecimiento;

use SICE\Ruta;
use SICE\HojaRuta;
use SICE\DetalleRuta;
use SICE\Novedad;
use SICE\Foto;
use SICE\InformacionDetalle;

use SICE\EquipoTrabajo;
use SICE\Usuario;

use SICE\Helpers\JwtAuth;
use SICE\Helpers\TimeFormat;

class RutasController extends Controller
{
    
    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }


    /**
    * Metodo para obtener la lista de rutas del usuario autenticado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/16
    *
    * @route /api/v1/rutas/lista-de-rutas
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $sClienteProyectoId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$aRutas = [];
	        		$listaRutas = Ruta::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->get();
	        		if( ! $listaRutas->isEmpty()){
	        			foreach($listaRutas as $oRuta){
	        				$oRutaParse = [];
	        				$listaEstablecimientos = $oRuta->establecimientos;
	        				$oRutaParse['UUID'] = $oRuta->id;
	        				$oRutaParse['Nombre'] = $oRuta->nombre;
	        				$oRutaParse['Lunes'] = $oRuta->lunes == 1 ? true : false;
	        				$oRutaParse['Martes'] = $oRuta->martes == 1 ? true : false;
	        				$oRutaParse['Miercoles'] = $oRuta->miercoles == 1 ? true : false;
	        				$oRutaParse['Jueves'] = $oRuta->jueves == 1 ? true : false;
	        				$oRutaParse['Viernes'] = $oRuta->viernes == 1 ? true : false;
	        				$oRutaParse['Sabado'] = $oRuta->sabado == 1 ? true : false;
	        				$oRutaParse['Domingo'] = $oRuta->domingo == 1 ? true : false;
	        				$oRutaParse['PDV'] = [];
	        				if( ! $listaEstablecimientos->isEmpty()){
	        					foreach ($listaEstablecimientos as $oEstablecimiento) {
	        						$oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
	        						$oEstablecimientoParse = [
	        							'UUID' => $oEstablecimiento->id,
	        							'codigo' => $oEstablecimiento->codigo,
	        							'nombre' => $oEstablecimiento->nombre,
	        							'direccionManzana' => $oEstablecimiento->direccion_manzana,
								        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
								        'direccionNumero' => $oEstablecimiento->direccion_numero,
								        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
								        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
								        'administrador' => $oEstablecimiento->administrador,
								        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
								        'emailContacto' => $oEstablecimiento->email_contacto,
								        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
								        'uuidCadena' => $oEstablecimiento->uuid_cadena,
								        'nombreCadena' => $oEstablecimiento->nombre_cadena,
								        'uuidCanal' => $oEstablecimiento->uuid_canal,
								        'nombreCanal' => $oEstablecimiento->nombre_canal,
								        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
								        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
								        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
								        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
								        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
								        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
								        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
								        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
								        'uuidZona' => $oEstablecimiento->uuid_zona,
								        'nombreZona' => $oEstablecimiento->nombre_zona,
								        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
								        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
								        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
								        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
								        'clasificacionEstablecimiento' => is_object($oClasificacion) ? $oClasificacion->clasificacion : '', 
								        'creadoPor' => $oEstablecimiento->creado_por,
								        'ultimaActualizacion' => $oEstablecimiento->ultima_actualizacion,
								        'ordenVisita' => $oEstablecimiento->pivot->orden_visita,
	        						];
	        						$oRutaParse['PDV'][] = $oEstablecimientoParse;
	        					}
	        				}
	        				$aRutas[] = $oRutaParse;
	        			}
	        		}
	        		$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
	        		$teamMemberInfo = [];
	        		if(is_object($oEquipoTrabajo)){
	        			$teamMemberInfo = [
	        				'UUID' => $oEquipoTrabajo->id,
					        'nombres' => $oEquipoTrabajo->nombre,
					        'apellidos' => $oEquipoTrabajo->apellido,
					        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
					        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
					        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
	        			];
	        		}
	        		$aResponse = [
		                'status' => true,
		                'code' => 200,
		                'message' => 'Lista de Rutas',
		                'infoMiembroEquipo' => $teamMemberInfo,
		                'listaRutas' => $aRutas,
		                'errors' => null,
		            ];
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
		                'infoMiembroEquipo' => null,
		                'listaRutas' => [],
		                'errors' => null,
		            ];	
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'infoMiembroEquipo' => null,
	                'listaRutas' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'listaRutas' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para generar una hoja de ruta en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/17
    *
    * @route /api/v1/rutas/generar-hoja-de-ruta
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function generateRoadmap(Request $request)
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	$validator = \Validator::make($request->all(), [
		        "ruta_id" => "required|exists:rutas,id",
	            "cliente_proyecto_id" => "required|exists:cliente_proyectos,id,estado,1",
            ],[
                'ruta_id.required' => 'Por favor seleccione una ruta.',
                'ruta_id.exists' => 'No existe ninguna ruta con ese UUID.',
                'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto del cliente al cual se va a asignar esta hoja de ruta.',
                'cliente_proyecto_id.exists' => 'No existe ningun proyecto de este cliente activo con ese ID, por favor seleccione otro.',
            ]);

            if ($validator->fails()) {
                $aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'No se puede generar la hoja de ruta debido a que existen errores en los datos.',
	                'infoMiembroEquipo' => null,
	                'hojaRuta' => null,
	                'errors' => $validator->errors(),
	            ];
                return response()->json($aResponse, 200);
            }

            $sClienteProyectoId = $request->input('cliente_proyecto_id');
            $sRutaId = $request->input('ruta_id');

        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
					$oGrupoProducto = GrupoProducto::where('cliente_proyecto_id', $oClienteProyecto->id)->first();
					$teamMemberInfo = [
						'UUID' => $oEquipoTrabajo->id,
						'nombres' => $oEquipoTrabajo->nombre,
						'apellidos' => $oEquipoTrabajo->apellido,
						'cargoEnProyecto' => $oEquipoTrabajo->cargo,
						'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
						'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
					];
	        		if($sRutaId){
	        			$oRuta = Ruta::find($sRutaId);
	        			if(is_object($oRuta)){
	        				$oHojaRuta = HojaRuta::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->where('fecha', date('Y-m-d'))->first();
							if( ! is_object($oHojaRuta)){
								$listaEstablecimientos = $oRuta->establecimientos;
								$oHojaRuta = new HojaRuta;
								$oHojaRuta->nombre = $oRuta->nombre.' '.TimeFormat::dateShortFormat(date('Y-m-d'));
								$oHojaRuta->fecha = date('Y-m-d');
								$oHojaRuta->visitas_por_hacer = $listaEstablecimientos->count();
								$oHojaRuta->visitas_realizadas = 0;
								$oHojaRuta->cliente_proyecto_id = $oClienteProyecto->id;
								$oHojaRuta->cliente_id = $oClienteProyecto->cliente_id;
								$oHojaRuta->proyecto_id = $oClienteProyecto->proyecto_id;
								$oHojaRuta->equipo_trabajo_id = $oEquipoTrabajo->id;
								$oHojaRuta->usuario_id = $aJwtAuthResponse['identity']->uuid;
								$oHojaRuta->ruta_id = $oRuta->id;
								
								if($oHojaRuta->save()){
									$hojaRuta = [
										'UUID' => $oHojaRuta->id,
										'Nombre' => $oHojaRuta->nombre,
										'Fecha' => $oHojaRuta->fecha,
										'VisitasPorHacer' => $oHojaRuta->visitas_por_hacer,
										'VisitasRealizadas' => $oHojaRuta->visitas_realizadas,
									];
									$detallesRuta = [];
									if( ! $listaEstablecimientos->isEmpty()){
										foreach($listaEstablecimientos as $oEstablecimiento){
											$oDetalleRuta = new DetalleRuta;
											$oDetalleRuta->estado = 0;
											$oDetalleRuta->observaciones = '';
											$oDetalleRuta->orden_visita = $oEstablecimiento->pivot->orden_visita;
											$oDetalleRuta->establecimiento_id = $oEstablecimiento->id;
											$oDetalleRuta->grupo_producto_id = $oGrupoProducto->id;
											$oDetalleRuta->hoja_ruta_id = $oHojaRuta->id;
											$oDetalleRuta->cliente_proyecto_id = $oClienteProyecto->id;
											$oDetalleRuta->cliente_id = $oClienteProyecto->cliente_id;
											$oDetalleRuta->proyecto_id = $oClienteProyecto->proyecto_id;
											$oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
											$oEstablecimientoParse = [
												'UUID' => $oEstablecimiento->id,
												'codigo' => $oEstablecimiento->codigo,
												'nombre' => $oEstablecimiento->nombre,
												'direccionManzana' => $oEstablecimiento->direccion_manzana,
												'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
												'direccionNumero' => $oEstablecimiento->direccion_numero,
												'direccionTransversal' => $oEstablecimiento->direccion_transversal,
												'direccionReferencia' => $oEstablecimiento->direccion_referencia,
												'administrador' => $oEstablecimiento->administrador,
												'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
												'emailContacto' => $oEstablecimiento->email_contacto,
												'geolocalizacion' => $oEstablecimiento->geolocalizacion,
												'uuidCadena' => $oEstablecimiento->uuid_cadena,
												'nombreCadena' => $oEstablecimiento->nombre_cadena,
												'uuidCanal' => $oEstablecimiento->uuid_canal,
												'nombreCanal' => $oEstablecimiento->nombre_canal,
												'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
												'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
												'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
												'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
												'uuidProvincia' => $oEstablecimiento->uuid_provincia,
												'nombreProvincia' => $oEstablecimiento->nombre_provincia,
												'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
												'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
												'uuidZona' => $oEstablecimiento->uuid_zona,
												'nombreZona' => $oEstablecimiento->nombre_zona,
												'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
												'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
												'uuidBarrio' => $oEstablecimiento->uuid_barrio,
												'nombreBarrio' => $oEstablecimiento->nombre_barrio,
												'clasificacion' => is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
											];
											if($oDetalleRuta->save()){
												$oDetalleRutaParse = [
													'UUID' => $oDetalleRuta->id,
													'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
													'OrdenVisita' => $oDetalleRuta->orden_visita,
													'Establecimiento' => $oEstablecimientoParse,
												];
												$detallesRuta[] = $oDetalleRutaParse;
											}
										}
									}
									$hojaRuta['detallesRuta'] = $detallesRuta;
									$aResponse = [
										'status' => true,
										'code' => 200,
										'message' => 'La hoja de ruta ha sido generada exitosamente.',
										'infoMiembroEquipo' => $teamMemberInfo,
										'hojaRuta' => $hojaRuta,
										'errors' => null,
									];
								}else{
									$aResponse = [
										'status' => false,
										'code' => 422,
										'message' => 'La hoja de ruta no pudo ser creada, por favor intentelo nuevamente luego de unos minutos.',
										'infoMiembroEquipo' => null,
										'hojaRuta' => null,
										'errors' => null,
									];
								}
							}else{
								$hojaRuta = [
									'UUID' => $oHojaRuta->id,
									'Nombre' => $oHojaRuta->nombre,
									'Fecha' => $oHojaRuta->fecha,
									'VisitasPorHacer' => $oHojaRuta->visitas_por_hacer,
									'VisitasRealizadas' => $oHojaRuta->visitas_realizadas,
								];
								$aDetalles = $oHojaRuta->detallesRuta;
								if( ! $aDetalles->isEmpty()){
									$detallesRuta = [];
									foreach($aDetalles as $oDetalleRuta){
										$oEstablecimiento = $oDetalleRuta->establecimiento;
										$oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
										if(is_object($oEstablecimiento)){
											$oEstablecimientoParse = [
												'UUID' => $oEstablecimiento->id,
												'codigo' => $oEstablecimiento->codigo,
												'nombre' => $oEstablecimiento->nombre,
												'direccionManzana' => $oEstablecimiento->direccion_manzana,
												'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
												'direccionNumero' => $oEstablecimiento->direccion_numero,
												'direccionTransversal' => $oEstablecimiento->direccion_transversal,
												'direccionReferencia' => $oEstablecimiento->direccion_referencia,
												'administrador' => $oEstablecimiento->administrador,
												'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
												'emailContacto' => $oEstablecimiento->email_contacto,
												'geolocalizacion' => $oEstablecimiento->geolocalizacion,
												'uuidCadena' => $oEstablecimiento->uuid_cadena,
												'nombreCadena' => $oEstablecimiento->nombre_cadena,
												'uuidCanal' => $oEstablecimiento->uuid_canal,
												'nombreCanal' => $oEstablecimiento->nombre_canal,
												'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
												'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
												'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
												'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
												'uuidProvincia' => $oEstablecimiento->uuid_provincia,
												'nombreProvincia' => $oEstablecimiento->nombre_provincia,
												'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
												'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
												'uuidZona' => $oEstablecimiento->uuid_zona,
												'nombreZona' => $oEstablecimiento->nombre_zona,
												'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
												'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
												'uuidBarrio' => $oEstablecimiento->uuid_barrio,
												'nombreBarrio' => $oEstablecimiento->nombre_barrio,
												'clasificacion' => is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
											];
										}else{
											$oEstablecimientoParse = [];
										}
										$oDetalleRutaParse = [
											'UUID' => $oDetalleRuta->id,
											'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
											'OrdenVisita' => $oDetalleRuta->orden_visita,
											'Establecimiento' => $oEstablecimientoParse,
										];
										$detallesRuta[] = $oDetalleRutaParse;
									}
									$hojaRuta['detallesRuta'] = $detallesRuta;
								}
								$aResponse = [
									'status' => false,
									'code' => 200,
									'message' => 'Su hoja de ruta para este dia ya se encuentra generada por lo que no se puede generar otra.',
									'infoMiembroEquipo' => $teamMemberInfo,
									'hojaRuta' => $hojaRuta,
									'errors' => null,
								];	
							}
	        			}else{
							$aResponse = [
								'status' => false,
								'code' => 400,
								'message' => 'La ruta seleccionada no existe o no esta disponible. Por favor seleccione otra.',
								'infoMiembroEquipo' => $teamMemberInfo,
								'hojaRuta' => null,
								'errors' => null,
							];	
						}
	        		}else{
						$aResponse = [
							'status' => false,
							'code' => 400,
							'message' => 'Por favor seleccione una ruta para generar su hoja de ruta',
							'infoMiembroEquipo' => $teamMemberInfo,
							'hojaRuta' => null,
							'errors' => null,
						];	
					}
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'No existe un proyecto asignado a este cliente ',
		                'infoMiembroEquipo' => null,
		                'hojaRuta' => null,
		                'errors' => null,
		            ];
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'infoMiembroEquipo' => null,
	                'hojaRuta' => null,
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'hojaRuta' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para listar las hojas de ruta del usuario que esta navegando
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/17
    *
    * @route /api/v1/rutas/lista-hojas-de-ruta
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function roadmapsList(Request $request, $sClienteProyectoId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
	        		$teamMemberInfo = [
        				'UUID' => $oEquipoTrabajo->id,
				        'nombres' => $oEquipoTrabajo->nombre,
				        'apellidos' => $oEquipoTrabajo->apellido,
				        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
				        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
				        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
        			];
        			$aHojasRuta = $oClienteProyecto->hojasRuta()->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->orderBy('fecha', 'desc')->take(10)->get();
        			$hojasRuta = [];
        			if( ! $aHojasRuta->isEmpty()){
        				foreach($aHojasRuta as $oHojaRuta){
        					$oHojaRutaParse = [
        						'UUID' => $oHojaRuta->id,
					        	'Nombre' => $oHojaRuta->nombre,
					        	'Fecha' => $oHojaRuta->fecha,
					        	'VisitasPorHacer' => $oHojaRuta->visitas_por_hacer,
					        	'VisitasRealizadas' => $oHojaRuta->visitas_realizadas,
        					];
        					$hojasRuta[] = $oHojaRutaParse;
        				}
        			}
        			$aResponse = [
		                'status' => true,
		                'code' => 200,
		                'message' => 'Lista de Hojas de Ruta.',
		                'infoMiembroEquipo' => $teamMemberInfo,
		                'hojasRuta' => $hojasRuta,
		                'errors' => null,
		            ];
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'Lista de Hojas de Ruta.',
		                'infoMiembroEquipo' => null,
		                'hojasRuta' => [],
		                'errors' => null,
		            ];
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'infoMiembroEquipo' => null,
	                'hojasRuta' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'hojasRuta' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para ver el detalle de una hoja de ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/rutas/detalle-hoja-de-ruta
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @param string  sHojaRutaId
    * @return \Illuminate\Http\Response
    */
    public function roadmapDetail(Request $request, $sClienteProyectoId = '', $sHojaRutaId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
	        		$teamMemberInfo = [
        				'UUID' => $oEquipoTrabajo->id,
				        'nombres' => $oEquipoTrabajo->nombre,
				        'apellidos' => $oEquipoTrabajo->apellido,
				        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
				        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
				        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
        			];
        			if($sHojaRutaId){
        				$oHojaRuta = HojaRuta::find($sHojaRutaId);
        				if(is_object($oHojaRuta)){
        					$hojaRuta = [
					        	'UUID' => $oHojaRuta->id,
					        	'Nombre' => $oHojaRuta->nombre,
					        	'Fecha' => $oHojaRuta->fecha,
					        	'VisitasPorHacer' => $oHojaRuta->visitas_por_hacer,
					        	'VisitasRealizadas' => $oHojaRuta->visitas_realizadas,
					        ];
				        	$detallesRuta = $oHojaRuta->detallesRuta()->orderBy('orden_visita', 'asc')->get();
				        	$aDetallesRuta = [];
				        	if( ! $detallesRuta->isEmpty()){
				        		foreach($detallesRuta as $oDetalleRuta){
				        			$oEstablecimiento = $oDetalleRuta->establecimiento;
				        			$oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
							        $oEstablecimientoParse = [
							        	'UUID' => $oEstablecimiento->id,
	        							'codigo' => $oEstablecimiento->codigo,
	        							'nombre' => $oEstablecimiento->nombre,
	        							'direccionManzana' => $oEstablecimiento->direccion_manzana,
								        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
								        'direccionNumero' => $oEstablecimiento->direccion_numero,
								        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
								        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
								        'administrador' => $oEstablecimiento->administrador,
								        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
								        'emailContacto' => $oEstablecimiento->email_contacto,
								        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
								        'uuidCadena' => $oEstablecimiento->uuid_cadena,
								        'nombreCadena' => $oEstablecimiento->nombre_cadena,
								        'uuidCanal' => $oEstablecimiento->uuid_canal,
								        'nombreCanal' => $oEstablecimiento->nombre_canal,
								        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
								        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
								        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
								        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
								        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
								        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
								        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
								        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
								        'uuidZona' => $oEstablecimiento->uuid_zona,
								        'nombreZona' => $oEstablecimiento->nombre_zona,
								        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
								        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
								        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
								        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
								        'clasificacion' => is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
							        ];
							        $oDetalleRutaParse = [
						        		'UUID' => $oDetalleRuta->id,
						        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
						        		'Observaciones' => $oDetalleRuta->observaciones,
						        		'OrdenVisita' => $oDetalleRuta->orden_visita,
						        		'FechaVisita' => $oDetalleRuta->fecha_visita,
						        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
						        		'HoraSalida' => $oDetalleRuta->hora_salida,
						        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
						        		'Establecimiento' => $oEstablecimientoParse,
						        	];
						        	$aDetallesRuta[] = $oDetalleRutaParse;
				        		}
				        	}
				        	$hojaRuta['detallesRuta'] = $aDetallesRuta;
				        	$aResponse = [
				                'status' => true,
				                'code' => 200,
				                'message' => 'Detalle de Hoja de Ruta.',
				                'infoMiembroEquipo' => $teamMemberInfo,
				                'hojaRuta' => $hojaRuta,
				                'errors' => null,
				            ];
        				}else{
        					$aResponse = [
				                'status' => false,
				                'code' => 400,
				                'message' => 'La hoja de ruta seleccionada no existe.',
				                'infoMiembroEquipo' => $teamMemberInfo,
				                'hojaRuta' => null,
				                'errors' => null,
				            ];	
        				}
        			}else{
        				$aResponse = [
			                'status' => false,
			                'code' => 400,
			                'message' => 'Por favor seleccione una hoja de ruta para poder ver su detalle.',
			                'infoMiembroEquipo' => $teamMemberInfo,
			                'hojaRuta' => null,
			                'errors' => null,
			            ];	
        			}
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'Lista de Hojas de Ruta.',
		                'infoMiembroEquipo' => null,
		                'hojaRuta' => null,
		                'errors' => null,
		            ];
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'infoMiembroEquipo' => null,
	                'hojasRuta' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'hojasRuta' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para actualizar la informacion de un detalle de hoja de ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/rutas/actualizar-detalle
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @param string  sDetalleRutaId
    * @return \Illuminate\Http\Response
    */
    public function roadmapUpdate(Request $request, $sClienteProyectoId = '', $sDetalleRutaId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
	        		$teamMemberInfo = [
        				'UUID' => $oEquipoTrabajo->id,
				        'nombres' => $oEquipoTrabajo->nombre,
				        'apellidos' => $oEquipoTrabajo->apellido,
				        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
				        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
				        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
        			];
        			if($sDetalleRutaId){
        				$oDetalleRuta = DetalleRuta::find($sDetalleRutaId);
        				if(is_object($oDetalleRuta)){
							$banDetalle = false;
        					if($request->observaciones){
        						$oDetalleRuta->observaciones = $request->observaciones;
        						$banDetalle = true;
        					}
        					if($request->fecha_visita){
        						$oDetalleRuta->fecha_visita = $request->fecha_visita;
        						$banDetalle = true;
        					}
        					if($request->hora_entrada){
        						$oDetalleRuta->hora_entrada = $request->hora_entrada;
        						$banDetalle = true;
        					}
        					if($request->hora_salida){
        						$oDetalleRuta->hora_salida = $request->hora_salida;
        						$banDetalle = true;
        					}
        					if($request->ubicacion){
        						$oDetalleRuta->ubicacion = $request->ubicacion;
        						$banDetalle = true;
        					}
        					if($banDetalle){
        						$oDetalleRuta->estado = 1;
        					}
        					$oEstablecimiento = $oDetalleRuta->establecimiento;
        					$oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
					        $oEstablecimientoParse = [
					        	'UUID' => $oEstablecimiento->id,
    							'codigo' => $oEstablecimiento->codigo,
    							'nombre' => $oEstablecimiento->nombre,
    							'direccionManzana' => $oEstablecimiento->direccion_manzana,
						        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
						        'direccionNumero' => $oEstablecimiento->direccion_numero,
						        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
						        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
						        'administrador' => $oEstablecimiento->administrador,
						        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
						        'emailContacto' => $oEstablecimiento->email_contacto,
						        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
						        'uuidCadena' => $oEstablecimiento->uuid_cadena,
						        'nombreCadena' => $oEstablecimiento->nombre_cadena,
						        'uuidCanal' => $oEstablecimiento->uuid_canal,
						        'nombreCanal' => $oEstablecimiento->nombre_canal,
						        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
						        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
						        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
						        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
						        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
						        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
						        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
						        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
						        'uuidZona' => $oEstablecimiento->uuid_zona,
						        'nombreZona' => $oEstablecimiento->nombre_zona,
						        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
						        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
						        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
						        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
						        'clasificacion' => is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
					        ];
					        $oDetalleRutaParse = [
				        		'UUID' => $oDetalleRuta->id,
				        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
				        		'Observaciones' => $oDetalleRuta->observaciones,
				        		'OrdenVisita' => $oDetalleRuta->orden_visita,
				        		'FechaVisita' => $oDetalleRuta->fecha_visita,
				        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
				        		'HoraSalida' => $oDetalleRuta->hora_salida,
				        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
				        		'Establecimiento' => $oEstablecimientoParse,
				        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
				        	];
        					if($oDetalleRuta->save()){
								$oHojaRuta = $oDetalleRuta->hojaRuta;
								$oHojaRuta->visitas_realizadas = DetalleRuta::where('hoja_ruta_id', $oHojaRuta->id)->where('estado', 1)->count();
								$oHojaRuta->save();
								$aResponse = [
					                'status' => true,
					                'code' => 200,
					                'message' => 'El detalle de hoja de ruta ha sido actualizado exitosamente.',
					                'infoMiembroEquipo' => $teamMemberInfo,
					                'detalleRuta' => $oDetalleRutaParse,
					                'errors' => null,
					            ];
        					}else{
        						$oDetalleRuta = DetalleRuta::find($sDetalleRutaId);
        						$oDetalleRutaParse = [
					        		'UUID' => $oDetalleRuta->id,
					        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
					        		'Observaciones' => $oDetalleRuta->observaciones,
					        		'OrdenVisita' => $oDetalleRuta->orden_visita,
					        		'FechaVisita' => $oDetalleRuta->fecha_visita,
					        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
					        		'HoraSalida' => $oDetalleRuta->hora_salida,
					        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
					        		'Establecimiento' => $oEstablecimientoParse,
					        	];
        						$aResponse = [
					                'status' => false,
					                'code' => 400,
					                'message' => 'El detalle de hoja de ruta no pudo ser actualizado. Por favor intentelo nuevamente',
					                'infoMiembroEquipo' => $teamMemberInfo,
					                'detalleRuta' => $oDetalleRutaParse,
					                'errors' => null,
					            ];
        					}
        				}else{
        					$aResponse = [
				                'status' => false,
				                'code' => 400,
				                'message' => 'El detalle de hoja de ruta seleccionado no existe.',
				                'infoMiembroEquipo' => $teamMemberInfo,
				                'detalleRuta' => null,
				                'errors' => null,
				            ];	
        				}
        			}else{
        				$aResponse = [
			                'status' => false,
			                'code' => 400,
			                'message' => 'Por favor seleccione un detalle de hoja de ruta para poder actualizarlo.',
			                'infoMiembroEquipo' => $teamMemberInfo,
			                'detalleRuta' => null,
			                'errors' => null,
			            ];	
        			}
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'Lista de Hojas de Ruta.',
		                'infoMiembroEquipo' => null,
		                'detalleRuta' => null,
		                'errors' => null,
		            ];
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'infoMiembroEquipo' => null,
	                'detalleRuta' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'detalleRuta' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

}
