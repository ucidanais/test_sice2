<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Proyecto;
use SICE\Cliente;
use SICE\ClienteProyecto;
use SICE\EquipoTrabajo;

use SICE\Establecimiento;
use SICE\Encuesta;
use SICE\Pregunta;
use SICE\RespuestaEncuesta;
use SICE\EncuestaFoto;
use SICE\Catalogo;

use SICE\Helpers\JwtAuth;

use Storage;

/**
* Clase para manejo del modulo de encuestas en el API de la aplicacion
* catalogos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2019/03/31
* @API
*/

class EncuestasController extends Controller
{
    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }

    /**
     * Metodo que devuelve la lista de encuestas activas del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/05/21
     *
     * @route /api/v1/encuestas/lista-de-encuestas
     * @method GET
     * @param  Illuminate\Http\Request  $request
     * @param  string  $sItemFiltroId
     * @param  string  $sTipoFiltro
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $sClienteProyectoId = '', $sItemFiltroId = '', $sTipoFiltro = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
            
            if( ! $sClienteProyectoId){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'Por favor seleccione un proyecto para este cliente',
                    'infoMiembroEquipo' => null,
                    'filtroPor' => $sTipoFiltro,
                    'infoItemFiltro' => null,
                    'listaEncuestas' => null,
                    'errores' => null,
                ];
                return response()->json($aResponse, 200);
            }

            $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);

            if( ! is_object($oClienteProyecto)){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'El proyecto seleccionado no existe.',
                    'infoMiembroEquipo' => null,
                    'filtroPor' => $sTipoFiltro,
                    'infoItemFiltro' => null,
                    'listaEncuestas' => null,
                    'errores' => null,
                ];
                return response()->json($aResponse, 200);
            }

            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
            $teamMemberInfo = [
                'UUID' => $oEquipoTrabajo->id,
                'nombres' => $oEquipoTrabajo->nombre,
                'apellidos' => $oEquipoTrabajo->apellido,
                'cargoEnProyecto' => $oEquipoTrabajo->cargo,
                'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
                'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
            ];

            
            
            $aEncuestasList = Encuesta::where('cliente_proyecto_id', $oClienteProyecto->id)->where('tipo', 'N');
            $oItemFiltroParse = [];
            $banCadenaFiltro = false;
            
            if($sItemFiltroId && $sTipoFiltro){
            	$oItemFiltro = Catalogo::find($sItemFiltroId);
                if(is_object($oItemFiltro)){
                    $oItemFiltroParse = [
                                        'UUIDItemFiltro' => $oItemFiltro->id,
                                        'CodigoItemFiltro' => $oItemFiltro->codigo1,
                                        'NombreItemFiltro' => $oItemFiltro->descripcion,
                                    ];
                    if(strtolower($sTipoFiltro) == 'cadena'){
                        //$aEncuestasList = $aEncuestasList->where('cadena_id', $oItemFiltro->id);
                        $banCadenaFiltro = true;
                    }
                    if(strtolower($sTipoFiltro) == 'canal'){
                        $aEncuestasList = $aEncuestasList->where('canal_id', $oItemFiltro->id);
                    }
                    if(strtolower($sTipoFiltro) == 'subcanal'){
                        $aEncuestasList = $aEncuestasList->where('subcanal_id', $oItemFiltro->id);
                    }
                    if(strtolower($sTipoFiltro) == 'provincia'){
                        $aEncuestasList = $aEncuestasList->where('provincia_id', $oItemFiltro->id);
                    }
                    if(strtolower($sTipoFiltro) == 'ciudad'){
                        $aEncuestasList = $aEncuestasList->where('ciudad_id', $oItemFiltro->id);
                    }
                }
            }
            $aEncuestasList = $aEncuestasList->orderBy('nombre', 'asc')->get();
            

            $aEncuestasListParse = [];
            if( ! $aEncuestasList->isEmpty()){
                foreach($aEncuestasList as $oEncuesta){
                    $exists = true;
                    if($banCadenaFiltro){
                        $exists = $oEncuesta->cadenas->contains($sItemFiltroId);
                    }

                    if($exists){
                        $oEncuestaParse = [
                            'UUID' => $oEncuesta->id,
                            'Nombre' => $oEncuesta->nombre,
                        ];
                        $aEncuestasListParse[] = $oEncuestaParse;
                    }
                }
            }

            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de Encuestas.',
                'infoMiembroEquipo' => null,
                'filtroPor' => $sTipoFiltro,
                'infoItemFiltro' => $oItemFiltroParse,
                'listaEncuestas' => $aEncuestasListParse,
                'errores' => null,
            ];
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'filtroPor' => $sTipoFiltro,
                'listaEncuestas' => null,
                'errores' => null,
            ];
        }
        return response()->json($aResponse, 200);
    }


    /**
     * Metodo que devuelve la lista de encuestas de supervisores activas del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2018/05/21
     *
     * @route /api/v1/encuestas/lista-de-encuestas-supervisores
     * @method GET
     * @param  Illuminate\Http\Request  $request
     * @param  string  $sItemFiltroId
     * @param  string  $sTipoFiltro
     * @return \Illuminate\Http\Response
     */
    public function supervisores(Request $request, $sClienteProyectoId = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
            
            if( ! $sClienteProyectoId){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'Por favor seleccione un proyecto para este cliente',
                    'infoMiembroEquipo' => null,
                    'listaEncuestas' => null,
                    'errores' => null,
                ];
                return response()->json($aResponse, 200);
            }

            $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);

            if( ! is_object($oClienteProyecto)){
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'El proyecto seleccionado no existe.',
                    'infoMiembroEquipo' => null,
                    'listaEncuestas' => null,
                    'errores' => null,
                ];
                return response()->json($aResponse, 200);
            }

            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
            $teamMemberInfo = [
                'UUID' => $oEquipoTrabajo->id,
                'nombres' => $oEquipoTrabajo->nombre,
                'apellidos' => $oEquipoTrabajo->apellido,
                'cargoEnProyecto' => $oEquipoTrabajo->cargo,
                'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
                'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
            ];

            
            
            $aEncuestasList = Encuesta::where('cliente_proyecto_id', $oClienteProyecto->id)->where('tipo', 'S')->orderBy('nombre', 'asc')->get();

            $aEncuestasListParse = [];
            
            if( ! $aEncuestasList->isEmpty() && $oEquipoTrabajo->cargo == 'Supervisor'){
                foreach($aEncuestasList as $oEncuesta){
                    $oEncuestaParse = [
                        'UUID' => $oEncuesta->id,
                        'Nombre' => $oEncuesta->nombre,
                    ];
                    $aEncuestasListParse[] = $oEncuestaParse;
                }
            }

            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de Encuestas.',
                'infoMiembroEquipo' => $teamMemberInfo,
                'listaEncuestas' => $aEncuestasListParse,
                'errores' => null,
            ];
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'listaEncuestas' => null,
                'errores' => null,
            ];
        }
        return response()->json($aResponse, 200);
    }


    /**
     * Metodo que devuelve la lista de preguntas de la encuesta
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/03/31
     *
     * @route /api/v1/encuestas/preguntas
     * @param string  $sPollId?
     * @param  Illuminate\Http\Request  $request
     * @method GET
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request, $sPollId = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){
            if( ! $sPollId){
                $aResponse = [
                    'status' => false,
                    'code' => 404,
                    'message' => 'El UUID de la encuesta es obligatorio',
                    'encuesta' => null,
                    'preguntas' => [],
                ];
                return response()->json($aResponse, 200);
            }
            $oPoll = Encuesta::find($sPollId);
            
            if( ! is_object($oPoll)){
                $aResponse = [
                    'status' => false,
                    'code' => 404,
                    'message' => 'No existe ninguna encuesta con el UUID: '.$sPollId.'.',
                    'encuesta' => null,
                    'preguntas' => [],
                ];
                return response()->json($aResponse, 200);
            }

            $aQuestionsList = $oPoll->preguntas()->orderBy('orden', 'asc')->get();

            $aQuestions = [];
            if(! $aQuestionsList-> isEmpty()){
                foreach ($aQuestionsList as $oPregunta) {   
                    $aQuestions[] = [
                        'UUID' => $oPregunta->id,
                        'ordenPregunta' => $oPregunta->orden,
                        'pregunta' => $oPregunta->pregunta,
                        'tipoDato' => $oPregunta->tipo_dato,
                        'opcionMultiple' => $oPregunta->opcion_multiple,
                        'opciones' => $oPregunta->opciones ? $oPregunta->opciones : [],
                    ];
                }
            }

            $oPollParse = [
                'UUID' => $oPoll->id,
                'nombreEncuesta' => $oPoll->nombre,
                'fechaCreacion' => date('Y-m-d H:i:s', strtotime($oPoll->created_at)),
                'ultimaModificacion' => date('Y-m-d H:i:s', strtotime($oPoll->updated_at)),
            ];

            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de preguntas de la encuesta: '.$oPoll->nombre.'.',
                'encuesta' => $oPollParse,
                'preguntas' => $aQuestions,
            ];

        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error de autenticacion.',
                'encuesta' => null,
                'preguntas' => [],
            ];
        }
        return response()->json($aResponse, 200);
    }


    /**
     * Metodo para almacenar en la BD la informacion de la encuesta obtenida en el locar.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/03/31
     *
     * @route /api/v1/encuestas/guardar-encuesta
     * @method POST
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){

            $validator = \Validator::make($request->all(), [
                //'establecimiento_id' => 'required|exists:establecimientos,id',
                //'establecimiento_id' => 'exists:establecimientos,id',
		        'cliente_proyecto_id' => 'required|exists:cliente_proyectos,id,estado,1',
            ],[
               	//'establecimiento_id.required' => 'Por favor seleccione el establecimiento.',
               	//'establecimiento_id.exists' => 'El establecimiento seleccionado no existe',
		        'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto', 
		        'cliente_proyecto_id.exists' => 'El proyecto seleccionado no existe', 
            ]);

            if ($validator->fails()) {
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'No se puede guardar la información de la encuesta debido a que existen errores en los datos: ',
                    'oEstablecimiento' => null,
                    'respuestas' => [],
                    'erroresRespuestas' => [],
                    'errores' => $validator->errors(),
                ];
                return response()->json($aResponse, 200);
            }


            $oEstablecimiento = $request->establecimiento_id ? Establecimiento::find($request->establecimiento_id) : null;

            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);
            
            $json = $request->input('respuestas', null);
            $listaRespuestas = [];
            $erroresRespuestas = [];
            $ahora = date('Y-m-d');
            if( ! is_null($json)){
                $respuestas = json_decode($json);
                if(is_array($respuestas)){
                    if(count($respuestas)){
                        foreach($respuestas as $respuesta){
                            $oPregunta = Pregunta::find($respuesta->preguntaId);
                            if(is_object($oPregunta)){
                                /*
                                $oRespuesas = RespuestaEncuesta::where('encuesta_id', $oPregunta->encuesta->id)
                                            ->where('establecimiento_id', $oEstablecimiento->id)
                                            ->whereBetween('created_at', [$ahora.' 00:00:00', $ahora.' 23:59:59'])->count();

                                if($oRespuesas){
                                    $aResponse = [
                                        'status' => false,
                                        'code' => 201,
                                        'message' => 'Ya esta generada la encuesta de este establecimiento para el día de hoy.',
                                        'oEstablecimiento' => null,
                                        'respuestas' => [],
                                        'erroresRespuestas' => [],
                                        'errores' => [],
                                    ];
                                    return response()->json($aResponse, 200);
                                }
                                */

                                $oPoll = $oPregunta->encuesta;
                                $oRespuestaEncuesta = new RespuestaEncuesta;
                                if($oPregunta->data_type == 'OM'){
                                    $oRespuestaEncuesta->valor = $respuesta->respuesta;
                                }elseif($oPregunta->data_type == 'O2'){
                                    $oRespuestaEncuesta->valor = $respuesta->respuesta;
                                    $oRespuestaEncuesta->valor_aux = json_encode(explode(',', $respuesta->respuesta_aux));
                                }else{
                                    $oRespuestaEncuesta->valor = $respuesta->respuesta;
                                }
                               // $oRespuestaEncuesta->visit_number = $visitNum;
                                $oRespuestaEncuesta->cliente_proyecto_id = $oClienteProyecto->id;
						        $oRespuestaEncuesta->cliente_id = $oClienteProyecto->cliente->id;
						        $oRespuestaEncuesta->proyecto_id = $oClienteProyecto->proyecto->id;
						        if(is_object($oEstablecimiento)){
                                    $oRespuestaEncuesta->establecimiento_id = $oEstablecimiento->id;
                                    $oRespuestaEncuesta->canal_id = $oEstablecimiento->uuid_canal;
                                    $oRespuestaEncuesta->subcanal_id = $oEstablecimiento->uuid_subcanal;
                                    $oRespuestaEncuesta->cadena_id = $oEstablecimiento->uuid_cadena;
                                    $oRespuestaEncuesta->provincia_id = $oEstablecimiento->uuid_provincia;
                                    $oRespuestaEncuesta->ciudad_id = $oEstablecimiento->uuid_ciudad;    
                                }
                                $oRespuestaEncuesta->encuesta_id = $oPregunta->encuesta->id;
                                $oRespuestaEncuesta->pregunta_id = $oPregunta->id;
						        $oRespuestaEncuesta->usuario_id = $aJwtAuthResponse['identity']->uuid;
                                
                                if($oRespuestaEncuesta->save()){
                                    $oRespuestaEncuestaParse = [
                                        'UUID' => $oRespuestaEncuesta->id,
                                        'PreguntaUUID' => $oRespuestaEncuesta->pregunta->id,
                                        'Pregunta' => $oRespuestaEncuesta->pregunta->pregunta,
                                        'Respuesta' => $oRespuestaEncuesta->valor,
                                        'RespuestaAux' => $oRespuestaEncuesta->valor_aux,
                                        'FechaCreacion' => date('Y-m-d H:i:s', strtotime($oRespuestaEncuesta->created_at)),
                                    ];
                                    $listaRespuestas[] = $oRespuestaEncuestaParse;
                                }else{
                                    $answerError = [
                                        'PreguntaUUID' => $oPregunta->id,
                                        'Pregunta' => $oPregunta->question,
                                        'Respuesta' => $respuesta->respuesta,
                                        'ErrorMessage' => 'Esta respuesta no pudo ser guardada.',
                                    ];
                                    $erroresRespuestas[] = $answerError;
                                }
                            }else{
                                $answerError = [
                                    'PreguntaUUID' => $respuesta->preguntaId,
                                    'Pregunta' => null,
                                    'Respuesta' => $respuesta->respuesta,
                                    'ErrorMessage' => 'Esta respuesta no pudo ser guardada debido a que no existe ninguna pregunta con ese UUID.',
                                ];
                                $erroresRespuestas[] = $answerError;
                            }
                        }
                    }
                }
            }
            $oEstablecimientoParse = null;
            if(is_object($oEstablecimiento)){
                $oEstablecimientoParse = [
                    'UUID' => $oEstablecimiento->id,
                    'codigo' => $oEstablecimiento->codigo,
                    'nombre' => $oEstablecimiento->nombre,
                    'direccionManzana' => $oEstablecimiento->direccion_manzana,
                    'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                    'direccionNumero' => $oEstablecimiento->direccion_numero,
                    'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                    'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                    'administrador' => $oEstablecimiento->administrador,
                    'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                    'emailContacto' => $oEstablecimiento->email_contacto,
                    'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                    'uuidCadena' => $oEstablecimiento->uuid_cadena,
                    'nombreCadena' => $oEstablecimiento->nombre_cadena,
                    'uuidCanal' => $oEstablecimiento->uuid_canal,
                    'nombreCanal' => $oEstablecimiento->nombre_canal,
                    'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                    'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                    'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                    'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                    'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                    'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                    'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                    'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                    'uuidZona' => $oEstablecimiento->uuid_zona,
                    'nombreZona' => $oEstablecimiento->nombre_zona,
                    'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                    'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                    'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                    'nombreBarrio' => $oEstablecimiento->nombre_barrio,
                ];    
            }
            
            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Almacenando Encuesta.',
                'oEstablecimiento' => $oEstablecimientoParse,
                'respuestas' => $listaRespuestas,
                'erroresRespuestas' => $erroresRespuestas,
                'errores' => [],
            ];
            
        }else{
            $errors[] = [
                'Authentication' => 'Error de autenticacion.'
            ];
            $aResponse = [
                'status' => false,
                'code' => 401,
                'message' => 'Almacenando Encuesta.',
                'oEstablecimiento' => null,
                'respuestas' => [],
                'erroresRespuestas' => [],
                'errores' => $errors,
            ];
        }
        return response()->json($aResponse, $aResponse['code']);
    }


    /**
     * Metodo para obtener la lista de fotos de una encuesta
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/04/11
     *
     * @route /api/v1/encuestas/fotos/lista
     * @method GET
     * @param  string $sEncuestaId
     * @param  string $sEstablecimientoId
     * @param  string $sClienteProyectoId
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listaFotos(Request $request, $sClienteProyectoId = '', $sEncuestaId = '', $sEstablecimientoId = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){

            if( ! $sEncuestaId || ! $sEstablecimientoId || ! $sClienteProyectoId){
                $aResponse = [
                    'status' => false,
                    'code' => 404,
                    'message' => 'Por favor complete los parametros.',
                    'oEstablecimiento' => $sEstablecimientoId,
                    'oEncuesta' => null,
                    'aFotos' => [],
                    'errores' => [],
                ];
                return response()->json($aResponse, 200);
            }

            
            $oEstablecimiento = Establecimiento::find($request->establecimiento_id);
            $oEncuesta = Encuesta::find($request->encuesta_id);
            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);

            if( ! is_object($oEncuesta) || ! is_object($oEstablecimiento) || ! is_object($oClienteProyecto)){
                $aResponse = [
                    'status' => false,
                    'code' => 404,
                    'message' => 'Por favor complete los parametros correctamente.',
                    'oEstablecimiento' => $oEstablecimiento,
                    'oEncuesta' => null,
                    'aFotos' => [],
                    'errores' => [],
                ];
                return response()->json($aResponse, 200);
            }

            $oEstablecimientoParse = [
                'UUID' => $oEstablecimiento->id,
                'codigo' => $oEstablecimiento->codigo,
                'nombre' => $oEstablecimiento->nombre,
                'direccionManzana' => $oEstablecimiento->direccion_manzana,
                'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                'direccionNumero' => $oEstablecimiento->direccion_numero,
                'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                'administrador' => $oEstablecimiento->administrador,
                'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                'emailContacto' => $oEstablecimiento->email_contacto,
                'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                'uuidCadena' => $oEstablecimiento->uuid_cadena,
                'nombreCadena' => $oEstablecimiento->nombre_cadena,
                'uuidCanal' => $oEstablecimiento->uuid_canal,
                'nombreCanal' => $oEstablecimiento->nombre_canal,
                'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                'uuidZona' => $oEstablecimiento->uuid_zona,
                'nombreZona' => $oEstablecimiento->nombre_zona,
                'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                'nombreBarrio' => $oEstablecimiento->nombre_barrio,
            ];
            
            $oEncuestaParse = [
                'UUID' => $oEncuesta->id,
                'Nombre' => $oEncuesta->nombre,
            ];

            $aEncuestaFotos = EncuestaFoto::where('encuesta_id', $oEncuesta->id)->where('establecimiento_id', $oEstablecimiento->id)->where('cliente_proyecto_id', $oClienteProyecto->id)->get();
            $oEncuestaFotoParse = null;
            $aFotos = [];
            if( ! $aEncuestaFotos->isEmpty()){
                foreach($aEncuestaFotos as $oEncuestaFoto){
                    $oEncuestaFotoParse = [
                        'UUID' => $oEncuestaFoto->id,
                        'Descripcion' => $oEncuestaFoto->descripcion,
                        'FotoURL' => Storage::url('encuestas/'.$oEncuesta->id.'/'.$oEncuestaFoto->foto),
                    ];
                    $aFotos[] = $oEncuestaFotoParse;
                }
            }
            
            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de Fotos.',
                'oEstablecimiento' => $oEstablecimientoParse,
                'oEncuesta' => $oEncuestaParse,
                'oaFotos' => $aFotos,
                'errores' => [],
            ];
            
        }else{
            $errors[] = [
                'Authentication' => 'Error de autenticacion.'
            ];
            $aResponse = [
                'status' => false,
                'code' => 401,
                'message' => 'Almacenando Encuesta.',
                'oEncuesta' => null,
                'oFoto' => null,
                'oEstablecimiento' => null,
                'errores' => $errors,
            ];
        }
        return response()->json($aResponse, 200);
    }

    /**
     * Metodo para almacenar en la BD la informacion de las fotos de la encuesta.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/04/11
     *
     * @route /api/v1/encuestas/fotos/guardar
     * @method POST
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addFoto(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){

            $validator = \Validator::make($request->all(), [
                'encuesta_id' => 'required|exists:encuestas,id',
                'establecimiento_id' => 'required|exists:establecimientos,id',
                'cliente_proyecto_id' => 'required|exists:cliente_proyectos,id,estado,1',
                'foto' => 'required|image',
            ],[
                'encuesta_id.required' => 'Por favor seleccione la encuesta.',
                'encuesta_id.exists' => 'La encuesta seleccionada no existe',
                'establecimiento_id.required' => 'Por favor seleccione el establecimiento.',
                'establecimiento_id.exists' => 'El establecimiento seleccionado no existe',
                'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto', 
                'cliente_proyecto_id.exists' => 'El proyecto seleccionado no existe', 
                'foto.required' => 'Por favor seleccione una foto',
                'foto.image' => 'Unicamente se aceptan archivos de tipo imagen (JPG, PNG, GIF)',
            ]);

            if ($validator->fails()) {
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'No se puede guardar la foto de la encuesta debido a que existen errores en los datos: ',
                    'oEncuesta' => null,
                    'oFoto' => null,
                    'oEstablecimiento' => null,
                    'errores' => $validator->errors(),
                ];
                return response()->json($aResponse, 200);
            }


            $oEstablecimiento = Establecimiento::find($request->establecimiento_id);
            $oEncuesta = Encuesta::find($request->encuesta_id);
            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);

            $oEstablecimientoParse = [
                'UUID' => $oEstablecimiento->id,
                'codigo' => $oEstablecimiento->codigo,
                'nombre' => $oEstablecimiento->nombre,
                'direccionManzana' => $oEstablecimiento->direccion_manzana,
                'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                'direccionNumero' => $oEstablecimiento->direccion_numero,
                'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                'administrador' => $oEstablecimiento->administrador,
                'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                'emailContacto' => $oEstablecimiento->email_contacto,
                'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                'uuidCadena' => $oEstablecimiento->uuid_cadena,
                'nombreCadena' => $oEstablecimiento->nombre_cadena,
                'uuidCanal' => $oEstablecimiento->uuid_canal,
                'nombreCanal' => $oEstablecimiento->nombre_canal,
                'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                'uuidZona' => $oEstablecimiento->uuid_zona,
                'nombreZona' => $oEstablecimiento->nombre_zona,
                'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                'nombreBarrio' => $oEstablecimiento->nombre_barrio,
            ];

            
            $oEncuestaParse = [
                'UUID' => $oEncuesta->id,
                'Nombre' => $oEncuesta->nombre,
            ];

            $oEncuestaFotoParse = null;

            $oEncuestaFoto = new EncuestaFoto;
            $oEncuestaFoto->descripcion = $request->descripcion;
            $oEncuestaFoto->cliente_proyecto_id = $oClienteProyecto->id;
            $oEncuestaFoto->cliente_id = $oClienteProyecto->cliente_id;
            $oEncuestaFoto->proyecto_id = $oClienteProyecto->proyecto_id;
            $oEncuestaFoto->establecimiento_id = $oEstablecimiento->id;
            $oEncuestaFoto->canal_id = $oEncuesta->canal_id;
            $oEncuestaFoto->subcanal_id = $oEncuesta->subcanal_id;
            $oEncuestaFoto->cadena_id = $oEncuesta->cadena_id;
            $oEncuestaFoto->provincia_id = $oEncuesta->provincia_id;
            $oEncuestaFoto->ciudad_id = $oEncuesta->ciudad_id;
            $oEncuestaFoto->encuesta_id = $oEncuesta->id;

            if ($request->hasFile('foto')){
                if ($request->file('foto')->isValid()){
                    $fotografia = $request->file('foto');
                    Storage::disk('public')->put(
                        'encuestas/'.$oEncuesta->id,
                        file_get_contents($fotografia->getRealPath())
                    );
                    $oEncuestaFoto->foto = $fotografia->getClientOriginalName();
                }
            }

            if($oEncuestaFoto->save()){
                $oEncuestaFotoParse = [
                    'UUID' => $oEncuestaFoto->id,
                    'Descripcion' => $oEncuestaFoto->descripcion,
                    'FotoURL' => Storage::url('encuestas/'.$oEncuesta->id.'/'.$oEncuestaFoto->foto),
                ];
            }
            
            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Foto de encuesta almacenada.',
                'oEncuesta' => $oEncuestaParse,
                'oFoto' => $oEncuestaFotoParse,
                'oEstablecimiento' => $oEstablecimientoParse,
                'errores' => [],
            ];
            
        }else{
            $errors[] = [
                'Authentication' => 'Error de autenticacion.'
            ];
            $aResponse = [
                'status' => false,
                'code' => 401,
                'message' => 'Almacenando Encuesta.',
                'oEncuesta' => null,
                'oFoto' => null,
                'oEstablecimiento' => null,
                'errores' => $errors,
            ];
        }
        return response()->json($aResponse, 200);
    }

    /**
     * Metodo para actualizar en la BD la informacion de una foto de la encuesta en particular seleccionada por su ID.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/04/11
     *
     * @route /api/v1/encuestas/fotos/editar
     * @method POST
     * @param  string $sEncuestaFotoID
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function editFoto(Request $request, $sEncuestaFotoID = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){

            $validator = \Validator::make($request->all(), [
                'foto' => 'required|image',
                'descripcion' => 'required'
            ],[
                'foto.required' => 'Por favor seleccione una foto',
                'foto.image' => 'Unicamente se aceptan archivos de tipo imagen (JPG, PNG, GIF)',
                'descripcion.required' => 'Por favor ingrese la descripción de la foto.'
            ]);

            if ($validator->fails()) {
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'No se puede guardar la foto de la encuesta debido a que existen errores en los datos: ',
                    'oEncuesta' => null,
                    'oFoto' => null,
                    'oEstablecimiento' => null,
                    'errores' => $validator->errors(),
                ];
                return response()->json($aResponse, 200);
            }

            if( ! $sEncuestaFotoID){
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'Por favor seleccione una foto para poder editarla: ',
                    'oEncuesta' => null,
                    'oFoto' => null,
                    'oEstablecimiento' => null,
                    'errores' => [],
                ];
                return response()->json($aResponse, 200);
            }

            $oEncuestaFoto = EncuestaFoto::find($sEncuestaFotoID);

            if( ! is_object($sEncuestaFotoID)){
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'La foto seleccionada no existe: ',
                    'oEncuesta' => null,
                    'oFoto' => null,
                    'oEstablecimiento' => null,
                    'errores' => [],
                ];
                return response()->json($aResponse, 200);
            }

            $oEstablecimiento = Establecimiento::find($request->establecimiento_id);
            $oEncuesta = Encuesta::find($request->encuesta_id);
            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);

            $oEstablecimientoParse = [
                'UUID' => $oEstablecimiento->id,
                'codigo' => $oEstablecimiento->codigo,
                'nombre' => $oEstablecimiento->nombre,
                'direccionManzana' => $oEstablecimiento->direccion_manzana,
                'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                'direccionNumero' => $oEstablecimiento->direccion_numero,
                'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                'administrador' => $oEstablecimiento->administrador,
                'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                'emailContacto' => $oEstablecimiento->email_contacto,
                'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                'uuidCadena' => $oEstablecimiento->uuid_cadena,
                'nombreCadena' => $oEstablecimiento->nombre_cadena,
                'uuidCanal' => $oEstablecimiento->uuid_canal,
                'nombreCanal' => $oEstablecimiento->nombre_canal,
                'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                'uuidZona' => $oEstablecimiento->uuid_zona,
                'nombreZona' => $oEstablecimiento->nombre_zona,
                'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                'nombreBarrio' => $oEstablecimiento->nombre_barrio,
            ];

            
            $oEncuestaParse = [
                'UUID' => $oEncuesta->id,
                'Nombre' => $oEncuesta->nombre,
            ];

            $oEncuestaFotoParse = null;

            $oEncuestaFoto->descripcion = $request->descripcion;
            
            $oldFoto = $oEncuestaFoto->foto;

            if ($request->hasFile('foto')){
                if ($request->file('foto')->isValid()){
                    $fotografia = $request->file('foto');
                    Storage::disk('public')->put(
                        'encuestas/'.$oEncuesta->id,
                        file_get_contents($fotografia->getRealPath())
                    );
                    $oEncuestaFoto->foto = 'encuestas/'.$fotografia->getClientOriginalName();
                }
            }

            if($oEncuestaFoto->save()){
                if($oldFoto != $oEncuestaFoto->foto){
                    Storage::disk('public')->delete($oldFoto);
                }
                $oEncuestaFotoParse = [
                    'UUID' => $oEncuestaFoto->id,
                    'Descripcion' => $oEncuestaFoto->descripcion,
                    'FotoURL' => Storage::url('encuestas/'.$oEncuesta->id.'/'.$oEncuestaFoto->foto),
                ];
            }
            
            $aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Foto de encuesta actualizada.',
                'oEncuesta' => $oEncuestaParse,
                'oFoto' => $oEncuestaFotoParse,
                'oEstablecimiento' => $oEstablecimientoParse,
                'errores' => [],
            ];
            
        }else{
            $errors[] = [
                'Authentication' => 'Error de autenticacion.'
            ];
            $aResponse = [
                'status' => false,
                'code' => 401,
                'message' => 'Almacenando Encuesta.',
                'oEncuesta' => null,
                'oFoto' => null,
                'oEstablecimiento' => null,
                'errores' => $errors,
            ];
        }
        return response()->json($aResponse, 200);
    }

    /**
     * Metodo para actualizar en la BD la informacion de una foto de la encuesta en particular seleccionada por su ID.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2019/04/11
     *
     * @route /api/v1/encuestas/fotos/editar
     * @method GET
     * @param  string $sEncuestaFotoID
     * @param  Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteFoto(Request $request, $sEncuestaFotoID = '')
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);
        if($aJwtAuthResponse['auth']){

            if( ! $sEncuestaFotoID){
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'Por favor seleccione una foto para poder eliminarla: ',
                ];
                return response()->json($aResponse, 200);
            }

            $oEncuestaFoto = EncuestaFoto::find($sEncuestaFotoID);

            if( ! is_object($oEncuestaFoto)){
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'La foto seleccionada no existe: ',
                ];
                return response()->json($aResponse, 200);
            }
            $oldFoto = $oEncuestaFoto->foto;
            if($oEncuestaFoto->delete()){
                if($oldFoto != $oEncuestaFoto->foto){
                    Storage::disk('public')->delete($oldFoto);
                }
                $aResponse = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Foto de encuesta eliminada exitosamente.',
                ];
            }else{
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'La foto no pudo ser eliminada por favor intentelo nuevamente luego de unos minutos.',
                ];
            }
            
        }else{
            $errors[] = [
                'Authentication' => 'Error de autenticacion.'
            ];
            $aResponse = [
                'status' => false,
                'code' => 401,
                'message' => 'Almacenando Encuesta.',
            ];
        }
        return response()->json($aResponse, 200);
    }

}
