<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\ClienteProyecto;

use SICE\Helpers\JwtAuth;

class ClienteProyectosController extends Controller
{

    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }

    
    /**
    * Metodo para mostrar la lista de proyectos por cliente configurados en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/08
    *
    * @route /api/v1/proyectos-por-cliente
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	$aResponse = [
	            'status' => true,
	            'code' => 200,
	            'message' => 'Proyectos por Cliente',
	            'clienteProyectoList' => ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')->select('cliente_proyectos.*', 'equipo_trabajo.cargo', 'equipo_trabajo.lider_proyecto')->where('cliente_proyectos.estado', 1)->where('equipo_trabajo.usuario_id', $aJwtAuthResponse['identity']->uuid)->get()->load('cliente')->load('proyecto'),
	        ];
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'clienteProyectoList' => [],
            ];
        }

        return response()->json($aResponse, 200);
    }
}
