<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Controllers\Controller;

use SICE\Establecimiento;
use SICE\ClienteProyecto;
use SICE\ClienteEstablecimiento;
use SICE\Catalogo;

use SICE\Helpers\JwtAuth;

class EstablecimientosController extends Controller
{

	public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }
    
    /**
    * Metodo para ver la lista de establecimientos por proyecto del cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /api/v1/establecimientos/lista-de-establecimientos
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $sClienteProyectoId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$listaEstablecimientos = Establecimiento::join('cliente_establecimientos', 'establecimientos.id', '=', 'cliente_establecimientos.establecimiento_id')
	        								->select('establecimientos.*', 'cliente_establecimientos.en_ruta')
	        								->where('cliente_establecimientos.cliente_proyecto_id', $oClienteProyecto->id)
	        								->get();
	        		$aResponse = [
		                'status' => true,
		                'code' => 200,
		                'message' => 'Lista de Establecimientos',
		                'listaEstablecimientos' => $listaEstablecimientos,
		                'errors' => null,
		            ];
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
		                'listaEstablecimientos' => [],
		                'errors' => null,
		            ];	
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'listaEstablecimientos' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'listaEstablecimientos' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para guardar un nuevo establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /api/v1/establecimientos/lista-de-establecimientos-por-usuario
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function listByUser(Request $request, $sClienteProyectoId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
	        	if($oClienteProyecto){
	        		$userName = $aJwtAuthResponse['identity']->name.' '.$aJwtAuthResponse['identity']->surname;
	        		$listaEstablecimientos = Establecimiento::join('cliente_establecimientos', 'establecimientos.id', '=', 'cliente_establecimientos.establecimiento_id')
	        								->select('establecimientos.*', 'cliente_establecimientos.en_ruta')
	        								->where('cliente_establecimientos.cliente_proyecto_id', $oClienteProyecto->id)
	        								->where('establecimientos.creado_por', $userName)
	        								->get();
	        		$aResponse = [
		                'status' => true,
		                'code' => 200,
		                'message' => 'Lista de Establecimientos',
		                'listaEstablecimientos' => $listaEstablecimientos,
		                'errors' => null,
		            ];
	        	}else{
	        		$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'El proyecto por cliente seleccionado no existe. Por favor seleccione otro.',
		                'listaEstablecimientos' => [],
		                'errors' => null,
		            ];	
	        	}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto por cliente para poder listar los establecimientos asociados al mismo.',
	                'listaEstablecimientos' => [],
	                'errors' => null,
	            ];
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'listaEstablecimientos' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para guardar un nuevo establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /api/v1/establecimientos/agregar-establecimiento
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
            $aResponse = [
                'status' => true,
                'code' => 200,
                'errors' => null,
            ];
            
            $validator = \Validator::make($request->all(), [
		        "nombre" => "required|unique:establecimientos,nombre",
	            "direccion_calle_principal" => "required",
	            "direccion_numero" => "required",
	            "direccion_transversal" => "required",
	            "direccion_referencia" => "required",
	            "email_contacto" => "email",
	            "foto" => "image",
	            "ubicacion" => "required",
	            "id_cadena" => "exists:catalogos,id,contexto,Cadenas",
	            "id_canal" => "required|exists:catalogos,id,contexto,Canales",
	            "id_subcanal" => "required|exists:catalogos,id,contexto,Subcanales",
	            "id_tipo_negocio" => "exists:catalogos,id,contexto,TiposNegocio",
	            "id_provincia" => "required|exists:catalogos,id,contexto,Provincias",
	            "id_ciudad" => "required|exists:catalogos,id,contexto,Ciudades",
	            "id_zona" => "exists:catalogos,id,contexto,Zonas",
	            "id_parroquia" => "exists:catalogos,id,contexto,Parroquias",
	            "id_barrio" => "exists:catalogos,id,contexto,Barrios",
	            "en_ruta" => "required|in:0,1",
	            "cliente_proyecto_id" => "required|exists:cliente_proyectos,id,estado,1",
            ],[
                'nombre.required' => 'El nombre del establecimiento es obligatorio.',
                'nombre.unique' => 'Ya existe un establecimiento con ese nombre.',
                'direccion_calle_principal.required' => 'La calle principal en la dirección es obligatoria.',
                'direccion_numero.required' => 'El número en la dirección es obligatorio.',
                'direccion_transversal.required' => 'La transversal en la dirección es obligatoria.',
                'direccion_referencia.required' => 'Una referencia en la dirección es obligatoria.',
                'direccion_referencia.required' => 'Una referencia en la dirección es obligatoria.',
                'email_contacto.email' => 'Por favor ingrese un email válido para la persona de contacto.',
                'foto.image' => 'Únicamente se aceptan archivos de tipo imagen (JPG, JPEG, GIF, PNG).',
                'ubicacion.required' => 'Por favor ingrese la georeferencia del establecimiento.',
                'id_cadena.exists' => 'La cadena seleccionada no existe.',
                'id_canal.required' => 'Por favor seleccione un Canal.',
                'id_canal.exists' => 'El canal seleccionado no existe.',
                'id_subcanal.required' => 'Por favor seleccione un subcanal.',
                'id_subcanal.exists' => 'El subcanal seleccionado no existe.',
                'id_tipo_negocio.exists' => 'El tipo de negocio seleccionado no existe.',
                'id_provincia.required' => 'Por favor seleccione una provincia.',
                'id_provincia.exists' => 'La provincia seleccionada no existe.',
                'id_ciudad.required' => 'Por favor seleccione una ciudad.',
                'id_ciudad.exists' => 'La ciudad seleccionada no existe.',
                'id_zona.exists' => 'La zona seleccionada no existe.',
                'id_parroquia.exists' => 'La parroquia seleccionada no existe.',
                'id_barrio.exists' => 'El barrio seleccionado no existe.',
                'en_ruta.required' => 'Por favor indique si éste establecimiento está o no en ruta',
                'en_ruta.in' => 'Unicamente indique SI(1) o NO(0)',
                'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto del cliente al cual se va a asignar este establecimiento.',
                'cliente_proyecto_id.exists' => 'No existe ningun proyecto de este cliente activo con ese ID, por favor seleccione otro.',
            ]);

            if ($validator->fails()) {
                $aResponse['status'] = false;
                $aResponse['code'] = 422;
                $aResponse['message'] = 'No se puede guardar el establecimiento debido a que existen errores en los datos.';
                $aResponse['oEstablecimiento'] = null;
                $aResponse['errors'] = $validator->errors();
                return response()->json($aResponse, 200);
            }

            $oEstablecimiento = new Establecimiento;
	        $oEstablecimiento->nombre = strtoupper($request->input('nombre'));
	        $oEstablecimiento->direccion_manzana = $request->input('direccion_manzana');
	        $oEstablecimiento->direccion_calle_principal = $request->input('direccion_calle_principal');
	        $oEstablecimiento->direccion_numero = $request->input('direccion_numero');
	        $oEstablecimiento->direccion_transversal = $request->input('direccion_transversal');
	        $oEstablecimiento->direccion_referencia = $request->input('direccion_referencia');
	        $oEstablecimiento->administrador = $request->input('administrador');
	        $oEstablecimiento->telefonos_contacto = $request->input('telefonos_contacto');
	        $oEstablecimiento->email_contacto = $request->input('email_contacto');
	        $oEstablecimiento->geolocalizacion = $request->input('ubicacion');

	        $sCodigoProvincia = '00';
	        $sCodigoCiudad = '00';
	        $sCodigoZona = '0';
	        $sCodigoParroquia = '000';
	        $sCodigoBarrio = '000';
	        $sCodigoCanal = '0';
	        $sCodigoSubcanal = '00';
	        $sCodigoTipoNegocio = '000';
	        $sCodigoCadena = '000';
	        
	        
	        if($request->input('id_provincia') != ""){
	            $oEstablecimiento->uuid_provincia = $request->input('id_provincia');
	            $oEstablecimiento->codigo_provincia = Catalogo::find($request->input('id_provincia'))->codigo1;
	            $oEstablecimiento->nombre_provincia = Catalogo::find($request->input('id_provincia'))->descripcion;
	            $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
	        }
	        if($request->input('id_ciudad') != ""){
	            $oEstablecimiento->uuid_ciudad = $request->input('id_ciudad');
	            $oEstablecimiento->codigo_ciudad = Catalogo::find($request->input('id_ciudad'))->codigo1;
	            $oEstablecimiento->nombre_ciudad = Catalogo::find($request->input('id_ciudad'))->descripcion;
	            $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
	        }
	        if($request->input('id_zona') != ""){
	            $oEstablecimiento->uuid_zona = $request->input('id_zona');
	            $oEstablecimiento->codigo_zona = Catalogo::find($request->input('id_zona'))->codigo1;
	            $oEstablecimiento->nombre_zona = Catalogo::find($request->input('id_zona'))->descripcion;
	            $sCodigoZona = $oEstablecimiento->codigo_zona;
	        }
	        if($request->input('id_parroquia') != ""){
	            $oEstablecimiento->uuid_parroquia = $request->input('id_parroquia');
	            $oEstablecimiento->codigo_parroquia = Catalogo::find($request->input('id_parroquia'))->codigo1;
	            $oEstablecimiento->nombre_parroquia = Catalogo::find($request->input('id_parroquia'))->descripcion;
	            $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
	        }
	        if($request->input('id_barrio') != ""){
	            $oEstablecimiento->uuid_barrio = $request->input('id_barrio');
	            $oEstablecimiento->codigo_barrio = Catalogo::find($request->input('id_barrio'))->codigo1;
	            $oEstablecimiento->nombre_barrio = Catalogo::find($request->input('id_barrio'))->descripcion;
	            $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
	        }
	        if($request->input('id_canal') != ""){
	            $oEstablecimiento->uuid_canal = $request->input('id_canal');
	            $oEstablecimiento->codigo_canal = Catalogo::find($request->input('id_canal'))->codigo1;
	            $oEstablecimiento->nombre_canal = Catalogo::find($request->input('id_canal'))->descripcion;
	            $sCodigoCanal = $oEstablecimiento->codigo_canal;
	        }
	        if($request->input('id_subcanal') != ""){
	            $oEstablecimiento->uuid_subcanal = $request->input('id_subcanal');
	            $oEstablecimiento->codigo_subcanal = Catalogo::find($request->input('id_subcanal'))->codigo1;
	            $oEstablecimiento->nombre_subcanal = Catalogo::find($request->input('id_subcanal'))->descripcion;
	            $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
	        }
	        if($request->input('id_tipo_negocio') != ""){
	            $oEstablecimiento->uuid_tipo_negocio = $request->input('id_tipo_negocio');
	            $oEstablecimiento->codigo_tipo_negocio = Catalogo::find($request->input('id_tipo_negocio'))->codigo1;
	            $oEstablecimiento->nombre_tipo_negocio = Catalogo::find($request->input('id_tipo_negocio'))->descripcion;
	            $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
	        }
	        if($request->input('id_cadena') != ""){
	            $oEstablecimiento->uuid_cadena = $request->input('id_cadena');
	            $oEstablecimiento->codigo_cadena = Catalogo::find($request->input('id_cadena'))->codigo1;
	            $oEstablecimiento->nombre_cadena = Catalogo::find($request->input('id_cadena'))->descripcion;
	            $sCodigoCadena = $oEstablecimiento->codigo_cadena;
	        }
	        
	        $oEstablecimiento->codigo = Establecimiento::generaCodigo(
	                                                                    $sCodigoProvincia,
	                                                                    $sCodigoCiudad,
	                                                                    $sCodigoZona,
	                                                                    $sCodigoParroquia,
	                                                                    $sCodigoBarrio,
	                                                                    $sCodigoCanal,
	                                                                    $sCodigoSubcanal,
	                                                                    $sCodigoTipoNegocio,
	                                                                    $sCodigoCadena
	                                                                );

	        if ($request->hasFile('foto')){
	            if ($request->file('foto')->isValid()){
	                $fotografia = $request->file('foto');
	                if(!is_dir('./resources/fotosEstablecimientos')){
	                    mkdir('./resources/fotosEstablecimientos',777);
	                }
	                $i=0;
	                $info = explode(".",$fotografia->getClientOriginalName());
	                $oEstablecimiento->foto = $fotografia->getClientOriginalName();
	                while(file_exists('./resources/fotosEstablecimientos/'.$oEstablecimiento->foto)){
	                    $i++;
	                    $oEstablecimiento->foto = $info[0].$i.".".$info[1];
	                }
	                $request->file('foto')->move('resources/fotosEstablecimientos',$oEstablecimiento->foto);
	            }
	        }

	        $oEstablecimiento->creado_por = $aJwtAuthResponse['identity']->name.' '.$aJwtAuthResponse['identity']->surname;

	        if($oEstablecimiento->save()){
	            $oClienteProyecto = ClienteProyecto::find($request->input('cliente_proyecto_id'));
	            if($oClienteProyecto){
	            	$oClienteEstablecimiento = new ClienteEstablecimiento;
	            	$oClienteEstablecimiento->en_ruta = $request->input('en_ruta');
			        $oClienteEstablecimiento->estado = 1;
			        $oClienteEstablecimiento->establecimiento_id = $oEstablecimiento->id;
			        $oClienteEstablecimiento->cliente_proyecto_id = $oClienteProyecto->id;
			        $oClienteEstablecimiento->cliente_id = $oClienteProyecto->cliente->id;
			        $oClienteEstablecimiento->proyecto_id = $oClienteProyecto->proyecto->id;
			        $oClienteEstablecimiento->save();
	            }
	            $aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'Establecimiento '.$oEstablecimiento->nombre.' creado satisfactoriamente',
	                'oEstablecimiento' => $oEstablecimiento,
	                'errors' => null,
	            ];
	        }else{
	            $aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Establecimiento '.$oEstablecimiento->nombre.' no pudo ser creado. ',
	                'oEstablecimiento' => null,
	                'errors' => null,
	            ];
	        }
            
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oEstablecimiento' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }


    /**
    * Metodo para actualizar un establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/08
    *
    * @route /api/v1/establecimientos/actualizar-establecimiento
    * @method PUT/PATCH
    * @param  Illuminate\Http\Request  $request
    * @param string  sId
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $sId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
            
            $oEstablecimiento = Establecimientio::find($sId);

            if( ! $oEstablecimiento){
            	$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'El establecimiento seleccionado no existe.',
	                'oEstablecimiento' => null,
	                'errors' => null,
	            ];
	            return response()->json($aResponse, 200);
            }

            $aResponse = [
                'status' => true,
                'code' => 200,
                'errors' => null,
            ];
            
            $validator = \Validator::make($request->all(), [
		        "nombre" => "required",
	            "direccion_calle_principal" => "required",
	            "direccion_numero" => "required",
	            "direccion_transversal" => "required",
	            "direccion_referencia" => "required",
	            "email_contacto" => "email",
	            "foto" => "image",
	            "ubicacion" => "required",
	            "id_cadena" => "exists:catalogos,id,contexto,Cadenas",
	            "id_canal" => "required|exists:catalogos,id,contexto,Canales",
	            "id_subcanal" => "required|exists:catalogos,id,contexto,Subcanales",
	            "id_tipo_negocio" => "exists:catalogos,id,contexto,TiposNegocio",
	            "id_provincia" => "required|exists:catalogos,id,contexto,Provincias",
	            "id_ciudad" => "required|exists:catalogos,id,contexto,Ciudades",
	            "id_zona" => "exists:catalogos,id,contexto,Zonas",
	            "id_parroquia" => "exists:catalogos,id,contexto,Parroquias",
	            "id_barrio" => "exists:catalogos,id,contexto,Barrios",
	            "en_ruta" => "required|in:0,1",
	            "cliente_proyecto_id" => "required|exists:cliente_proyectos,id,estado,1",
            ],[
                'nombre.required' => 'El nombre del establecimiento es obligatorio.',
                'direccion_calle_principal.required' => 'La calle principal en la dirección es obligatoria.',
                'direccion_numero.required' => 'El número en la dirección es obligatorio.',
                'direccion_transversal.required' => 'La transversal en la dirección es obligatoria.',
                'direccion_referencia.required' => 'Una referencia en la dirección es obligatoria.',
                'direccion_referencia.required' => 'Una referencia en la dirección es obligatoria.',
                'email_contacto.email' => 'Por favor ingrese un email válido para la persona de contacto.',
                'foto.image' => 'Únicamente se aceptan archivos de tipo imagen (JPG, JPEG, GIF, PNG).',
                'ubicacion.required' => 'Por favor ingrese la georeferencia del establecimiento.',
                'id_cadena.exists' => 'La cadena seleccionada no existe.',
                'id_canal.required' => 'Por favor seleccione un Canal.',
                'id_canal.exists' => 'El canal seleccionado no existe.',
                'id_subcanal.required' => 'Por favor seleccione un subcanal.',
                'id_subcanal.exists' => 'El subcanal seleccionado no existe.',
                'id_tipo_negocio.exists' => 'El tipo de negocio seleccionado no existe.',
                'id_provincia.required' => 'Por favor seleccione una provincia.',
                'id_provincia.exists' => 'La provincia seleccionada no existe.',
                'id_ciudad.required' => 'Por favor seleccione una ciudad.',
                'id_ciudad.exists' => 'La ciudad seleccionada no existe.',
                'id_zona.exists' => 'La zona seleccionada no existe.',
                'id_parroquia.exists' => 'La parroquia seleccionada no existe.',
                'id_barrio.exists' => 'El barrio seleccionado no existe.',
                'en_ruta.required' => 'Por favor indique si éste establecimiento está o no en ruta',
                'en_ruta.in' => 'Unicamente indique SI(1) o NO(0)',
                'cliente_proyecto_id.required' => 'Por favor selecciones el proyecto del cliente al cual se va a asignar este establecimiento.',
                'cliente_proyecto_id.exists' => 'No existe ningun proyecto de este cliente activo con ese ID, por favor seleccione otro.',
            ]);

            if ($validator->fails()) {
                $aResponse['status'] = false;
                $aResponse['code'] = 422;
                $aResponse['message'] = 'No se puede guardar el establecimiento debido a que existen errores en los datos.';
                $aResponse['oVehicle'] = null;
                $aResponse['errors'] = $validator->errors();
                return response()->json($aResponse, 200);
            }

            $fotoAnterior = $oEstablecimiento->foto;
            $oEstablecimiento->nombre = strtoupper($request->input('nombre'));
	        $oEstablecimiento->direccion_manzana = $request->input('direccion_manzana');
	        $oEstablecimiento->direccion_calle_principal = $request->input('direccion_calle_principal');
	        $oEstablecimiento->direccion_numero = $request->input('direccion_numero');
	        $oEstablecimiento->direccion_transversal = $request->input('direccion_transversal');
	        $oEstablecimiento->direccion_referencia = $request->input('direccion_referencia');
	        $oEstablecimiento->administrador = $request->input('administrador');
	        $oEstablecimiento->telefonos_contacto = $request->input('telefonos_contacto');
	        $oEstablecimiento->email_contacto = $request->input('email_contacto');
	        $oEstablecimiento->geolocalizacion = $request->input('ubicacion');

	        $sCodigoProvincia = '00';
	        $sCodigoCiudad = '00';
	        $sCodigoZona = '0';
	        $sCodigoParroquia = '000';
	        $sCodigoBarrio = '000';
	        $sCodigoCanal = '0';
	        $sCodigoSubcanal = '00';
	        $sCodigoTipoNegocio = '000';
	        $sCodigoCadena = '000';
	        
	        
	        if($request->input('id_provincia') != ""){
	            $oEstablecimiento->uuid_provincia = $request->input('id_provincia');
	            $oEstablecimiento->codigo_provincia = Catalogo::find($request->input('id_provincia'))->codigo1;
	            $oEstablecimiento->nombre_provincia = Catalogo::find($request->input('id_provincia'))->descripcion;
	            $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
	        }
	        if($request->input('id_ciudad') != ""){
	            $oEstablecimiento->uuid_ciudad = $request->input('id_ciudad');
	            $oEstablecimiento->codigo_ciudad = Catalogo::find($request->input('id_ciudad'))->codigo1;
	            $oEstablecimiento->nombre_ciudad = Catalogo::find($request->input('id_ciudad'))->descripcion;
	            $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
	        }
	        if($request->input('id_zona') != ""){
	            $oEstablecimiento->uuid_zona = $request->input('id_zona');
	            $oEstablecimiento->codigo_zona = Catalogo::find($request->input('id_zona'))->codigo1;
	            $oEstablecimiento->nombre_zona = Catalogo::find($request->input('id_zona'))->descripcion;
	            $sCodigoZona = $oEstablecimiento->codigo_zona;
	        }
	        if($request->input('id_parroquia') != ""){
	            $oEstablecimiento->uuid_parroquia = $request->input('id_parroquia');
	            $oEstablecimiento->codigo_parroquia = Catalogo::find($request->input('id_parroquia'))->codigo1;
	            $oEstablecimiento->nombre_parroquia = Catalogo::find($request->input('id_parroquia'))->descripcion;
	            $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
	        }
	        if($request->input('id_barrio') != ""){
	            $oEstablecimiento->uuid_barrio = $request->input('id_barrio');
	            $oEstablecimiento->codigo_barrio = Catalogo::find($request->input('id_barrio'))->codigo1;
	            $oEstablecimiento->nombre_barrio = Catalogo::find($request->input('id_barrio'))->descripcion;
	            $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
	        }
	        if($request->input('id_canal') != ""){
	            $oEstablecimiento->uuid_canal = $request->input('id_canal');
	            $oEstablecimiento->codigo_canal = Catalogo::find($request->input('id_canal'))->codigo1;
	            $oEstablecimiento->nombre_canal = Catalogo::find($request->input('id_canal'))->descripcion;
	            $sCodigoCanal = $oEstablecimiento->codigo_canal;
	        }
	        if($request->input('id_subcanal') != ""){
	            $oEstablecimiento->uuid_subcanal = $request->input('id_subcanal');
	            $oEstablecimiento->codigo_subcanal = Catalogo::find($request->input('id_subcanal'))->codigo1;
	            $oEstablecimiento->nombre_subcanal = Catalogo::find($request->input('id_subcanal'))->descripcion;
	            $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
	        }
	        if($request->input('id_tipo_negocio') != ""){
	            $oEstablecimiento->uuid_tipo_negocio = $request->input('id_tipo_negocio');
	            $oEstablecimiento->codigo_tipo_negocio = Catalogo::find($request->input('id_tipo_negocio'))->codigo1;
	            $oEstablecimiento->nombre_tipo_negocio = Catalogo::find($request->input('id_tipo_negocio'))->descripcion;
	            $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
	        }
	        if($request->input('id_cadena') != ""){
	            $oEstablecimiento->uuid_cadena = $request->input('id_cadena');
	            $oEstablecimiento->codigo_cadena = Catalogo::find($request->input('id_cadena'))->codigo1;
	            $oEstablecimiento->nombre_cadena = Catalogo::find($request->input('id_cadena'))->descripcion;
	            $sCodigoCadena = $oEstablecimiento->codigo_cadena;
	        }
	        
	        $oEstablecimiento->codigo = Establecimiento::generaCodigo(
	                                                                    $sCodigoProvincia,
	                                                                    $sCodigoCiudad,
	                                                                    $sCodigoZona,
	                                                                    $sCodigoParroquia,
	                                                                    $sCodigoBarrio,
	                                                                    $sCodigoCanal,
	                                                                    $sCodigoSubcanal,
	                                                                    $sCodigoTipoNegocio,
	                                                                    $sCodigoCadena
	                                                                );

	        if ($request->hasFile('foto')){
	            if ($request->file('foto')->isValid()){
	                $fotografia = $request->file('foto');
	                if(!is_dir('./resources/fotosEstablecimientos')){
	                    mkdir('./resources/fotosEstablecimientos',777);
	                }
	                $i=0;
	                $info = explode(".",$fotografia->getClientOriginalName());
	                $oEstablecimiento->foto = $fotografia->getClientOriginalName();
	                while(file_exists('./resources/fotosEstablecimientos/'.$oEstablecimiento->foto)){
	                    $i++;
	                    $oEstablecimiento->foto = $info[0].$i.".".$info[1];
	                }
	                $request->file('foto')->move('resources/fotosEstablecimientos',$oEstablecimiento->foto);
	            }
	        }

	        $oEstablecimiento->ultima_actualizacion = $aJwtAuthResponse['identity']->name.' '.$aJwtAuthResponse['identity']->surname;

	        if($oEstablecimiento->save()){
	            if($fotoAnterior && ($fotoAnterior != $oEstablecimiento->foto)){
                    unlink('./resources/fotosEstablecimientos/'.$fotoAnterior);
                }
                $oClienteProyecto = ClienteProyecto::find($request->input('cliente_proyecto_id'));
                if($oClienteProyecto){
                	$oClienteEstablecimiento = $oEstablecimiento->clienteEstablecimientos()->where('cliente_proyecto_id', $oClienteProyecto->id)->first();
                	if( ! $oClienteEstablecimiento){
                		$oClienteEstablecimiento = new ClienteEstablecimiento;
                		$oClienteEstablecimiento->estado = 1;
                	}
                	$oClienteEstablecimiento->en_ruta = $request->input('en_ruta');
			        $oClienteEstablecimiento->establecimiento_id = $oEstablecimiento->id;
			        $oClienteEstablecimiento->cliente_proyecto_id = $oClienteProyecto->id;
			        $oClienteEstablecimiento->cliente_id = $oClienteProyecto->cliente->id;
			        $oClienteEstablecimiento->proyecto_id = $oClienteProyecto->proyecto->id;
			        $oClienteEstablecimiento->save();
                }
	            $aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'Establecimiento '.$oEstablecimiento->nombre.' creado satisfactoriamente',
	                'oEstablecimiento' => $oEstablecimiento,
	                'errors' => null,
	            ];
	        }else{
	            $aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Establecimiento '.$oEstablecimiento->nombre.' no pudo ser creado',
	                'oEstablecimiento' => null,
	                'errors' => null,
	            ];
	        }
            
        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oEstablecimiento' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }


    /**
    * Metodo para obtener el detalle de un establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/08
    *
    * @route /api/v1/establecimientos/ver-establecimiento
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sId
    * @param string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function show(Request $request, $sClienteProyectoId = '', $sId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	if($sClienteProyectoId){
        		$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        		if($oClienteProyecto){
        			$oClienteEstablecimiento = ClienteEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)
        															 ->where('establecimiento_id', $sId)
        															 ->first();
        			if($oClienteEstablecimiento){
        				$oClienteEstablecimiento->establecimiento;
        				$oClienteEstablecimiento->cliente;
        				$oClienteEstablecimiento->proyecto;
        				$aResponse = [
			                'status' => true,
			                'code' => 200,
			                'message' => 'Detalle del Establecimiento para el proyecto '.$oClienteProyecto->proyecto->nombre.' del cliente '.$oClienteProyecto->cliente->nombre.'.',
			                'oEstablecimiento' => $oClienteEstablecimiento,
			                'errors' => null,
			            ];
        			}else{
        				$aResponse = [
			                'status' => false,
			                'code' => 400,
			                'message' => 'El establecimiento seleccionado no existe o no se encuentra asignado en éste proyecto del cliente seleccionado.',
			                'oEstablecimiento' => null,
			                'errors' => null,
			            ];	
        			}
        		}else{
        			$aResponse = [
		                'status' => false,
		                'code' => 400,
		                'message' => 'El proyecto por cliente seleccionado no existe, por favor seleccione otro.',
		                'oEstablecimiento' => null,
		                'errors' => null,
		            ];	
        		}
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione el proyecto por cliente al que pertenece el establecimiento para poder ver su detalle.',
	                'oEstablecimiento' => null,
	                'errors' => null,
	            ];	
        	}
        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'oEstablecimiento' => null,
                'errors' => null,
            ];
        }
        return response()->json($aResponse, 200);
    }
}
