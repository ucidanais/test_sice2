<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Catalogo;

use SICE\Proyecto;
use SICE\Cliente;
use SICE\ClienteProyecto;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\PresentacionProducto;
use SICE\ProductoCadena;
use SICE\GrupoProducto;

use SICE\Establecimiento;
use SICE\ClienteEstablecimiento;
use SICE\ClasificacionEstablecimiento;

use SICE\Ruta;
use SICE\HojaRuta;
use SICE\DetalleRuta;
use SICE\Novedad;
use SICE\Foto;
use SICE\InformacionDetalle;

use SICE\EquipoTrabajo;
use SICE\Usuario;

use SICE\Helpers\JwtAuth;
use SICE\Helpers\TimeFormat;

use Storage;

class NovedadesController extends Controller
{
    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }


    /**
    * Metodo para ver la lista de novedades de un detalle de hoja de ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/novedades/lista-de-novedades
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sClienteProyectoId
    * @param string  sDetalleRutaId
    * @return \Illuminate\Http\Response
    */
    public function novelties(Request $request, $sClienteProyectoId = '', $sDetalleRutaId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	
        	if( ! $sClienteProyectoId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un proyecto para este cliente',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedades' => null,
	                'errors' => null,
	            ];
	            return response()->json($aResponse, 200);
        	}

        	$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);

        	if( ! is_object($oClienteProyecto)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'El proyecto seleccionado no existe.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedades' => null,
	                'errors' => null,
	            ];
	            return response()->json($aResponse, 200);
        	}

        	if( ! $sDetalleRutaId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione un PDV para ver sus novedades.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedades' => null,
	                'errors' => null,
	            ];
	            return response()->json($aResponse, 200);
        	}

        	$oDetalleRuta = DetalleRuta::find($sDetalleRutaId);

        	if( ! is_object($oDetalleRuta)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'El PDV seleccionado no existe.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedades' => null,
	                'errors' => null,
	            ];
	            return response()->json($aResponse, 200);
        	}

        	$aNovedades = $oDetalleRuta->novedades;
        	
        	$oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	$aListaNovedades = [];
        	if( ! $aNovedades->isEmpty()){
        		foreach($aNovedades as $oNovedad){
        			$oNovedadParse = [
			        	'UUID' => $oNovedad->id,
			        	'TipoNovedad' => $oNovedad->tipo_novedad,
				        'Detalle' => $oNovedad->detalle,
				        'Producto' => $oNovedad->producto,
				        'Competencia' => $oNovedad->competencia,
				        'Cliente' => $oNovedad->cliente,
				        'ReferenteA' => $oNovedad->referente_a,
				        'Rotacion' => $oNovedad->rotacion,
				        'PVP' => $oNovedad->pvp,
				        'PrecioAfiliado' => $oNovedad->precio_afiliado,
				        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
				        'Fabricante' => $oNovedad->fabricante,
				        'Distribuidor' => $oNovedad->distribuidor,
				        'Gestion' => $oNovedad->gestion,
				        'Establecimiento' => $oNovedad->establecimiento->nombre,
				        'Canal' => $oNovedad->canal->descripcion,
				        'Subcanal' => $oNovedad->subcanal->descripcion,
				        'Cadena' => $oNovedad->cadena,
				        'Provincia' => $oNovedad->provincia->descripcion,
				        'Ciudad' => $oNovedad->ciudad->descripcion,
			        ];
			        $aListaNovedades[] = $oNovedadParse;
        		}
        	}

        	$aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de Novedades.',
                'infoMiembroEquipo' => $teamMemberInfo,
                'infoDetalleRuta' => $oDetalleRutaParse,
                'novedades' => $aListaNovedades,
                'errors' => null,
            ];

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedades' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para agregar una novedad al detalle de hoja de ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/novedades/agregar-novedad
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function addNovelty(Request $request)
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){
        	
        	$validator = \Validator::make($request->all(), [
		        'tipo_novedad' => 'required',
		        'pvp' => 'numeric',
		        'precio_afiliado' => 'numeric',
		        'precio_no_afiliado' => 'numeric',
		        'detalle_ruta_id' => 'required|exists:detalle_rutas,id',
		        'cliente_proyecto_id' => 'required|exists:cliente_proyectos,id,estado,1',
            ],[
                'tipo_novedad.required' => 'Por favor ingrese el tipo de novedad.',
                'pvp.numeric' => 'Por favor ingrese un PVP válido.',
                'precio_afiliado.numeric' => 'Por favor ingrese un precio de afiliado válido.',
                'precio_no_afiliado.numeric' => 'Por favor ingrese un precio de NO afiliado válido.',
                'detalle_ruta_id.required' => 'Por favor seleccione un PDV de la ruta válido.',
                'detalle_ruta_id.exists' => 'El PDV de la ruta seleccionado no existe, por favor seleccione otro.',
                'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto del cliente al cual se va a asignar este establecimiento.',
                'cliente_proyecto_id.exists' => 'No existe ningun proyecto de este cliente activo con ese ID, por favor seleccione otro.',
            ]);

            if ($validator->fails()) {
                $aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'No se puede agregar la novedad debido a que existen errores en los datos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => $validator->errors(),
	            ];
                return response()->json($aResponse, 200);
            }

            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            $oDetalleRuta = DetalleRuta::find($request->detalle_ruta_id);
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	$oNovedad = new Novedad;
        	$oNovedad->tipo_novedad = $request->input('tipo_novedad', 'visibilidad');
	        if($request->has('detalle')){
	        	$oNovedad->detalle = $request->input('detalle', '');
	        }
	        if($request->has('producto')){
	        	$oNovedad->producto = $request->input('producto', '');
	        }
	        if($request->has('competencia')){
	        	$oNovedad->competencia = $request->input('competencia', '');
	        }
	        if($request->has('cliente')){
	        	$oNovedad->cliente = $request->input('cliente', '');
	        }
	        if($request->has('referente_a')){
	        	$oNovedad->referente_a = $request->input('referente_a','');
	        }
	        if($request->has('rotacion')){
	        	$oNovedad->rotacion = $request->input('rotacion','');
	        }
	        if($request->has('pvp')){
	        	$oNovedad->pvp = $request->input('pvp',0.00);
	        }
	        if($request->has('precio_afiliado')){
	        	$oNovedad->precio_afiliado = $request->input('precio_afiliado', 0.00);
	        }
	        if($request->has('precio_no_afiliado')){
	        	$oNovedad->precio_no_afiliado = $request->input('precio_no_afiliado', 0.00);
	        }
	        if($request->has('fabricante')){
	        	$oNovedad->fabricante = $request->input('fabricante', '');
	        }
	        if($request->has('distribuidor')){
	        	$oNovedad->distribuidor = $request->input('distribuidor', '');
	        }
	        if($request->has('gestion')){
	        	$oNovedad->gestion = $request->input('gestion', '');
	        }
	        $oNovedad->establecimiento_id = $oEstablecimiento->id;
	        $oNovedad->canal_id = $oEstablecimiento->uuid_canal;
	        $oNovedad->subcanal_id = $oEstablecimiento->uuid_subcanal;
	        $oNovedad->cadena_id = $oEstablecimiento->uuid_cadena;
	        $oNovedad->cadena = $oEstablecimiento->nombre_cadena;
	        $oNovedad->provincia_id = $oEstablecimiento->uuid_provincia;
	        $oNovedad->ciudad_id = $oEstablecimiento->uuid_ciudad;
	        $oNovedad->detalle_ruta_id = $oDetalleRuta->id;
	        $oNovedad->cliente_proyecto_id = $oClienteProyecto->id;
	        $oNovedad->cliente_id = $oClienteProyecto->cliente_id;
	        $oNovedad->proyecto_id = $oClienteProyecto->proyecto_id;

	        if($oNovedad->save()){
	        	$oNovedadParse = [
		        	'UUID' => $oNovedad->id,
		        	'TipoNovedad' => $oNovedad->tipo_novedad,
			        'Detalle' => $oNovedad->detalle,
			        'Producto' => $oNovedad->producto,
			        'Competencia' => $oNovedad->competencia,
			        'Cliente' => $oNovedad->cliente,
			        'ReferenteA' => $oNovedad->referente_a,
			        'Rotacion' => $oNovedad->rotacion,
			        'PVP' => $oNovedad->pvp,
			        'PrecioAfiliado' => $oNovedad->precio_afiliado,
			        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
			        'Fabricante' => $oNovedad->fabricante,
			        'Distribuidor' => $oNovedad->distribuidor,
			        'Gestion' => $oNovedad->gestion,
			        'Establecimiento' => $oNovedad->establecimiento->nombre,
			        'Canal' => $oNovedad->canal->descripcion,
			        'Subcanal' => $oNovedad->subcanal->descripcion,
			        'Cadena' => $oNovedad->cadena,
			        'Provincia' => $oNovedad->provincia->descripcion,
			        'Ciudad' => $oNovedad->ciudad->descripcion,
		        ];

	        	$aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'Novedad agregada exitosamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'errors' => null,
	            ];
	        }else{
	        	$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad no pudo ser agregada, por favor intentelo nuevamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => null,
	                'errors' => null,
	            ];
	        }

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedad' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para actualizar la informacion de una novedad del detalle de hoja de ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/novedades/actualizar-novedad
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @param string  sNovedadId
    * @return \Illuminate\Http\Response
    */
    public function updateNovelty(Request $request, $sNovedadId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if( ! $sNovedadId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione una novedad para poder actualziarla.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	$oNovedad = Novedad::find($sNovedadId);
        	if( ! is_object($oNovedad)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad seleccionada no existe. Por favor seleccione otra.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}
        	
        	$validator = \Validator::make($request->all(), [
		        'pvp' => 'numeric',
		        'precio_afiliado' => 'numeric',
		        'precio_no_afiliado' => 'numeric',
            ],[
                'pvp.numeric' => 'Por favor ingrese un PVP válido.',
                'precio_afiliado.numeric' => 'Por favor ingrese un precio de afiliado válido.',
                'precio_no_afiliado.numeric' => 'Por favor ingrese un precio de NO afiliado válido.',
            ]);

            if ($validator->fails()) {
                $aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'No se puede actualizar la novedad debido a que existen errores en los datos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => $validator->errors(),
	            ];
                return response()->json($aResponse, 200);
            }

            $oClienteProyecto = $oNovedad->clienteProyecto;
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            $oDetalleRuta = $oNovedad->detalleRuta;
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	if($request->has('detalle')){
	        	$oNovedad->detalle = $request->input('detalle', '');
	        }
	        if($request->has('producto')){
	        	$oNovedad->producto = $request->input('producto', '');
	        }
	        if($request->has('competencia')){
	        	$oNovedad->competencia = $request->input('competencia', '');
	        }
	        if($request->has('cliente')){
	        	$oNovedad->cliente = $request->input('cliente', '');
	        }
	        if($request->has('referente_a')){
	        	$oNovedad->referente_a = $request->input('referente_a','');
	        }
	        if($request->has('rotacion')){
	        	$oNovedad->rotacion = $request->input('rotacion','');
	        }
	        if($request->has('pvp')){
	        	$oNovedad->pvp = $request->input('pvp',0.00);
	        }
	        if($request->has('precio_afiliado')){
	        	$oNovedad->precio_afiliado = $request->input('precio_afiliado', 0.00);
	        }
	        if($request->has('precio_no_afiliado')){
	        	$oNovedad->precio_no_afiliado = $request->input('precio_no_afiliado', 0.00);
	        }
	        if($request->has('fabricante')){
	        	$oNovedad->fabricante = $request->input('fabricante', '');
	        }
	        if($request->has('distribuidor')){
	        	$oNovedad->distribuidor = $request->input('distribuidor', '');
	        }
	        if($request->has('gestion')){
	        	$oNovedad->gestion = $request->input('gestion', '');
	        }

	        if($oNovedad->save()){
	        	$oNovedadParse = [
		        	'UUID' => $oNovedad->id,
		        	'TipoNovedad' => $oNovedad->tipo_novedad,
			        'Detalle' => $oNovedad->detalle,
			        'Producto' => $oNovedad->producto,
			        'Competencia' => $oNovedad->competencia,
			        'Cliente' => $oNovedad->cliente,
			        'ReferenteA' => $oNovedad->referente_a,
			        'Rotacion' => $oNovedad->rotacion,
			        'PVP' => $oNovedad->pvp,
			        'PrecioAfiliado' => $oNovedad->precio_afiliado,
			        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
			        'Fabricante' => $oNovedad->fabricante,
			        'Distribuidor' => $oNovedad->distribuidor,
			        'Gestion' => $oNovedad->gestion,
			        'Establecimiento' => $oNovedad->establecimiento->nombre,
			        'Canal' => $oNovedad->canal->descripcion,
			        'Subcanal' => $oNovedad->subcanal->descripcion,
			        'Cadena' => $oNovedad->cadena,
			        'Provincia' => $oNovedad->provincia->descripcion,
			        'Ciudad' => $oNovedad->ciudad->descripcion,
		        ];

	        	$aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'Novedad actualizada exitosamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'errors' => null,
	            ];
	        }else{
	        	$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad no pudo ser agregada, por favor intentelo nuevamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => null,
	                'errors' => null,
	            ];
	        }

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedad' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }


    /**
    * Metodo para ver la lista de fotos de una novedad
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/21
    *
    * @route /api/v1/novedades/lista-de-fotos
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sNovedadId
    * @return \Illuminate\Http\Response
    */
    public function photosList(Request $request, $sNovedadId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if( ! $sNovedadId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione una novedad para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'fotos' => [],
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	$oNovedad = Novedad::find($sNovedadId);
        	if( ! is_object($oNovedad)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad seleccionada no existe. Por favor seleccione otra para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'fotos' => [],
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

            $oClienteProyecto = $oNovedad->clienteProyecto;
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            $oDetalleRuta = $oNovedad->detalleRuta;
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	$oNovedadParse = [
	        	'UUID' => $oNovedad->id,
	        	'TipoNovedad' => $oNovedad->tipo_novedad,
		        'Detalle' => $oNovedad->detalle,
		        'Producto' => $oNovedad->producto,
		        'Competencia' => $oNovedad->competencia,
		        'Cliente' => $oNovedad->cliente,
		        'ReferenteA' => $oNovedad->referente_a,
		        'Rotacion' => $oNovedad->rotacion,
		        'PVP' => $oNovedad->pvp,
		        'PrecioAfiliado' => $oNovedad->precio_afiliado,
		        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
		        'Fabricante' => $oNovedad->fabricante,
		        'Distribuidor' => $oNovedad->distribuidor,
		        'Gestion' => $oNovedad->gestion,
		        'Establecimiento' => $oNovedad->establecimiento->nombre,
		        'Canal' => $oNovedad->canal->descripcion,
		        'Subcanal' => $oNovedad->subcanal->descripcion,
		        'Cadena' => $oNovedad->cadena,
		        'Provincia' => $oNovedad->provincia->descripcion,
		        'Ciudad' => $oNovedad->ciudad->descripcion,
	        ];

        	$aPhotosList = [];
        	$aPhotos = $oNovedad->fotos;

        	if( ! $aPhotos->isEmpty()){
        		foreach($aPhotos as $oPhoto){
        			$oPhotoParse = [
        				'UUID' => $oPhoto->id,
        				'momento' => $oPhoto->momento_foto,
        				'path' => asset(Storage::disk('public')->url($oPhoto->imagen)),
        			];
        			$aPhotosList[] = $oPhotoParse;
        		}
        	}

        	$aResponse = [
                'status' => true,
                'code' => 200,
                'message' => 'Lista de Fotos.',
                'infoMiembroEquipo' => $teamMemberInfo,
                'infoDetalleRuta' => $oDetalleRutaParse,
                'novedad' => $oNovedadParse,
                'fotos' => $aPhotosList,
                'errors' => null,
            ];

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedad' => null,
                'fotos' => [],
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para agregar una nueva foto a una novedad registrada previamente.
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/23
    *
    * @route /api/v1/novedades/agregar-foto
    * @method POST
    * @param  Illuminate\Http\Request  $request
    * @param string  sNovedadId
    * @return \Illuminate\Http\Response
    */
    public function addPhoto(Request $request, $sNovedadId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if( ! $sNovedadId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione una novedad para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'foto' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	$oNovedad = Novedad::find($sNovedadId);
        	if( ! is_object($oNovedad)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad seleccionada no existe. Por favor seleccione otra para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'foto' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

            $oClienteProyecto = $oNovedad->clienteProyecto;
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            $oDetalleRuta = $oNovedad->detalleRuta;
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	$oNovedadParse = [
	        	'UUID' => $oNovedad->id,
	        	'TipoNovedad' => $oNovedad->tipo_novedad,
		        'Detalle' => $oNovedad->detalle,
		        'Producto' => $oNovedad->producto,
		        'Competencia' => $oNovedad->competencia,
		        'Cliente' => $oNovedad->cliente,
		        'ReferenteA' => $oNovedad->referente_a,
		        'Rotacion' => $oNovedad->rotacion,
		        'PVP' => $oNovedad->pvp,
		        'PrecioAfiliado' => $oNovedad->precio_afiliado,
		        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
		        'Fabricante' => $oNovedad->fabricante,
		        'Distribuidor' => $oNovedad->distribuidor,
		        'Gestion' => $oNovedad->gestion,
		        'Establecimiento' => $oNovedad->establecimiento->nombre,
		        'Canal' => $oNovedad->canal->descripcion,
		        'Subcanal' => $oNovedad->subcanal->descripcion,
		        'Cadena' => $oNovedad->cadena,
		        'Provincia' => $oNovedad->provincia->descripcion,
		        'Ciudad' => $oNovedad->ciudad->descripcion,
	        ];

        	$validator = \Validator::make($request->all(), [
		        'imagen' => 'required|image',
		        'momento_foto' => 'required',
            ],[
                'imagen.required' => 'Por favor seleccione una imagen.',
                'imagen.image' => 'Por favor seleccione una imagen con formato JPG, PNG o GIF.',
                'momento_foto.required' => 'Por favor seleccione el momento de la foto.',
            ]);

            if ($validator->fails()) {
                $aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'No se puede actualizar la novedad debido a que existen errores en los datos.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'foto' => null,
	                'errors' => $validator->errors(),
	            ];
                return response()->json($aResponse, 200);
            }

            $oFoto = new Foto;
            $oFoto->momento_foto = $request->momento_foto;
            $oFoto->novedad_id = $oNovedad->id;

            if ($request->hasFile('imagen')){
	            if ($request->file('imagen')->isValid()){
	            	$imagen = $request->file('imagen');
	            	$nombreImagen = time().'_'.$imagen->getClientOriginalName();
	                $path = Storage::disk('public')->put('novedades/'.$oNovedad->id.'/'.$nombreImagen,file_get_contents($imagen->getRealPath()));
	                $oFoto->imagen = 'app/public/novedades/'.$oNovedad->id.'/'.$nombreImagen;
	            }
	        }else{
	        	$aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'Por favor seleccione una imagen para poder agregarla a la novedad.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'foto' => null,
	                'errors' => $validator->errors(),
	            ];
                return response()->json($aResponse, 200);
	        }

	        if($oFoto->save()){
	        	$oPhotoParse = [
    				'UUID' => $oFoto->id,
    				'momento' => $oFoto->momento_foto,
    				'path' => asset(Storage::disk('public')->url($oFoto->imagen)),
    			];
    			$aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'La foto a sido agregada exitosamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'foto' => $oPhotoParse,
	                'errors' => $validator->errors(),
	            ];
	        }else{
	        	$aResponse = [
	                'status' => false,
	                'code' => 422,
	                'message' => 'La imagen no pudo ser guardada, por favor intentelo nuevamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'foto' => null,
	                'errors' => $validator->errors(),
	            ];
	        }

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedad' => null,
                'foto' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }

    /**
    * Metodo para agregar una nueva foto a una novedad registrada previamente.
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/08/24
    *
    * @route /api/v1/novedades/eliminar-foto
    * @method GET
    * @param  Illuminate\Http\Request  $request
    * @param string  sNovedadId
    * @param string  sFotoId
    * @return \Illuminate\Http\Response
    */
    public function deletePhoto(Request $request, $sNovedadId = '', $sFotoId = '')
    {
    	$oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

        	if( ! $sNovedadId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione una novedad para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	$oNovedad = Novedad::find($sNovedadId);
        	if( ! is_object($oNovedad)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La novedad seleccionada no existe. Por favor seleccione otra para poder ver sus fotos.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	if( ! $sFotoId){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'Por favor seleccione una foto para poder eliminarla.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

        	$oFoto = Foto::find($sFotoId);
        	if( ! is_object($oFoto)){
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La foto seleccionada no existe. Por favor seleccione otra para poder eliminarla.',
	                'infoMiembroEquipo' => null,
	                'infoDetalleRuta' => null,
	                'novedad' => null,
	                'errors' => null,
	            ];
                return response()->json($aResponse, 200);
        	}

            $oClienteProyecto = $oNovedad->clienteProyecto;
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
    		$teamMemberInfo = [
				'UUID' => $oEquipoTrabajo->id,
		        'nombres' => $oEquipoTrabajo->nombre,
		        'apellidos' => $oEquipoTrabajo->apellido,
		        'cargoEnProyecto' => $oEquipoTrabajo->cargo,
		        'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
		        'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
			];
            $oDetalleRuta = $oNovedad->detalleRuta;
            $oEstablecimiento = $oDetalleRuta->establecimiento;
	        $oEstablecimientoParse = [
	        	'UUID' => $oEstablecimiento->id,
				'codigo' => $oEstablecimiento->codigo,
				'nombre' => $oEstablecimiento->nombre,
				'direccionManzana' => $oEstablecimiento->direccion_manzana,
		        'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
		        'direccionNumero' => $oEstablecimiento->direccion_numero,
		        'direccionTransversal' => $oEstablecimiento->direccion_transversal,
		        'direccionReferencia' => $oEstablecimiento->direccion_referencia,
		        'administrador' => $oEstablecimiento->administrador,
		        'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
		        'emailContacto' => $oEstablecimiento->email_contacto,
		        'geolocalizacion' => $oEstablecimiento->geolocalizacion,
		        'uuidCadena' => $oEstablecimiento->uuid_cadena,
		        'nombreCadena' => $oEstablecimiento->nombre_cadena,
		        'uuidCanal' => $oEstablecimiento->uuid_canal,
		        'nombreCanal' => $oEstablecimiento->nombre_canal,
		        'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
		        'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
		        'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
		        'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
		        'uuidProvincia' => $oEstablecimiento->uuid_provincia,
		        'nombreProvincia' => $oEstablecimiento->nombre_provincia,
		        'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
		        'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
		        'uuidZona' => $oEstablecimiento->uuid_zona,
		        'nombreZona' => $oEstablecimiento->nombre_zona,
		        'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
		        'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
		        'uuidBarrio' => $oEstablecimiento->uuid_barrio,
		        'nombreBarrio' => $oEstablecimiento->nombre_barrio,
	        ];
	        $oDetalleRutaParse = [
        		'UUID' => $oDetalleRuta->id,
        		'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
        		'Observaciones' => $oDetalleRuta->observaciones,
        		'OrdenVisita' => $oDetalleRuta->orden_visita,
        		'FechaVisita' => $oDetalleRuta->fecha_visita,
        		'HoraEntrada' => $oDetalleRuta->hora_entrada,
        		'HoraSalida' => $oDetalleRuta->hora_salida,
        		'Geolocalizacion' => $oDetalleRuta->ubicacion,
        		'Establecimiento' => $oEstablecimientoParse,
        		'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
        	];

        	$oNovedadParse = [
	        	'UUID' => $oNovedad->id,
	        	'TipoNovedad' => $oNovedad->tipo_novedad,
		        'Detalle' => $oNovedad->detalle,
		        'Producto' => $oNovedad->producto,
		        'Competencia' => $oNovedad->competencia,
		        'Cliente' => $oNovedad->cliente,
		        'ReferenteA' => $oNovedad->referente_a,
		        'Rotacion' => $oNovedad->rotacion,
		        'PVP' => $oNovedad->pvp,
		        'PrecioAfiliado' => $oNovedad->precio_afiliado,
		        'PrecioNoAfiliado' => $oNovedad->precio_no_afiliado,
		        'Fabricante' => $oNovedad->fabricante,
		        'Distribuidor' => $oNovedad->distribuidor,
		        'Gestion' => $oNovedad->gestion,
		        'Establecimiento' => $oNovedad->establecimiento->nombre,
		        'Canal' => $oNovedad->canal->descripcion,
		        'Subcanal' => $oNovedad->subcanal->descripcion,
		        'Cadena' => $oNovedad->cadena,
		        'Provincia' => $oNovedad->provincia->descripcion,
		        'Ciudad' => $oNovedad->ciudad->descripcion,
	        ];
	        $imagen = $oFoto->imagen;
        	if($oFoto->delete()){
        		Storage::delete($imagen);
        		$aResponse = [
	                'status' => true,
	                'code' => 200,
	                'message' => 'La foto a sido eliminada exitosamente.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'errors' => [],
	            ];
        	}else{
        		$aResponse = [
	                'status' => false,
	                'code' => 400,
	                'message' => 'La foto no pudo ser eliminada, por favor intentelo nuevamente luego de unos minutos.',
	                'infoMiembroEquipo' => $teamMemberInfo,
	                'infoDetalleRuta' => $oDetalleRutaParse,
	                'novedad' => $oNovedadParse,
	                'errors' => [],
	            ];
        	}

        }else{
        	$aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'infoMiembroEquipo' => null,
                'infoDetalleRuta' => null,
                'novedad' => null,
                'foto' => null,
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }
}
