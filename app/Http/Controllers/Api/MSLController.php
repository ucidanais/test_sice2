<?php

namespace SICE\Http\Controllers\Api;

use Illuminate\Http\Request;

use SICE\Http\Controllers\Controller;

use SICE\Catalogo;

use SICE\Proyecto;
use SICE\Cliente;
use SICE\ClienteProyecto;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\PresentacionProducto;
use SICE\ProductoCadena;
use SICE\GrupoProducto;

use SICE\Establecimiento;
use SICE\ClienteEstablecimiento;
use SICE\ClasificacionEstablecimiento;

use SICE\Ruta;
use SICE\HojaRuta;
use SICE\DetalleRuta;
use SICE\Novedad;
use SICE\Foto;
use SICE\InformacionDetalle;

use SICE\EquipoTrabajo;
use SICE\Usuario;

use SICE\Helpers\JwtAuth;
use SICE\Helpers\TimeFormat;

use Storage;



class MSLController extends Controller
{
    public function __construct(){
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 86400");
    }

    /**
     * Metodo para ingresar msl
     * @Autor Julio Silvera
     * @FechaCreacion  2019/01/25
     *
     * @route /api/v1/msl/agregar-msl
     * @method POST
     * @param  Illuminate\Http\Request  $request
     * @param string  sClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function addMSL(Request $request)
    {
        $oJwtAuth = new JwtAuth;
        $aJwtAuthResponse = $oJwtAuth->checkToken($request->header('Authentication', null), true);

        if($aJwtAuthResponse['auth']){

            $validator = \Validator::make($request->all(), [
                'tipo' => 'required',
                'es_competencia' => 'required|in:1,0',
                'producto_competencia' => 'required_if:es_competencia,1',
                'stock' => 'numeric',
                'pvp' => 'numeric',
                'precio_afiliado' => 'numeric',
                'precio_no_afiliado' => 'numeric',
                'cliente_proyecto_id' => 'required|exists:cliente_proyectos,id,estado,1',
                'presentacion_producto_id' => 'required|exists:presentacion_productos,id',
                'detalle_ruta_id' => 'required|exists:detalle_rutas,id',
            ],[
                'tipo.required' => 'Por favor ingrese el tipo de información.',
                'es_competencia.required' => 'Por favor indique si esta informacion es o no de la competencia.',
                'es_competencia.in' => 'Por favor unicamente indique SI (1) o NO (0).',
                'es_competencia.required_if' => 'Por favor ingrese el producto de la competencia.',
                'stock.numeric' => 'Por favor ingrese un valor de stock válido (unicamente numeros).',
                'pvp.numeric' => 'Por favor ingrese un PVP válido.',
                'precio_afiliado.numeric' => 'Por favor ingrese un precio de afiliado válido.',
                'precio_no_afiliado.numeric' => 'Por favor ingrese un precio de NO afiliado válido.',
                'detalle_ruta_id.required' => 'Por favor seleccione un PDV de la ruta válido.',
                'detalle_ruta_id.exists' => 'El PDV de la ruta seleccionado no existe, por favor seleccione otro.',
                'cliente_proyecto_id.required' => 'Por favor seleccione el proyecto del cliente al cual se va a agregar esta información.',
                'cliente_proyecto_id.exists' => 'No existe ningun proyecto de este cliente activo con ese ID, por favor seleccione otro.',
                'presentacion_producto_id.required' => 'Por favor seleccione la presentación de producto sobre la cual se esta obteniendo la información.',
                'presentacion_producto_id.exists' => 'No existe ninguna presentación de producto con ese ID, por favor seleccione otra.',
            ]);

            if ($validator->fails()) {
                $aResponse = [
                    'status' => false,
                    'code' => 422,
                    'message' => 'No se puede agregar la información debido a que existen errores en los datos.',
                    'infoMiembroEquipo' => null,
                    'infoDetalleRuta' => null,
                    'informacionDetalle' => null,
                    'errors' => $validator->errors(),
                ];
                return response()->json($aResponse, 200);
            }


            $oClienteProyecto = ClienteProyecto::find($request->cliente_proyecto_id);
            $oEquipoTrabajo = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->where('usuario_id', $aJwtAuthResponse['identity']->uuid)->first();
            $teamMemberInfo = [
                'UUID' => $oEquipoTrabajo->id,
                'nombres' => $oEquipoTrabajo->nombre,
                'apellidos' => $oEquipoTrabajo->apellido,
                'cargoEnProyecto' => $oEquipoTrabajo->cargo,
                'esLiderProyecto' => $oEquipoTrabajo->lider_proyecto == 1 ? true : false,
                'jefeInmediato' => $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '',
            ];
            $oDetalleRuta = DetalleRuta::find($request->detalle_ruta_id);
            $oEstablecimiento = $oDetalleRuta->establecimiento;
            $oEstablecimientoParse = [
                'UUID' => $oEstablecimiento->id,
                'codigo' => $oEstablecimiento->codigo,
                'nombre' => $oEstablecimiento->nombre,
                'direccionManzana' => $oEstablecimiento->direccion_manzana,
                'direccionCallePrincipal' => $oEstablecimiento->direccion_calle_principal,
                'direccionNumero' => $oEstablecimiento->direccion_numero,
                'direccionTransversal' => $oEstablecimiento->direccion_transversal,
                'direccionReferencia' => $oEstablecimiento->direccion_referencia,
                'administrador' => $oEstablecimiento->administrador,
                'telefonosContacto' => $oEstablecimiento->telefonos_contacto,
                'emailContacto' => $oEstablecimiento->email_contacto,
                'geolocalizacion' => $oEstablecimiento->geolocalizacion,
                'uuidCadena' => $oEstablecimiento->uuid_cadena,
                'nombreCadena' => $oEstablecimiento->nombre_cadena,
                'uuidCanal' => $oEstablecimiento->uuid_canal,
                'nombreCanal' => $oEstablecimiento->nombre_canal,
                'uuidSubcanal' => $oEstablecimiento->uuid_subcanal,
                'nombreSubcanal' => $oEstablecimiento->nombre_subcanal,
                'uuidTipoNegocio' => $oEstablecimiento->uuid_tipo_negocio,
                'nombreTipoNegocio' => $oEstablecimiento->nombre_tipo_negocio,
                'uuidProvincia' => $oEstablecimiento->uuid_provincia,
                'nombreProvincia' => $oEstablecimiento->nombre_provincia,
                'uuidCiudad' => $oEstablecimiento->uuid_ciudad,
                'nombreCiudad' => $oEstablecimiento->nombre_ciudad,
                'uuidZona' => $oEstablecimiento->uuid_zona,
                'nombreZona' => $oEstablecimiento->nombre_zona,
                'uuidParroquia' => $oEstablecimiento->uuid_parroquia,
                'nombreParroquia' => $oEstablecimiento->nombre_parroquia,
                'uuidBarrio' => $oEstablecimiento->uuid_barrio,
                'nombreBarrio' => $oEstablecimiento->nombre_barrio,
            ];
            $oDetalleRutaParse = [
                'UUID' => $oDetalleRuta->id,
                'Estado' => $oDetalleRuta->estado == 0 ? 'Pendiente de Visitar' : 'Visitado',
                'Observaciones' => $oDetalleRuta->observaciones,
                'OrdenVisita' => $oDetalleRuta->orden_visita,
                'FechaVisita' => $oDetalleRuta->fecha_visita,
                'HoraEntrada' => $oDetalleRuta->hora_entrada,
                'HoraSalida' => $oDetalleRuta->hora_salida,
                'Geolocalizacion' => $oDetalleRuta->ubicacion,
                'Establecimiento' => $oEstablecimientoParse,
                'ClienteProyectoId' => $oDetalleRuta->cliente_proyecto_id,
            ];

            $oPresentacionProducto = PresentacionProducto::find($request->presentacion_producto_id);

            $sMslExisteDetalle = InformacionDetalle::where('establecimiento_id',$oEstablecimiento->id)
                ->where('detalle_ruta_id',$oDetalleRuta->id)
                ->where('presentacion_producto_id', $oPresentacionProducto->id)
                ->where('created_at', 'LIKE', date('Y-m-d') . '%')

                ->first();

            if(isset($sMslExisteDetalle->id))
            {
                $oInformacionDetalle = InformacionDetalle::find($sMslExisteDetalle->id);
            }else{
                $oInformacionDetalle = new InformacionDetalle;
            }


            $oInformacionDetalle->cliente_proyecto_id = $oClienteProyecto->id;
            $oInformacionDetalle->cliente_id = $oClienteProyecto->cliente_id;
            $oInformacionDetalle->proyecto_id = $oClienteProyecto->proyecto_id;
            $oInformacionDetalle->establecimiento_id = $oEstablecimiento->id;
            $oInformacionDetalle->canal_id = ($oEstablecimiento->uuid_canal) ? $oEstablecimiento->uuid_canal :'';
            $oInformacionDetalle->subcanal_id = ($oEstablecimiento->uuid_subcanal) ? $oEstablecimiento->uuid_subcanal : '';
            $oInformacionDetalle->cadena_id = ($oEstablecimiento->uuid_cadena) ? $oEstablecimiento->uuid_cadena : '';
            $oInformacionDetalle->provincia_id = $oEstablecimiento->uuid_provincia;
            $oInformacionDetalle->ciudad_id = $oEstablecimiento->uuid_ciudad;
            $oInformacionDetalle->detalle_ruta_id = $oDetalleRuta->id;
            $oInformacionDetalle->familia_producto_id = $oPresentacionProducto->categoriaProducto->familia_producto_id;
            $oInformacionDetalle->categoria_producto_id = $oPresentacionProducto->categoria_producto_id;
            $oInformacionDetalle->presentacion_producto_id = $oPresentacionProducto->id;
            $oInformacionDetalle->tipo = $request->tipo;
            $oInformacionDetalle->es_competencia = $request->es_competencia;

            if($request->has('producto_competencia')){
                $oInformacionDetalle->producto_competencia = $request->input('producto_competencia', '');
            }
            if($request->has('valor')){
                $oInformacionDetalle->valor = $request->input('valor', '');
            }
            if($request->has('stock')){
                $oInformacionDetalle->stock = $request->input('stock', '');
            }
            if($request->has('pvp')){
                $oInformacionDetalle->pvp = $request->input('pvp', '');
            }
            if($request->has('precio_afiliado')){
                $oInformacionDetalle->precio_afiliado = $request->input('precio_afiliado', '');
            }
            if($request->has('precio_no_afiliado')){
                $oInformacionDetalle->precio_no_afiliado = $request->input('precio_no_afiliado', '');
            }
            
            if($oInformacionDetalle->save()){
                $oInformacionDetalleParse = [
                    'UUID' => $oInformacionDetalle->id,
                    'Tipo' => $oInformacionDetalle->tipo,
                    'EsCompetencia' => $oInformacionDetalle->es_competencia == 1 ? 'SI' : 'NO',
                    'ProductoCompetencia' => $oInformacionDetalle->producto_competencia,
                    'Valor' => $oInformacionDetalle->valor,
                    'Stock' => $oInformacionDetalle->stock,
                    'PVP' => $oInformacionDetalle->pvp,
                    'PrecioAfiliado' => $oInformacionDetalle->precio_afiliado,
                    'PrecioNoAfiliado' => $oInformacionDetalle->precio_no_afiliado,
                    'Establecimiento' => $oInformacionDetalle->establecimiento->nombre,
                    'Canal' => ($oInformacionDetalle->canal->descripcion) ? $oInformacionDetalle->canal->descripcion : '',
                    'Subcanal' => ($oInformacionDetalle->subcanal->descripcion) ? $oInformacionDetalle->subcanal->descripcion :'',
                    'Cadena' => ($oInformacionDetalle->cadena->descripcion) ? $oInformacionDetalle->cadena->descripcion : '',
                    'Provincia' => $oInformacionDetalle->provincia->descripcion,
                    'Ciudad' => $oInformacionDetalle->ciudad->descripcion,
                    'FamiliaProducto' => $oInformacionDetalle->familiaProducto->nombre,
                    'CategoriaProducto' => $oInformacionDetalle->categoriaProducto->nombre,
                    'PresentacionProducto' => $oInformacionDetalle->presentacionProducto->nombre,
                ];

                $aResponse = [
                    'status' => true,
                    'code' => 200,
                    'message' => 'Informacion agregada exitosamente.',
                    'infoMiembroEquipo' => $teamMemberInfo,
                    'infoDetalleRuta' => $oDetalleRutaParse,
                    'informacionDetalle' => $oInformacionDetalleParse,
                    'errors' => null,
                ];
            }else{
                $aResponse = [
                    'status' => false,
                    'code' => 400,
                    'message' => 'La informacion no pudo ser agregada, por favor intentelo nuevamente.',
                    'infoMiembroEquipo' => $teamMemberInfo,
                    'infoDetalleRuta' => $oDetalleRutaParse,
                    'informacionDetalle' => null,
                    'errors' => null,
                ];
            }
            

        }else{
            $aResponse = [
                'status' => false,
                'code' => 400,
                'message' => 'Error en autenticacion',
                'errors' => null,
            ];
        }

        return response()->json($aResponse, 200);
    }
}
