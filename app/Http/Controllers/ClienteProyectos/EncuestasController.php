<?php

namespace SICE\Http\Controllers\ClienteProyectos;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Http\Requests\EncuestaFormRequest;

use SICE\Encuesta;
use SICE\Pregunta;
use SICE\RespuestaEncuesta;
use SICE\EncuestaFoto;
use SICE\Catalogo;

use SICE\ClienteProyecto;
use SICE\Cliente;
use SICE\Proyecto;

use SICE\Usuario;

use SICE\Helpers\TimeFormat;

use Flashy;
use Auth;
use Storage;
use Mapper;
use Excel;

/**
* Clase para administración de encuestas por proyecto
* @Autor Raúl Chauvin
* @FechaCreacion  2019/04/09
* @Configuracion
* @Proyectos
*/

class EncuestasController extends Controller
{
    /**
     * Disco de storage.
     *
     * @var sStorageDisk
     */
    protected $sStorageDisk;

    /**
     * Instancia de storage.
     *
     * @var oStorage
     */
    protected $oStorage;

    /**
     * Constructor del Controller, iniciamos los middlewares para validar que el usuario tenga los permisos correctos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/05/15
     */
    public function __construct(){
        $this->sStorageDisk = config('app.env') == 'production' ? 's3' : 'local';
        $this->oStorage = config('app.env') == 'production' ? Storage::disk('s3') : Storage::disk('local');
    }

    /**
    * Metodo para mostrar la lista de encuestas del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/09
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/lista-de-encuestas
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/lista-de-encuestas')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($sClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($sClienteProyectoId);
            if($oProyectoCliente){
                $aEncuestas = $oProyectoCliente->encuestas;
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'aEncuestas' => $aEncuestas,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.encuestas.listaEncuestas", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }



    /**
    * Metodo para mostrar el formulario de creacion de encuestas en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/09
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/agregar-encuesta
    * @method GET
    * @param  \Illuminate\Http\Request  $request
    * @param int $sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function create(Request $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/agregar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($sClienteProyectoId){
            $oProyectoCliente = ClienteProyecto::find($sClienteProyectoId);
            if($oProyectoCliente){
                $aData = [
                    'oClienteProyecto' => $oProyectoCliente,
                    'oCliente' => $oProyectoCliente->cliente,
                    'oProyecto' => $oProyectoCliente->proyecto,
                    'aCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
                    'oEncuesta' => null,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Encuestas',
                ];
                return view("configuracion.proyectosPorCliente.encuestas.crearEditarEncuesta", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }


    /**
    * Metodo para almacenar en la base de datos una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/agregar-encuesta
    * @method POST
    * @param  SICE\Http\Requests\EncuestaFormRequest $request
    * @param string $sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function store(EncuestaFormRequest $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/agregar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $request->pregunta){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor agregue al menos 1 pregunta</div>');  
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$sClienteProyectoId]);
        }

        $pregunta =new Pregunta;
        if($sClienteProyectoId){
            $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
            if($oClienteProyecto){
                $oEncuesta = new Encuesta;
                $oEncuesta->nombre = $request->input('nombre');
                $oEncuesta->tipo = $request->input('tipo');
                $oEncuesta->cliente_proyecto_id = $request->input('cliente_proyecto_id');
                $oEncuesta->cliente_id = $request->input('cliente_id');
                $oEncuesta->proyecto_id = $request->input('proyecto_id');
                $oEncuesta->canal_id = $request->input('canal_id', null);
                $oEncuesta->subcanal_id = $request->input('subcanal_id', null);
                $oEncuesta->cadena_id = $request->input('cadena_id', null);
                $oEncuesta->provincia_id = $request->input('provincia_id', null);
                $oEncuesta->ciudad_id = $request->input('ciudad_id', null);

                if($oEncuesta->save()){
                    
                    if($oEncuesta->tipo != 'S'){
                        if(is_array($request->cadenas)){
                            if(count($request->cadenas)){
                                $oEncuesta->cadenas()->sync($request->cadenas);
                            }
                        }
                    }

                    if(is_array($request->pregunta)){
                        if(count($request->pregunta)){
                            foreach($request->pregunta as $key => $value){

                                $oPregunta = new Pregunta;
                                $oPregunta->pregunta = $request->input('pregunta')[$key];
                                $oPregunta->tipo_dato = $request->input('tipo_dato')[$key];
                                $oPregunta->opcion_multiple = ($request->input('tipo_dato')[$key] == 'O' || $request->input('tipo_dato')[$key] == 'OM') ? 1 : 0;
                                $oPregunta->opciones = explode(',', $request->input('opciones')[$key]);
                                $oPregunta->orden = $request->input('orden')[$key];
                                $oPregunta->ponderacion = 0;
                                $oPregunta->encuesta_id = $oEncuesta->id;
                                $oPregunta->save();
                            }
                        }
                    }

                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta '.$oEncuesta->nombre.' agregada exitosamente</div>');
                    
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta '.$oEncuesta->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                }
                return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oClienteProyecto->id]);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }


    /**
    * Metodo para mostrar el formulario de creacion de encuestas en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/09
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/editar-encuesta
    * @method GET
    * @param  \Illuminate\Http\Request  $request
    * @param string $sClienteProyectoId
    * @param string $sEncuestaId
    * @return \Illuminate\Http\Response
    */
    public function edit(Request $request, $sClienteProyectoId = '', $sEncuestaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/editar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($sClienteProyectoId);

        if( ! is_object($oProyectoCliente)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEncuestaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una encuesta para poder editarla</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta = Encuesta::find($sEncuestaId);

        if( ! is_object($oEncuesta)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta seleccionada no existe</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }
        
        $aData = [
            'oClienteProyecto' => $oProyectoCliente,
            'oCliente' => $oProyectoCliente->cliente,
            'oProyecto' => $oProyectoCliente->proyecto,
            'aCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
            'oEncuesta' => $oEncuesta,
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Encuestas',
        ];
        return view("configuracion.proyectosPorCliente.encuestas.crearEditarEncuesta", $aData);
    }


    /**
    * Metodo para almacenar en la base de datos una ruta para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/22
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/editar-encuesta
    * @method POST
    * @param  SICE\Http\Requests\EncuestaFormRequest $request
    * @param string $sClienteProyectoId
    * @param string $sEncuestaId
    * @return \Illuminate\Http\Response
    */
    public function update(EncuestaFormRequest $request, $sClienteProyectoId = '', $sEncuestaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/editar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($sClienteProyectoId);

        if( ! is_object($oProyectoCliente)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEncuestaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una encuesta para poder editarla</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta = Encuesta::find($sEncuestaId);

        if( ! is_object($oEncuesta)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta seleccionada no existe</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        if( ! $request->pregunta){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor agregue al menos 1 pregunta</div>');  
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);  
        }


        $oEncuesta->nombre = $request->input('nombre');
        $oEncuesta->tipo = $request->input('tipo');
        $oEncuesta->cliente_proyecto_id = $request->input('cliente_proyecto_id');
        $oEncuesta->cliente_id = $request->input('cliente_id');
        $oEncuesta->proyecto_id = $request->input('proyecto_id');
        $oEncuesta->canal_id = $request->input('canal_id', null);
        $oEncuesta->subcanal_id = $request->input('subcanal_id', null);
        $oEncuesta->cadena_id = $request->input('cadena_id', null);
        $oEncuesta->provincia_id = $request->input('provincia_id', null);
        $oEncuesta->ciudad_id = $request->input('ciudad_id', null);

        if($oEncuesta->save()){
            
            $oEncuesta->cadenas()->detach();
            if($oEncuesta->tipo != 'S'){
                if(is_array($request->cadenas)){
                    if(count($request->cadenas)){
                        $oEncuesta->cadenas()->sync($request->cadenas);
                    }
                }
            }

            if(is_array($request->pregunta)){
                if(count($request->pregunta)){
                    foreach($request->pregunta as $key => $value){
                        if(isset($request->input('pregunta_id')[$key])){
                            $oPregunta = Pregunta::find($request->input('pregunta_id')[$key]);
                            if(is_object($oPregunta)){
                                $oPregunta->pregunta = $request->input('pregunta')[$key];
                                $oPregunta->tipo_dato = $request->input('tipo_dato')[$key];
                                $oPregunta->opcion_multiple = ($request->input('tipo_dato')[$key] == 'O' || $request->input('tipo_dato')[$key] == 'OM') ? 1 : 0;
                                $oPregunta->opciones = explode(',', $request->input('opciones')[$key]);
                                $oPregunta->orden = $request->input('orden')[$key];
                                $oPregunta->ponderacion = 0;
                                $oPregunta->encuesta_id = $oEncuesta->id;
                                $oPregunta->save();
                            }
                        }else{
                            $oPregunta = new Pregunta;
                            $oPregunta->pregunta = $request->input('pregunta')[$key];
                            $oPregunta->tipo_dato = $request->input('tipo_dato')[$key];
                            $oPregunta->opcion_multiple = ($request->input('tipo_dato')[$key] == 'O' || $request->input('tipo_dato')[$key] == 'OM') ? 1 : 0;
                            $oPregunta->opciones = explode(',', $request->input('opciones')[$key]);
                            $oPregunta->orden = $request->input('orden')[$key];
                            $oPregunta->ponderacion = 0;
                            $oPregunta->encuesta_id = $oEncuesta->id;
                            $oPregunta->save();
                        }
                    }
                }
            }

            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta '.$oEncuesta->nombre.' actualizada exitosamente</div>');
            
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta '.$oEncuesta->nombre.' no pudo ser actualizada, por favor intentelo nuevamente luego de unos minutos.</div>');
        }
        return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
    }



    /**
    * Metodo para eliminar una encuesta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/14
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/eliminar-encuesta
    * @method GET
    * @param string $sProyectoClienteId
    * @param string $sPreguntaId
    * @return \Illuminate\Http\Response
    */
    public function destroy($sProyectoClienteId = '', $sEncuestaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/eliminar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sProyectoClienteId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($sProyectoClienteId);

        if( ! is_object($oProyectoCliente)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEncuestaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una encuesta para poder editarla</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta = Encuesta::find($sEncuestaId);

        if( ! is_object($oEncuesta)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta seleccionada no existe</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        
        $fotos = $oEncuesta->fotos;
        $respuestas = $oEncuesta->respuestaEncuestas;

        if( ! $fotos->isEmpty() || ! $respuestas->isEmpty()){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La encuesta no puede ser eliminada debido a que ya se levantaron encuestas en el campo</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta->preguntas()->delete();
        $oEncuesta->cadenas()->detach();

        $preguntas = $oEncuesta->preguntas;

        if( ! $preguntas->isEmpty()){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor primero elimine las preguntas de la encuesta</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);   
        }

        if($oEncuesta->delete()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La encuesta a sido eliminada exitosamente</div>');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La encuesta no pudo ser eliminada, por favor intentelo nuevamente luego de unos minutos.</div>');
        }

        return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);

    }



    /**
    * Metodo para eliminar una preguntade la encuesta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/14
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/eliminar-pregunta
    * @method GET
    * @param int $sPreguntaId
    * @return \Illuminate\Http\Response
    */
    public function destroyPregunta($sPreguntaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/eliminar-encuesta')){
            $aResponse = [
                'status' => false,
                'errorCode' => 419,
                'message' => 'Usted no tiene permisos para navegar en esta seccion del sistema',
            ];
            return response()->json($aResponse, 200);
        }

        if( ! $sPreguntaId){
            $aResponse = [
                'status' => false,
                'errorCode' => 422,
                'message' => 'Seleccione una pregunta para eliminarla.',
            ];
            return response()->json($aResponse, 200);   
        }

        $oPregunta = Pregunta::find($sPreguntaId);

        if( ! is_object($oPregunta)){
            $aResponse = [
                'status' => false,
                'errorCode' => 404,
                'message' => 'La pregunta seleccionada no existe.',
            ];
            return response()->json($aResponse, 200);   
        }

        $respuestas = $oPregunta->respuestaEncuestas;
        
        if( ! $respuestas->isEmpty()){
            $aResponse = [
                'status' => false,
                'errorCode' => 422,
                'message' => 'La pregunta seleccionada no se puede eliminar debido a que ya tiene respuestas.',
            ];
            return response()->json($aResponse, 200);   
        }

        if($oPregunta->delete()){
            $aResponse = [
                'status' => true,
                'errorCode' => 200,
                'message' => 'La pregunta seleccionada a sido eliminada.',
            ];
        }else{
            $aResponse = [
                'status' => false,
                'errorCode' => 422,
                'message' => 'La pregunta seleccionada no pudo ser eliminada, por favor intentelo nuevamente luego de unos minutos.',
            ];
        }

        return response()->json($aResponse, 200);
    }




    /**
    * Metodo para moctrar el formulario que genera el reporte de la encuesta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/14
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/filtrar-encuesta
    * @method GET
    * @param string $sProyectoClienteId
    * @param string $sEncuestaId
    * @return \Illuminate\Http\Response
    */
    public function filtros($sProyectoClienteId = '', $sEncuestaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/filtrar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sProyectoClienteId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($sProyectoClienteId);

        if( ! is_object($oProyectoCliente)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEncuestaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una encuesta para poder editarla</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta = Encuesta::find($sEncuestaId);

        if( ! is_object($oEncuesta)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta seleccionada no existe</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $aData = [
            'oClienteProyecto' => $oProyectoCliente,
            'oCliente' => $oProyectoCliente->cliente,
            'oProyecto' => $oProyectoCliente->proyecto,
            'oEncuesta' => $oEncuesta,
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Encuestas',
        ];
        return view("configuracion.proyectosPorCliente.encuestas.filtrarEncuesta", $aData);
        

    }



    /**
    * Metodo para generar el reporte de la encuesta en formato Excel
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/04/14
    *
    * @route /configuracion/proyectos-por-cliente/encuestas/filtrar-encuesta
    * @method POST
    * @param string $sProyectoClienteId
    * @param string $sEncuestaId
    * @return \Illuminate\Http\Response
    */
    public function descarga(Request $request, $sProyectoClienteId = '', $sEncuestaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/encuestas/filtrar-encuesta')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sProyectoClienteId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de encuestas</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($sProyectoClienteId);

        if( ! is_object($oProyectoCliente)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEncuestaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una encuesta para poder editarla</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }

        $oEncuesta = Encuesta::find($sEncuestaId);

        if( ! is_object($oEncuesta)){
            \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Encuesta seleccionada no existe</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.encuestas.list', [$oProyectoCliente->id]);
        }


        if($request->tipoReporte == 'fotos'){
            
            $reporte = EncuestaFoto::where('cliente_proyecto_id', $oProyectoCliente->id)
                                    ->where('cliente_id', $oProyectoCliente->cliente_id)
                                    ->where('proyecto_id', $oProyectoCliente->proyecto_id)
                                    ->where('encuesta_id', $oEncuesta->id);

            if($request->desde){
                $reporte = $reporte->where('created_at', '<=', $request->desde);
            }

            if($request->hasta){
                $reporte = $reporte->where('created_at', '>=', $request->hasta);
            }

            $reporte = $reporte->orderBy('created_at', 'desc')->get();

            $aData = [
                'oProyectoCliente' => $oProyectoCliente,
                'oEncuesta' => $oEncuesta,
                'filtros' => [
                    'desde' => TimeFormat::dateLongFormat($request->desde),
                    'hasta' => TimeFormat::dateLongFormat($request->hasta),
                ],
                'reporte' => $reporte,
            ];
            $pdfVisibilidad = \PDF::loadView('configuracion.proyectosPorCliente.encuestas.exportarEncuestas', $aData);
            return @$pdfVisibilidad->download('reporteEncuesta_'.date('YmdHis').'.pdf');
        }else{
            $reporte = RespuestaEncuesta::where('cliente_proyecto_id', $oProyectoCliente->id)
                                        ->where('encuesta_id', $oEncuesta->id);

            if($request->desde){
                $reporte = $reporte->where('created_at', '>=', $request->desde.' 00:00:00');
            }

            if($request->hasta){
                $reporte = $reporte->where('created_at', '<=', $request->hasta.' 23:59:59');
            }

            $reporte = $reporte->orderBy('created_at', 'desc')->get();

            
            Excel::create('reporteEncuesta_'.date('YmdHis'), function($excel) use($reporte, $oProyectoCliente, $oEncuesta){
                $excel->setTitle('Reporte de Encuesta');
                $excel->setCreator('SICE')
                      ->setCompany('Go Trade');
                $excel->setDescription('Reporte de encuesta '.$oEncuesta->nombre.' generado desde SICE el '.date('Y-M-d H:i:s'));
                $excel->sheet('Reporte de Encuesta', function($sheet) use($reporte, $oProyectoCliente, $oEncuesta){
                    
                    $aRespuestas = [];
                    $aRespuestas[] = [
                        'PUNTO DE VENTA',
                        'CANAL',
                        'SUBCANAL',
                        'CADENA',
                        'PROVINCIA',
                        'CIUDAD',
                        'DIRECCION',
                        'FECHA DE ENCUESTA',
                        'PREGUNTA',
                        'RESPUESTA',
                    ];
                    foreach($reporte as $oRespuestaEncuesta){
                        $aRespuestas[] = [
                            $oRespuestaEncuesta->establecimiento->nombre,
                            $oRespuestaEncuesta->establecimiento->nombre_canal,
                            $oRespuestaEncuesta->establecimiento->nombre_subcanal,
                            $oRespuestaEncuesta->establecimiento->nombre_cadena,
                            $oRespuestaEncuesta->establecimiento->nombre_provincia,
                            $oRespuestaEncuesta->establecimiento->nombre_ciudad,
                            $oRespuestaEncuesta->establecimiento->direccion_calle_principal.' '.$oRespuestaEncuesta->establecimiento->direccion_numero.' '.$oRespuestaEncuesta->establecimiento->direccion_transversal.'. Manzana:'.$oRespuestaEncuesta->establecimiento->direccion_manzana.'. Referencia'.$oRespuestaEncuesta->establecimiento->direccion_referencia,
                            $oRespuestaEncuesta->created_at,
                            $oRespuestaEncuesta->pregunta->pregunta,
                            $oRespuestaEncuesta->valor,
                        ];
                    }
                    $sheet->rows($aRespuestas);
                });
            })->download('xls');
        }
        

    }
}
