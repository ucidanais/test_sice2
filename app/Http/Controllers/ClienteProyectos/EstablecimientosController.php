<?php

namespace SICE\Http\Controllers\ClienteProyectos;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Requests\EstablecimientoUpdateFormRequest;
use SICE\Http\Controllers\Controller;

use SICE\Establecimiento;
use SICE\Catalogo;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\Cliente;
use SICE\Proyecto;
use SICE\ClienteEstablecimiento;
use SICE\ClasificacionEstablecimiento;

use SICE\Helpers\TimeFormat;

use Flashy;
use Auth;
use Storage;
use Mapper;

/**
* Clase para administración de establecimientos por proyecto de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/10
* @Configuracion
* @Proyectos
*/

class EstablecimientosController extends Controller
{

	/**
     * Disco de storage.
     *
     * @var sStorageDisk
     */
    protected $sStorageDisk;

    /**
     * Instancia de storage.
     *
     * @var oStorage
     */
    protected $oStorage;

    /**
     * Constructor del Controller, iniciamos los middlewares para validar que el usuario tenga los permisos correctos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/05/15
     */
    public function __construct(){
        $this->sStorageDisk = config('app.env') == 'production' ? 's3' : 'local';
        $this->oStorage = config('app.env') == 'production' ? Storage::disk('s3') : Storage::disk('local');
    }

    /**
    * Metodo para mostrar la lista de establecimientos por proyecto de cliente 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/lista-de-establecimientos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param  string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/lista-de-establecimientos')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
        	Flashy::error('Por favor seleccione un proyecto de un cliente para poder ver sus establecimientos.');
            return back();	
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
        	Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        $aClienteProyectoId = [];
        $aClienteProyectoId[] = $oClienteProyecto->id;

        $aCadenas = $request->input('aCadenas') ? $request->input('aCadenas') : [];
        $aProvincias = $request->input('aProvincias') ? $request->input('aProvincias') : [];
        $aCiudades = $request->input('aCiudades') ? $request->input('aCiudades') : [];
        $aCanales = $request->input('aCanales') ? $request->input('aCanales') : [];
        $aSubcanales = $request->input('aSubcanales') ? $request->input('aSubcanales') : [];
        $sValorBusqueda = $request->input('sValorBusqueda') ? $request->input('sValorBusqueda') : null;

        $busquedaEstablecimientos = Establecimiento::buscarEstablecimientoReporte(
                                                                                    $aCadenas,
                                                                                    $aProvincias,
                                                                                    $aCiudades,
                                                                                    $aCanales,
                                                                                    $aSubcanales,
                                                                                    $aClienteProyectoId,
                                                                                    50,
                                                                                    $sValorBusqueda
                                                                                );

        $aProvincias = Catalogo::where('contexto','Provincias')->orderBy('descripcion', 'asc')->get();
        $aCiudades = Catalogo::where('contexto','Ciudades')->orderBy('descripcion', 'asc')->get();
        $aCanales = Catalogo::where('contexto','Canales')->orderBy('descripcion', 'asc')->get();
        $aSubcanales = Catalogo::where('contexto','Subcanales')->orderBy('descripcion', 'asc')->get();
        $aCadenas = Catalogo::where('contexto','Cadenas')->orderBy('descripcion', 'asc')->get();

        $data = [
            'listaEstablecimientos' => $busquedaEstablecimientos['establecimientos'],
            'numEstablecimientos' => $busquedaEstablecimientos['numEstab'],
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
            'listaProvincias' => $aProvincias,
            'listaCiudades' => $aCiudades,
            'listaCanales' => $aCanales,
            'listaSubcanales' => $aSubcanales,
            'listaCadenas' => $aCadenas,
            'aProvinciasFiltro' => $request->input('aProvincias') ? $request->input('aProvincias') : [],
            'aCiudadesFiltro' => $request->input('aCiudades') ? $request->input('aCiudades') : [],
            'aCadenasFiltro' => $request->input('aCadenas') ? $request->input('aCadenas') : [],
            'aCanalesFiltro' => $request->input('aCanales') ? $request->input('aCanales') : [],
            'aSubcanalesFiltro' => $request->input('aSubcanales') ? $request->input('aSubcanales') : [],
            'sValorBusquedaFiltro' => $request->input('sValorBusqueda') ? $request->input('sValorBusqueda') : null,
        ];
        return view("configuracion.proyectosPorCliente.establecimientos.listaEstablecimientos", $data);
    }


    /**
    * Metodo para mostrar la lista de establecimientos por proyecto de cliente en un mapa puntillado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/mapa-de-establecimientos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param  string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function indexMapa(Request $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/mapa-de-establecimientos')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
        	Flashy::error('Por favor seleccione un proyecto de un cliente para poder ver sus establecimientos.');
            return back();	
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
        	Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        $aClienteProyectoId = [];
        $aClienteProyectoId[] = $oClienteProyecto->id;

        $aCadenas = $request->input('aCadenas') ? $request->input('aCadenas') : [];
        $aProvincias = $request->input('aProvincias') ? $request->input('aProvincias') : [];
        $aCiudades = $request->input('aCiudades') ? $request->input('aCiudades') : [];
        $aCanales = $request->input('aCanales') ? $request->input('aCanales') : [];
        $aSubcanales = $request->input('aSubcanales') ? $request->input('aSubcanales') : [];
        $sValorBusqueda = $request->input('sValorBusqueda') ? $request->input('sValorBusqueda') : null;

        $busquedaEstablecimientos = Establecimiento::buscarEstablecimientoReporte(
                                                                                    $aCadenas,
                                                                                    $aProvincias,
                                                                                    $aCiudades,
                                                                                    $aCanales,
                                                                                    $aSubcanales,
                                                                                    $aClienteProyectoId,
                                                                                    null,
                                                                                    $sValorBusqueda
                                                                                );

        $aMarkerIcons = [
            0 => 'http://labs.google.com/ridefinder/images/mm_20_purple.png',
            1 => 'http://labs.google.com/ridefinder/images/mm_20_yellow.png',
            2 => 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
            3 => 'http://labs.google.com/ridefinder/images/mm_20_white.png',
            4 => 'http://labs.google.com/ridefinder/images/mm_20_green.png',
            5 => 'http://labs.google.com/ridefinder/images/mm_20_red.png',
            6 => 'http://labs.google.com/ridefinder/images/mm_20_black.png',
            7 => 'http://labs.google.com/ridefinder/images/mm_20_orange.png',
            8 => 'http://labs.google.com/ridefinder/images/mm_20_gray.png',
            9 => 'http://labs.google.com/ridefinder/images/mm_20_brown.png',
            10 => 'http://maps.google.com/mapfiles/ms/micons/blue-dot.png',
            11 => 'http://maps.google.com/mapfiles/ms/micons/red-dot.png',
            12 => 'http://maps.google.com/mapfiles/ms/micons/yellow-dot.png',
            13 => 'http://maps.google.com/mapfiles/ms/micons/orange-dot.png',
            14 => 'http://maps.google.com/mapfiles/ms/micons/ltblue-dot.png',
            15 => 'http://maps.google.com/mapfiles/ms/micons/green-dot.png',
        ];

        $aProvincias = Catalogo::where('contexto','Provincias')->orderBy('descripcion', 'asc')->get();
        $aCiudades = Catalogo::where('contexto','Ciudades')->orderBy('descripcion', 'asc')->get();
        $aCanales = Catalogo::where('contexto','Canales')->orderBy('descripcion', 'asc')->get();
        $aSubcanales = Catalogo::where('contexto','Subcanales')->orderBy('descripcion', 'asc')->get();
        $aCadenas = Catalogo::where('contexto','Cadenas')->orderBy('descripcion', 'asc')->get();

        $data = [
            'numEstablecimientos' => $busquedaEstablecimientos['numEstab'],
            'listaEstablecimientos' => $busquedaEstablecimientos['establecimientos'],
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
            'listaProvincias' => $aProvincias,
            'listaCiudades' => $aCiudades,
            'listaCanales' => $aCanales,
            'listaSubcanales' => $aSubcanales,
            'listaCadenas' => $aCadenas,
            'aProvinciasFiltro' => $request->input('aProvincias') ? $request->input('aProvincias') : [],
            'aCiudadesFiltro' => $request->input('aCiudades') ? $request->input('aCiudades') : [],
            'aCadenasFiltro' => $request->input('aCadenas') ? $request->input('aCadenas') : [],
            'aCanalesFiltro' => $request->input('aCanales') ? $request->input('aCanales') : [],
            'aSubcanalesFiltro' => $request->input('aSubcanales') ? $request->input('aSubcanales') : [],
            'sValorBusquedaFiltro' => $request->input('sValorBusqueda') ? $request->input('sValorBusqueda') : null,
        ];
        return view("configuracion.proyectosPorCliente.establecimientos.mapaEstablecimientos", $data);
    }



    /**
    * Metodo para exportar la lista de establecimientos por proyecto de cliente en un archivo de Excel
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/12
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/exportar-establecimientos
    * @method GET
    * @param  string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function exportExcel($sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/exportar-establecimientos')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente para poder ver sus establecimientos.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        $listaEstablecimientos = Establecimiento::buscarEstablecimientoPorClienteProyecto(null, $sClienteProyectoId, null);

        \Excel::create('PDV - '.$oClienteProyecto->cliente->nombre.' - '.$oClienteProyecto->proyecto->nombre.' - '.date('YmdHis'), function($excel) use($listaEstablecimientos, $oClienteProyecto){
            // Set the title
            $excel->setTitle('Lista de Pisos de Venta en el Proyecto '.$oClienteProyecto->proyecto->nombre.' para el cliente '.$oClienteProyecto->cliente->nombre.'');
            // Chain the setters
            $excel->setCreator('Go Trade')
                  ->setCompany('Sanchez Bellolio Mercadeo S.A.');
            // Call them separately
            $excel->setDescription('Archivo con la lista de pisos de venta utilizados en el proyecto '.$oClienteProyecto->proyecto->nombre.' para el cliente '.$oClienteProyecto->cliente->nombre.'');
            $excel->sheet('Lista de PDV', function($sheet) use($listaEstablecimientos, $oClienteProyecto){
                $sheet->appendRow(
                                    [
                                            'CODIGO', 
                                            'NOMBRE', 
                                            'PROVINCIA', 
                                            'CIUDAD', 
                                            'ZONA', 
                                            'PARROQUIA', 
                                            'BARRIO', 
                                            'MANZANA', 
                                            'CALLE PRINCIPAL', 
                                            'NUMERO', 
                                            'TRANSVERSAL', 
                                            'REFERENCIA', 
                                            'CANAL', 
                                            'SUBCANAL', 
                                            'TIPO DE NEGOCIO', 
                                            'CADENA', 
                                            'ADMINISTRADOR', 
                                            'TELEFONOS DE CONTACTO', 
                                            'EMAIL DE CONTACTO', 
                                            'ESTA EN RUTA', 
                                            'CLASIFICACION', 
                                            'FECHA DE CREACION', 
                                            'CREADO POR', 
                                            'FECHA ULTIMA ACTUALIZACION', 
                                            'ACTUALIZADO POR'
                                    ]);
                $sheet->cells('A1:X1', function($cells) {
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                });
                if( ! $listaEstablecimientos['establecimientos']->isEmpty()){
                    foreach($listaEstablecimientos['establecimientos'] as $oEstablecimiento){
                        $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
                        $sheet->appendRow(
                            [
                                    $oEstablecimiento->codigo,
                                    $oEstablecimiento->nombre,
                                    $oEstablecimiento->nombre_provincia,
                                    $oEstablecimiento->nombre_ciudad,
                                    $oEstablecimiento->nombre_zona,
                                    $oEstablecimiento->nombre_parroquia,
                                    $oEstablecimiento->nombre_barrio,
                                    $oEstablecimiento->direccion_manzana,
                                    $oEstablecimiento->direccion_calle_principal,
                                    $oEstablecimiento->direccion_numero,
                                    $oEstablecimiento->direccion_transversal,
                                    $oEstablecimiento->direccion_referencia,
                                    $oEstablecimiento->nombre_canal,
                                    $oEstablecimiento->nombre_subcanal,
                                    $oEstablecimiento->nombre_tipo_negocio,
                                    $oEstablecimiento->nombre_cadena,
                                    $oEstablecimiento->administrador,
                                    $oEstablecimiento->telefonos_contacto,
                                    $oEstablecimiento->email_contacto,
                                    $oEstablecimiento->en_ruta ? 'SI' : 'NO',
                                    is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
                                    TimeFormat::dateLongFormat($oEstablecimiento->created_at),
                                    $oEstablecimiento->creado_por,
                                    TimeFormat::dateLongFormat($oEstablecimiento->updated_at),
                                    $oEstablecimiento->ultima_actualizacion,
                            ]);     
                    }
                }
            });
        })->download('xls');

        return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);

    }


    

    /**
    * Metodo para mostrar la lista de establecimientos por proyecto de cliente 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/detalle-establecimiento
    * @method GET
    * @param  string  sClienteProyectoId
    * @param  string  sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function show($sClienteProyectoId = '', $sEstablecimientoId = '')
    {
    	if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/detalle-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
        	Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();	
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
        	Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if( ! $sEstablecimientoId){
        	Flashy::error('Por favor seleccione un establecimiento para ver su detalle.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
        	Flashy::error('El establecimiento seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }

        $data = [
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
            'oEstablecimiento' => $oEstablecimiento,
            'novedades' => $oEstablecimiento->novedades()->where('cliente_proyecto_id', $oClienteProyecto->id)->take(10)->get(),
            'rutas' => $oEstablecimiento->rutas()->where('cliente_proyecto_id', $oClienteProyecto->id)->get(),
        ];

        $coordenadas = $oEstablecimiento->geolocalizacion ? explode(',', $oEstablecimiento->geolocalizacion) : [0.000,0.000];

        $oMapper = Mapper::map(
                    trim($coordenadas[0]), 
                    trim($coordenadas[1]), 
                    [
                        'zoom' => 15, 
                        'locate' => false,
                        'draggable' => false, 
                        'marker' => false,
                    ]
                );

        $oMapper->marker(
                        trim($coordenadas[0]), 
                        trim($coordenadas[1])
                );

        return view("configuracion.proyectosPorCliente.establecimientos.detalleEstablecimiento", $data);	
    }


    /**
     * Metodo que muestra el formulario de edicion de un establecimiento de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /configuracion/proyectos-por-cliente/establecimientos/editar-establecimiento
     * @method GET
     * @param  string  $sId
     * @param  string  $sClienteProyectoId
     * @return \Illuminate\Http\Response
     */
    public function edit($sClienteProyectoId = '', $sId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/editar-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if($sId){
            $oEstablecimiento = Establecimiento::find($sId);
            if($oEstablecimiento){
                $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
                $aData = [
                    'oEstablecimiento' => $oEstablecimiento,
                    'oClienteProyecto' => $oClienteProyecto,
                    'oClasificacion' => is_object($oClasificacion) ? $oClasificacion : null,
                    'listaClasificaciones' => explode(',', $oClienteProyecto->clasificaciones),
                    'sGeolocalizacion' => $oEstablecimiento->geolocalizacion,
                    'listaProvincias' => Catalogo::where('contexto', 'Provincias')->get(),
                    'listaCanales' => Catalogo::where('contexto', 'Canales')->get(),
                    'listaCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
                    'idTipoNegocio' => $oEstablecimiento->codigo_tipo_negocio != '' ? Catalogo::where('contexto', 'TiposNegocio')->where('codigo1', $oEstablecimiento->codigo_tipo_negocio)->first()->id : 0,
                    'idCanal' => $oEstablecimiento->codigo_canal != '' ? Catalogo::where('contexto', 'Canales')->where('codigo1', $oEstablecimiento->codigo_canal)->first()->id : 0,
                    'idSubcanal' => $oEstablecimiento->codigo_subcanal != '' ? Catalogo::where('contexto', 'Subcanales')->where('codigo1', $oEstablecimiento->codigo_subcanal)->first()->id : 0,
                    'idCadena' => $oEstablecimiento->codigo_cadena != '' ? Catalogo::where('contexto', 'Cadenas')->where('codigo1', $oEstablecimiento->codigo_cadena)->first()->id : 0,
                    'idProvincia' => $oEstablecimiento->codigo_provincia != '' ? Catalogo::where('contexto', 'Provincias')->where('codigo1', $oEstablecimiento->codigo_provincia)->first()->id : 0,
                    'idCiudad' => $oEstablecimiento->codigo_ciudad != '' ? Catalogo::where('contexto', 'Ciudades')->where('codigo1', $oEstablecimiento->codigo_ciudad)->first()->id : 0,
                    'idZona' => $oEstablecimiento->codigo_zona != '' ? Catalogo::where('contexto', 'Zonas')->where('codigo1', $oEstablecimiento->codigo_zona)->first()->id : 0,
                    'idParroquia' => $oEstablecimiento->codigo_parroquia != '' ? Catalogo::where('contexto', 'Parroquias')->where('codigo1', $oEstablecimiento->codigo_parroquia)->first()->id : 0,
                    'idBarrio' => $oEstablecimiento->codigo_barrio != '' ? Catalogo::where('contexto', 'Barrios')->where('codigo1', $oEstablecimiento->codigo_barrio)->first()->id : 0,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Establecimiento',
                ];

                $coordinates = $oEstablecimiento->geolocalizacion ? explode(',', $oEstablecimiento->geolocalizacion) : [0.000,0.000];

                Mapper::map(
                        $coordinates[0], 
                        $coordinates[1], 
                        [
                            'zoom' => 15, 
                            'locate' => false,
                            'draggable' => true, 
                            'marker' => false,
                        ]
                    )
                    ->marker(
                            $coordinates[0], 
                            $coordinates[1], 
                            [
                                'draggable' => true,
                                'eventDragEnd' => '$("#geolocalizacion").val(this.position.lat()+","+this.position.lng());',
                            ]
                    );

                return view('configuracion.proyectosPorCliente.establecimientos.crearEditarEstablecimiento', $aData);
            }else{
                Flashy::error('El establecimiento seleccionado no existe');
                return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
            }
        }else{
            Flashy::error('Por favor seleccione un establecimiento para poder editarlo.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }
    }

    /**
     * Metodo que actualiza la informacion del establecimienti en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /configuracion/proyectos-por-cliente/establecimientos/editar-establecimiento
     * @method POST
     * @param  \SICE\Http\Requests\EstablecimientoUpdateFormRequest  $establecimientoUpdateFormRequest
     * @param  string  $sClienteProyectoId
     * @param  string  $sId
     * @return \Illuminate\Http\Response
     */
    public function update(EstablecimientoUpdateFormRequest $establecimientoFormRequest, $sClienteProyectoId = '', $sId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/editar-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if($sId){
            $oEstablecimiento = Establecimiento::find($sId);
            if($oEstablecimiento){
                $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oEstablecimiento->id)->first();
                $fotoAnterior = $oEstablecimiento->foto;
                $oEstablecimiento->nombre = $establecimientoFormRequest->input('nombre');
                $oEstablecimiento->direccion_calle_principal = $establecimientoFormRequest->input('direccion_calle_principal');
                $oEstablecimiento->direccion_numero = $establecimientoFormRequest->input('direccion_numero');
                $oEstablecimiento->direccion_transversal = $establecimientoFormRequest->input('direccion_transversal');
                $oEstablecimiento->direccion_referencia = $establecimientoFormRequest->input('direccion_referencia');
                $oEstablecimiento->administrador = $establecimientoFormRequest->input('administrador');
                $oEstablecimiento->telefonos_contacto = $establecimientoFormRequest->input('telefonos_contacto');
                $oEstablecimiento->email_contacto = $establecimientoFormRequest->input('email_contacto');
                $oEstablecimiento->geolocalizacion = $establecimientoFormRequest->input('geolocalizacion');

                $sCodigoProvincia = '00';
                $sCodigoCiudad = '00';
                $sCodigoZona = '0';
                $sCodigoParroquia = '000';
                $sCodigoBarrio = '000';
                $sCodigoCanal = '0';
                $sCodigoSubcanal = '00';
                $sCodigoTipoNegocio = '000';
                $sCodigoCadena = '000';
                
                if($establecimientoFormRequest->input('id_canal') != ""){
                    $oEstablecimiento->uuid_canal = $establecimientoFormRequest->input('id_canal');
                    $oEstablecimiento->codigo_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->codigo1;
                    $oEstablecimiento->nombre_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->descripcion;
                    $sCodigoCanal = $oEstablecimiento->codigo_canal;
                }else{
                    $oEstablecimiento->uuid_canal = null;
                    $oEstablecimiento->codigo_canal = null;
                    $oEstablecimiento->nombre_canal = null;
                }
                if($establecimientoFormRequest->input('id_subcanal') != ""){
                    $oEstablecimiento->uuid_subcanal = $establecimientoFormRequest->input('id_subcanal');
                    $oEstablecimiento->codigo_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->codigo1;
                    $oEstablecimiento->nombre_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->descripcion;
                    $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
                }else{
                    $oEstablecimiento->uuid_subcanal = null;
                    $oEstablecimiento->codigo_subcanal = null;
                    $oEstablecimiento->nombre_subcanal = null;
                }
                if($establecimientoFormRequest->input('id_tipo_negocio') != ""){
                    $oEstablecimiento->uuid_tipo_negocio = $establecimientoFormRequest->input('id_tipo_negocio');
                    $oEstablecimiento->codigo_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->codigo1;
                    $oEstablecimiento->nombre_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->descripcion;
                    $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
                }else{
                    $oEstablecimiento->uuid_tipo_negocio = null;
                    $oEstablecimiento->codigo_tipo_negocio = null;
                    $oEstablecimiento->nombre_tipo_negocio = null;
                }
                if($establecimientoFormRequest->input('id_provincia') != ""){
                    $oEstablecimiento->uuid_provincia = $establecimientoFormRequest->input('id_provincia');
                    $oEstablecimiento->codigo_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->codigo1;
                    $oEstablecimiento->nombre_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->descripcion;
                    $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
                }else{
                    $oEstablecimiento->uuid_provincia = null;
                    $oEstablecimiento->codigo_provincia = null;
                    $oEstablecimiento->nombre_provincia = null;
                }
                if($establecimientoFormRequest->input('id_ciudad') != ""){
                    $oEstablecimiento->uuid_ciudad = $establecimientoFormRequest->input('id_ciudad');
                    $oEstablecimiento->codigo_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->codigo1;
                    $oEstablecimiento->nombre_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->descripcion;
                    $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
                }else{
                    $oEstablecimiento->uuid_ciudad = null;
                    $oEstablecimiento->codigo_ciudad = null;
                    $oEstablecimiento->nombre_ciudad = null;
                }
                if($establecimientoFormRequest->input('id_zona') != ""){
                    $oEstablecimiento->uuid_zona = $establecimientoFormRequest->input('id_zona');
                    $oEstablecimiento->codigo_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->codigo1;
                    $oEstablecimiento->nombre_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->descripcion;
                    $sCodigoZona = $oEstablecimiento->codigo_zona;
                }else{
                    $oEstablecimiento->uuid_zona = null;
                    $oEstablecimiento->codigo_zona = null;
                    $oEstablecimiento->nombre_zona = null;
                }
                if($establecimientoFormRequest->input('id_parroquia') != ""){
                    $oEstablecimiento->uuid_parroquia = $establecimientoFormRequest->input('id_parroquia');
                    $oEstablecimiento->codigo_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->codigo1;
                    $oEstablecimiento->nombre_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->descripcion;
                    $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
                }else{
                    $oEstablecimiento->uuid_parroquia = null;
                    $oEstablecimiento->codigo_parroquia = null;
                    $oEstablecimiento->nombre_parroquia = null;
                }
                if($establecimientoFormRequest->input('id_barrio') != ""){
                    $oEstablecimiento->uuid_barrio = $establecimientoFormRequest->input('id_barrio');
                    $oEstablecimiento->codigo_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->codigo1;
                    $oEstablecimiento->nombre_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->descripcion;
                    $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
                }else{
                    $oEstablecimiento->uuid_barrio = null;
                    $oEstablecimiento->codigo_barrio = null;
                    $oEstablecimiento->nombre_barrio = null;
                }
                if($establecimientoFormRequest->input('id_cadena') != ""){
                    $oEstablecimiento->uuid_cadena = $establecimientoFormRequest->input('id_cadena');
                    $oEstablecimiento->codigo_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->codigo1;
                    $oEstablecimiento->nombre_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->descripcion;
                    $sCodigoCadena = $oEstablecimiento->codigo_cadena;
                }else{
                    $oEstablecimiento->uuid_cadena = null;
                    $oEstablecimiento->codigo_cadena = null;
                    $oEstablecimiento->nombre_cadena = null;
                }
                
                $oEstablecimiento->codigo = Establecimiento::generaCodigo(
                                                                                $sCodigoProvincia,
                                                                                $sCodigoCiudad,
                                                                                $sCodigoZona,
                                                                                $sCodigoParroquia,
                                                                                $sCodigoBarrio,
                                                                                $sCodigoCanal,
                                                                                $sCodigoSubcanal,
                                                                                $sCodigoTipoNegocio,
                                                                                $sCodigoCadena
                                                                            );

                $oEstablecimiento->ultima_actualizacion = \Auth::user()->nombre.' '.\Auth::user()->apellido;

                if ($establecimientoFormRequest->hasFile('foto')){
                    if ($establecimientoFormRequest->file('foto')->isValid()){
                        $fotografia = $establecimientoFormRequest->file('foto');
                        if(!is_dir('./resources/fotosEstablecimientos')){
                            mkdir('./resources/fotosEstablecimientos',777);
                        }
                        $i=0;
                        $info = explode(".",$fotografia->getClientOriginalName());
                        $oEstablecimiento->foto = $fotografia->getClientOriginalName();
                        while(file_exists('./resources/fotosEstablecimientos/'.$oEstablecimiento->foto)){
                            $i++;
                            $oEstablecimiento->foto = $info[0].$i.".".$info[1];
                        }
                        $establecimientoFormRequest->file('foto')->move('resources/fotosEstablecimientos',$oEstablecimiento->foto);
                    }
                }
                
                if($oEstablecimiento->save()){
                    if($fotoAnterior && ($fotoAnterior != $oEstablecimiento->foto)){
                        unlink('./resources/fotosEstablecimientos/'.$fotoAnterior);
                    }

                    if( ! is_object($oClasificacion)){
                        $oClasificacion = new ClasificacionEstablecimiento;
                    }

                    $oClasificacion->clasificacion = $establecimientoFormRequest->clasificacion;
                    $oClasificacion->cliente_proyecto_id = $oClienteProyecto->id;
                    $oClasificacion->cliente_id = $oClienteProyecto->cliente_id;
                    $oClasificacion->proyecto_id = $oClienteProyecto->proyecto_id;
                    $oClasificacion->establecimiento_id = $oEstablecimiento->id;
                    $oClasificacion->save();

                    Flashy::success('Establecimientio '.$oEstablecimiento->nombre.' actualizado satisfactoriamente');
                    return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
                }else{
                    Flashy::error('Establecimientio '.$oEstablecimiento->nombre.' no pudo ser actualizado');
                    return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
                }
            }else{
                Flashy::error('El establecimiento seleccionado no existe');
                return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
            }
        }else{
            Flashy::error('Por favor seleccione un establecimiento para poder editarlo.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }
    }


    /**
    * Metodo para quitar un establecimiento del proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/quitar-establecimiento
    * @method GET
    * @param  string  sClienteProyectoId
    * @param  string  sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function destroy($sClienteProyectoId = '', $sEstablecimientoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/quitar-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if( ! $sEstablecimientoId){
            Flashy::error('Por favor seleccione un establecimiento para eliminarlo de este proyecto.');
            return back();
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
            Flashy::error('El establecimiento seleccionado no existe. Por favor seleccione otro.');
            return back();
        }

        $oClienteEstablecimiento = ClienteEstablecimiento::where('establecimiento_id', $oEstablecimiento->id)
                                                         ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                                         ->first();
        if( ! is_object($oClienteEstablecimiento)){
            Flashy::error('El establecimiento seleccionado no esta asignado a este proyecto.');
            return back();
        }

        if($oClienteEstablecimiento->delete()){
            Flashy::success('El establecimiento seleccionado ha sido eliminado de este proyecto.');
        }else{
            Flashy::error('El establecimiento seleccionado no ha podido ser eliminado de este proyecto.Por favor intentelo nuevamente luego de unos minutos.');
        }
        
        return back();
    }


    /**
    * Metodo para asignar un establecimiento al proyecto seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/19
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/seleccionar-establecimiento
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param  string  sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function assignList(Request $request, $sClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/asignar-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        $data = [
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
        ];

        $listaEstablecimientos = Establecimiento::buscarEstablecimientoSinClienteProyecto($request->input('valor_busqueda', ''));

        $data = [
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
            'oCliente' => $oClienteProyecto->cliente,
            'oProyecto' => $oClienteProyecto->proyecto,
            'listaEstablecimientos' => $listaEstablecimientos,
        ];
        return view("configuracion.proyectosPorCliente.establecimientos.listaGeneralEstablecimientos", $data);
    }

    /**
    * Metodo para asignar un establecimiento al proyecto seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/19
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos/asignar-establecimiento
    * @method GET
    * @param  \Illuminate\Http\Request  $request
    * @param  string  sClienteProyectoId
    * @param  string  sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function assign($sClienteProyectoId = '', $sEstablecimientoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos/asignar-establecimiento')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();  
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if( ! $sEstablecimientoId){
            Flashy::error('Por favor seleccione un establecimiento para eliminarlo de este proyecto.');
            return back();
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
            Flashy::error('El establecimiento seleccionado no existe. Por favor seleccione otro.');
            return back();
        }

        $oClienteEstablecimiento = ClienteEstablecimiento::where('establecimiento_id', $oEstablecimiento->id)
                                                         ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                                         ->first();
        if( is_object($oClienteEstablecimiento)){
            Flashy::error('El establecimiento seleccionado ya esta asignado a este proyecto.');
            return back();
        }

        $oClienteEstablecimiento = new ClienteEstablecimiento;
        $oClienteEstablecimiento->en_ruta = 0;
        $oClienteEstablecimiento->estado = 1;
        $oClienteEstablecimiento->establecimiento_id = $oEstablecimiento->id;
        $oClienteEstablecimiento->cliente_proyecto_id = $oClienteProyecto->id;
        $oClienteEstablecimiento->cliente_id = $oClienteProyecto->cliente->id;
        $oClienteEstablecimiento->proyecto_id = $oClienteProyecto->proyecto->id;

        if($oClienteEstablecimiento->save()){
            Flashy::success('El establecimiento seleccionado ha sido asignado a este proyecto.');
        }else{
            Flashy::error('El establecimiento seleccionado no ha podido ser asignado a este proyecto.Por favor intentelo nuevamente luego de unos minutos.');
        }
        
        return back();
    }
}
