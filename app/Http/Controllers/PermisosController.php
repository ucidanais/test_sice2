<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Permiso;
use SICE\Modulo;
use SICE\Usuario;
use SICE\Http\Requests\PermisoFormRequest;

/**
* Clase para administración de permisos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Autenticacion/Autorizacion
*/

class PermisosController extends Controller
{
    /**
    * Metodo para mostrar la lista de permisos del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/permisos/lista-de-permisos
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/lista-de-permisos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaPermisos' => Permiso::paginate(50),
            'menuActivo' => 'seguridades',
            'titulo' => 'SICE :: Permisos',
        ];
        return view("auth.permisos.listaPermisos", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo permiso en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/permisos/agregar-permiso
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/agregar-permiso')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aListaModulos = Modulo::lists('nombre','id');

        $aData = [
            'menuActivo' => 'seguridades',
            'listaModulos' => $aListaModulos,
            'titulo' => 'SICE :: Crear nuevo Permiso',
        ];
        return view("auth.permisos.crearEditarPermiso", $aData);
    }

    /**
    * Metodo para guardar un nuevo permiso en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/permisos/agregar-permiso
    * @method POST
    * @param  \SICE\Http\Requests\PermisoFormRequest  $permisoFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(PermisoFormRequest $permisoFormRequest)
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/agregar-permiso')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $oPermiso = new Permiso;
        $oPermiso->modulo_id = $permisoFormRequest->input('modulo_id');
        $oPermiso->nombre = $permisoFormRequest->input('nombre');
        $oPermiso->path = $permisoFormRequest->input('path');
        
        if($oPermiso->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$oPermiso->nombre.' guardado satisfactoriamente</div>');
            return redirect('seguridades/permisos');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$oPermiso->nombre.' no pudo ser guardado</div>');
            return redirect('seguridades/permisos');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un permiso del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/permisos/editar-permiso
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/editar-permiso')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oPermiso = Permiso::find($iId);
            if($oPermiso){
                $aListaModulos = Modulo::lists('nombre','id');
                $aData = [
                    'objPermiso' => $oPermiso,
                    'listaModulos' => $aListaModulos,
                    'menuActivo' => 'seguridades',
                    'titulo' => 'SICE :: Editar Permiso',
                ];
                return view('auth.permisos.crearEditarPermiso', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El permiso seleccionado no existe</div>');
                return redirect('seguridades/permisos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un permiso para poder editarlo.</div>');
            return redirect('seguridades/permisos');
        }
    }

    /**
     * Metodo que actualiza la informacion del permiso en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/permisos/editar-permiso
     * @method POST
     * @param  \SICE\Http\Requests\PermisoFormRequest  $permisoFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(PermisoFormRequest $permisoFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/editar-permiso')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oPermiso = Permiso::find($iId);
            if($oPermiso){
                $oPermiso->modulo_id = $permisoFormRequest->input('modulo_id');
                $oPermiso->nombre = $permisoFormRequest->input('nombre');
                $oPermiso->path = $permisoFormRequest->input('path');
                
                if($oPermiso->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$oPermiso->nombre.' guardado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$oPermiso->nombre.' no pudo ser guardado</div>');
                }
                return redirect('seguridades/permisos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El permiso seleccionado no existe</div>');
                return redirect('seguridades/permisos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un permiso para poder actualizar su información</div>');
            return redirect('seguridades/permisos');
        }
    }

    /**
     * Metodo que elimina un permiso del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/permisos/eliminar-permiso
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/permisos/eliminar-permiso')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oPermiso = Permiso::find($iId);
            if($oPermiso){
                $nombre = $oPermiso->nombre;
                $oPermiso->usuarios()->detach();
                if($oPermiso->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Permiso '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('seguridades/permisos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El permiso seleccionado no existe</div>');
                return redirect('seguridades/permisos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un permiso para poder eliminarlo</div>');
            return redirect('seguridades/permisos');
        }
    }
}
