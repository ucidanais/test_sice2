<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Cliente;
use SICE\ParametroCliente;
use SICE\Usuario;
use SICE\Http\Requests\ClienteFormRequest;

/**
* Clase para administración de clientes
* @Autor Raúl Chauvin
* @FechaCreacion  2016/08/17
* @Parametrizacion
* @Configuracion
*/

class ClientesController extends Controller
{
    /**
    * Metodo para mostrar la lista de clientes del sistema 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/17
    *
    * @route /configuracion/clientes/lista-de-clientes
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/lista-de-clientes')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'listaClientes' => Cliente::buscarCliente($request->input('valor_busqueda')),
            'menuActivo' => 'configuracion',
            'titulo' => 'ERP :: Lista de Clientes',
        ];
        return view("configuracion.clientes.listaClientes", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/17
    *
    * @route /configuracion/clientes/agregar-cliente
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/agregar-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'desde' => null,
            'hasta' => null,
            'numUsuarios' => 0,
            'menuActivo' => 'configuracion',
            'titulo' => 'ERP :: Agregar nuevo Cliente',
        ];
        return view("configuracion.clientes.crearEditarCliente", $data);
    }

    /**
    * Metodo para guardar un nuevo cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/17
    *
    * @route /configuracion/clientes/agregar-cliente
    * @method POST
    * @param  \SICE\Http\Requests\ClienteFormRequest  $clienteFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ClienteFormRequest $clienteFormRequest)
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/agregar-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $oCliente = new Cliente;
        $oCliente->codigo = $clienteFormRequest->input('codigo');
        $oCliente->nombre = $clienteFormRequest->input('nombre');
        $oCliente->estado = $clienteFormRequest->input('estado');

        if ($clienteFormRequest->hasFile('logotipo')){
            if ($clienteFormRequest->file('logotipo')->isValid()){
                $logo = $clienteFormRequest->file('logotipo');
                if(!is_dir('./resources/logosClientes')){
                    mkdir('./resources/logosClientes',777);
                }
                $i=0;
                $info = explode(".",$logo->getClientOriginalName());
                $oCliente->logotipo = $logo->getClientOriginalName();
                while(file_exists('./resources/logosClientes/'.$oCliente->logotipo)){
                    $i++;
                    $oCliente->logotipo = $info[0].$i.".".$info[1];
                }
                $clienteFormRequest->file('logotipo')->move('resources/logosClientes',$oCliente->logotipo);
            }
        }
        
        if($oCliente->save()){
            $oLicencia = new ParametroCliente;
            $oNumUsuarios = new ParametroCliente;

            $oLicencia->cliente_id = $oCliente->id;
            $oLicencia->codigo = '01';
            $oLicencia->parametro = 'Vigencia de Licencia';
            $oLicencia->vigencia_inicio = $clienteFormRequest->input('desde');
            $oLicencia->vigencia_fin = $clienteFormRequest->input('hasta');
            $oLicencia->save();

            $oNumUsuarios->cliente_id = $oCliente->id;
            $oNumUsuarios->codigo = '02';
            $oNumUsuarios->parametro = 'Numero de Usuarios';
            $oNumUsuarios->valor_numero = $clienteFormRequest->input('numero_usuarios');
            $oNumUsuarios->save();

            \Session::flash('message', '<div class="alert alert-success">Cliente '.$oCliente->nombre.' creado satisfactoriamente</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Cliente '.$oCliente->nombre.' no pudo ser creado</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
    * Metodo para mostrar el detalle de un cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/17
    *
    * @route /configuracion/clientes/detalle-de-cliente
    * @method GET
    * @param  int $iId
    * @return \Illuminate\Http\Response
    */
    public function show($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/detalle-de-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un cliente de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/17
     *
     * @route /configuracion/clientes/editar-cliente
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/editar-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCliente = Cliente::find($iId);
            if($oCliente){
                $data = [
                    'oCliente' => $oCliente,
                    'oLicencia' => $oCliente->parametrosCliente()->where('codigo', '01')->first(),
                    'oNumUsuarios' => $oCliente->parametrosCliente()->where('codigo', '02')->first(),
                    'desde' => $oCliente->parametrosCliente->count() ? $oCliente->parametrosCliente()->where('codigo', '01')->first()->vigencia_inicio : null,
                    'hasta' => $oCliente->parametrosCliente->count() ? $oCliente->parametrosCliente()->where('codigo', '01')->first()->vigencia_fin : null,
                    'numUsuarios' => $oCliente->parametrosCliente->count() ? $oCliente->parametrosCliente()->where('codigo', '02')->first()->valor_numero : 0,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'ERP :: Editar Cliente',
                ];
                return view("configuracion.clientes.crearEditarCliente", $data);
            }else{
                \Session::flash('message', '<div class="alert alert-success">La empresa seleccionada no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione una empresa para poder editarla.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
     * Metodo que actualiza la informacion del cliente en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/17
     *
     * @route /configuracion/clientes/editar-cliente
     * @method POST
     * @param  \SICE\Http\Requests\ClienteFormRequest  $clienteFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteFormRequest $clienteFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/editar-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCliente = Cliente::find($iId);
            if($oCliente){
                $oCliente->codigo = $clienteFormRequest->input('codigo');
                $oCliente->nombre = $clienteFormRequest->input('nombre');
                $oCliente->estado = $clienteFormRequest->input('estado');

                $logotipoAnterior = $oCliente->logotipo;
                if ($clienteFormRequest->hasFile('logotipo')){
                    if ($clienteFormRequest->file('logotipo')->isValid()){
                        $logo = $clienteFormRequest->file('logotipo');
                        if(!is_dir('./resources/logosClientes')){
                            mkdir('./resources/logosClientes',777);
                        }
                        $i=0;
                        $info = explode(".",$logo->getClientOriginalName());
                        $oCliente->logotipo = $logo->getClientOriginalName();
                        while(file_exists('./resources/logosClientes/'.$oCliente->logotipo)){
                            $i++;
                            $oCliente->logotipo = $info[0].$i.".".$info[1];
                        }
                        $clienteFormRequest->file('logotipo')->move('resources/logosClientes',$oCliente->logotipo);
                    }
                }
                
                if($oCliente->save()){
                    if($logotipoAnterior && ($logotipoAnterior != $oCliente->logotipo)){
                        unlink('./resources/logosClientes/'.$logotipoAnterior);
                    }

                    $oLicencia = ParametroCliente::find($clienteFormRequest->input('idLicencia'));
                    $oNumUsuarios = ParametroCliente::find($clienteFormRequest->input('idNumUsuarios'));

                    $oLicencia->cliente_id = $oCliente->id;
                    $oLicencia->codigo = '01';
                    $oLicencia->parametro = 'Vigencia de Licencia';
                    $oLicencia->vigencia_inicio = $clienteFormRequest->input('desde');
                    $oLicencia->vigencia_fin = $clienteFormRequest->input('hasta');
                    $oLicencia->save();

                    $oNumUsuarios->cliente_id = $oCliente->id;
                    $oNumUsuarios->codigo = '02';
                    $oNumUsuarios->parametro = 'Numero de Usuarios';
                    $oNumUsuarios->valor_numero = $clienteFormRequest->input('numero_usuarios');
                    $oNumUsuarios->save();

                    
                    \Session::flash('message', '<div class="alert alert-success">Cliente '.$oCliente->nombre.' actualizado satisfactoriamente</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }else{
                    \Session::flash('message', '<div class="alert alert-danger">Cliente '.$oCliente->nombre.' no pudo ser actualizado</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }
            }else{
                \Session::flash('message', '<div class="alert alert-success">El cliente seleccionado no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione un cliente para poder editarlo.</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
     * Metodo que elimina un cliente del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/17
     *
     * @route /configuracion/clientes/eliminar-cliente
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/clientes/eliminar-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCliente = Cliente::find($iId);
            if($oCliente){
                $nombre = $oCliente->nombre;
                $aClientes = $oCliente->clientesProyecto;
                $aEquipoTrabajo = $oCliente->equiposTrabajo;
                $aEncuestas = $oCliente->encuestas;
                $aFamiliasProducto = $oCliente->familiasProducto;
                $aCategoriasProducto = $oCliente->categoriasProducto;
                $aPresentacionesProducto = $oCliente->presentacionesProducto;
                $aProductosCadena = $oCliente->productosCadena;
                $aClasificacionesEstablecimiento = $oCliente->clasificacionesEstablecimiento;
                $aRutas = $oCliente->rutas;
                $aGruposProducto = $oCliente->gruposProducto;
                
                if(count($aClientes)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene clientes relacionados</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aEquipoTrabajo)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene equipos de trabajo relacionados</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aEncuestas)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene encuestas relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aFamiliasProducto)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene familias de productos relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aCategoriasProducto)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene categorias de productos relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aPresentacionesProducto)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene presentaciones de productos relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aProductosCadena)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene productos relacionados</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aClasificacionesEstablecimiento)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene clasificaciones de establecimientos relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aRutas)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene rutas relacionadas</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                if(count($aGruposProducto)){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado debido a que tiene grupos de productos relacionados</div>');
                    return redirect('configuracion/clientes/lista-de-clientes');
                }

                $oCliente->parametrosCliente()->delete();
                $oldLogo = $oCliente->logotipo;
                if($oCliente->delete()){
                    if($oldLogo){
                        unlink('./resources/logosClientes/'.$oldLogo);
                    }
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('configuracion/clientes/lista-de-clientes');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/clientes/lista-de-clientes');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder eliminarlo</div>');
            return redirect('configuracion/clientes/lista-de-clientes');
        }
    }

    /**
     * Metodo que cambia el estado de un cliente del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/17
     *
     * @route /configuracion/clientes/cambiar-estado-cliente
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function cambiaEstado($iId = '')
    {

        if(!Usuario::verificaPermiso('configuracion/clientes/cambiar-estado-cliente')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCliente = Cliente::find($iId);
            if($oCliente){
                $oCliente->estado = $oCliente->estado == 1 ? 0 : 1;
                $oCliente->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del cliente '.$oCliente->nombre.' satisfactoriamente</div>');
                return redirect('configuracion/clientes');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Cliente seleccionado no existe</div>');
                return redirect('configuracion/clientes');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un cliente para poder cambiar su estado</div>');
            return redirect('configuracion/clientes');
        }
    }
}
