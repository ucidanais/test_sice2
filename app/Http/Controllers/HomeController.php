<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;
use SICE\Http\Requests;

use SICE\Usuario;

use Auth;

use Flashy;

class HomeController extends Controller
{
    /**
    * Metodo para mostrar la portada del sistema 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/17
    *
    * @route /home
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function index()
    {

        if(Auth::user()->tipo == 'E'){
            $oCliente = Usuario::getCliente();
            if(is_object($oCliente)){
                return redirect()->route('clientes.proyectos.list');
            }
            return redirect()->route('profile');
        }

        $data = [
        	'titulo' => 'SICE :: Inicio',
            'menuActivo' => 'dashboard',
        ];
        return view('home',$data);
    }
}
