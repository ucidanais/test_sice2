<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\PresentacionProducto;
use SICE\ProductoCadena;
use SICE\ClienteProyecto;
use SICE\GrupoProducto;
use SICE\Usuario;
use SICE\Catalogo;
use SICE\Http\Requests\PresentacionProductoFormRequest;

/**
* Clase para administración de presentaciones de producto en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/03/21
* @Configuracion
*/

class PresentacionesProductosController extends Controller
{
    /**
    * Metodo para mostrar la lista de presentaciones de producto del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/presentaciones-de-productos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iCategoriaProductoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iCategoriaProductoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                $aPresentacionesProducto = $oCategoriaProducto->presentacionesProducto;
                $aData = [
                    'oCategoriaProducto' => $oCategoriaProducto,
                    'oFamiliaProducto' => $oCategoriaProducto->familiaProducto,
                    'oCliente' => $oCategoriaProducto->cliente,
                    'oProyecto' => $oCategoriaProducto->proyecto,
                    'aPresentacionesProducto' => $aPresentacionesProducto,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.presentacionesProducto.listaPresentacionesProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto para poder ver sus categorias</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de creacion de una presentacion de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto
    * @method GET
    * @param int $iCategoriaProductoId
    * @return \Illuminate\Http\Response
    */
    public function create($iCategoriaProductoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                $aPresentacionesProducto = $oCategoriaProducto->presentacionesProducto()->marca()->orderBy('nombre', 'asc')->get();
                $oClienteProyecto = $oCategoriaProducto->clienteProyecto;
                $aGruposProducto = $oClienteProyecto->gruposProducto;
                $aData = [
                    'oCategoriaProducto' => $oCategoriaProducto,
                    'oFamiliaProducto' => $oCategoriaProducto->familiaProducto,
                    'oClienteProyecto' => $oCategoriaProducto->clienteProyecto,
                    'oCliente' => $oCategoriaProducto->cliente,
                    'oProyecto' => $oCategoriaProducto->proyecto,
                    'listaCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
                    'listaPresentacionesProducto' => $aPresentacionesProducto,
                    'listaGruposProducto' => $aGruposProducto,
                    'oPresentacionProducto' => null,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.presentacionesProducto.crearEditarPresentacionProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos una presentacion de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto
    * @method POST
    * @param  SICE\Http\Requests\PresentacionProductoFormRequest $presentacionProductoFormRequest
    * @param int $iCategoriaProductoId
    * @return \Illuminate\Http\Response
    */
    public function store(PresentacionProductoFormRequest $presentacionProductoFormRequest, $iCategoriaProductoId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-presentacion-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                $oClienteProyecto = ClienteProyecto::find($oCategoriaProducto->familiaProducto->cliente_proyecto_id);
                $oPresentacionProducto = new PresentacionProducto;
                $oPresentacionProducto->nombre = $presentacionProductoFormRequest->input('nombre');
                $oPresentacionProducto->precio = $presentacionProductoFormRequest->input('precio');
                $oPresentacionProducto->contexto = $presentacionProductoFormRequest->input('contexto');
                $oPresentacionProducto->es_competencia = $presentacionProductoFormRequest->input('es_competencia', 0);
                $oPresentacionProducto->categoria_producto_id = $oCategoriaProducto->id;
                $oPresentacionProducto->familia_producto_id = $oCategoriaProducto->familiaProducto->id;
                $oPresentacionProducto->presentacion_producto_id = $presentacionProductoFormRequest->input('presentacion_producto_id') ? $presentacionProductoFormRequest->input('presentacion_producto_id') : NULL;
                $oPresentacionProducto->cliente_proyecto_id = $oCategoriaProducto->cliente_proyecto_id;
                $oPresentacionProducto->cliente_id = $oCategoriaProducto->cliente_id;
                $oPresentacionProducto->proyecto_id = $oCategoriaProducto->proyecto_id;
                if($oPresentacionProducto->save()){
                    
                    if(is_array($presentacionProductoFormRequest->grupos)){
                        if(count($presentacionProductoFormRequest->grupos)){
                            $oPresentacionProducto->gruposProducto()->sync($presentacionProductoFormRequest->grupos);
                        }
                    }
                    
                    if($presentacionProductoFormRequest->input('cadenas')){
                        if(is_array($presentacionProductoFormRequest->input('cadenas'))){
                            foreach($presentacionProductoFormRequest->input('cadenas') as $key => $value){
                                $aValue = explode('|', $value);
                                $oCadena = Catalogo::find($aValue[0]);
                                
                                if(isset($aValue[1])){
                                    $oProductoCadena = ProductoCadena::where('codigo_cadena', $oCadena->id)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('clasificacion_establecimiento', $aValue[1])->where('cliente_proyecto_id', $oPresentacionProducto->cliente_proyecto_id)->first();
                                }else{
                                    $oProductoCadena = ProductoCadena::where('codigo_cadena', $oCadena->id)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oPresentacionProducto->cliente_proyecto_id)->first();    
                                }
                                

                                if( ! $oProductoCadena){
                                    if(is_object($oClienteProyecto)){
                                        $oProductoCadena = new ProductoCadena;
                                        $oProductoCadena->codigo_cadena = $oCadena->id;
                                        $oProductoCadena->nombre_cadena = $oCadena->descripcion;
                                        $oProductoCadena->clasificacion_establecimiento = isset($aValue[1]) ? $aValue[1] : '';
                                        $oProductoCadena->presentacion_producto_id = $oPresentacionProducto->id;
                                        $oProductoCadena->categoria_producto_id = $oPresentacionProducto->categoria_producto_id;
                                        $oProductoCadena->familia_producto_id = $oPresentacionProducto->familia_producto_id;
                                        $oProductoCadena->cliente_proyecto_id = $oClienteProyecto->id;
                                        $oProductoCadena->cliente_id = $oClienteProyecto->cliente_id;
                                        $oProductoCadena->proyecto_id = $oClienteProyecto->proyecto_id;
                                        $oProductoCadena->save();
                                    }
                                }
                            }
                        }
                    }
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Presentacion de producto '.$oPresentacionProducto->nombre.' agregara exitosamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Presentacion de producto '.$oPresentacionProducto->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                }
                return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de productos seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de productos</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de edición de una presentacion de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto
    * @method GET
    * @param int $iCategoriaProductoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function edit($iCategoriaProductoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                if($iId){
                    $oPresentacionProducto = PresentacionProducto::find($iId);
                    if($oPresentacionProducto){
                        $aPresentacionesProducto = $oCategoriaProducto->presentacionesProducto()->marca()->orderBy('nombre', 'asc')->get();
                        $oClienteProyecto = $oPresentacionProducto->clienteProyecto;
                        $aGruposProducto = $oClienteProyecto->gruposProducto;
                        $aData = [
                            'oCategoriaProducto' => $oCategoriaProducto,
                            'oFamiliaProducto' => $oCategoriaProducto->familiaProducto,
                            'oClienteProyecto' => $oPresentacionProducto->clienteProyecto,
                            'oCliente' => $oCategoriaProducto->cliente,
                            'oProyecto' => $oCategoriaProducto->proyecto,
                            'oPresentacionProducto' => $oPresentacionProducto,
                            'listaCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
                            'listaPresentacionesProducto' => $aPresentacionesProducto,
                            'listaGruposProducto' => $aGruposProducto,
                            'menuActivo' => 'configuracion',
                            'titulo' => 'SICE :: Configurar Proyecto por cliente',
                        ];
                        return view("configuracion.proyectosPorCliente.presentacionesProducto.crearEditarPresentacionProducto", $aData);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una presentacion de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos los cambios realizados presentacion de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto
    * @method POST
    * @param  SICE\Http\Requests\PresentacionProductoFormRequest $presentacionProductoFormRequest
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function update(PresentacionProductoFormRequest $presentacionProductoFormRequest, $iId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-presentacion-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $iCategoriaProductoId = $presentacionProductoFormRequest->input('categoria_producto_id');

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                $oClienteProyecto = ClienteProyecto::find($oCategoriaProducto->familiaProducto->cliente_proyecto_id);
                if($iId){
                    $oPresentacionProducto = PresentacionProducto::find($iId);
                    if($oPresentacionProducto){
                        $oPresentacionProducto->nombre = $presentacionProductoFormRequest->input('nombre');
                        $oPresentacionProducto->precio = $presentacionProductoFormRequest->input('precio');
                        $oPresentacionProducto->contexto = $presentacionProductoFormRequest->input('contexto');
                        $oPresentacionProducto->es_competencia = $presentacionProductoFormRequest->input('es_competencia', 0);
                        $oPresentacionProducto->categoria_producto_id = $oCategoriaProducto->id;
                        $oPresentacionProducto->familia_producto_id = $oCategoriaProducto->familiaProducto->id;
                        $oPresentacionProducto->presentacion_producto_id = $presentacionProductoFormRequest->input('presentacion_producto_id') ? $presentacionProductoFormRequest->input('presentacion_producto_id') : NULL;
                        $oPresentacionProducto->cliente_proyecto_id = $oCategoriaProducto->cliente_proyecto_id;
                        $oPresentacionProducto->cliente_id = $oCategoriaProducto->cliente_id;
                        $oPresentacionProducto->proyecto_id = $oCategoriaProducto->proyecto_id;
                        if($oPresentacionProducto->save()){
                            
                            $oPresentacionProducto->gruposProducto()->detach();
                            if(is_array($presentacionProductoFormRequest->grupos)){
                                if(count($presentacionProductoFormRequest->grupos)){
                                    $oPresentacionProducto->gruposProducto()->sync($presentacionProductoFormRequest->grupos);
                                }
                            }

                            if($presentacionProductoFormRequest->input('cadenas')){
                                if(is_array($presentacionProductoFormRequest->input('cadenas'))){
                                    
                                    ProductoCadena::where('presentacion_producto_id',$oPresentacionProducto->id)->delete();
                                    
                                    foreach($presentacionProductoFormRequest->input('cadenas') as $key => $value){
                                        $aValue = explode('|', $value);
                                        $oCadena = Catalogo::find($aValue[0]);
                                        
                                        if(isset($aValue[1])){
                                            $oProductoCadena = ProductoCadena::where('codigo_cadena', $oCadena->id)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('clasificacion_establecimiento', $aValue[1])->where('cliente_proyecto_id', $oPresentacionProducto->cliente_proyecto_id)->first();
                                        }else{
                                            $oProductoCadena = ProductoCadena::where('codigo_cadena', $oCadena->id)->where('presentacion_producto_id', $oPresentacionProducto->id)->where('cliente_proyecto_id', $oPresentacionProducto->cliente_proyecto_id)->first();    
                                        }

                                        
                                        if( ! $oProductoCadena){
                                            if($oClienteProyecto){
                                                $oProductoCadena = new ProductoCadena;
                                                $oProductoCadena->codigo_cadena = $oCadena->id;
                                                $oProductoCadena->nombre_cadena = $oCadena->descripcion;
                                                $oProductoCadena->clasificacion_establecimiento = isset($aValue[1]) ? $aValue[1] : '';
                                                $oProductoCadena->presentacion_producto_id = $oPresentacionProducto->id;
                                                $oProductoCadena->categoria_producto_id = $oPresentacionProducto->categoria_producto_id;
                                                $oProductoCadena->familia_producto_id = $oPresentacionProducto->familia_producto_id;
                                                $oProductoCadena->cliente_proyecto_id = $oClienteProyecto->id;
                                                $oProductoCadena->cliente_id = $oClienteProyecto->cliente_id;
                                                $oProductoCadena->proyecto_id = $oClienteProyecto->proyecto_id;
                                                $oProductoCadena->save();
                                            }
                                        }
                                    }
                                }
                            }
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Presentacion de producto '.$oPresentacionProducto->nombre.' actualizada exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Presentacion de producto '.$oPresentacionProducto->nombre.' no pudo ser actualizada, por favor intentelo nuevamente luego de unos minutos.</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una presentacion de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para eliminar una presentacion de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/eliminar-presentacion-de-producto
    * @method GET
    * @param int $iCategoriaProductoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function destroy($iCategoriaProductoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-presentacion-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iCategoriaProductoId){
            $oCategoriaProducto = CategoriaProducto::find($iCategoriaProductoId);
            if($oCategoriaProducto){
                if($iId){
                    $oPresentacionProducto = PresentacionProducto::find($iId);
                    if($oPresentacionProducto){
                        $aCadenas = $oPresentacionProducto->productosCadena;
                        if(!$aCadenas->isEmpty()){
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto "'.$oPresentacionProducto->nombre.'" no puede ser eliminada debido a que tiene cadenas de establecimientos asociadas</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);    
                        }
                        $gruposProducto = $oPresentacionProducto->gruposProducto;
                        if(!$gruposProducto->isEmpty()){
                            $oPresentacionProducto->gruposProducto()->detach();
                            //$oPresentacionProducto->gruposProducto()->detach($oPresentacionProducto->id);
                        }
                        if($oPresentacionProducto->delete()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto "'.$oPresentacionProducto->nombre.'" ha sido eliminada exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto "'.$oPresentacionProducto->nombre.'" no puede ser eliminada.Por favor intentelo nuevamente luego de unos minutos o contacte al administrador del sistema</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La presentacion de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una presentacion de producto para poder eliminarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/presentaciones-de-productos/'.$oCategoriaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
