<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\FamiliaProducto;
use SICE\CategoriaProducto;
use SICE\ClienteProyecto;
use SICE\Usuario;
use SICE\Http\Requests\CategoriaProductoFormRequest;

/**
* Clase para administración de categorias de producto en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/03/21
* @Configuracion
*/

class CategoriasProductosController extends Controller
{
    /**
    * Metodo para mostrar la lista de categorias de producto del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/categorias-de-productos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iFamiliaProductoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iFamiliaProductoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/categorias-de-productos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                $aCategoriasProducto = $oFamiliaProducto->categoriasProducto;
                $aData = [
                    'oFamiliaProducto' => $oFamiliaProducto,
                    'oCliente' => $oFamiliaProducto->cliente,
                    'oProyecto' => $oFamiliaProducto->proyecto,
                    'aCategoriasProducto' => $aCategoriasProducto,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.categoriasProducto.listaCategoriasProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto para poder ver sus categorias</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de creacion de una categoria de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto
    * @method GET
    * @param int $iFamiliaProductoId
    * @return \Illuminate\Http\Response
    */
    public function create($iFamiliaProductoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                $aData = [
                    'oFamiliaProducto' => $oFamiliaProducto,
                    'oCliente' => $oFamiliaProducto->cliente,
                    'oProyecto' => $oFamiliaProducto->proyecto,
                    'oCategoriaProducto' => null,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Configurar Proyecto por cliente',
                ];
                return view("configuracion.proyectosPorCliente.categoriasProducto.crearEditarCategoriaProducto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La familia de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos una categoria de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto
    * @method POST
    * @param  SICE\Http\Requests\CategoriaProductoFormRequest $categoriaProductoFormRequest
    * @param int $iFamiliaProductoId
    * @return \Illuminate\Http\Response
    */
    public function store(CategoriaProductoFormRequest $categoriaProductoFormRequest, $iFamiliaProductoId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-categoria-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                $oCategoriaProducto = new CategoriaProducto;
                $oCategoriaProducto->nombre = $categoriaProductoFormRequest->input('nombre');
                $oCategoriaProducto->familia_producto_id = $oFamiliaProducto->id;
                $oCategoriaProducto->cliente_proyecto_id = $oFamiliaProducto->cliente_proyecto_id;
                $oCategoriaProducto->cliente_id = $categoriaProductoFormRequest->input('cliente_id');
                $oCategoriaProducto->proyecto_id = $categoriaProductoFormRequest->input('proyecto_id');
                if($oCategoriaProducto->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto '.$oCategoriaProducto->nombre.' agregara exitosamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto '.$oCategoriaProducto->nombre.' no pudo ser agregada, por favor intentelo nuevamente luego de unos minutos.</div>');
                }
                return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de productos seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de productos</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para mostrar el formulario de edición de una categoria de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto
    * @method GET
    * @param int $iFamiliaProductoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function edit($iFamiliaProductoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                if($iId){
                    $oCategoriaProducto = CategoriaProducto::find($iId);
                    if($oCategoriaProducto){
                        $aData = [
                            'oFamiliaProducto' => $oFamiliaProducto,
                            'oCliente' => $oCategoriaProducto->cliente,
                            'oProyecto' => $oCategoriaProducto->proyecto,
                            'oCategoriaProducto' => $oCategoriaProducto,
                            'menuActivo' => 'configuracion',
                            'titulo' => 'SICE :: Configurar Proyecto por cliente',
                        ];
                        return view("configuracion.proyectosPorCliente.categoriasProducto.crearEditarCategoriaProducto", $aData);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para almacenar en la base de datos los cambios realizados categoria de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto
    * @method POST
    * @param  SICE\Http\Requests\CategoriaProductoFormRequest $categoriaProductoFormRequest
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function update(CategoriaProductoFormRequest $categoriaProductoFormRequest, $iId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-categoria-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $iFamiliaProductoId = $categoriaProductoFormRequest->input('familia_producto_id');

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                if($iId){
                    $oCategoriaProducto = CategoriaProducto::find($iId);
                    if($oCategoriaProducto){
                        $oCategoriaProducto->nombre = $categoriaProductoFormRequest->input('nombre');
                        $oCategoriaProducto->familia_producto_id = $oFamiliaProducto->id;
                        $oCategoriaProducto->cliente_proyecto_id = $oFamiliaProducto->cliente_proyecto_id;
                        $oCategoriaProducto->cliente_id = $categoriaProductoFormRequest->input('cliente_id');
                        $oCategoriaProducto->proyecto_id = $categoriaProductoFormRequest->input('proyecto_id');
                        if($oCategoriaProducto->save()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto '.$oCategoriaProducto->nombre.' actualizada exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Categoria de producto '.$oCategoriaProducto->nombre.' no pudo ser actualizada, por favor intentelo nuevamente luego de unos minutos.</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto para poder editarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto seleccionada no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }

    /**
    * Metodo para eliminar una categoria de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/03/21
    *
    * @route /configuracion/proyectos-por-cliente/productos/eliminar-categoria-de-producto
    * @method GET
    * @param int $iFamiliaProductoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function destroy($iFamiliaProductoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-categoria-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iFamiliaProductoId){
            $oFamiliaProducto = FamiliaProducto::find($iFamiliaProductoId);
            if($oFamiliaProducto){
                if($iId){
                    $oCategoriaProducto = CategoriaProducto::find($iId);
                    if($oCategoriaProducto){
                        $aPresentaciones = $oCategoriaProducto->presentacionesProducto;
                        $aCadenas = $oCategoriaProducto->productosCadena;
                        if(!$aPresentaciones->isEmpty()){
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto "'.$oCategoriaProducto->nombre.'" no puede ser eliminada debido a que tiene presemtaciones de producto asociadas</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);    
                        }
                        if(!$aCadenas->isEmpty()){
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto "'.$oCategoriaProducto->nombre.'" no puede ser eliminada debido a que tiene cadenas de establecimientos asociadas</div>');
                            return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);    
                        }
                        if($oCategoriaProducto->delete()){
                            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto "'.$oCategoriaProducto->nombre.'" ha sido eliminada exitosamente</div>');
                        }else{
                            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto "'.$oCategoriaProducto->nombre.'" no puede ser eliminada.Por favor intentelo nuevamente luego de unos minutos o contacte al administrador del sistema</div>');
                        }
                        return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La categoria de producto seleccionada no existe</div>');
                        return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);    
                    }
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una categoria de producto para poder eliminarla</div>');
                    return redirect('configuracion/proyectos-por-cliente/productos/categorias-de-productos/'.$oFamiliaProducto->id);
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Familia de producto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una familia de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }
}
