<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\CanalFormRequest;

/**
* Clase para administración de canales
* @Autor Raúl Chauvin
* @FechaCreacion  2016/08/08
* @Parametrizacion
*/

class CanalesController extends Controller
{
    /**
    * Metodo para mostrar la lista de canales 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/canales/lista-de-canales
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('catalogos/canales/lista-de-canales')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'listaCanales' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Canales'),
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Canales',
        ];
        return view("catalogos.canales.listaCanales", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo canal
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/08
    *
    * @route /catalogos/canales/agregar-canal
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('catalogos/canales/agregar-canal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Crear Nuevo Canal',
        ];
        return view("catalogos.canales.crearEditarCanal", $aData);
    }

    
    /**
    * Metodo para guardar un nuevo Canal 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/canales/agregar-canal
    * @method POST
    * @param  \SICE\Http\Requests\CanalFormRequest  $canalFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(CanalFormRequest $canalFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/canales/agregar-canal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        
        $oCanal = new Catalogo;
        $oCanal->contexto = 'Canales';
        $oCanal->codigo1 = Catalogo::generaCodigo('Canales', null);
        $oCanal->descripcion = $canalFormRequest->input('descripcion');

        if($oCanal->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$oCanal->descripcion.' guardado satisfactoriamente</div>');
            return redirect('catalogos/canales/lista-de-canales');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$oCanal->desripcion.' no pudo ser guardado</div>');
            return redirect('catalogos/canales/lista-de-canales');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de un canal de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/canales/editar-canal
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/canales/editar-canal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCanal = Catalogo::find($iId);
            if($oCanal){
                $aData = [
                    'oCanal' => $oCanal,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Canal',
                ];
                return view('catalogos.canales.crearEditarCanal', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El canal seleccionado no existe</div>');
                return redirect('catalogos/canales/lista-de-canales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un canal para poder editarlo.</div>');
            return redirect('catalogos/canales/lista-de-canales');
        }
    }

    
    /**
     * Metodo que actualiza la informacion del canal en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/08
     *
     * @route /catalogos/canales/editar-canal
     * @method POST
     * @param  \SICE\Http\Requests\CanalFormRequest  $canalFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(CanalFormRequest $canalFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/canales/editar-canal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCanal = Catalogo::find($iId);
            if($oCanal){
                $oCanal->contexto = 'Canales';
                $oCanal->descripcion = $canalFormRequest->input('descripcion');
             
                if($oCanal->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$oCanal->descripcion.' guardado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$oCanal->descripcion.' no pudo ser guardado</div>');
                }
                return redirect('catalogos/canales/lista-de-canales');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El canal seleccionado no existe</div>');
                return redirect('catalogos/canales/lista-de-canales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un canal para poder actualizar su información</div>');
            return redirect('catalogos/canales/lista-de-canales');
        }
    }

    
    /**
     * Metodo que elimina un Canal del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/canales/eliminar-canal
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/canales/eliminar-canal')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oCanal = Catalogo::find($iId);
            if($oCanal){
                $nombre = $oCanal->descripcion;
                $aSubcanales = $oCanal->catalogos;
                
                if( ! $aSubcanales->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$nombre.' no pudede ser eliminado debido a que tiene subcanales relacionados</div>');
                    return redirect('catalogos/canales/lista-de-canales');
                }
                if($oCanal->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Canal '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('catalogos/canales/lista-de-canales');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El canal seleccionado no existe</div>');
                return redirect('catalogos/canales/lista-de-canales');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un canal para poder eliminarlo</div>');
            return redirect('catalogos/canales/lista-de-canales');
        }
    }
}
