<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\ProvinciaFormRequest;

/**
* Clase para administración de provincias
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class ProvinciasController extends Controller
{
    /**
    * Metodo para mostrar la lista de provincias 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/provincias/lista-de-provincias
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/lista-de-provincias')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'listaProvincias' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Provincias'),
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Provincias',
        ];
        return view("catalogos.provincias.listaProvincias", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de una nueva provincia
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/provincias/agregar-provincia
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/agregar-provincia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Crear Nueva Provincia',
        ];
        return view("catalogos.provincias.crearEditarProvincia", $aData);
    }

    
    /**
    * Metodo para guardar una nueva provincia
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/provincias/agregar-provincia
    * @method POST
    * @param  \SICE\Http\Requests\ProvinciaFormRequest  $provinciaFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ProvinciaFormRequest $provinciaFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/agregar-provincia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        
        $oProvincia = new Catalogo;
        $oProvincia->contexto = 'Provincias';
        $oProvincia->codigo1 = Catalogo::generaCodigo('Provincias', null);
        $oProvincia->descripcion = $provinciaFormRequest->input('descripcion');
        $oProvincia->coordenadas_poligono = $provinciaFormRequest->input('coordenadas_poligono');

        if($oProvincia->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$oProvincia->descripcion.' guardada satisfactoriamente</div>');
            return redirect('catalogos/provincias');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$oProvincia->desripcion.' no pudo ser guardada</div>');
            return redirect('catalogos/provincias');
        }
        
    }

    
    /**
     * Metodo que muestra el formulario de edicion de una provincia de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/provincias/editar-provincia
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/editar-provincia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProvincia = Catalogo::find($iId);
            if($oProvincia){
                $aData = [
                    'oProvincia' => $oProvincia,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Provincia',
                ];
                return view('catalogos.provincias.crearEditarProvincia', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La provincia seleccionada no existe</div>');
                return redirect('catalogos/provincias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una provincia para poder editarla.</div>');
            return redirect('catalogos/provincias');
        }
    }

    
    /**
     * Metodo que actualiza la informacion de la provincia en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/provincias/editar-provincia
     * @method POST
     * @param  \SICE\Http\Requests\ProvinciaFormRequest  $provinciaFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ProvinciaFormRequest $provinciaFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/editar-provincia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProvincia = Catalogo::find($iId);
            if($oProvincia){
                $oProvincia->contexto = 'Provincias';
                $oProvincia->descripcion = $provinciaFormRequest->input('descripcion');
                $oProvincia->coordenadas_poligono = $provinciaFormRequest->input('coordenadas_poligono');
             
                if($oProvincia->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$oProvincia->descripcion.' guardada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$oProvincia->descripcion.' no pudo ser guardada</div>');
                }
                return redirect('catalogos/provincias');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La provincia seleccionada no existe</div>');
                return redirect('catalogos/provincias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una provincia para poder actualizar su información</div>');
            return redirect('catalogos/provincias');
        }
    }

    
    /**
     * Metodo que elimina una provincia del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/provincias/eliminar-provincia
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/provincias/eliminar-provincia')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProvincia = Catalogo::find($iId);
            if($oProvincia){
                $nombre = $oProvincia->descripcion;
                $aCiudades = $oProvincia->catalogos;
                
                if( ! $aCiudades->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$nombre.' no pudo ser eliminada debido a que tiene ciudades relacionadas</div>');
                    return redirect('catalogos/provincias');
                }

                if($oProvincia->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$nombre.' eliminada satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Provincia '.$nombre.' no pudo ser eliminada</div>');
                }
                return redirect('catalogos/provincias');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La provincia seleccionada no existe</div>');
                return redirect('catalogos/provincias');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una provincia para poder eliminarla</div>');
            return redirect('catalogos/provincias');
        }
    }
}
