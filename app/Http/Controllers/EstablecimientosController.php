<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Establecimiento;
use SICE\Catalogo;
use SICE\Usuario;
use SICE\ClienteEstablecimiento;
use SICE\Http\Requests\EstablecimientoFormRequest;
use SICE\Http\Requests\EstablecimientoUpdateFormRequest;
use SICE\Http\Requests\EstablecimientoImportFormRequest;

use SICE\Helpers\TimeFormat;

use Flashy;
use Auth;
use Storage;
use Mapper;

/**
* Clase para administración de establecimientos
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class EstablecimientosController extends Controller
{
    /**
    * Metodo para mostrar la lista de establecimientos 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /catalogos/establecimientos/lista-de-establecimientos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/lista-de-establecimientos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'listaEstablecimientos' => Establecimiento::buscarEstablecimiento($request->input('valor_busqueda')),
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Establecimientos',
        ];
        return view("catalogos.establecimientos.listaEstablecimientos", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /catalogos/establecimientos/agregar-establecimiento
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/agregar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaProvincias' => Catalogo::where('contexto', 'Provincias')->get(),
            'listaCanales' => Catalogo::where('contexto', 'Canales')->get(),
            'listaCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
            'idCanal' => 0,
            'idSubcanal' => 0,
            'idTipoNegocio' => 0,
            'idCadena' => 0,
            'idProvincia' => 0,
            'idCiudad' => 0,
            'idZona' => 0,
            'idParroquia' => 0,
            'idBarrio' => 0,
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Crear Nuevo Establecimiento',
        ];
        return view("catalogos.establecimientos.crearEditarEstablecimiento", $aData);
    }

    /**
    * Metodo para guardar un nuevo establecimiento
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /catalogos/establecimientos/agregar-establecimiento
    * @method POST
    * @param  \SICE\Http\Requests\EstablecimientoFormRequest  $establecimientoFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(EstablecimientoFormRequest $establecimientoFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/agregar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $oEstablecimiento = new Establecimiento;
        
        $oEstablecimiento->nombre = $establecimientoFormRequest->input('nombre');
        $oEstablecimiento->direccion_calle_principal = $establecimientoFormRequest->input('direccion_calle_principal');
        $oEstablecimiento->direccion_numero = $establecimientoFormRequest->input('direccion_numero');
        $oEstablecimiento->direccion_transversal = $establecimientoFormRequest->input('direccion_transversal');
        $oEstablecimiento->direccion_referencia = $establecimientoFormRequest->input('direccion_referencia');
        $oEstablecimiento->administrador = $establecimientoFormRequest->input('administrador');
        $oEstablecimiento->telefonos_contacto = $establecimientoFormRequest->input('telefonos_contacto');
        $oEstablecimiento->email_contacto = $establecimientoFormRequest->input('email_contacto');
        $oEstablecimiento->geolocalizacion = $establecimientoFormRequest->input('geolocalizacion');

        $sCodigoProvincia = '00';
        $sCodigoCiudad = '00';
        $sCodigoZona = '0';
        $sCodigoParroquia = '000';
        $sCodigoBarrio = '000';
        $sCodigoCanal = '0';
        $sCodigoSubcanal = '00';
        $sCodigoTipoNegocio = '000';
        $sCodigoCadena = '000';
        
        if($establecimientoFormRequest->input('id_canal') != ""){
            $oEstablecimiento->uuid_canal = $establecimientoFormRequest->input('id_canal');
            $oEstablecimiento->codigo_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->codigo1;
            $oEstablecimiento->nombre_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->descripcion;
            $sCodigoCanal = $oEstablecimiento->codigo_canal;
        }
        if($establecimientoFormRequest->input('id_subcanal') != ""){
            $oEstablecimiento->uuid_subcanal = $establecimientoFormRequest->input('id_subcanal');
            $oEstablecimiento->codigo_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->codigo1;
            $oEstablecimiento->nombre_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->descripcion;
            $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
        }
        if($establecimientoFormRequest->input('id_tipo_negocio') != ""){
            $oEstablecimiento->uuid_tipo_negocio = $establecimientoFormRequest->input('id_tipo_negocio');
            $oEstablecimiento->codigo_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->codigo1;
            $oEstablecimiento->nombre_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->descripcion;
            $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
        }
        if($establecimientoFormRequest->input('id_provincia') != ""){
            $oEstablecimiento->uuid_provincia = $establecimientoFormRequest->input('id_provincia');
            $oEstablecimiento->codigo_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->codigo1;
            $oEstablecimiento->nombre_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->descripcion;
            $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
        }
        if($establecimientoFormRequest->input('id_ciudad') != ""){
            $oEstablecimiento->uuid_ciudad = $establecimientoFormRequest->input('id_ciudad');
            $oEstablecimiento->codigo_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->codigo1;
            $oEstablecimiento->nombre_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->descripcion;
            $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
        }
        if($establecimientoFormRequest->input('id_zona') != ""){
            $oEstablecimiento->uuid_zona = $establecimientoFormRequest->input('id_zona');
            $oEstablecimiento->codigo_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->codigo1;
            $oEstablecimiento->nombre_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->descripcion;
            $sCodigoZona = $oEstablecimiento->codigo_zona;
        }
        if($establecimientoFormRequest->input('id_parroquia') != ""){
            $oEstablecimiento->uuid_parroquia = $establecimientoFormRequest->input('id_parroquia');
            $oEstablecimiento->codigo_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->codigo1;
            $oEstablecimiento->nombre_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->descripcion;
            $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
        }
        if($establecimientoFormRequest->input('id_barrio') != ""){
            $oEstablecimiento->uuid_barrio = $establecimientoFormRequest->input('id_barrio');
            $oEstablecimiento->codigo_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->codigo1;
            $oEstablecimiento->nombre_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->descripcion;
            $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
        }
        if($establecimientoFormRequest->input('id_cadena') != ""){
            $oEstablecimiento->uuid_cadena = $establecimientoFormRequest->input('id_cadena');
            $oEstablecimiento->codigo_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->codigo1;
            $oEstablecimiento->nombre_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->descripcion;
            $sCodigoCadena = $oEstablecimiento->codigo_cadena;
        }
        
        $oEstablecimiento->codigo = Establecimiento::generaCodigo(
                                                                        $sCodigoProvincia,
                                                                        $sCodigoCiudad,
                                                                        $sCodigoZona,
                                                                        $sCodigoParroquia,
                                                                        $sCodigoBarrio,
                                                                        $sCodigoCanal,
                                                                        $sCodigoSubcanal,
                                                                        $sCodigoTipoNegocio,
                                                                        $sCodigoCadena
                                                                    );

        $oEstablecimiento->creado_por = \Auth::user()->nombre.' '.\Auth::user()->apellido;

        if ($establecimientoFormRequest->hasFile('foto')){
            if ($establecimientoFormRequest->file('foto')->isValid()){
                $fotografia = $establecimientoFormRequest->file('foto');
                if(!is_dir('./resources/fotosEstablecimientos')){
                    mkdir('./resources/fotosEstablecimientos',777);
                }
                $i=0;
                $info = explode(".",$fotografia->getClientOriginalName());
                $oEstablecimiento->foto = $fotografia->getClientOriginalName();
                while(file_exists('./resources/fotosEstablecimientos/'.$oEstablecimiento->foto)){
                    $i++;
                    $oEstablecimiento->foto = $info[0].$i.".".$info[1];
                }
                $establecimientoFormRequest->file('foto')->move('resources/fotosEstablecimientos',$oEstablecimiento->foto);
            }
        }
        
        if($oEstablecimiento->save()){
            \Session::flash('message', '<div class="alert alert-success">Establecimientio '.$oEstablecimiento->nombre.' creado satisfactoriamente</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Establecimientio '.$oEstablecimiento->nombre.' no pudo ser creado</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }

    /**
     * Metodo que muestra detalle de un establecimiento de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /catalogos/estblecimientos/detalle-de-establecimiento
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function show($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/detalle-de-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oEstablecimiento = Establecimiento::find($iId);
            if($oEstablecimiento){
                $aData = [
                    'oEstablecimiento' => $oEstablecimiento,
                    'aRutas' => $oEstablecimiento->rutas,
                    'aNovedades' => $oEstablecimiento->novedades()->take(10)->get(),
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Detalle de Establecimiento',
                ];

                $coordenadas = $oEstablecimiento->geolocalizacion ? explode(',', $oEstablecimiento->geolocalizacion) : [0.000,0.000];

                $oMapper = Mapper::map(
                            trim($coordenadas[0]), 
                            trim($coordenadas[1]), 
                            [
                                'zoom' => 15, 
                                'locate' => false,
                                'draggable' => false, 
                                'marker' => false,
                            ]
                        );

                $oMapper->marker(
                                trim($coordenadas[0]), 
                                trim($coordenadas[1])
                        );
                return view('catalogos.establecimientos.detalleEstablecimiento', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un establecimiento para poder ver su detalle.</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un establecimiento de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /catalogos/estblecimientos/editar-establecimiento
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/editar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oEstablecimiento = Establecimiento::find($iId);
            if($oEstablecimiento){
                $aData = [
                    'oEstablecimiento' => $oEstablecimiento,
                    'listaProvincias' => Catalogo::where('contexto', 'Provincias')->get(),
                    'listaCanales' => Catalogo::where('contexto', 'Canales')->get(),
                    'listaCadenas' => Catalogo::where('contexto', 'Cadenas')->get(),
                    'idTipoNegocio' => $oEstablecimiento->codigo_tipo_negocio != '' ? Catalogo::where('contexto', 'TiposNegocio')->where('codigo1', $oEstablecimiento->codigo_tipo_negocio)->first()->id : 0,
                    'idCanal' => $oEstablecimiento->codigo_canal != '' ? Catalogo::where('contexto', 'Canales')->where('codigo1', $oEstablecimiento->codigo_canal)->first()->id : 0,
                    'idSubcanal' => $oEstablecimiento->codigo_subcanal != '' ? Catalogo::where('contexto', 'Subcanales')->where('codigo1', $oEstablecimiento->codigo_subcanal)->first()->id : 0,
                    'idCadena' => $oEstablecimiento->codigo_cadena != '' ? Catalogo::where('contexto', 'Cadenas')->where('codigo1', $oEstablecimiento->codigo_cadena)->first()->id : 0,
                    'idProvincia' => $oEstablecimiento->codigo_provincia != '' ? Catalogo::where('contexto', 'Provincias')->where('codigo1', $oEstablecimiento->codigo_provincia)->first()->id : 0,
                    'idCiudad' => $oEstablecimiento->codigo_ciudad != '' ? Catalogo::where('contexto', 'Ciudades')->where('codigo1', $oEstablecimiento->codigo_ciudad)->first()->id : 0,
                    'idZona' => $oEstablecimiento->codigo_zona != '' ? Catalogo::where('contexto', 'Zonas')->where('codigo1', $oEstablecimiento->codigo_zona)->first()->id : 0,
                    'idParroquia' => $oEstablecimiento->codigo_parroquia != '' ? Catalogo::where('contexto', 'Parroquias')->where('codigo1', $oEstablecimiento->codigo_parroquia)->first()->id : 0,
                    'idBarrio' => $oEstablecimiento->codigo_barrio != '' ? Catalogo::where('contexto', 'Barrios')->where('codigo1', $oEstablecimiento->codigo_barrio)->first()->id : 0,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Establecimiento',
                ];
                return view('catalogos.establecimientos.crearEditarEstablecimiento', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un establecimiento para poder editarlo.</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }

    /**
     * Metodo que actualiza la informacion del establecimienti en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /catalogos/establecimientos/editar-establecimiento
     * @method POST
     * @param  \SICE\Http\Requests\EstablecimientoUpdateFormRequest  $establecimientoUpdateFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(EstablecimientoUpdateFormRequest $establecimientoFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/editar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oEstablecimiento = Establecimiento::find($iId);
            if($oEstablecimiento){
                $fotoAnterior = $oEstablecimiento->foto;
                $oEstablecimiento->nombre = $establecimientoFormRequest->input('nombre');
                $oEstablecimiento->direccion_calle_principal = $establecimientoFormRequest->input('direccion_calle_principal');
                $oEstablecimiento->direccion_numero = $establecimientoFormRequest->input('direccion_numero');
                $oEstablecimiento->direccion_transversal = $establecimientoFormRequest->input('direccion_transversal');
                $oEstablecimiento->direccion_referencia = $establecimientoFormRequest->input('direccion_referencia');
                $oEstablecimiento->administrador = $establecimientoFormRequest->input('administrador');
                $oEstablecimiento->telefonos_contacto = $establecimientoFormRequest->input('telefonos_contacto');
                $oEstablecimiento->email_contacto = $establecimientoFormRequest->input('email_contacto');
                //$oEstablecimiento->geolocalizacion = $establecimientoFormRequest->input('geolocalizacion');

                $sCodigoProvincia = '00';
                $sCodigoCiudad = '00';
                $sCodigoZona = '0';
                $sCodigoParroquia = '000';
                $sCodigoBarrio = '000';
                $sCodigoCanal = '0';
                $sCodigoSubcanal = '00';
                $sCodigoTipoNegocio = '000';
                $sCodigoCadena = '000';
                
                if($establecimientoFormRequest->input('id_canal') != ""){
                    $oEstablecimiento->uuid_canal = $establecimientoFormRequest->input('id_canal');
                    $oEstablecimiento->codigo_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->codigo1;
                    $oEstablecimiento->nombre_canal = Catalogo::find($establecimientoFormRequest->input('id_canal'))->descripcion;
                    $sCodigoCanal = $oEstablecimiento->codigo_canal;
                }
                if($establecimientoFormRequest->input('id_subcanal') != ""){
                    $oEstablecimiento->uuid_subcanal = $establecimientoFormRequest->input('id_subcanal');
                    $oEstablecimiento->codigo_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->codigo1;
                    $oEstablecimiento->nombre_subcanal = Catalogo::find($establecimientoFormRequest->input('id_subcanal'))->descripcion;
                    $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
                }
                if($establecimientoFormRequest->input('id_tipo_negocio') != ""){
                    $oEstablecimiento->uuid_tipo_negocio = $establecimientoFormRequest->input('id_tipo_negocio');
                    $oEstablecimiento->codigo_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->codigo1;
                    $oEstablecimiento->nombre_tipo_negocio = Catalogo::find($establecimientoFormRequest->input('id_tipo_negocio'))->descripcion;
                    $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
                }
                if($establecimientoFormRequest->input('id_provincia') != ""){
                    $oEstablecimiento->uuid_provincia = $establecimientoFormRequest->input('id_provincia');
                    $oEstablecimiento->codigo_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->codigo1;
                    $oEstablecimiento->nombre_provincia = Catalogo::find($establecimientoFormRequest->input('id_provincia'))->descripcion;
                    $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
                }
                if($establecimientoFormRequest->input('id_ciudad') != ""){
                    $oEstablecimiento->uuid_ciudad = $establecimientoFormRequest->input('id_ciudad');
                    $oEstablecimiento->codigo_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->codigo1;
                    $oEstablecimiento->nombre_ciudad = Catalogo::find($establecimientoFormRequest->input('id_ciudad'))->descripcion;
                    $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
                }
                if($establecimientoFormRequest->input('id_zona') != ""){
                    $oEstablecimiento->uuid_zona = $establecimientoFormRequest->input('id_zona');
                    $oEstablecimiento->codigo_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->codigo1;
                    $oEstablecimiento->nombre_zona = Catalogo::find($establecimientoFormRequest->input('id_zona'))->descripcion;
                    $sCodigoZona = $oEstablecimiento->codigo_zona;
                }
                if($establecimientoFormRequest->input('id_parroquia') != ""){
                    $oEstablecimiento->uuid_parroquia = $establecimientoFormRequest->input('id_parroquia');
                    $oEstablecimiento->codigo_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->codigo1;
                    $oEstablecimiento->nombre_parroquia = Catalogo::find($establecimientoFormRequest->input('id_parroquia'))->descripcion;
                    $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
                }
                if($establecimientoFormRequest->input('id_barrio') != ""){
                    $oEstablecimiento->uuid_barrio = $establecimientoFormRequest->input('id_barrio');
                    $oEstablecimiento->codigo_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->codigo1;
                    $oEstablecimiento->nombre_barrio = Catalogo::find($establecimientoFormRequest->input('id_barrio'))->descripcion;
                    $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
                }
                if($establecimientoFormRequest->input('id_cadena') != ""){
                    $oEstablecimiento->uuid_cadena = $establecimientoFormRequest->input('id_cadena');
                    $oEstablecimiento->codigo_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->codigo1;
                    $oEstablecimiento->nombre_cadena = Catalogo::find($establecimientoFormRequest->input('id_cadena'))->descripcion;
                    $sCodigoCadena = $oEstablecimiento->codigo_cadena;
                }
                
                $oEstablecimiento->codigo = Establecimiento::generaCodigo(
                                                                                $sCodigoProvincia,
                                                                                $sCodigoCiudad,
                                                                                $sCodigoZona,
                                                                                $sCodigoParroquia,
                                                                                $sCodigoBarrio,
                                                                                $sCodigoCanal,
                                                                                $sCodigoSubcanal,
                                                                                $sCodigoTipoNegocio,
                                                                                $sCodigoCadena
                                                                            );

                $oEstablecimiento->ultima_actualizacion = \Auth::user()->nombre.' '.\Auth::user()->apellido;

                if ($establecimientoFormRequest->hasFile('foto')){
                    if ($establecimientoFormRequest->file('foto')->isValid()){
                        $fotografia = $establecimientoFormRequest->file('foto');
                        if(!is_dir('./resources/fotosEstablecimientos')){
                            mkdir('./resources/fotosEstablecimientos',777);
                        }
                        $i=0;
                        $info = explode(".",$fotografia->getClientOriginalName());
                        $oEstablecimiento->foto = $fotografia->getClientOriginalName();
                        while(file_exists('./resources/fotosEstablecimientos/'.$oEstablecimiento->foto)){
                            $i++;
                            $oEstablecimiento->foto = $info[0].$i.".".$info[1];
                        }
                        $establecimientoFormRequest->file('foto')->move('resources/fotosEstablecimientos',$oEstablecimiento->foto);
                    }
                }
                
                if($oEstablecimiento->save()){
                    if($fotoAnterior && ($fotoAnterior != $oEstablecimiento->foto)){
                        unlink('./resources/fotosEstablecimientos/'.$fotoAnterior);
                    }
                    \Session::flash('message', '<div class="alert alert-success">Establecimientio '.$oEstablecimiento->nombre.' actualizado satisfactoriamente</div>');
                    return redirect('catalogos/establecimientos/lista-de-establecimientos');
                }else{
                    \Session::flash('message', '<div class="alert alert-danger">Establecimientio '.$oEstablecimiento->nombre.' no pudo ser actualizado</div>');
                    return redirect('catalogos/establecimientos/lista-de-establecimientos');
                }
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un establecimiento para poder editarlo.</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }


    /**
    * Metodo para mostrar el formulario de importacion de una lista de establecimientos
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /catalogos/establecimientos/importar-establecimientos
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function load()
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/importar-establecimientos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'titulo' => 'SICE :: Importar Establecimientos',
        ];
        return view("catalogos.establecimientos.importarEstablecimiento", $aData);
    }

    /**
    * Metodo para guardar en la base de datos la lista de establecimientos importada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/30
    *
    * @route /catalogos/establecimientos/importar-establecimientos
    * @method POST
    * @param  \SICE\Http\Requests\EstablecimientoImportRequest  $establecimientoImportRequest
    * @return \Illuminate\Http\Response
    */
    public function save(EstablecimientoImportRequest  $establecimientoImportRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/importar-establecimientos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if(!is_dir('./resources/uploads')){
            mkdir('./resources/uploads',777);
        }

        if ($establecimientoImportRequest->hasFile('listaEstablecimientos')){
            if ($establecimientoImportRequest->file('listaEstablecimientos')->isValid()){
                $establecimientos = $establecimientoImportRequest->file('listaEstablecimientos');
                $establecimientos->move("resources/uploads",$establecimientos->getClientOriginalName());
                Excel::load('resources/uploads/'.$establecimientos->getClientOriginalName(),function($reader){
                    $aEstablecimientos = $reader->get();
                    $aErrores = [];
                    $iNumFila = 2;
                    foreach($aEstablecimientos as $filaEstablecimiento){
                        $oEstablecimiento = Establecimiento::where('codigo', $filaEstablecimiento->codigo)->first();
                        if(!$oEstablecimiento){
                            $oEstablecimiento = new Establecimiento;
                        }
                        $oEstablecimiento->nombre = $filaEstablecimiento->nombre;
                        $oEstablecimiento->direccion_manzana = $filaEstablecimiento->direccionManzana;
                        $oEstablecimiento->direccion_calle_principal = $filaEstablecimiento->direccionCallePrincipal;
                        $oEstablecimiento->direccion_numero = $filaEstablecimiento->direccionNumero;
                        $oEstablecimiento->direccion_transversal = $filaEstablecimiento->direccionTransversal;
                        $oEstablecimiento->direccion_referencia = $filaEstablecimiento->direccionReferencia;
                        $oEstablecimiento->administrador = $filaEstablecimiento->administrador;
                        $oEstablecimiento->telefonos_contacto = $filaEstablecimiento->telefonos_contacto;
                        $oEstablecimiento->email_contacto = $filaEstablecimiento->emailContacto;
                        $oEstablecimiento->ubicacion = $filaEstablecimiento->ubicacion;

                        $sCodigoProvincia = '00';
                        $sCodigoCiudad = '00';
                        $sCodigoZona = '0';
                        $sCodigoParroquia = '000';
                        $sCodigoBarrio = '000';
                        $sCodigoCanal = '0';
                        $sCodigoSubcanal = '00';
                        $sCodigoTipoNegocio = '000';
                        $sCodigoCadena = '000';
                        
                        if($filaEstablecimiento->codigoProvincia != ""){
                            $oEstablecimiento->codigo_provincia = $filaEstablecimiento->codigoProvincia;
                            if($oCatalogo = Catalogo::where('contexto', 'Provincias')->where('codigo1', $filaEstablecimiento->codigoProvincia)->first()){
                                $oEstablecimiento->nombre_provincia = $oCatalogo->descripcion;
                                $sCodigoProvincia = $oEstablecimiento->codigo_provincia;
                            }
                        }
                        if($filaEstablecimiento->codigoCiudad != ""){
                            $oEstablecimiento->codigo_ciudad = $filaEstablecimiento->codigoCiudad;
                            if($oCatalogo = Catalogo::where('contexto', 'Ciudades')->where('codigo1', $filaEstablecimiento->codigoCiudad)->first()){
                                $oEstablecimiento->nombre_ciudad = $oCatalogo->descripcion;
                                $sCodigoCiudad = $oEstablecimiento->codigo_ciudad;
                            }
                        }
                        if($filaEstablecimiento->codigoZona != ""){
                            $oEstablecimiento->codigo_zona = $filaEstablecimiento->codigoZona;
                            if($oCatalogo = Catalogo::where('contexto', 'Zonas')->where('codigo1', $filaEstablecimiento->codigoZona)->first()){
                                $oEstablecimiento->nombre_zona = $oCatalogo->descripcion;
                                $sCodigoZona = $oEstablecimiento->codigo_zona;
                            }
                        }
                        if($filaEstablecimiento->codigoParroquia != ""){
                            $oEstablecimiento->codigo_parroquia = $filaEstablecimiento->codigoParroquia;
                            if($oCatalogo = Catalogo::where('contexto', 'Parroquias')->where('codigo1', $filaEstablecimiento->codigoParroquia)->first()){
                                $oEstablecimiento->nombre_parroquia = $oCatalogo->descripcion;
                                $sCodigoParroquia = $oEstablecimiento->codigo_parroquia;
                            }
                        }
                        if($filaEstablecimiento->codigoBarrio != ""){
                            $oEstablecimiento->codigo_barrio = $filaEstablecimiento->codigoBarrio;
                            if($oCatalogo = Catalogo::where('contexto', 'Barrios')->where('codigo1', $filaEstablecimiento->codigoBarrio)->first()){
                                $oEstablecimiento->nombre_barrio = $oCatalogo->descripcion;
                                $sCodigoBarrio = $oEstablecimiento->codigo_barrio;
                            }
                        }
                        if($filaEstablecimiento->codigoCanal != ""){
                            $oEstablecimiento->codigo_canal = $filaEstablecimiento->codigoCanal;
                            if($oCatalogo = Catalogo::where('contexto', 'Canales')->where('codigo1', $filaEstablecimiento->codigoCanal)->first()){
                                $oEstablecimiento->nombre_canal = $oCatalogo->descripcion;
                                $sCodigoCanal = $oEstablecimiento->codigo_canal;
                            }
                        }
                        if($filaEstablecimiento->codigoSubcanal != ""){
                            $oEstablecimiento->codigo_subcanal = $filaEstablecimiento->codigoSubcanal;
                            if($oCatalogo = Catalogo::where('contexto', 'Subcanales')->where('codigo1', $filaEstablecimiento->codigoSubcanal)->first()){
                                $oEstablecimiento->nombre_subcanal = $oCatalogo->descripcion;
                                $sCodigoSubcanal = $oEstablecimiento->codigo_subcanal;
                            }
                        }
                        if($filaEstablecimiento->codigoTipoNegocio != ""){
                            $oEstablecimiento->codigo_tipo_negocio = $filaEstablecimiento->codigoTipoNegocio;
                            if($oCatalogo = Catalogo::where('contexto', 'TiposNegocio')->where('codigo1', $filaEstablecimiento->codigoTipoNegocio)->first()){
                                $oEstablecimiento->nombre_tipo_negocio = $oCatalogo->descripcion;
                                $sCodigoTipoNegocio = $oEstablecimiento->codigo_tipo_negocio;
                            }
                        }
                        if($filaEstablecimiento->codigoCadena != ""){
                            $oEstablecimiento->codigo_cadena = $filaEstablecimiento->codigoCadena;
                            if($oCatalogo = Catalogo::where('contexto', 'Cadenas')->where('codigo1', $filaEstablecimiento->codigoCadena)->first()){
                                $oEstablecimiento->nombre_cadena = $oCatalogo->descripcion;
                                $sCodigoCadena = $oEstablecimiento->codigo_cadena;
                            }
                        }
                        
                        $oEstablecimiento->codigo = Establecimiento::generaCodigo(
                                                                    $sCodigoProvincia,
                                                                    $sCodigoCiudad,
                                                                    $sCodigoZona,
                                                                    $sCodigoParroquia,
                                                                    $sCodigoBarrio,
                                                                    $sCodigoCanal,
                                                                    $sCodigoSubcanal,
                                                                    $sCodigoTipoNegocio,
                                                                    $sCodigoCadena
                                                                );

                        $oEstablecimiento->estado = 1;
                        if(!$oEstablecimiento->save()){
                            $aErrores[] = $iNumFila;
                        }
                        $iNumFila++;
                    }
                });
                if(count($aErrores)){
                    $sListaFilas = implode(',', $aErrores);
                    \Session::flash('message', '<div class="alert alert-warning">Se realizo la importacion de establecimientos, sin embargo las siguientes filas no pudieron ser guardadas: '.$sListaFilas.'</div>');
                }else{
                    \Session::flash('message', '<div class="alert alert-success">Lista de establecimientos importada satisfactoriamente</div>');
                }
            }else{
                \Session::flash('message', '<div class="alert alert-danger">No se puede importar el listado de establecimientos debido a que el archivo no es valido, unicamente se pueden importar archivos de tipo XLS y XLSX</div>');
            }
        }else{
            \Session::flash('message', '<div class="alert alert-danger">Por favor seleccione un archivo para poder importar la lista de establecimientos al sistema</div>');
        }
        return redirect('catalogos/establecimientos/lista-de-establecimientos');
    }

    /**
     * Metodo que elimina un establecimiento del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/30
     *
     * @route /catalogos/establecimientos/eliminar-establecimiento
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/establecimientos/eliminar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oEstablecimiento = Establecimiento::find($iId);
            if($oEstablecimiento){
                
                $aRutas = $oEstablecimiento->rutas;
                $aDetallesRuta = $oEstablecimiento->detallesRuta;
                $aNovedades = $oEstablecimiento->novedades;
                $aInformacionDetalles = $oEstablecimiento->informacionDetalles;
                $nombre = $oEstablecimiento->nombre;

                if( 
                    ! $aRutas->isEmpty() ||
                    ! $aDetallesRuta->isEmpty() || 
                    ! $aNovedades->isEmpty() || 
                    ! $aInformacionDetalles->isEmpty()
                )
                {
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>No se puede eliminar el establecimiento '.$nombre.' debido a que se encuentra relacionado con otros elementos del sistema.</div>');
                    return redirect('catalogos/establecimientos/lista-de-establecimientos');        
                }

                $oEstablecimiento->clasificacionesEstablecimiento()->delete();
                $oEstablecimiento->clienteEstablecimientos()->delete();
                
                if($oEstablecimiento->delete()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Establecimiento '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Establecimiento '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un establecimiento para poder eliminarlo.</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }


    /**
     * Metodo que cambia el estado de un usuario del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/31
     *
     * @route /catalogos/establecimientos/cambiar-estado-establecimiento
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function status($iId = '')
    {

        if(!Usuario::verificaPermiso('catalogos/establecimientos/cambiar-estado-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oEstablecimiento = Establecimiento::find($iId);
            if($oEstablecimiento){
                $oEstablecimiento->estado = $oEstablecimiento->estado == 1 ? 0 : 1;
                $oEstablecimiento->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del establecimiento '.$oEstablecimiento->nombre.' / '.$oEstablecimiento->email.' satisfactoriamente</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Establecimiento seleccionado no existe</div>');
                return redirect('catalogos/establecimientos/lista-de-establecimientos');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un establecimiento para poder cambiar su estado</div>');
            return redirect('catalogos/establecimientos/lista-de-establecimientos');
        }
    }
}
