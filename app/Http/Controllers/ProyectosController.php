<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Proyecto;
use SICE\ParametroProyecto;
use SICE\Usuario;
use SICE\Http\Requests\ProyectoFormRequest;

/**
* Clase para administración de proyectos
* @Autor Raúl Chauvin
* @FechaCreacion  2016/08/16
* @Parametrizacion
* @Configuracion
*/

class ProyectosController extends Controller
{
    /**
    * Metodo para mostrar la lista de proyectos del sistema 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/16
    *
    * @route /configuracion/proyectos/lista-de-proyectos
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/lista-de-proyectos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $data = [
            'listaProyectos' => Proyecto::paginate(50),
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Proyectos',
        ];
        return view("configuracion.proyectos.listaProyectos", $data);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/16
    *
    * @route /configuracion/proyectos/agregar-proyecto
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/agregar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Crear Nuevo Proyecto',
        ];
        return view("configuracion.proyectos.crearEditarProyecto", $aData);
    }

    /**
    * Metodo para guardar un nuevo proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/16
    *
    * @route /configuracion/proyectos/agregar-proyecto
    * @method POST
    * @param  \SICE\Http\Requests\ProyectoFormRequest  $proyectoFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ProyectoFormRequest $proyectoFormRequest)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/agregar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
        
        $oProyecto = new Proyecto;
        $oProyecto->codigo = $proyectoFormRequest->input('codigo');
        $oProyecto->nombre = $proyectoFormRequest->input('nombre');
        $oProyecto->estado = $proyectoFormRequest->input('estado');

        if($oProyecto->save()){

            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$oProyecto->nombre.' guardado satisfactoriamente</div>');
            return redirect('configuracion/proyectos');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$oProyecto->nombre.' no pudo ser guardado</div>');
            return redirect('configuracion/proyectos');
        }
    }

    /**
    * Metodo para mostrar el detalle de un proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/16
    *
    * @route /configuracion/proyectos/detalle-de-proyecto
    * @method GET
    * @param  int $iId
    * @return \Illuminate\Http\Response
    */
    public function show($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/detalle-de-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un proyecto de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/16
     *
     * @route /configuracion/proyectos/editar-proyecto
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/editar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProyecto = Proyecto::find($iId);
            if($oProyecto){
                $aData = [
                    'oProyecto' => $oProyecto,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Editar Proyecto',
                ];
                return view('configuracion.proyectos.crearEditarProyecto', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder editarlo.</div>');
            return redirect('configuracion/proyectos');
        }
    }

    /**
     * Metodo que actualiza la informacion del proyecto en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/16
     *
     * @route /configuracion/proyectos/editar-proyecto
     * @method POST
     * @param  \SICE\Http\Requests\ProyectoFormRequest  $proyectoFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ProyectoFormRequest $proyectoFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/editar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProyecto = Proyecto::find($iId);
            if($oProyecto){
                $oProyecto->codigo = $proyectoFormRequest->input('codigo');
                $oProyecto->nombre = $proyectoFormRequest->input('nombre');
                $oProyecto->estado = $proyectoFormRequest->input('estado');
             
                if($oProyecto->save()){

                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$oProyecto->nombre.' guardado satisfactoriamente.</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$oProyecto->nombre.' no pudo ser guardado</div>');
                }
                return redirect('configuracion/proyectos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La proyecto seleccionada no existe</div>');
                return redirect('configuracion/proyectos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una proyecto para poder actualizar su información</div>');
            return redirect('configuracion/proyectos');
        }
    }

    /**
     * Metodo que elimina un proyecto del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/17
     *
     * @route /configuracion/proyectos/eliminar-proyecto
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos/eliminar-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProyecto = Proyecto::find($iId);
            if($oProyecto){
                $nombre = $oProyecto->nombre;
                $aClientes = $oProyecto->clientesProyecto;
                $aEquipoTrabajo = $oProyecto->equiposTrabajo;
                $aEncuestas = $oProyecto->encuestas;
                $aFamiliasProducto = $oProyecto->familiasProducto;
                $aCategoriasProducto = $oProyecto->categoriasProducto;
                $aPresentacionesProducto = $oProyecto->presentacionesProducto;
                $aProductosCadena = $oProyecto->productosCadena;
                $aClasificacionesEstablecimiento = $oProyecto->clasificacionesEstablecimiento;
                $aRutas = $oProyecto->rutas;
                $aGruposProducto = $oProyecto->gruposProducto;
                
                if( ! $aClientes->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene clientes relacionados</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aEquipoTrabajo->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene equipos de trabajo relacionados</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aEncuestas->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene encuestas relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aFamiliasProducto->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene familias de productos relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aCategoriasProducto->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene categorias de productos relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aPresentacionesProducto->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene presentaciones de productos relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aProductosCadena->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene productos relacionados</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aClasificacionesEstablecimiento->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene clasificaciones de establecimientos relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aRutas->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene rutas relacionadas</div>');
                    return redirect('configuracion/proyectos');
                }

                if( ! $aGruposProducto->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado debido a que tiene grupos de productos relacionados</div>');
                    return redirect('configuracion/proyectos');
                }

                $oProyecto->parametrosProyecto()->delete();

                if($oProyecto->delete()){

                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' eliminado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto '.$nombre.' no pudo ser eliminado</div>');
                }
                return redirect('configuracion/proyectos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder eliminarlo</div>');
            return redirect('configuracion/proyectos');
        }
    }

    /**
     * Metodo que cambia el estado de un proyecto del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/08/16
     *
     * @route /configuracion/proyectos/cambiar-estado-proyecto
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function cambiaEstado($iId = '')
    {

        if(!Usuario::verificaPermiso('configuracion/proyectos/cambiar-estado-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oProyecto = Proyecto::find($iId);
            if($oProyecto){
                $oProyecto->estado = $oProyecto->estado == 1 ? 0 : 1;
                $oProyecto->save();
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Se cambio el estado del proyecto '.$oProyecto->nombre.' / '.$oProyecto->email.' satisfactoriamente</div>');
                return redirect('configuracion/proyectos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder cambiar su estado</div>');
            return redirect('configuracion/proyectos');
        }
    }
}
