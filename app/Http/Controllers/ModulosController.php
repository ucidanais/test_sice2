<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Modulo;
use SICE\Usuario;
use SICE\Http\Requests\ModuloFormRequest;
use SICE\Http\Requests\ModuloUpdateFormRequest;

/**
* Clase para administración de modulos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Autenticacion/Autorizacion
*/

class ModulosController extends Controller
{
    /**
    * Metodo para mostrar la lista de modulos del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/modulos/lista-de-modulos
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/lista-de-modulos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'listaModulos' => Modulo::orderBy('nombre', 'asc')->paginate(20),
            'menuActivo' => 'seguridades',
            'titulo' => 'SICE :: Modulos',
        ];
        return view("auth.modulos.listaModulos", $aData);
        
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo modulo del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/modulos/agregar-modulo
    * @method GET
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/agregar-modulo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $aData = [
            'menuActivo' => 'seguridades',
            'titulo' => 'SICE :: Crear nuevo Módulo',
        ];
        return view("auth.modulos.crearEditarModulo", $aData);
    }

    /**
    * Metodo para guardar un nuevo modulo en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /seguridades/modulos/agregar-modulo
    * @method POST
    * @param  \SICE\Http\Requests\ModuloFormRequest  $moduloFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(ModuloFormRequest $moduloFormRequest)
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/agregar-modulo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        $oModulo = new Modulo;
        $oModulo->nombre = $moduloFormRequest->input('nombre');
        $oModulo->tipo = $moduloFormRequest->input('tipo');
        
        if($oModulo->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$oModulo->nombre.' guardado satisfactoriamente</div>');
            return redirect('seguridades/modulos');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$oModulo->nombre.' no pudo ser guardado</div>');
            return redirect('seguridades/modulos');
        }
    }

    /**
     * Metodo que muestra el formulario de edicion de un modulo del sistema de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/modulos/editar-modulo
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/editar-modulo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oModulo = Modulo::find($iId);
            if($oModulo){
                $aData = [
                    'objModulo' => $oModulo,
                    'menuActivo' => 'seguridades',
                    'titulo' => 'SICE :: Editar Modulo',
                ];
                return view('auth.modulos.crearEditarModulo', $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El módulo seleccionado no existe</div>');
                return redirect('seguridades/modulos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un modulo para poder editarlo.</div>');
            return redirect('seguridades/modulos');
        }
    }

    /**
     * Metodo que actualiza la informacion del modulo en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/modulos/editar-modulo
     * @method POST
     * @param  \SICE\Http\Requests\ModuloUpdateFormRequest  $moduloFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(ModuloUpdateFormRequest $moduloFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/editar-modulo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oModulo = Modulo::find($iId);
            if($oModulo){
                $oModulo->nombre = $moduloFormRequest->input('nombre');
                $oModulo->tipo = $moduloFormRequest->input('tipo');
                
                if($oModulo->save()){
                    \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$oModulo->nombre.' guardado satisfactoriamente</div>');
                }else{
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$oModulo->nombre.' no pudo ser guardado</div>');
                }
                return redirect('seguridades/modulos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El modulo seleccionado no existe</div>');
                return redirect('seguridades/modulos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un modulo para poder actualizar su información</div>');
            return redirect('seguridades/modulos');
        }
    }

    /**
     * Metodo que elimina un modulo del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /seguridades/modulos/eliminar-modulo
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('seguridades/modulos/eliminar-modulo')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($iId){
            $oModulo = Modulo::find($iId);
            if($oModulo){
                $nombre = $oModulo->nombre;
                $aPermisos = $oModulo->permisos;
                if( ! $aPermisos->isEmpty()){
                    \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$nombre.' no pudo ser eliminado debido a que tiene permisos asociados al mismo</div>');
                }else{
                    if($oModulo->delete()){
                        \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$nombre.' eliminado satisfactoriamente</div>');
                    }else{
                        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Modulo '.$nombre.' no pudo ser eliminado</div>');
                    }
                }
                return redirect('seguridades/modulos');
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El modulo seleccionado no existe</div>');
                return redirect('seguridades/modulos');
            }
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un modulo para poder eliminarlo</div>');
            return redirect('seguridades/modulos');
        }
    }
}
