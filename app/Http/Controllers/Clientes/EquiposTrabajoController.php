<?php

namespace SICE\Http\Controllers\Clientes;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Cliente;
use SICE\Proyecto;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\EquipoTrabajo;

use Auth;

use Flashy;

/**
* Clase para reporteria de proyectos del cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/14
* @Reportes
*/

class EquiposTrabajoController extends Controller
{

	protected $oCliente;

    public function __construct(){
        $this->oCliente = Usuario::getCliente();

        if( ! is_object($this->oCliente)){
            Auth::logout();
            return redirect('/')->with('mensajeLogin', '<div class="alert alert-danger">Usted está intentando ingresar a una sección no permitida del sistema.</div>');
        }

    }
    
    /**
    * Metodo para mostrar la lista de colaboradores que se encuentran trabajando en el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/15
    *
    * @route /clientes/proyectos/equipo-trabajo
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function index($sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su detalle.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();
        $aEmpleados = $oClienteProyecto->equiposTrabajo()->where('tipo', 'E')->where('lider_proyecto', 0)->orderBy('apellido', 'asc')->get();
        
        $aData = [
            'oCliente' => $this->oCliente,
            'oClienteProyecto' => $oClienteProyecto,
            'aLideresProyecto' => $aLideresProyecto,
            'aEmpleados' => $aEmpleados,
            'menuActivo' => 'reportes',
            'titulo' => 'SICE :: Equipo de Trabajo',
        ];
        return view("clientes.equipoTrabajo", $aData);
    }
}
