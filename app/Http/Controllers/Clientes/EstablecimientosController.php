<?php

namespace SICE\Http\Controllers\Clientes;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Establecimiento;
use SICE\Catalogo;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\Cliente;
use SICE\Proyecto;
use SICE\ClienteEstablecimiento;

use SICE\Helpers\TimeFormat;

use Flashy;
use Auth;
use Storage;
use Mapper;

/**
* Clase para administración de establecimientos por proyecto de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/10
* @Configuracion
* @Proyectos
*/

class EstablecimientosController extends Controller
{
    /**
     * Disco de storage.
     *
     * @var sStorageDisk
     */
    protected $sStorageDisk;

    /**
     * Instancia de storage.
     *
     * @var oStorage
     */
    protected $oStorage;

    /**
     * Instancia de Cliente.
     *
     * @var oStorage
     */
    protected $oCliente;

    /**
     * Constructor del Controller, iniciamos los middlewares para validar que el usuario tenga los permisos correctos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2017/05/15
     */
    public function __construct(){
        $this->oCliente = Usuario::getCliente();

        if( ! is_object($this->oCliente)){
            Auth::logout();
            return redirect('/')->with('mensajeLogin', '<div class="alert alert-danger">Usted está intentando ingresar a una sección no permitida del sistema.</div>');
        }

        $this->sStorageDisk = config('app.env') == 'production' ? 's3' : 'local';
        $this->oStorage = config('app.env') == 'production' ? Storage::disk('s3') : Storage::disk('local');
    }


    /**
    * Metodo para mostrar la lista de establecimientos por proyecto de cliente en un mapa puntillado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /clientes/proyectos/establecimientos/mapa
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param  string  $sClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function mapa(Request $request, $sClienteProyectoId = '')
    {
        /*if(!Usuario::verificaPermiso('clientes/proyectos/establecimientos/mapa')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect()->route('clientes.proyectos.list');
        }*/

        $aClientesProyecto = ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')->select('cliente_proyectos.id','cliente_proyectos.descripcion')->where('cliente_proyectos.estado', 1)->where('equipo_trabajo.usuario_id', Auth::user()->id)->get();

        if($aClientesProyecto->isEmpty()){
        	Flashy::error('Ups!. Parece que no has sido asignado a ningún proyecto aún.');
            return redirect()->route('clientes.proyectos.list');
        }

        if( ! $sClienteProyectoId){
            Flashy::error('Ups!. Parece que no has seleccionado ningun proyecto para ver sus PDV.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyectoSelected = ClienteProyecto::find($sClienteProyectoId);

        if( ! is_object($oClienteProyectoSelected)){
            Flashy::error('Ups!. Parece que el proyecto que seleccionaste no existe o no esta disponible.');
            return redirect()->route('clientes.proyectos.list');
        }

        $aClienteProyectoId = [];
        if($sClienteProyectoId){
        	$oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        	if(is_object($oClienteProyecto)){
        		$aClienteProyectoId[] = $oClienteProyecto->id;
        	}else{
        		foreach($aClientesProyecto as $oClienteProyecto){
		        	$aClienteProyectoId[] = $oClienteProyecto->id;
		        }	
        	}
        }else{
        	foreach($aClientesProyecto as $oClienteProyecto){
	        	$aClienteProyectoId[] = $oClienteProyecto->id;
	        }
        }

        if($request->input('aClienteProyecto')){
        	if(is_array($request->input('aClienteProyecto'))){
	        	if(count($request->input('aClienteProyecto'))){
	        		$aClienteProyectoId = $request->input('aClienteProyecto');
	        	}
	        }
        }

        $aCadenas = $request->input('aCadenas') ? $request->input('aCadenas') : [];
        $aProvincias = $request->input('aProvincias') ? $request->input('aProvincias') : [];
        $aCiudades = $request->input('aCiudades') ? $request->input('aCiudades') : [];
        $aCanales = $request->input('aCanales') ? $request->input('aCanales') : [];
        $aSubcanales = $request->input('aSubcanales') ? $request->input('aSubcanales') : [];

        $aLideresProyecto = $oClienteProyectoSelected->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();

        $busquedaEstablecimientos = Establecimiento::buscarEstablecimientoReporte(
																		            $aCadenas,
																		            $aProvincias,
																		            $aCiudades,
																		            $aCanales,
																		            $aSubcanales,
																		            $aClienteProyectoId,
																		            null
																		        );

        $aMarkerIcons = [
        	0 => 'http://labs.google.com/ridefinder/images/mm_20_purple.png',
			1 => 'http://labs.google.com/ridefinder/images/mm_20_yellow.png',
			2 => 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
			3 => 'http://labs.google.com/ridefinder/images/mm_20_white.png',
			4 => 'http://labs.google.com/ridefinder/images/mm_20_green.png',
			5 => 'http://labs.google.com/ridefinder/images/mm_20_red.png',
			6 => 'http://labs.google.com/ridefinder/images/mm_20_black.png',
			7 => 'http://labs.google.com/ridefinder/images/mm_20_orange.png',
			8 => 'http://labs.google.com/ridefinder/images/mm_20_gray.png',
			9 => 'http://labs.google.com/ridefinder/images/mm_20_brown.png',
			10 => 'http://maps.google.com/mapfiles/ms/micons/blue-dot.png',
	        11 => 'http://maps.google.com/mapfiles/ms/micons/red-dot.png',
	        12 => 'http://maps.google.com/mapfiles/ms/micons/yellow-dot.png',
	        13 => 'http://maps.google.com/mapfiles/ms/micons/orange-dot.png',
	        14 => 'http://maps.google.com/mapfiles/ms/micons/ltblue-dot.png',
	        15 => 'http://maps.google.com/mapfiles/ms/micons/green-dot.png',
        ];

        $aMarkerProjectId = [];
        $aMarkerProjectName = [];
        $key = 0;
        foreach($aClientesProyecto as $oClienteProyecto){
        	$aMarkerProjectId[$oClienteProyecto->id] = $aMarkerIcons[$key];
        	$aMarkerProjectName[$oClienteProyecto->descripcion] = $aMarkerIcons[$key];
        	$key++;
        	if($key > 15){
        		$key = 0;
        	}
        }

        $aProvincias = Catalogo::where('contexto','Provincias')->orderBy('descripcion', 'asc')->get();
        $aCiudades = Catalogo::where('contexto','Ciudades')->orderBy('descripcion', 'asc')->get();
        $aCanales = Catalogo::where('contexto','Canales')->orderBy('descripcion', 'asc')->get();
        $aSubcanales = Catalogo::where('contexto','Subcanales')->orderBy('descripcion', 'asc')->get();
        $aCadenas = Catalogo::where('contexto','Cadenas')->orderBy('descripcion', 'asc')->get();

        $data = [
            'oCliente' => $this->oCliente,
            'oClienteProyectoSelected' => $oClienteProyectoSelected,
            'aLideresProyecto' => $aLideresProyecto,
            'numEstablecimientos' => $busquedaEstablecimientos['numEstab'],
            'listaEstablecimientos' => $busquedaEstablecimientos['establecimientos'],
            'listaClientesProyecto' => $aClientesProyecto,
            'listaProvincias' => $aProvincias,
            'listaCiudades' => $aCiudades,
            'listaCanales' => $aCanales,
            'listaSubcanales' => $aSubcanales,
            'listaCadenas' => $aCadenas,
            'aMarkerProjectId' => $aMarkerProjectId,
            'aMarkerProjectName' => $aMarkerProjectName,
            'menuActivo' => 'cliente',
            'titulo' => 'SICE :: Establecimientos',
            'aClienteProyectoFiltro' => $request->input('aClienteProyecto') ? $request->input('aClienteProyecto') : [$sClienteProyectoId],
            'aProvinciasFiltro' => $request->input('aProvincias') ? $request->input('aProvincias') : [],
            'aCiudadesFiltro' => $request->input('aCiudades') ? $request->input('aCiudades') : [],
            'aCadenasFiltro' => $request->input('aCadenas') ? $request->input('aCadenas') : [],
            'aCanalesFiltro' => $request->input('aCanales') ? $request->input('aCanales') : [],
            'aSubcanalesFiltro' => $request->input('aSubcanales') ? $request->input('aSubcanales') : [],
        ];
        return view("clientes.establecimientos.mapaEstablecimientos", $data);
    }



    /**
    * Metodo para exportar la lista de establecimientos por proyecto de cliente en un archivo de Excel
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/12
    *
    * @route /clientes/proyectos/establecimientos/exportar
    * @method GET
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function exportExcel(Request $request)
    {
        /*
        if(!Usuario::verificaPermiso('clientes/proyectos/establecimientos/exportar')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect()->route('home');
        }
        */

        $aClientesProyecto = ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')->select('cliente_proyectos.id')->where('cliente_proyectos.estado', 1)->where('equipo_trabajo.usuario_id', Auth::user()->id)->get();

        if($aClientesProyecto->isEmpty()){
        	Flashy::error('Ups!. Parece que no has sido asignado a ningún proyecto aún.');
            return redirect()->route('home');
        }

        $aClienteProyectoId = [];
        foreach($aClientesProyecto as $oClienteProyecto){
        	$aClienteProyectoId[] = $oClienteProyecto->id;
        }

        if($request->input('aClienteProyecto')){
        	if(is_array($request->input('aClienteProyecto'))){
	        	if(count($request->input('aClienteProyecto'))){
	        		$aClienteProyectoId = $request->input('aClienteProyecto');
	        	}
	        }
        }

        $aCadenas = $request->input('aCadenas') ? $request->input('aCadenas') : [];
        $aProvincias = $request->input('aProvincias') ? $request->input('aProvincias') : [];
        $aCiudades = $request->input('aCiudades') ? $request->input('aCiudades') : [];
        $aCanales = $request->input('aCanales') ? $request->input('aCanales') : [];
        $aSubcanales = $request->input('aSubcanales') ? $request->input('aSubcanales') : [];

        $busquedaEstablecimientos = Establecimiento::buscarEstablecimientoReporte(
            $aCadenas,
            $aProvincias,
            $aCiudades,
            $aCanales,
            $aSubcanales,
            $aClienteProyectoId,
            null);

        \Excel::create('PDV - '.$oClienteProyecto->cliente->nombre.' - '.$oClienteProyecto->proyecto->nombre.' - '.date('YmdHis'), function($excel) use($listaEstablecimientos, $oClienteProyecto){
            // Set the title
            $excel->setTitle('Lista de Pisos de Venta en el Proyecto '.$oClienteProyecto->proyecto->nombre.' para el cliente '.$oClienteProyecto->cliente->nombre.'');
            // Chain the setters
            $excel->setCreator('Go Trade')
                  ->setCompany('Sanchez Bellolio Mercadeo S.A.');
            // Call them separately
            $excel->setDescription('Archivo con la lista de pisos de venta utilizados en el proyecto '.$oClienteProyecto->proyecto->nombre.' para el cliente '.$oClienteProyecto->cliente->nombre.'');
            $excel->sheet('Lista de PDV', function($sheet) use($listaEstablecimientos){
                $sheet->appendRow(
                                    [
                                            'PROYECTO',
                                            'CODIGO', 
                                            'NOMBRE', 
                                            'PROVINCIA', 
                                            'CIUDAD', 
                                            'ZONA', 
                                            'PARROQUIA', 
                                            'BARRIO', 
                                            'MANZANA', 
                                            'CALLE PRINCIPAL', 
                                            'NUMERO', 
                                            'TRANSVERSAL', 
                                            'REFERENCIA', 
                                            'CANAL', 
                                            'SUBCANAL', 
                                            'TIPO DE NEGOCIO', 
                                            'CADENA', 
                                            'ADMINISTRADOR', 
                                            'TELEFONOS DE CONTACTO', 
                                            'EMAIL DE CONTACTO', 
                                            'ESTA EN RUTA', 
                                            'FECHA DE CREACION', 
                                            'CREADO POR', 
                                            'FECHA ULTIMA ACTUALIZACION', 
                                            'ACTUALIZADO POR'
                                    ]);
                $sheet->cells('A1:X1', function($cells) {
                    $cells->setFontSize(12);
                    $cells->setFontWeight('bold');
                });
                if( ! $listaEstablecimientos['establecimientos']->isEmpty()){
                    foreach($listaEstablecimientos['establecimientos'] as $oEstablecimiento){
                        $oClienteEstablecimiento = ClienteEstablecimiento::find($oEstablecimiento->cliente_proyecto_id);
                        $proyecto = is_object($oClienteEstablecimiento) ? $oClienteEstablecimiento->descripcion : 'SIN ASIGNAR';
                        $sheet->appendRow(
                            [
                                    $proyecto,
                                    $oEstablecimiento->codigo,
                                    $oEstablecimiento->nombre,
                                    $oEstablecimiento->nombre_provincia,
                                    $oEstablecimiento->nombre_ciudad,
                                    $oEstablecimiento->nombre_zona,
                                    $oEstablecimiento->nombre_parroquia,
                                    $oEstablecimiento->nombre_barrio,
                                    $oEstablecimiento->direccion_manzana,
                                    $oEstablecimiento->direccion_calle_principal,
                                    $oEstablecimiento->direccion_numero,
                                    $oEstablecimiento->direccion_transversal,
                                    $oEstablecimiento->direccion_referencia,
                                    $oEstablecimiento->nombre_canal,
                                    $oEstablecimiento->nombre_subcanal,
                                    $oEstablecimiento->nombre_tipo_negocio,
                                    $oEstablecimiento->nombre_cadena,
                                    $oEstablecimiento->administrador,
                                    $oEstablecimiento->telefonos_contacto,
                                    $oEstablecimiento->email_contacto,
                                    $oEstablecimiento->en_ruta ? 'SI' : 'NO',
                                    TimeFormat::dateLongFormat($oEstablecimiento->created_at),
                                    $oEstablecimiento->creado_por,
                                    TimeFormat::dateLongFormat($oEstablecimiento->updated_at),
                                    $oEstablecimiento->ultima_actualizacion,
                            ]);     
                    }
                }
            });
        })->download('xls');

        return redirect()->route('clientes.proyectos.establecimientos.mapa');

    }


    

    /**
    * Metodo para mostrar la lista de establecimientos por proyecto de cliente 
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/10
    *
    * @route /clientes/proyectos/establecimientos/detalle
    * @method GET
    * @param  string  sClienteProyectoId
    * @param  string  sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function show($sClienteProyectoId = '', $sEstablecimientoId = '')
    {
    	/*
    	if(!Usuario::verificaPermiso('clientes/proyectos/establecimientos/detalle')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }
        */

        if( ! $sClienteProyectoId){
        	Flashy::error('Por favor seleccione un proyecto de un cliente.');
            return back();	
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
        	Flashy::error('No existe una asignación de éste proyecto al cliente.');
            return back();
        }

        if( ! $sEstablecimientoId){
        	Flashy::error('Por favor seleccione un establecimiento para ver su detalle.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
        	Flashy::error('El establecimiento seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos.lista', [$oClienteProyecto->id]);
        }

        $data = [
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Establecimientos',
            'oClienteProyecto' => $oClienteProyecto,
            'oEstablecimiento' => $oEstablecimiento,
            'novedades' => $oEstablecimiento->novedades()->where('cliente_proyecto_id', $oClienteProyecto->id)->take(10)->get(),
            'rutas' => $oEstablecimiento->rutas()->where('cliente_proyecto_id', $oClienteProyecto->id)->get(),
        ];

        $coordenadas = $oEstablecimiento->geolocalizacion ? explode(',', $oEstablecimiento->geolocalizacion) : [0.000,0.000];

        $oMapper = Mapper::map(
                    trim($coordenadas[0]), 
                    trim($coordenadas[1]), 
                    [
                        'zoom' => 15, 
                        'locate' => false,
                        'draggable' => false, 
                        'marker' => false,
                    ]
                );

        $oMapper->marker(
                        trim($coordenadas[0]), 
                        trim($coordenadas[1])
                );

        return view("clientes.establecimientos.detalleEstablecimiento", $data);	
    }
}
