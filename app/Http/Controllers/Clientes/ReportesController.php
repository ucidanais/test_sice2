<?php

namespace SICE\Http\Controllers\Clientes;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Helpers\TimeFormat;

use SICE\Catalogo;
use SICE\Cliente;
use SICE\Proyecto;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\Novedad;
use SICE\DetalleRutaView;
use SICE\HojaRutaView;
use SICE\EquipoTrabajo;
use SICE\Ruta;
use SICE\HojaRuta;
use SICE\DetalleRuta;
use SICE\Establecimiento;
use SICE\InformacionDetalle;
use SICE\ClasificacionEstablecimiento;

use Auth;
use Flashy;
use Excel;

/**
* Clase para reporteria de proyectos del cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/14
* @Reportes
*/

class ReportesController extends Controller
{
    protected $oCliente;

    public function __construct(){
        $this->oCliente = Usuario::getCliente();

        if( ! is_object($this->oCliente)){
            Auth::logout();
            return redirect('/')->with('mensajeLogin', '<div class="alert alert-danger">Usted está intentando ingresar a una sección no permitida del sistema.</div>');
        }

    }

    /**
    * Metodo para mostrar el reporte de productividad del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @route /clientes/proyectos/reporte-productividad
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function productividad(Request $request, $sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su reporte de productividad.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()
                                             ->where('lider_proyecto', 1)
                                             ->orderBy('apellido', 'asc')
                                             ->get();

        $firstMonthDay = new \DateTime();
        $firstMonthDay->modify('first day of this month');

        if($request->input('desde')){
            $sDesdeFiltro = $request->input('desde').' 00:00:00';
        }else{
            if ($request->session()->has('desdeProd_temp')) {
                $sDesdeFiltro = $request->session()->get('desdeProd_temp');
            }else{
                $sDesdeFiltro = $firstMonthDay->format('Y-m-d').' 00:00:00';
            }
        }
        $request->session()->flash('desdeProd_temp', $sDesdeFiltro);

        if($request->input('hasta')){
            $sHastaFiltro = $request->input('hasta').' 00:00:00';
        }else{
            if ($request->session()->has('hastaProd_temp')) {
                $sHastaFiltro = $request->session()->get('hastaProd_temp');
            }else{
                $sHastaFiltro = date('Y-m-d H:i:s');
            }
        }
        $request->session()->flash('hastaProd_temp', $sHastaFiltro);

        if($request->exportar){
            $resumenVisitas = @DetalleRutaView::where('cliente_proyecto_id', $oClienteProyecto->id)
                                              ->desde($sDesdeFiltro)
                                              ->hasta($sHastaFiltro)
                                              ->get();
            if( ! $resumenVisitas->isEmpty()){
                Excel::create('reporteProductividad_'.date('YmdHis'), function($excel) use($resumenVisitas){
                    $excel->setTitle('Reporte de Productividad');
                    $excel->setCreator('SICE')
                          ->setCompany('Go Trade');
                    $excel->setDescription('Reporte de productividad generado desde SICE el '.date('Y-M-d H:i:s'));
                    $excel->sheet('Productividad', function($sheet) use($resumenVisitas){
                        
                        $aDetallesRuta = [];
                        $aDetallesRutaCabecera = [];
                        $aDetallesRutaCabecera[] = [
                            'SUPERVISOR',
                            'MERCADERISTA',
                            'PUNTO DE VENTA',
                            'CANAL',
                            'SUBCANAL',
                            'CADENA',
                            'CLASIFICACION',
                            'PROVINCIA',
                            'CIUDAD',
                            'DIRECCION',
                            'VISITADO',
                            'FECHA DE VISITA',
                            'HORA DE ENTRADA',
                            'HORA DE SALIDA',
                            'OBSERVACIONES',
                        ];
                        foreach($resumenVisitas as $oDetalleRutaView){
                            $oHojaRuta = HojaRuta::find($oDetalleRutaView->hoja_ruta_id);
                            $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oDetalleRutaView->cliente_proyecto_id)->where('establecimiento_id', $oDetalleRutaView->establecimiento_id)->first();
                            $nombreMercaderista = '';
                            $nombreSupervisor = '';
                            if(is_object($oHojaRuta)){
                                $nombreMercaderista = $oHojaRuta->equipoTrabajo->nombre_completo;
                                if($oHojaRuta->equipoTrabajo->usuario_reporta_id){
                                    $nombreSupervisor = $oHojaRuta->equipoTrabajo->usuarioReporta->nombre.' '.$oHojaRuta->equipoTrabajo->usuarioReporta->apellido;
                                }
                            }
                            $aDetallesRuta[] = [
                                $nombreSupervisor,
                                $nombreMercaderista,
                                $oDetalleRutaView->nombre,
                                $oDetalleRutaView->nombre_canal,
                                $oDetalleRutaView->nombre_subcanal,
                                $oDetalleRutaView->nombre_cadena,
                                is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
                                $oDetalleRutaView->nombre_provincia,
                                $oDetalleRutaView->nombre_ciudad,
                                $oDetalleRutaView->direccion_calle_principal.' '.$oDetalleRutaView->direccion_numero.' '.$oDetalleRutaView->direccion_transversal.'. Manzana:'.$oDetalleRutaView->direccion_manzana.'. Referencia'.$oDetalleRutaView->direccion_referencia,
                                $oDetalleRutaView->estado == 1 ? 'SI' : 'NO',
                                $oDetalleRutaView->fecha_visita,
                                $oDetalleRutaView->hora_entrada,
                                $oDetalleRutaView->hora_salida,
                                $oDetalleRutaView->observaciones,
                            ];
                        }
                        asort($aDetallesRuta);
                        $aDetallesRuta = array_merge($aDetallesRutaCabecera, $aDetallesRuta);
                        $sheet->rows($aDetallesRuta);
                    });
                })->download('xls');
            }else{
                Flashy::info('Ups! Parece que la busqueda no produjo resultados.');
                return back();
            }
        }else{
            $totalVisitas = @DetalleRutaView::where('cliente_proyecto_id', $oClienteProyecto->id)
                                            ->desde($sDesdeFiltro)
                                            ->hasta($sHastaFiltro)
                                            ->count();

            $iVisitasMes = @DetalleRutaView::where('cliente_proyecto_id', $oClienteProyecto->id)
                                            ->visitados()
                                            ->desde($sDesdeFiltro)
                                            ->hasta($sHastaFiltro)
                                            ->count();

            $iNoVisitasMes = @DetalleRutaView::where('cliente_proyecto_id', $oClienteProyecto->id)
                                            ->noVisitados()
                                            ->desde($sDesdeFiltro)
                                            ->hasta($sHastaFiltro)
                                            ->count();

            $aBySubcanal = @DetalleRutaView::select(\DB::raw('distinct(nombre_subcanal) as nombre, count(id) as num'))
                                        ->visitados()
                                        ->desde($sDesdeFiltro)
                                        ->hasta($sHastaFiltro)
                                        ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                        ->groupBy('nombre_subcanal')
                                        ->orderBy('num', 'desc')
                                        ->get();
            
            
            $totalCadenas = @DetalleRutaView::visitados()
                                        ->desde($sDesdeFiltro)
                                        ->hasta($sHastaFiltro)
                                        ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                        ->whereNotNull('nombre_cadena')
                                        ->count();
            
            $aByCadena = @DetalleRutaView::select(\DB::raw('distinct(nombre_cadena) as nombre, count(id) as num'))
                                        ->visitados()
                                        ->desde($sDesdeFiltro)
                                        ->hasta($sHastaFiltro)
                                        ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                        ->groupBy('nombre_cadena')
                                        ->orderBy('num', 'desc')
                                        ->get();

            $aByProvincias = @DetalleRutaView::select(\DB::raw('distinct(nombre_provincia) as nombre, count(id) as num'))
                                            ->visitados()
                                            ->desde($sDesdeFiltro)
                                            ->hasta($sHastaFiltro)
                                            ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                            ->groupBy('nombre_provincia')
                                            ->orderBy('num', 'desc')
                                            ->get();

            $totalVisitasView = @DetalleRutaView::select(\DB::raw('count(id) as num'))
                                                ->visitados()
                                                ->desde($sDesdeFiltro)
                                                ->hasta($sHastaFiltro)
                                                ->where('cliente_proyecto_id', $oClienteProyecto->id)
                                                ->first()
                                                ->num;
            
            $aData = [
                'oCliente' => $this->oCliente,
                'oClienteProyecto' => $oClienteProyecto,
                'aLideresProyecto' => $aLideresProyecto,
                'totalVisitas' => $totalVisitas,
                'iVisitasMes' => $iVisitasMes,
                'iNoVisitasMes' => $iNoVisitasMes,
                'aBySubcanal' => $aBySubcanal,
                'aByCadena' => $aByCadena,
                'iTotalCadenas' => $totalCadenas,
                'aByProvincias' => $aByProvincias,
                'totalVisitasView' => $totalVisitasView,
                'sDesdeFiltro' => date('Y-m-d', strtotime($sDesdeFiltro)),
                'sHastaFiltro' => date('Y-m-d', strtotime($sHastaFiltro)),
                'menuActivo' => 'reportes',
                'titulo' => 'SICE :: Reporte de Productividad',
            ];
            return view("clientes.reportes.productividad", $aData);
        }
    }


    /**
    * Metodo para mostrar el reporte de visibilidad del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @route /clientes/proyectos/reporte-visibilidad
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function visibilidad(Request $request, $sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su reporte de visibilidad.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();

        $firstMonthDay = new \DateTime();
        //$firstMonthDay->modify('first day of this month');

        if($request->input('desde')){
            $sDesdeFiltro = $request->input('desde').' 00:00:00';
        }else{
            if ($request->session()->has('desde_temp')) {
                $sDesdeFiltro = $request->session()->get('desde_temp');
            }else{
                $sDesdeFiltro = $firstMonthDay->format('Y-m-d').' 00:00:00';
            }
        }
        $request->session()->flash('desde_temp', $sDesdeFiltro);

        if($request->input('hasta')){
            $sHastaFiltro = $request->input('hasta').' 23:59:59';
        }else{
            if ($request->session()->has('hasta_temp')) {
                $sHastaFiltro = $request->session()->get('hasta_temp');
            }else{
                $sHastaFiltro = date('Y-m-d H:i:s');
            }
        }
        $request->session()->flash('hasta_temp', $sHastaFiltro);

        if($request->input('aProvincias')){
            $aProvinciasFiltro = $request->input('aProvincias');    
        }else{
            if ($request->session()->has('aProvincias_temp')) {
                $aProvinciasFiltro = $request->session()->get('aProvincias_temp');
            }else{
                $aProvinciasFiltro = [];
            }
        }
        $request->session()->flash('aProvincias_temp', $aProvinciasFiltro);

        if($request->input('aCiudades')){
            $aCiudadesFiltro = $request->input('aCiudades');    
        }else{
            if ($request->session()->has('aCiudades_temp')) {
                $aCiudadesFiltro = $request->session()->get('aCiudades_temp');
            }else{
                $aCiudadesFiltro = [];
            }
        }
        $request->session()->flash('aCiudades_temp', $aCiudadesFiltro);

        if($request->input('aCadenas')){
            $aCadenasFiltro = $request->input('aCadenas');    
        }else{
            if ($request->session()->has('aCadenas_temp')) {
                $aCadenasFiltro = $request->session()->get('aCadenas_temp');
            }else{
                $aCadenasFiltro = [];
            }
        }
        $request->session()->flash('aCadenas_temp', $aCadenasFiltro);

        if($request->input('aCanales')){
            $aCanalesFiltro = $request->input('aCanales');    
        }else{
            if ($request->session()->has('aCanales_temp')) {
                $aCanalesFiltro = $request->session()->get('aCanales_temp');
            }else{
                $aCanalesFiltro = [];
            }
        }
        $request->session()->flash('aCanales_temp', $aCanalesFiltro);

        if($request->input('aSubcanales')){
            $aSubcanalesFiltro = $request->input('aSubcanales');    
        }else{
            if ($request->session()->has('aSubcanales_temp')) {
                $aSubcanalesFiltro = $request->session()->get('aSubcanales_temp');
            }else{
                $aSubcanalesFiltro = [];
            }
        }
        $request->session()->flash('aSubcanales_temp', $aSubcanalesFiltro);

        if($request->exportar){
            $aNovedades = Novedad::buscarNovedades(
                $sDesdeFiltro,
                $sHastaFiltro,
                $aCadenasFiltro,
                $aProvinciasFiltro,
                $aCiudadesFiltro,
                $aCanalesFiltro,
                $aSubcanalesFiltro,
                ['visibilidad'],
                $oClienteProyecto->id,
                null
            );

            $cadenas = '';
            if(count($aCadenasFiltro)){
                foreach($aCadenasFiltro as $sIdCadena){
                    $cadenas .= Catalogo::find($sIdCadena)->descripcion.', ';
                }
                $cadenas = substr($cadenas, 0, -1);
            }

            $provincias = '';
            if(count($aProvinciasFiltro)){
                foreach($aProvinciasFiltro as $sIdProvincia){
                    $provincias .= Catalogo::find($sIdProvincia)->descripcion.', ';
                }
                $provincias = substr($provincias, 0, -1);
            }

            $ciudades = '';
            if(count($aCiudadesFiltro)){
                foreach($aCiudadesFiltro as $sIdCiudad){
                    $ciudades .= Catalogo::find($sIdCiudad)->descripcion.', ';
                }
                $ciudades = substr($ciudades, 0, -1);
            }

            $canales = '';
            if(count($aCanalesFiltro)){
                foreach($aCanalesFiltro as $sIdCanal){
                    $canales .= Catalogo::find($sIdCanal)->descripcion.', ';
                }
                $canales = substr($canales, 0, -1);
            }

            $subcanales = '';
            if(count($aSubcanalesFiltro)){
                foreach($aSubcanalesFiltro as $sIdSubcanal){
                    $subcanales .= Catalogo::find($sIdSubcanal)->descripcion.', ';
                }
                $subcanales = substr($subcanales, 0, -1);
            }
    
            if($aNovedades->isEmpty()){
                Flashy::warning('Ups!. Lo sentimos, la busqueda no produjo resultados, por favor pruebe aplicando otros filtros.');
                return redirect()->route('clientes.proyectos.reporte-visibilidad', [$sClienteProyectoId]);
            }
            
            $aData = [
                'oClienteProyecto' => $oClienteProyecto,
                'filtros' => [
                    'desde' => TimeFormat::dateLongFormat($sDesdeFiltro),
                    'hasta' => TimeFormat::dateLongFormat($sHastaFiltro),
                    'cadenas' => $cadenas,
                    'provincias' => $provincias,
                    'ciudades' => $ciudades,
                    'canales' => $canales,
                    'subcanales' => $subcanales,
                ],
                'listaNovedades' => $aNovedades,
            ];
            $pdfVisibilidad = \PDF::loadView('clientes.reportes.exportar-visibilidad', $aData);
            return @$pdfVisibilidad->download('reporteVisibilidad_'.date('YmdHis').'.pdf');
        }else{
            $aNovedades = Novedad::buscarNovedades(
                $sDesdeFiltro,
                $sHastaFiltro,
                $aCadenasFiltro,
                $aProvinciasFiltro,
                $aCiudadesFiltro,
                $aCanalesFiltro,
                $aSubcanalesFiltro,
                ['visibilidad'],
                $oClienteProyecto->id,
                5
            );
    
            $aProvincias = Catalogo::where('contexto','Provincias')->orderBy('descripcion', 'asc')->get();
            $aCiudades = Catalogo::where('contexto','Ciudades')->orderBy('descripcion', 'asc')->get();
            $aCanales = Catalogo::where('contexto','Canales')->orderBy('descripcion', 'asc')->get();
            $aSubcanales = Catalogo::where('contexto','Subcanales')->orderBy('descripcion', 'asc')->get();
            $aCadenas = Catalogo::where('contexto','Cadenas')->orderBy('descripcion', 'asc')->get();
            
            $aData = [
                'oCliente' => $this->oCliente,
                'oClienteProyecto' => $oClienteProyecto,
                'aLideresProyecto' => $aLideresProyecto,
                'listaProvincias' => $aProvincias,
                'listaCiudades' => $aCiudades,
                'listaCanales' => $aCanales,
                'listaSubcanales' => $aSubcanales,
                'listaCadenas' => $aCadenas,
                'aProvinciasFiltro' => $aProvinciasFiltro,
                'aCiudadesFiltro' => $aCiudadesFiltro,
                'aCadenasFiltro' => $aCadenasFiltro,
                'aCanalesFiltro' => $aCanalesFiltro,
                'aSubcanalesFiltro' => $aSubcanalesFiltro,
                'sDesdeFiltro' => $sDesdeFiltro,
                'sHastaFiltro' => $sHastaFiltro,
                'listaNovedades' => $aNovedades,
                'menuActivo' => 'reportes',
                'titulo' => 'SICE :: Reporte de Visibilidad',
            ];
            return view("clientes.reportes.visibilidad", $aData);
        }
    }


    /**
    * Metodo para mostrar el reporte de timing del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @route /clientes/proyectos/reporte-timing
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function rendimientoRutas(Request $request, $sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su reporte de rendimiento de rutas.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();

        $aMercaderistas = EquipoTrabajo::has('rutas')->where('cliente_proyecto_id', $oClienteProyecto->id)->get();

        if($request->input('equipoTrabajo')){
            $equipoTrabajoFiltro = $request->input('equipoTrabajo');    
        }else{
            if ($request->session()->has('equipoTrabajo_temp')) {
                $equipoTrabajoFiltro = $request->session()->get('equipoTrabajo_temp');
            }else{
                $equipoTrabajoFiltro = ! $aMercaderistas->isEmpty() ? $aMercaderistas[0]->id : '';
            }
        }
        $request->session()->flash('equipoTrabajo_temp', $equipoTrabajoFiltro);

        if($request->input('ruta')){
            $rutaFiltro = $request->input('ruta');    
        }else{
            if ($request->session()->has('ruta_temp')) {
                $rutaFiltro = $request->session()->get('ruta_temp');
            }else{
                $rutasMercaderista = ! $aMercaderistas->isEmpty() ? $aMercaderistas[0]->rutas : null;
                $rutaFiltro = $rutasMercaderista ? $rutasMercaderista->first()->id : '';
            }
        }
        $request->session()->flash('ruta_temp', $rutaFiltro);

        $aHojasRuta = HojaRuta::has('detallesRuta')
                              ->where('cliente_proyecto_id', $oClienteProyecto->id)
                              ->where('equipo_trabajo_id', $equipoTrabajoFiltro)
                              ->where('ruta_id', $rutaFiltro)
                              ->orderBy('created_at', 'desc')
                              ->take(5)
                              ->get();
        
        if($request->exportar){
            Excel::create('reporteTiming_'.date('YmdHis'), function($excel) use($aHojasRuta, $oClienteProyecto){
                $excel->setTitle('Reporte de Tiempos');
                $excel->setCreator('SICE')
                      ->setCompany('Go Trade');
                $excel->setDescription('Reporte de tiempos generado desde SICE el '.date('Y-M-d H:i:s'));
                $excel->sheet('Timing', function($sheet) use($aHojasRuta, $oClienteProyecto){
                    
                    $aDetallesRutaExcel = [];
                    $aDetallesRutaExcelCabecera = [];
                    $aDetallesRutaExcelCabecera[] = [
                        'SUPERVISOR',
                        'MERCADERISTA',
                        'PUNTO DE VENTA',
                        'CANAL',
                        'SUBCANAL',
                        'CADENA',
                        'CLASIFICACION',
                        'PROVINCIA',
                        'CIUDAD',
                        'RUTA',
                        'FECHA DE VISITA',
                        'HORA DE ENTRADA',
                        'HORA DE SALIDA',
                        'TIEMPO EFECTIVO',
                        'TIEMPO MUERTO',
                    ];
                    foreach($aHojasRuta as $oHojaRuta){
                        $tiempoTotalTrabajo = 0;
                        $tiempoTotalMuerto = 0;
                        $aDetallesRuta = $oHojaRuta->detallesRuta()->whereNotNull('hora_entrada')->whereNotNull('hora_salida')->orderBy('hora_entrada', 'asc')->get();
                        foreach ($aDetallesRuta as $key => $oDetalleRuta){
                            if($key == 0){
                                $horaAux = '';
                            }else{
                                $keyAnt = $key-1;
                                $horaAux = $aDetallesRuta[$keyAnt]->hora_salida;
                            }
                            $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oDetalleRuta->establecimiento->id)->first();
                            $tiempoTrabajo = TimeFormat::timeElapsed($oDetalleRuta->hora_entrada, $oDetalleRuta->hora_salida);
                            $tiempoMuerto = TimeFormat::timeElapsed($horaAux, $oDetalleRuta->hora_entrada);
                            $tiempoTotalTrabajo += $tiempoTrabajo;
                            $tiempoTotalMuerto += $tiempoMuerto;
                            $aDetallesRutaExcel[] = [
                                $oHojaRuta->equipoTrabajo->usuario_reporta_id ? $oHojaRuta->equipoTrabajo->usuarioReporta->nombre.' '.$oHojaRuta->equipoTrabajo->usuarioReporta->apellido : '',
                                $oHojaRuta->equipoTrabajo->nombre_completo,
                                $oDetalleRuta->establecimiento->nombre,
                                $oDetalleRuta->establecimiento->nombre_canal,
                                $oDetalleRuta->establecimiento->nombre_subcanal,
                                $oDetalleRuta->establecimiento->nombre_cadena,
                                is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
                                $oDetalleRuta->establecimiento->nombre_provincia,
                                $oDetalleRuta->establecimiento->nombre_ciudad,
                                $oHojaRuta->ruta->nombre,
                                $oDetalleRuta->fecha_visita,
                                $oDetalleRuta->hora_entrada,
                                $oDetalleRuta->hora_salida,
                                TimeFormat::convertMinutes($tiempoTrabajo),
                                TimeFormat::convertMinutes($tiempoMuerto),
                            ];
                        }
                        $aDetallesRutaExcel[] = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            'TOTAL',
                            TimeFormat::convertMinutes($tiempoTotalTrabajo),
                            TimeFormat::convertMinutes($tiempoTotalMuerto),
                        ];
                        $aDetallesRutaExcel[] = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                        ];
                    }
                    asort($aDetallesRutaExcel);
                    $aDetallesRutaExcel = array_merge($aDetallesRutaExcelCabecera, $aDetallesRutaExcel);
                    $sheet->rows($aDetallesRutaExcel);
                });
            })->download('xls');
        }else{
            $aData = [
                'oCliente' => $this->oCliente,
                'oClienteProyecto' => $oClienteProyecto,
                'aLideresProyecto' => $aLideresProyecto,
                'aMercaderistas' => $aMercaderistas,
                'equipoTrabajoFiltro' => $equipoTrabajoFiltro,
                'rutaFiltro' => $rutaFiltro,
                'listaHojasRuta' => $aHojasRuta,
                'oEquipoTrabajo' => EquipoTrabajo::find($equipoTrabajoFiltro),
                'oRuta' => Ruta::find($rutaFiltro),
                'menuActivo' => 'reportes',
                'titulo' => 'SICE :: Reporte de Rendimiento de Ruta',
            ];
            return view("clientes.reportes.rendimiento-rutas", $aData);
        }
    }


    /**
    * Metodo para mostrar el reporte de timing del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @route /clientes/proyectos/reporte-timing
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function timing(Request $request, $sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su reporte de tiempos.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();

        $firstMonthDay = new \DateTime();
        $firstMonthDay->modify('first day of this month');

        if($request->input('desde')){
            $sDesdeFiltro = $request->input('desde').' 00:00:00';
        }else{
            if ($request->session()->has('desdeProd_temp')) {
                $sDesdeFiltro = $request->session()->get('desdeProd_temp');
            }else{
                $sDesdeFiltro = $firstMonthDay->format('Y-m-d').' 00:00:00';
            }
        }
        $request->session()->flash('desdeProd_temp', $sDesdeFiltro);

        if($request->input('hasta')){
            $sHastaFiltro = $request->input('hasta').' 00:00:00';
        }else{
            if ($request->session()->has('hastaProd_temp')) {
                $sHastaFiltro = $request->session()->get('hastaProd_temp');
            }else{
                $sHastaFiltro = date('Y-m-d H:i:s');
            }
        }
        $request->session()->flash('hastaProd_temp', $sHastaFiltro);

        if($request->exportar){
            
            $aHojasRuta = HojaRuta::has('detallesRuta')
                              ->where('cliente_proyecto_id', $oClienteProyecto->id)
                              ->desde($sDesdeFiltro)
                              ->hasta($sHastaFiltro)
                              ->orderBy('equipo_trabajo_id', 'asc')
                              ->orderBy('ruta_id', 'asc')
                              ->orderBy('created_at', 'desc')
                              ->get();

            Excel::create('reporteTiming_'.date('YmdHis'), function($excel) use($aHojasRuta, $oClienteProyecto){
                $excel->setTitle('Reporte de Tiempos');
                $excel->setCreator('SICE')
                      ->setCompany('Go Trade');
                $excel->setDescription('Reporte de tiempos generado desde SICE el '.date('Y-M-d H:i:s'));
                $excel->sheet('Timing', function($sheet) use($aHojasRuta, $oClienteProyecto){
                    
                    $aDetallesRutaExcel = [];
                    $aDetallesRutaExcelCabecera = [];
                    $aDetallesRutaExcelCabecera[] = [
                        'SUPERVISOR',
                        'MERCADERISTA',
                        'PUNTO DE VENTA',
                        'CANAL',
                        'SUBCANAL',
                        'CADENA',
                        'CLASIFICACION',
                        'PROVINCIA',
                        'CIUDAD',
                        'RUTA',
                        'FECHA DE VISITA',
                        'HORA DE ENTRADA',
                        'HORA DE SALIDA',
                        'TIEMPO EFECTIVO',
                        'TIEMPO MUERTO',
                    ];
                    foreach($aHojasRuta as $oHojaRuta){
                        $tiempoTotalTrabajo = 0;
                        $tiempoTotalMuerto = 0;
                        $aDetallesRuta = $oHojaRuta->detallesRuta()->whereNotNull('hora_entrada')->whereNotNull('hora_salida')->orderBy('hora_entrada', 'asc')->get();
                        foreach ($aDetallesRuta as $key => $oDetalleRuta){
                            if($key == 0){
                                $horaAux = '';
                            }else{
                                $keyAnt = $key-1;
                                $horaAux = $aDetallesRuta[$keyAnt]->hora_salida;
                            }
                            $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oDetalleRuta->establecimiento->id)->first();
                            $tiempoTrabajo = TimeFormat::timeElapsed($oDetalleRuta->hora_entrada, $oDetalleRuta->hora_salida);
                            $tiempoMuerto = TimeFormat::timeElapsed($horaAux, $oDetalleRuta->hora_entrada);
                            $tiempoTotalTrabajo += $tiempoTrabajo;
                            $tiempoTotalMuerto += $tiempoMuerto;
                            $aDetallesRutaExcel[] = [
                                $oHojaRuta->equipoTrabajo->usuario_reporta_id ? $oHojaRuta->equipoTrabajo->usuarioReporta->nombre.' '.$oHojaRuta->equipoTrabajo->usuarioReporta->apellido : '',
                                $oHojaRuta->equipoTrabajo->nombre_completo,
                                $oDetalleRuta->establecimiento->nombre,
                                $oDetalleRuta->establecimiento->nombre_canal,
                                $oDetalleRuta->establecimiento->nombre_subcanal,
                                $oDetalleRuta->establecimiento->nombre_cadena,
                                is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
                                $oDetalleRuta->establecimiento->nombre_provincia,
                                $oDetalleRuta->establecimiento->nombre_ciudad,
                                $oHojaRuta->ruta ? $oHojaRuta->ruta->nombre : '',
                                $oDetalleRuta->fecha_visita,
                                $oDetalleRuta->hora_entrada,
                                $oDetalleRuta->hora_salida,
                                TimeFormat::convertMinutes($tiempoTrabajo),
                                TimeFormat::convertMinutes($tiempoMuerto),
                            ];
                        }
                        $aDetallesRutaExcel[] = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            'TOTAL',
                            TimeFormat::convertMinutes($tiempoTotalTrabajo),
                            TimeFormat::convertMinutes($tiempoTotalMuerto),
                        ];
                        $aDetallesRutaExcel[] = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                        ];
                    }
                    asort($aDetallesRutaExcel);
                    $aDetallesRutaExcel = array_merge($aDetallesRutaExcelCabecera, $aDetallesRutaExcel);
                    $sheet->rows($aDetallesRutaExcel);
                });
            })->download('xls');
        }else{
            $aTiemposTotales = HojaRutaView::where('cliente_proyecto_id', $oClienteProyecto->id)
                                            ->desde($sDesdeFiltro)
                                            ->hasta($sHastaFiltro)
                                            ->hasHoraEntrada()
                                            ->hasHoraSalida()
                                            ->orderBy('equipo_trabajo_id', 'asc')
                                            ->orderBy('ruta_id', 'asc')
                                            ->orderBy('created_at', 'desc')
                                            ->orderBy('hora_entrada', 'asc')
                                            ->get();

            $tiempoTotalEfectivo = 0;
            $tiempoTotalMuerto = 0;
            $aTiempoMercaderista = [];
            $aTiemposCanal = [];
            $aTiemposSubcanal = [];
            $aTiemposCadena = [];
            $aTiemposProvincia = [];
            if( ! $aTiemposTotales->isEmpty()){
                foreach($aTiemposTotales as $key => $oHojaRutaView){
                    $oEquipoTrabajo = EquipoTrabajo::find($oHojaRutaView->equipo_trabajo_id);
                    if($key == 0){
                        $horaAux = '';
                    }else{
                        $keyAnt = $key-1;
                        if($aTiemposTotales[$keyAnt]->fecha != $oHojaRutaView->fecha){
                            $horaAux = '';
                        }else{
                            $horaAux = $aTiemposTotales[$keyAnt]->hora_salida;
                        }
                    }

                    $tiempoEfectivo = abs(TimeFormat::timeElapsedHours($oHojaRutaView->hora_entrada, $oHojaRutaView->hora_salida));
                    $tiempoMuerto = abs(TimeFormat::timeElapsedHours($horaAux, $oHojaRutaView->hora_entrada));
                    
                    $tiempoTotalEfectivo += $tiempoEfectivo;
                    $tiempoTotalMuerto += $tiempoMuerto;
                    if($oEquipoTrabajo->usuario_reporta_id){
                        $idSupervisor = $oEquipoTrabajo->usuario_reporta_id;
                    }else{
                        $idSupervisor = 0;
                    }
                    if(isset($aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id])){
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['horasEfectivas'] += $tiempoEfectivo;
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['horasMuertas'] += $tiempoMuerto;
                    }else{
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['mercaderista'] = $oEquipoTrabajo->nombre_completo;
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['supervisor'] = $oEquipoTrabajo->usuario_reporta_id ? $oEquipoTrabajo->usuarioReporta->nombre.' '.$oEquipoTrabajo->usuarioReporta->apellido : '';
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['horasEfectivas'] = $tiempoEfectivo;
                        $aTiempoMercaderista[$idSupervisor][$oEquipoTrabajo->id]['horasMuertas'] = $tiempoMuerto;
                    }
                    ksort($aTiempoMercaderista);

                    if(isset($aTiemposProvincia[$oHojaRutaView->nombre_provincia])){
                        $aTiemposProvincia[$oHojaRutaView->nombre_provincia]['horasEfectivas'] += $tiempoEfectivo;
                        $aTiemposProvincia[$oHojaRutaView->nombre_provincia]['horasMuertas'] += $tiempoMuerto;
                    }else{
                        $aTiemposProvincia[$oHojaRutaView->nombre_provincia]['horasEfectivas'] = $tiempoEfectivo;
                        $aTiemposProvincia[$oHojaRutaView->nombre_provincia]['horasMuertas'] = $tiempoMuerto;
                    }

                    if(isset($aTiemposCanal[$oHojaRutaView->nombre_canal])){
                        $aTiemposCanal[$oHojaRutaView->nombre_canal] += $tiempoEfectivo;
                    }else{
                        $aTiemposCanal[$oHojaRutaView->nombre_canal] = $tiempoEfectivo;
                    }

                    if(isset($aTiemposSubcanal[$oHojaRutaView->nombre_subcanal])){
                        $aTiemposSubcanal[$oHojaRutaView->nombre_subcanal] += $tiempoEfectivo;
                    }else{
                        $aTiemposSubcanal[$oHojaRutaView->nombre_subcanal] = $tiempoEfectivo;
                    }

                    if(isset($aTiemposCadena[$oHojaRutaView->nombre_cadena])){
                        $aTiemposCadena[$oHojaRutaView->nombre_cadena] += $tiempoEfectivo;
                    }else{
                        $aTiemposCadena[$oHojaRutaView->nombre_cadena] = $tiempoEfectivo;
                    }
                }
            }
            
            
            $aData = [
                'oCliente' => $this->oCliente,
                'oClienteProyecto' => $oClienteProyecto,
                'aLideresProyecto' => $aLideresProyecto,
                'sDesdeFiltro' => $sDesdeFiltro,
                'sHastaFiltro' => $sHastaFiltro,
                'tiempoTotalEfectivo' => $tiempoTotalEfectivo,
                'tiempoTotalMuerto' => $tiempoTotalMuerto,
                'tiemposMercaderistas' => $aTiempoMercaderista,
                'tiemposCanal' => $aTiemposCanal,
                'tiemposSubcanal' => $aTiemposSubcanal,
                'tiemposCadena' => $aTiemposCadena,
                'tiemposProvincia' => $aTiemposProvincia,
                'menuActivo' => 'reportes',
                'titulo' => 'SICE :: Reporte de Tiempos',
            ];
            return view("clientes.reportes.timing", $aData);
        }
    }




    /**
    * Metodo para mostrar el reporte de faltantes de stock
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @route /clientes/proyectos/reporte-faltantes-de-stock
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function faltantesSock(Request $request, $sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su reporte de faltantes de stock.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();

        $firstMonthDay = new \DateTime();
        //$firstMonthDay->modify('first day of this month');

        if($request->input('desde')){
            $sDesdeFiltro = $request->input('desde').' 00:00:00';
        }else{
            if ($request->session()->has('desde_temp')) {
                $sDesdeFiltro = $request->session()->get('desde_temp');
            }else{
                $sDesdeFiltro = $firstMonthDay->format('Y-m-d').' 00:00:00';
            }
        }
        $request->session()->flash('desde_temp', $sDesdeFiltro);

        if($request->input('hasta')){
            $sHastaFiltro = $request->input('hasta').' 23:59:59';
        }else{
            if ($request->session()->has('hasta_temp')) {
                $sHastaFiltro = $request->session()->get('hasta_temp');
            }else{
                $sHastaFiltro = date('Y-m-d H:i:s');
            }
        }
        $request->session()->flash('hasta_temp', $sHastaFiltro);

        if($request->input('aProvincias')){
            $aProvinciasFiltro = $request->input('aProvincias');    
        }else{
            if ($request->session()->has('aProvincias_temp')) {
                $aProvinciasFiltro = $request->session()->get('aProvincias_temp');
            }else{
                $aProvinciasFiltro = [];
            }
        }
        $request->session()->flash('aProvincias_temp', $aProvinciasFiltro);

        if($request->input('aCiudades')){
            $aCiudadesFiltro = $request->input('aCiudades');    
        }else{
            if ($request->session()->has('aCiudades_temp')) {
                $aCiudadesFiltro = $request->session()->get('aCiudades_temp');
            }else{
                $aCiudadesFiltro = [];
            }
        }
        $request->session()->flash('aCiudades_temp', $aCiudadesFiltro);

        if($request->input('aCadenas')){
            $aCadenasFiltro = $request->input('aCadenas');    
        }else{
            if ($request->session()->has('aCadenas_temp')) {
                $aCadenasFiltro = $request->session()->get('aCadenas_temp');
            }else{
                $aCadenasFiltro = [];
            }
        }
        $request->session()->flash('aCadenas_temp', $aCadenasFiltro);

        if($request->input('aCanales')){
            $aCanalesFiltro = $request->input('aCanales');    
        }else{
            if ($request->session()->has('aCanales_temp')) {
                $aCanalesFiltro = $request->session()->get('aCanales_temp');
            }else{
                $aCanalesFiltro = [];
            }
        }
        $request->session()->flash('aCanales_temp', $aCanalesFiltro);

        if($request->input('aSubcanales')){
            $aSubcanalesFiltro = $request->input('aSubcanales');    
        }else{
            if ($request->session()->has('aSubcanales_temp')) {
                $aSubcanalesFiltro = $request->session()->get('aSubcanales_temp');
            }else{
                $aSubcanalesFiltro = [];
            }
        }
        $request->session()->flash('aSubcanales_temp', $aSubcanalesFiltro);

        if($request->input('tipo')){
            $sTipoFiltro = $request->input('tipo');    
        }else{
            if ($request->session()->has('sTipo_temp')) {
                $sTipoFiltro = $request->session()->get('sTipo_temp');
            }else{
                $sTipoFiltro = [];
            }
        }
        $request->session()->flash('sTipo_temp', $sTipoFiltro);

        if($request->exportar){
            $aInformacionDetalles = InformacionDetalle::buscarInformacionDetalle(
                $sDesdeFiltro,
                $sHastaFiltro,
                $aCadenasFiltro,
                $aProvinciasFiltro,
                $aCiudadesFiltro,
                $aCanalesFiltro,
                $aSubcanalesFiltro,
                $sTipoFiltro,
                $oClienteProyecto->id,
                null
            );

            if($aInformacionDetalles->isEmpty()){
                Flashy::error('Ups!. Lo sentimos, la busqueda no produjo resultados, por favor pruebe aplicando otros filtros.');
                return redirect()->route('clientes.proyectos.reporte-faltantes-stock', [$sClienteProyectoId]);
            }
            
            Excel::create('reporteFaltantesStock_'.date('YmdHis'), function($excel) use($aInformacionDetalles, $oClienteProyecto){
                    $excel->setTitle('Reporte de Faltantes de Stock');
                    $excel->setCreator('SICE')
                          ->setCompany('Go Trade');
                    $excel->setDescription('Reporte de faltantes de stock generado desde SICE el '.date('Y-M-d H:i:s'));
                    $excel->sheet('MSL', function($sheet) use($aInformacionDetalles, $oClienteProyecto){
                        
                        $aInformacionDetalle = [];

                        $aInformacionDetalle[] = [
                            'FECHA',
                            'SUPERVISOR',
                            'MERCADERISTA',
                            'PROVINCIA',
                            'CIUDAD',
                            'CADENA',
                            'CANAL',
                            'SUBCANAL',
                            'ESTABLECIMIENTO',
                            'CLASIFICACION',
                            'FAMILIA DE PRODUCTO',
                            'CATEGORIA DE PRODUCTO',
                            'PRESENTACION DE PRODUCTO',
                            'EAN',
                            'ESTADO',
                        ];
                        foreach($aInformacionDetalles->chunk(10) as $aInformacionDetallesChunk){
                            foreach($aInformacionDetallesChunk as $oInformacionDetalle){
                                $oHojaRuta = HojaRuta::find($oInformacionDetalle->detalleRuta->hoja_ruta_id);
                                $nombreMercaderista = '';
                                $nombreSupervisor = '';
                                if(is_object($oHojaRuta)){
                                    $nombreMercaderista = $oHojaRuta->equipoTrabajo->nombre_completo;
                                    if($oHojaRuta->equipoTrabajo->usuario_reporta_id){
                                        $nombreSupervisor = $oHojaRuta->equipoTrabajo->usuarioReporta->nombre.' '.$oHojaRuta->equipoTrabajo->usuarioReporta->apellido;
                                    }
                                }
                                $oClasificacion = ClasificacionEstablecimiento::where('cliente_proyecto_id', $oClienteProyecto->id)->where('establecimiento_id', $oInformacionDetalle->establecimiento->id)->first();
                                $aInformacionDetalle[] = [
                                    TimeFormat::dateShortFormat($oInformacionDetalle->created_at),
                                    $nombreSupervisor,
                                    $nombreMercaderista,
                                    $oInformacionDetalle->provincia->descripcion,
                                    $oInformacionDetalle->ciudad->descripcion,
                                    $oInformacionDetalle->cadena->descripcion,
                                    $oInformacionDetalle->canal->descripcion,
                                    $oInformacionDetalle->subcanal->descripcion,
                                    $oInformacionDetalle->establecimiento->nombre,
                                    is_object($oClasificacion) ? $oClasificacion->clasificacion : '',
                                    $oInformacionDetalle->familiaProducto->nombre,
                                    $oInformacionDetalle->categoriaProducto->nombre,
                                    $oInformacionDetalle->presentacionProducto->nombre,
                                    $oInformacionDetalle->presentacionProducto->contexto,
                                    $oInformacionDetalle->valor,

                                ];
                            }
                        }
                        $sheet->rows($aInformacionDetalle);
                    });
                })->download('xls');

        }else{
            
            $aProvincias = Catalogo::where('contexto','Provincias')->orderBy('descripcion', 'asc')->get();
            $aCiudades = Catalogo::where('contexto','Ciudades')->orderBy('descripcion', 'asc')->get();
            $aCanales = Catalogo::where('contexto','Canales')->orderBy('descripcion', 'asc')->get();
            $aSubcanales = Catalogo::where('contexto','Subcanales')->orderBy('descripcion', 'asc')->get();
            $aCadenas = Catalogo::where('contexto','Cadenas')->orderBy('descripcion', 'asc')->get();
            $aTipos = InformacionDetalle::distinct()->select('tipo')->get();
            
            $aData = [
                'oCliente' => $this->oCliente,
                'oClienteProyecto' => $oClienteProyecto,
                'aLideresProyecto' => $aLideresProyecto,
                'listaProvincias' => $aProvincias,
                'listaCiudades' => $aCiudades,
                'listaCanales' => $aCanales,
                'listaSubcanales' => $aSubcanales,
                'listaCadenas' => $aCadenas,
                'listaTipos' => $aTipos,
                'aProvinciasFiltro' => $aProvinciasFiltro,
                'aCiudadesFiltro' => $aCiudadesFiltro,
                'aCadenasFiltro' => $aCadenasFiltro,
                'aCanalesFiltro' => $aCanalesFiltro,
                'aSubcanalesFiltro' => $aSubcanalesFiltro,
                'sDesdeFiltro' => $sDesdeFiltro,
                'sHastaFiltro' => $sHastaFiltro,
                'sTipoFiltro' => $sTipoFiltro,
                'menuActivo' => 'reportes',
                'titulo' => 'SICE :: Reporte de Faltantes de Stock',
            ];
            return view("clientes.reportes.faltantes-stock", $aData);
        }
    }
}
