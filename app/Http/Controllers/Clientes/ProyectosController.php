<?php

namespace SICE\Http\Controllers\Clientes;

use Illuminate\Http\Request;

use SICE\Http\Requests;
use SICE\Http\Controllers\Controller;

use SICE\Catalogo;
use SICE\Cliente;
use SICE\Proyecto;
use SICE\Usuario;
use SICE\ClienteProyecto;
use SICE\Novedad;
use SICE\DetalleRutaView;
use SICE\EquipoTrabajo;
use SICE\Ruta;
use SICE\Establecimiento;

use Auth;
use Flashy;

/**
* Clase para reporteria de proyectos del cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/14
* @Reportes
*/

class ProyectosController extends Controller
{

    protected $oCliente;

    public function __construct(){
        $this->oCliente = Usuario::getCliente();

        if( ! is_object($this->oCliente)){
            Auth::logout();
            return redirect('/')->with('mensajeLogin', '<div class="alert alert-danger">Usted está intentando ingresar a una sección no permitida del sistema.</div>');
        }

    }

    /**
    * Metodo para mostrar la lista de clientes para los cuales se han generado o se desean generar nuevos proyectos
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/10
    *
    * @route /clientes/proyectos
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $listaProyectos = ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')
                                         ->select('cliente_proyectos.*')
                                         ->where('cliente_proyectos.estado', 1)
                                         ->where('equipo_trabajo.usuario_id', Auth::user()->id)
                                         ->get();
        $aData = [
            'listaProyectos' => $listaProyectos,
            'oCliente' => $this->oCliente,
            'menuActivo' => 'reportes',
            'titulo' => 'SICE :: Proyectos',
        ];
        return view("clientes.listaProyectos", $aData);
    }

    /**
    * Metodo para mostrar el detalle de un proyecto seleccionado previamente por su ID
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/15
    *
    * @route /clientes/proyectos/detalle
    * @param string sClienteProyectoId
    * @method GET/POST
    * @return \Illuminate\Http\Response
    */
    public function show($sClienteProyectoId = '')
    {
        if( ! $sClienteProyectoId){
            Flashy::error('Por favor seleccione un proyecto para poder ver su detalle.');
            return redirect()->route('clientes.proyectos.list');
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);
        if( ! is_object($oClienteProyecto)){
            Flashy::error('Ups! Parece que el proyecto seleccionado no existe. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $validaClienteProyectoUsuario = ClienteProyecto::join('equipo_trabajo', 'cliente_proyectos.id', '=', 'equipo_trabajo.cliente_proyecto_id')->select('cliente_proyectos.id')->where('cliente_proyectos.estado', 1)->where('equipo_trabajo.usuario_id', Auth::user()->id)->where('cliente_proyectos.id', $oClienteProyecto->id)->first();

        if( ! is_object($validaClienteProyectoUsuario)){
            Flashy::error('Ups! Parece que usted no está asignado al proyecto seleccionado. Por favor seleccione otro.');
            return redirect()->route('clientes.proyectos.list');   
        }

        $aLideresProyecto = $oClienteProyecto->equiposTrabajo()->where('lider_proyecto', 1)->orderBy('apellido', 'asc')->get();
        
        $iColaboradores = EquipoTrabajo::where('cliente_proyecto_id', $oClienteProyecto->id)->count();
        $iVisitas = 0;
        $iPDV = 0;
        $aRutas = $oClienteProyecto->rutas;
        if( ! $aRutas->isEmpty()){
            $aEstablecimientosAux = [];
            foreach($aRutas as $oRuta){
                $iVisitas += $oRuta->establecimientos->count();
                $aEstab = $oRuta->establecimientos;
                if( ! $aEstab->isEmpty()){
                    foreach($aEstab as $oEstablecimiento){
                       if( ! in_array($oEstablecimiento->id, $aEstablecimientosAux)){
                            $iPDV++;
                            $aEstablecimientosAux[] = $oEstablecimiento->id;
                        }
                    }
                }
            }
        }

        $aNovedades = $oClienteProyecto->novedades()->orderBy('created_at', 'desc')->take(10)->get();

        $firstMonthDay = new \DateTime();
        $firstMonthDay->modify('first day of this month');
        $lastMonthDay = new \DateTime();
        $lastMonthDay->modify('last day of this month');

        $iVisitasMes = $oClienteProyecto->detalleRutas()->visitados()->desde($firstMonthDay->format('Y-m-d H:i:s'))->hasta($lastMonthDay->format('Y-m-d H:i:s'))->count();
        $iNoVisitasMes = $oClienteProyecto->detalleRutas()->noVisitados()->desde($firstMonthDay->format('Y-m-d H:i:s'))->hasta($lastMonthDay->format('Y-m-d H:i:s'))->count();
        
        $aData = [
            'oCliente' => $this->oCliente,
            'oClienteProyecto' => $oClienteProyecto,
            'aLideresProyecto' => $aLideresProyecto,
            'menuActivo' => 'reportes',
            'iColaboradores' => $iColaboradores,
            'iVisitas' => $iVisitas,
            'iPDV' => $iPDV,
            'aNovedades' => $aNovedades,
            'iVisitasMes' => $iVisitasMes,
            'iNoVisitasMes' => $iNoVisitasMes,
            'titulo' => 'SICE :: Detalle de Proyecto',
        ];
        return view("clientes.detalleProyecto", $aData);
    }
}
