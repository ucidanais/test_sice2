<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Ruta;
use SICE\EquipoTrabajo;
use SICE\ClienteProyecto;
use SICE\ClienteEstablecimiento;
use SICE\Usuario;
use SICE\Proyecto;
use SICE\Establecimiento;
use SICE\Http\Requests\RutaFormRequest;

/**
* Clase para administración de rutas en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2017/03/22
* @Configuracion
*/

class RutaEstablecimientosController extends Controller
{
    /**
    * Metodo para mostrar la lista de establecimientos por ruta del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/11
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param string $sRutaId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $sRutaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/lista-de-establecimientos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($sRutaId){
            $oRuta = Ruta::find($sRutaId);
            if($oRuta){
            	$oClienteProyecto = $oRuta->clienteProyecto;
            	$oCliente = $oRuta->cliente;
            	$oProyecto = $oRuta->proyecto;
                $aEstablecimientosRuta = $oRuta->establecimientos;
                $aMarkerIcons = [
                    0 => 'http://labs.google.com/ridefinder/images/mm_20_purple.png',
                    1 => 'http://labs.google.com/ridefinder/images/mm_20_yellow.png',
                    2 => 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
                    3 => 'http://labs.google.com/ridefinder/images/mm_20_white.png',
                    4 => 'http://labs.google.com/ridefinder/images/mm_20_green.png',
                    5 => 'http://labs.google.com/ridefinder/images/mm_20_red.png',
                    6 => 'http://labs.google.com/ridefinder/images/mm_20_black.png',
                    7 => 'http://labs.google.com/ridefinder/images/mm_20_orange.png',
                    8 => 'http://labs.google.com/ridefinder/images/mm_20_gray.png',
                    9 => 'http://labs.google.com/ridefinder/images/mm_20_brown.png',
                    10 => 'http://maps.google.com/mapfiles/ms/micons/blue-dot.png',
                    11 => 'http://maps.google.com/mapfiles/ms/micons/red-dot.png',
                    12 => 'http://maps.google.com/mapfiles/ms/micons/yellow-dot.png',
                    13 => 'http://maps.google.com/mapfiles/ms/micons/orange-dot.png',
                    14 => 'http://maps.google.com/mapfiles/ms/micons/ltblue-dot.png',
                    15 => 'http://maps.google.com/mapfiles/ms/micons/green-dot.png',
                ];
                $aData = [
                    'oClienteProyecto' => $oClienteProyecto,
                    'oCliente' => $oCliente,
                    'oProyecto' => $oProyecto,
                    'oRuta' => $oRuta,
                    'aEstablecimientosRuta' => $aEstablecimientosRuta,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Lista de Establecimientos por ruta',
                ];
                return view("configuracion.proyectosPorCliente.rutas.establecimientos.listaEstablecimientos", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta para ver la lista de locales de la misma.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }


    /**
    * Metodo para mostrar la lista de establecimientos del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/18
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos-por-ruta/establecimientos-del-proyecto
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param string $sRutaId
    * @return \Illuminate\Http\Response
    */
    public function establecimientos(Request $request, $sRutaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/establecimientos-del-proyecto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if($sRutaId){
            $oRuta = Ruta::find($sRutaId);
            if($oRuta){
                $oClienteProyecto = $oRuta->clienteProyecto;
                $oCliente = $oRuta->cliente;
                $oProyecto = $oRuta->proyecto;
                $aEstablecimientos = $oClienteProyecto->clienteEstablecimientos()->activos()->get();
                $aMarkerIcons = [
                    0 => 'http://labs.google.com/ridefinder/images/mm_20_purple.png',
                    1 => 'http://labs.google.com/ridefinder/images/mm_20_yellow.png',
                    2 => 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
                    3 => 'http://labs.google.com/ridefinder/images/mm_20_white.png',
                    4 => 'http://labs.google.com/ridefinder/images/mm_20_green.png',
                    5 => 'http://labs.google.com/ridefinder/images/mm_20_red.png',
                    6 => 'http://labs.google.com/ridefinder/images/mm_20_black.png',
                    7 => 'http://labs.google.com/ridefinder/images/mm_20_orange.png',
                    8 => 'http://labs.google.com/ridefinder/images/mm_20_gray.png',
                    9 => 'http://labs.google.com/ridefinder/images/mm_20_brown.png',
                    10 => 'http://maps.google.com/mapfiles/ms/micons/blue-dot.png',
                    11 => 'http://maps.google.com/mapfiles/ms/micons/red-dot.png',
                    12 => 'http://maps.google.com/mapfiles/ms/micons/yellow-dot.png',
                    13 => 'http://maps.google.com/mapfiles/ms/micons/orange-dot.png',
                    14 => 'http://maps.google.com/mapfiles/ms/micons/ltblue-dot.png',
                    15 => 'http://maps.google.com/mapfiles/ms/micons/green-dot.png',
                ];
                $aData = [
                    'oClienteProyecto' => $oClienteProyecto,
                    'oCliente' => $oCliente,
                    'oProyecto' => $oProyecto,
                    'oRuta' => $oRuta,
                    'aEstablecimientos' => $aEstablecimientos,
                    'menuActivo' => 'configuracion',
                    'titulo' => 'SICE :: Lista de Establecimientos del proyecto',
                ];
                return view("configuracion.proyectosPorCliente.rutas.establecimientos.establecimientosProyecto", $aData);
            }else{
                \Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
            }           
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta para ver la lista de locales de la misma.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }
    }


    /**
    * Metodo para agregar un establecimiento a la ruta seleccionada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/18
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos-por-ruta/agregar-establecimiento
    * @method GET
    * @param  \Illuminate\Http\Request  $request
    * @param string $sRutaId
    * @param string $sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request, $sRutaId = '', $sEstablecimientoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/agregar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sRutaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta y un PDV para poder agregarlo a la misma.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oRuta = Ruta::find($sRutaId);
        if( ! is_object($oRuta)){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEstablecimientoId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un PDV para poder agregarlo a la ruta.</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.shops', [$oRuta->id]);
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe. Por favor seleccione otro.</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.shops', [$oRuta->id]);
        }

        $numEstablecimientos = $oRuta->establecimientos()->count();

        $oRuta->establecimientos()->attach($oEstablecimiento->id, ['orden_visita' => $numEstablecimientos+1]);

        \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento '.$oEstablecimiento->nombre.' ha sido agregado a la ruta '.$oRuta->nombre.'.</div>');
        return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.shops', [$oRuta->id]);
    }


    /**
    * Metodo para actualizar el orden de visita de los establecimientos de la ruta
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/07/08
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos-por-ruta/actualizar-orden
    * @method PUT / PATCH
    * @param  \Illuminate\Http\Request  $request
    * @param string $sRutaId
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $sRutaId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/actualizar-orden')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sRutaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta y un PDV para poder agregarlo a la misma.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oRuta = Ruta::find($sRutaId);
        if( ! is_object($oRuta)){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if(is_array($request->input('establecimiento_id'))){
            foreach($request->establecimiento_id as $key => $value){
                $oEstablecimiento = Establecimiento::find($value);
                if(is_object($oEstablecimiento) && isset($request->orden[$key])){
                    $oEstablecimiento->rutas()->updateExistingPivot($oRuta->id, ['orden_visita' => $request->orden[$key]]);
                }
            }
        }

        \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El orden de los establecimientos ha sido actualizado exitosamente.</div>');
        return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list', [$oRuta->id]);
    }


    /**
    * Metodo para eliminar un establecimiento de la ruta seleccionada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/17
    *
    * @route /configuracion/proyectos-por-cliente/establecimientos-por-ruta/quitar-establecimiento
    * @method DELETE
    * @param  \Illuminate\Http\Request  $request
    * @param string $sRutaId
    * @param string $sEstablecimientoId
    * @return \Illuminate\Http\Response
    */
    public function delete(Request $request, $sRutaId = '', $sEstablecimientoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/establecimientos-por-ruta/quitar-establecimiento')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $sRutaId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione una ruta y un PDV para poder eliminarlo de la misma.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oRuta = Ruta::find($sRutaId);
        if( ! is_object($oRuta)){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>La ruta seleccionada no existe.</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $sEstablecimientoId){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un PDV para poder eliminarlo de la ruta.</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list', [$oRuta->id]);
        }

        $oEstablecimiento = Establecimiento::find($sEstablecimientoId);
        if( ! is_object($oEstablecimiento)){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento seleccionado no existe. Por favor seleccione otro.</div>');
            return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list', [$oRuta->id]);
        }

        $oRuta->establecimientos()->detach($oEstablecimiento->id);
        
        \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El establecimiento '.$oEstablecimiento->nombre.' ha sido quitado de la ruta '.$oRuta->nombre.'.</div>');
        return redirect()->route('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list', [$oRuta->id]);
    }
}
