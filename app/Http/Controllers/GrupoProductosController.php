<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\GrupoProducto;
use SICE\ClienteProyecto;
use SICE\Usuario;
use SICE\Http\Requests\GrupoProductoCreateRequest;
use SICE\Http\Requests\GrupoProductoUpdateRequest;

/**
* Clase para administración de grupos de producto en proyectos por cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2019/05/15
* @Configuracion
*/

class GrupoProductosController extends Controller
{
    /**
    * Metodo para mostrar la lista de grupos de producto del proyecto configurado para el cliente seleccionado
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/grupos-de-productos
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/grupos-de-productos')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto para poder ver su lista de familias de producto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
        if( ! is_object($oProyectoCliente)){
         	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');   
        }

        $aGruposProducto = $oProyectoCliente->gruposProducto;
        $aData = [
            'oClienteProyecto' => $oProyectoCliente,
            'oCliente' => $oProyectoCliente->cliente,
            'oProyecto' => $oProyectoCliente->proyecto,
            'aGruposProducto' => $aGruposProducto,
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Configurar Proyecto por cliente',
        ];
        return view("configuracion.proyectosPorCliente.gruposProducto.listaGruposProducto", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un grupo de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-grupo-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function create($iClienteProyectoId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-grupo-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){

        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
        if( ! is_object($oProyectoCliente)){
        	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
                return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $aData = [
            'oClienteProyecto' => $oProyectoCliente,
            'oCliente' => $oProyectoCliente->cliente,
            'oProyecto' => $oProyectoCliente->proyecto,
            'oGrupoProducto' => null,
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Configurar Proyecto por cliente',
        ];
        return view("configuracion.proyectosPorCliente.gruposProducto.crearEditarGrupoProducto", $aData);
    }

    /**
    * Metodo para almacenar en la base de datos un grupo de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/agregar-grupo-de-producto
    * @method POST
    * @param  SICE\Http\Requests\GrupoProductoCreateRequest $request
    * @param int $iClienteProyectoId
    * @return \Illuminate\Http\Response
    */
    public function store(GrupoProductoCreateRequest $request, $iClienteProyectoId)
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/agregar-grupo-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);

        if( ! is_object($oProyectoCliente)){
         	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');   
        }

        $oGrupoProducto = new GrupoProducto;
        $oGrupoProducto->nombre = $request->input('nombre');
        $oGrupoProducto->cliente_proyecto_id = $oProyectoCliente->id;
        $oGrupoProducto->cliente_id = $oProyectoCliente->cliente_id;
        $oGrupoProducto->proyecto_id = $oProyectoCliente->proyecto_id;

        if($oGrupoProducto->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Grupo de producto '.$oGrupoProducto->nombre.' agregado exitosamente</div>');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Grupo de producto '.$oGrupoProducto->nombre.' no pudo ser agregado, por favor intentelo nuevamente luego de unos minutos.</div>');
        }
        return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
    }

    /**
    * Metodo para mostrar el formulario de edición de un grupo de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-grupo-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function edit($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-grupo-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
        if( ! is_object($oProyectoCliente)){
        	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $iId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un grupo de producto para poder editarlo</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
        }

        $oGrupoProducto = GrupoProducto::find($iId);
        if( ! is_object($oGrupoProducto)){
         	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El grupo de producto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);   
        }

        $aData = [
            'oClienteProyecto' => $oProyectoCliente,
            'oCliente' => $oProyectoCliente->cliente,
            'oProyecto' => $oProyectoCliente->proyecto,
            'oGrupoProducto' => $oGrupoProducto,
            'menuActivo' => 'configuracion',
            'titulo' => 'SICE :: Configurar Proyecto por cliente',
        ];
        return view("configuracion.proyectosPorCliente.gruposProducto.crearEditarGrupoProducto", $aData);
    }

    /**
    * Metodo para almacenar en la base de datos los cambios realizados grupo de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2019/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/editar-grupo-de-producto
    * @method POST
    * @param  SICE\Http\Requests\GrupoProductoUpdateRequest $request
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function update(GrupoProductoUpdateRequest $request, $iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/editar-grupo-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
        if( ! is_object($oProyectoCliente)){
        	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $iId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un grupo de producto para poder editarlo</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
        }

        $oGrupoProducto = GrupoProducto::find($iId);
        if( ! is_object($oGrupoProducto)){
         	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El grupo de producto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);   
        }

        $oGrupoProducto->nombre = $request->input('nombre');
        $oGrupoProducto->cliente_proyecto_id = $oProyectoCliente->id;
        $oGrupoProducto->cliente_id = $oProyectoCliente->cliente_id;
        $oGrupoProducto->proyecto_id = $oProyectoCliente->proyecto_id;
        if($oGrupoProducto->save()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Grupo de producto '.$oGrupoProducto->nombre.' actualizado exitosamente</div>');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Grupo de producto '.$oGrupoProducto->nombre.' no pudo ser actualizado, por favor intentelo nuevamente luego de unos minutos.</div>');
        }
        return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
    }

    /**
    * Metodo para eliminar un grupo de productos para el proyecto
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/05/15
    *
    * @route /configuracion/proyectos-por-cliente/productos/eliminar-grupo-de-producto
    * @method GET
    * @param int $iClienteProyectoId
    * @param int $iId
    * @return \Illuminate\Http\Response
    */
    public function destroy($iClienteProyectoId = '', $iId = '')
    {
        if(!Usuario::verificaPermiso('configuracion/proyectos-por-cliente/productos/eliminar-grupo-de-producto')){
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Usted no tiene permisos para navegar en esta seccion del sistema</div>');
            return redirect('home');
        }

        if( ! $iClienteProyectoId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un proyecto</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        $oProyectoCliente = ClienteProyecto::find($iClienteProyectoId);
        if( ! is_object($oProyectoCliente)){
        	\Session::flash('message', '<div role="alert" class="alert alert-warning alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Proyecto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/listar-por-cliente');
        }

        if( ! $iId){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>Por favor seleccione un grupo de producto para poder eliminarlo</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
        }

        $oGrupoProducto = GrupoProducto::find($iId);
        if( ! is_object($oGrupoProducto)){
        	\Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El grupo de producto seleccionado no existe</div>');
            return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);    
        }

        $oGrupoProducto->presentacionesProducto()->detach();
        
        if($oGrupoProducto->delete()){
            \Session::flash('message', '<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El grupo de producto "'.$oGrupoProducto->nombre.'" ha sido eliminado exitosamente</div>');
        }else{
            \Session::flash('message', '<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>El grupo de producto "'.$oGrupoProducto->nombre.'" no puede ser eliminado. Por favor intentelo nuevamente luego de unos minutos o contacte al administrador del sistema</div>');
        }
        return redirect('configuracion/proyectos-por-cliente/productos/grupos-de-productos/'.$oProyectoCliente->id);
    }
}
