<?php

namespace SICE\Http\Controllers;

use Illuminate\Http\Request;

use SICE\Http\Requests;

use SICE\Catalogo;
use SICE\Usuario;
use SICE\Http\Requests\BarrioFormRequest;

use Flashy;

/**
* Clase para administración de barrios
* @Autor Raúl Chauvin
* @FechaCreacion  2016/07/01
* @Parametrizacion
*/

class BarriosController extends Controller
{
    /**
    * Metodo para mostrar la lista de barrios (el metodo acepta tambien el ID de la parroquia para realizar el filtrado)
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/barrios/lista-de-barrios
    * @method GET/POST
    * @param  \Illuminate\Http\Request  $request
    * @param int $iParroquiaId
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request, $iParroquiaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/lista-de-barrios')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        $aData = [
            'listaBarrios' => Catalogo::buscarCatalogo($request->input('valor_busqueda'), 'Barrios', $iParroquiaId),
            'menuActivo' => 'catalogos',
            'iParroquiaId' => $iParroquiaId,
            'titulo' => 'SICE :: Barrios',
        ];
        return view("catalogos.barrios.listaBarrios", $aData);
    }

    /**
    * Metodo para mostrar el formulario de creacion de un nuevo barrio
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/barrios/agregar-barrio
    * @method GET
    * @param int $iParroquiaId
    * @return \Illuminate\Http\Response
    */
    public function create($iParroquiaId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/agregar-barrio')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
        $iIdProvincia = 0;
        $iIdCiudad = 0;
        $iIdZona = 0;
        $iIdSector = 0;
        if($iParroquiaId){
            $oSector = Catalogo::find($iParroquiaId);
            if($oSector){
                $iIdZona = $oSector->catalogoPadre->id;
                $iIdCiudad = $oSector->catalogoPadre->catalogoPadre->id;
                $iIdProvincia = $oSector->catalogoPadre->catalogoPadre->catalogoPadre->id;
            }
        }

        $aData = [
            'menuActivo' => 'catalogos',
            'provinciaId' => $iIdProvincia,
            'ciudadId' => $iIdCiudad,
            'zonaId' => $iIdZona,
            'parroquiaId' => $iParroquiaId,
            'listaProvincias' => $aListaProvincias,
            'titulo' => 'SICE :: Crear Nuevo Barrio',
        ];
        return view("catalogos.barrios.crearEditarBarrio", $aData);
    }

    /**
    * Metodo para guardar un nuevo barrio
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/07/01
    *
    * @route /catalogos/barrios/agregar-barrio
    * @method POST
    * @param  \SICE\Http\Requests\BarrioFormRequest  $barrioFormRequest
    * @return \Illuminate\Http\Response
    */
    public function store(BarrioFormRequest $barrioFormRequest)
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/agregar-barrio')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }
        

        $oBarrio = new Catalogo;
        $oCatalogoPadre = Catalogo::find($barrioFormRequest->input('catalogo_id'));
        $oBarrio->contexto = 'Barrios';
        $oBarrio->codigo1 = Catalogo::generaCodigo('Barrios', $oCatalogoPadre->id);
        $oBarrio->codigo2 = $oCatalogoPadre->codigo1;
        $oBarrio->valor2 = $oCatalogoPadre->descripcion;
        $oBarrio->descripcion = $barrioFormRequest->input('descripcion');
        $oBarrio->coordenadas_poligono = $barrioFormRequest->input('coordenadas_poligono');
        $oBarrio->catalogo_id = $barrioFormRequest->input('catalogo_id');
        
        if($oBarrio->save()){
            Flashy::success('Barrio '.$oBarrio->descripcion.' guardado satisfactoriamente');
        }else{
            Flashy::error('Barrio '.$oBarrio->descripcion.' no pudo ser guardado');
        }
        return redirect('catalogos/barrios');
    }

    /**
     * Metodo que muestra el formulario de edicion de un barrio de acuerdo a su ID
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/barrios/editar-barrio
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function edit($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/editar-barrio')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if($iId){
            $oBarrio = Catalogo::find($iId);
            if($oBarrio){
                $aListaProvincias = Catalogo::where('contexto', 'Provincias')->lists('descripcion','id');
                $aData = [
                    'oBarrio' => $oBarrio,
                    'listaProvincias' => $aListaProvincias,
                    'provinciaId' => $oBarrio->catalogoPadre->catalogoPadre->catalogoPadre->catalogoPadre->id,
                    'ciudadId' => $oBarrio->catalogoPadre->catalogoPadre->catalogoPadre->id,
                    'zonaId' => $oBarrio->catalogoPadre->catalogoPadre->id,
                    'parroquiaId' => $oBarrio->catalogoPadre->id,
                    'menuActivo' => 'catalogos',
                    'titulo' => 'SICE :: Editar Barrio',
                ];
                return view('catalogos.barrios.crearEditarBarrio', $aData);
            }else{
                Flashy::error('El barrio seleccionado no existe');
            }
        }else{
            Flashy::error('Por favor seleccione un barrio para poder editarlo.');
        }
        return redirect('catalogos/barrios');
    }

    /**
     * Metodo que actualiza la informacion del barrio en la base de datos
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/barrios/editar-barrio
     * @method POST
     * @param  \SICE\Http\Requests\BarrioFormRequest  $barrioFormRequest
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function update(BarrioFormRequest $barrioFormRequest, $iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/editar-barrio')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if($iId){
            $oBarrio = Catalogo::find($iId);
            if($oBarrio){
                $oBarrio->contexto = 'Barrios';
                $oBarrio->descripcion = $barrioFormRequest->input('descripcion');
                $oBarrio->coordenadas_poligono = $barrioFormRequest->input('coordenadas_poligono');
             
                if($oBarrio->save()){
                    Flashy::success('Barrio '.$oBarrio->descripcion.' guardado exitosamente.');
                }else{
                    Flashy::error('Barrio '.$oBarrio->descripcion.' no pudo ser guardado');
                }
            }else{
                Flashy::error('El barrio seleccionado no existe');
            }
        }else{
            Flashy::error('Por favor seleccione un barrio para poder actualizar su información');
        }
        return redirect('catalogos/barrios');
    }

    /**
     * Metodo que elimina un barrio del sistema
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/07/01
     *
     * @route /catalogos/barrios/eliminar-barrio
     * @method GET
     * @param  int  $iId
     * @return \Illuminate\Http\Response
     */
    public function destroy($iId = '')
    {
        if(!Usuario::verificaPermiso('catalogos/barrios/eliminar-barrio')){
            Flashy::error('Usted no tiene permisos para navegar en esta seccion del sistema');
            return redirect('home');
        }

        if($iId){
            $oBarrio = Catalogo::find($iId);
            if($oBarrio){
                $nombre = $oBarrio->descripcion;
                
                if($oBarrio->delete()){
                    Flashy::success('Barrio '.$nombre.' eliminado satisfactoriamente');
                }else{
                    Flashy::error('Barrio '.$nombre.' no pudo ser eliminado');
                }
            }else{
                Flashy::error('El barrio seleccionado no existe');
            }
        }else{
            Flashy::error('Por favor seleccione un barrio para poder eliminarlo');
        }
        return redirect('catalogos/barrios');
    }
}
