<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// RUTAS DE LOGIN
Route::get('/','LoginController@login');
Route::post('/','LoginController@validaLogin');

Route::get('login','LoginController@login')->name('login');
Route::post('login','LoginController@validaLogin')->name('login.validate');

Route::get('salir','LoginController@cerrarSesion')->name('logout');

/*****************************************************************

	Grupo de rutas para la administracion del sistema

******************************************************************/

Route::group(['middleware' => 'auth'], function(){

	Route::get('home', 'HomeController@index')->name('home');
	Route::get('mi-perfil', 'UsuariosController@miPerfil')->name('profile');
	Route::post('mi-perfil', 'UsuariosController@miPerfilSave')->name('profile.save');

	Route::group(['middleware' => 'superadmin'], function(){

		/*****************************************************************
    
	    	Grupo de rutas para el modulo de Autenticacion/Autorizacion
	    
	    ******************************************************************/
		Route::group(['prefix' => 'seguridades'], function(){
			/*****************************************************************
    
		    	Grupo de rutas para la administracion de modulos
		    
		    ******************************************************************/
			Route::group(['prefix' => 'modulos'], function(){
				
				// Rutas para ver la lista de modulos
				Route::match(['get','post'],'', 'ModulosController@index');
				Route::match(['get','post'],'lista-de-modulos', 'ModulosController@index')->name('seguridades.modulos.list');

				// Rutas para agregar un nuevo modulo
				Route::get('agregar-modulo', 'ModulosController@create')->name('seguridades.modulos.create');
				Route::post('agregar-modulo', 'ModulosController@store')->name('seguridades.modulos.store');

				// Rutas para editar un modulo
				Route::get('editar-modulo/{iId?}', 'ModulosController@edit')->name('seguridades.modulos.edit');
				Route::post('editar-modulo/{iId?}', 'ModulosController@update')->name('seguridades.modulos.update');

				// Ruta para eliminar un modulo
				Route::get('eliminar-modulo/{iId?}', 'ModulosController@destroy')->name('seguridades.modulos.destroy');
			});

			/*****************************************************************
		    
		    	Grupo de rutas para la administracion de permisos
		    
		    ******************************************************************/
			Route::group(['prefix' => 'permisos'], function(){
				// Rutas para ver la lista de permisos
				Route::match(['get','post'],'', 'PermisosController@index');
				Route::match(['get','post'],'lista-de-permisos', 'PermisosController@index')->name('seguridades.permisos.list');

				// Rutas para agregar un nuevo permiso
				Route::get('agregar-permiso', 'PermisosController@create')->name('seguridades.permisos.create');
				Route::post('agregar-permiso', 'PermisosController@store')->name('seguridades.permisos.store');

				// Rutas para editar un permiso
				Route::get('editar-permiso/{iId?}', 'PermisosController@edit')->name('seguridades.permisos.edit');
				Route::post('editar-permiso/{iId?}', 'PermisosController@update')->name('seguridades.permisos.update');

				// Ruta para eliminar un permiso
				Route::get('eliminar-permiso/{iId?}', 'PermisosController@destroy')->name('seguridades.permisos.destroy');
			});
		});
	});

	Route::group(['middleware' => 'admin'], function(){
		/*****************************************************************
    
	    	Grupo de rutas para el modulo de Autenticacion/Autorizacion
	    
	    ******************************************************************/
		Route::group(['prefix' => 'seguridades'], function(){
			/*****************************************************************
		    
		    	Grupo de rutas para la administracion de usuarios
		    
		    ******************************************************************/
			Route::group(['prefix' => 'usuarios'], function(){
				// Rutas para ver la lista de usuarios
				Route::match(['get','post'], '', 'UsuariosController@index');
				Route::match(['get','post'], 'lista-de-usuarios', 'UsuariosController@index')->name('seguridades.usuarios.list');

				// Rutas para agregar un nuevo usuario
				Route::get('agregar-usuario', 'UsuariosController@create')->name('seguridades.usuarios.create');
				Route::post('agregar-usuario', 'UsuariosController@store')->name('seguridades.usuarios.update');

				// Rutas para editar un usuario
				Route::get('editar-usuario/{iId?}', 'UsuariosController@edit')->name('seguridades.usuarios.edit');
				Route::post('editar-usuario/{iId?}', 'UsuariosController@update')->name('seguridades.usuarios.update');

				// Ruta para ver el detalle de un usuario
				Route::get('detalle-de-usuario/{iId?}', 'UsuariosController@show')->name('seguridades.usuarios.show');

				// Ruta para eliminar un usuario
				Route::get('eliminar-usuario/{iId?}', 'UsuariosController@destroy')->name('seguridades.usuarios.destroy');

				// Ruta para cambiar el estado de un usuario
				Route::get('cambiar-estado-usuario/{iId?}', 'UsuariosController@cambiaEstado')->name('seguridades.usuarios.change-status');
			});
		});

	});


	Route::group(['middleware' => 'backfoffice'], function(){
		
		/*****************************************************************
    
	    	Grupo de rutas para el modulo de Configuracion/Parametrizacion
	    
	    ******************************************************************/
		Route::group(['prefix' => 'configuracion'], function(){
			/*****************************************************************
		    
		    	Grupo de rutas para la administracion de proyectos
		    
		    ******************************************************************/
			Route::group(['prefix' => 'proyectos'], function(){
				// Rutas para ver la lista de proyectos
				Route::match(['get','post'], '', 'ProyectosController@index');
				Route::match(['get','post'], 'lista-de-proyectos', 'ProyectosController@index')->name('configuracion.proyectos.list');

				// Rutas para agregar un nuevo proyecto
				Route::get('agregar-proyecto', 'ProyectosController@create')->name('configuracion.proyectos.create');
				Route::post('agregar-proyecto', 'ProyectosController@store')->name('configuracion.proyectos.store');

				// Rutas para editar un proyecto
				Route::get('editar-proyecto/{iId?}', 'ProyectosController@edit')->name('configuracion.proyectos.edit');
				Route::post('editar-proyecto/{iId?}', 'ProyectosController@update')->name('configuracion.proyectos.update');

				// Ruta para ver el detalle de un proyecto
				Route::get('detalle-de-proyecto/{iId?}', 'ProyectosController@show')->name('configuracion.proyectos.show');

				// Ruta para eliminar un proyecto
				Route::get('eliminar-proyecto/{iId?}', 'ProyectosController@destroy')->name('configuracion.proyectos.destroy');

				// Ruta para cambiar el estado de un proyecto
				Route::get('cambiar-estado-proyecto/{iId?}', 'ProyectosController@cambiaEstado')->name('configuracion.proyectos.change-status');
			});

			/*****************************************************************
		    
		    	Grupo de rutas para la administracion de clientes
		    
		    ******************************************************************/
			Route::group(['prefix' => 'clientes'], function(){
				// Rutas para ver la lista de clientes
				Route::match(['get','post'], '', 'ClientesController@index');
				Route::match(['get','post'], 'lista-de-clientes', 'ClientesController@index')->name('configuracion.clientes.list');

				// Rutas para agregar un nuevo cliente
				Route::get('agregar-cliente', 'ClientesController@create')->name('configuracion.clientes.create');
				Route::post('agregar-cliente', 'ClientesController@store')->name('configuracion.clientes.store');

				// Rutas para editar un cliente
				Route::get('editar-cliente/{iId?}', 'ClientesController@edit')->name('configuracion.clientes.edit');
				Route::post('editar-cliente/{iId?}', 'ClientesController@update')->name('configuracion.clientes.update');

				// Ruta para ver el detalle de un cliente
				Route::get('detalle-de-cliente/{iId?}', 'ClientesController@show')->name('configuracion.clientes.show');

				// Ruta para eliminar un cliente
				Route::get('eliminar-cliente/{iId?}', 'ClientesController@destroy')->name('configuracion.clientes.destroy');

				// Ruta para cambiar el estado de un cliente
				Route::get('cambiar-estado-cliente/{iId?}', 'ClientesController@cambiaEstado')->name('configuracion.clientes.change-status');

				/*****************************************************************
		    
			    	Grupo de rutas para la administracion de usuarios
			    
			    ******************************************************************/
				Route::group(['prefix' => 'usuarios'], function(){
					// Rutas para ver la lista de usuarios
					Route::match(['get','post'], 'lista-de-usuarios/{clienteId?}', 'UsuariosClienteController@index')->name('configuracion.clientes.usuarios.list');

					// Rutas para agregar un nuevo usuario
					Route::get('agregar-usuario/{clienteId?}', 'UsuariosClienteController@create')->name('configuracion.clientes.usuarios.create');
					Route::post('agregar-usuario/{clienteId?}', 'UsuariosClienteController@store')->name('configuracion.clientes.usuarios.store');

					// Rutas para editar un usuario
					Route::get('editar-usuario/{clienteId?}/{iId?}', 'UsuariosClienteController@edit')->name('configuracion.clientes.usuarios.edit');
					Route::post('editar-usuario/{clienteId?}/{iId?}', 'UsuariosClienteController@update')->name('configuracion.clientes.usuarios.update');

					// Ruta para ver el detalle de un usuario
					Route::get('detalle-de-usuario/{clienteId?}/{iId?}', 'UsuariosClienteController@show')->name('configuracion.clientes.usuarios.show');

					// Ruta para eliminar un usuario
					Route::get('eliminar-usuario/{clienteId?}/{iId?}', 'UsuariosClienteController@destroy')->name('configuracion.clientes.usuarios.destroy');

					// Ruta para cambiar el estado de un usuario
					Route::get('cambiar-estado-usuario/{clienteId?}/{iId?}', 'UsuariosClienteController@cambiaEstado')->name('configuracion.clientes.usuarios.change-status');
				});
			});

			/*****************************************************************
		    
		    	Grupo de rutas para la administracion de proyectos
		    
		    ******************************************************************/
			Route::group(['prefix' => 'proyectos-por-cliente'], function(){
				// Ruta para mostrar los proyectos que se han generado para cada cliente
				Route::match(['get','post'],'listar-por-cliente', 'ClienteProyectosController@clientes')->name('configuracion.proyectos-por-cliente.list-by-cliente');
				Route::match(['get','post'],'listar-por-proyecto', 'ClienteProyectosController@proyectos')->name('configuracion.proyectos-por-cliente.list-by-proyecto');

				// Rutas para configurar un proyecto para un determinado cliente
				Route::get('generar-proyecto/{clienteProyecto?}/{iIdCliente?}', 'ClienteProyectosController@create')->name('configuracion.proyectos-por-cliente.create');
				Route::post('generar-proyecto', 'ClienteProyectosController@store')->name('configuracion.proyectos-por-cliente.store');

				// Rutas para editar la configuracion de un proyecto para un determinado cliente
				Route::get('editar-proyecto-por-cliente/{iIdProyectoCliente?}', 'ClienteProyectosController@edit')->name('configuracion.proyectos-por-cliente.edit');
				Route::post('editar-proyecto-por-cliente/{iIdProyectoCliente?}', 'ClienteProyectosController@update')->name('configuracion.proyectos-por-cliente.update');

				// Ruta para ver el detalle del Proyecto con el cual se esta trabajando para el cliente seleccionado.
				Route::get('detalle-proyecto-por-cliente/{iIdProyectoCliente?}', 'ClienteProyectosController@show')->name('configuracion.proyectos-por-cliente.show');

				// Ruta para cambiar el estado del proyecto asignado al cliente
				Route::get('cambiar-estado/{sClienteProyectoId?}', 'ClienteProyectosController@cambiarEstado')->name('configuracion.proyectos-por-cliente.change-status');

				/****************************************************************************************
		    
			    	Grupo de rutas para la administracion de EQUIPOS DE TRABAJO
			    
			    ****************************************************************************************/
                                
                // Rutas para administracion de equipos de trabajo
                Route::get('equipo-de-trabajo/{sClienteProyectoId?}', 'EquiposTrabajoController@equipoTrabajo')->name('configuracion.proyectos-por-cliente.equipos-trabajo.list');

                // Ruta para asignar un colaborador al proyecto
                Route::post('asignar-usuario-al-equipo-de-trabajo/{sClienteProyectoId?}', 'EquiposTrabajoController@asignarUsuarioProyecto')->name('configuracion.proyectos-por-cliente.equipos-trabajo.asignar');

                // Ruta para actualizar informacion de un colaborador en el proyecto
                Route::post('actualizar-usuario-del-equipo-de-trabajo/{sEquipoTrabajoId?}', 'EquiposTrabajoController@actualizaUsuarioProyecto')->name('configuracion.proyectos-por-cliente.equipos-trabajo.actualizar');

                // Ruta para quitar a un colaborador del proyecto
                Route::get('quitar-colaborador-del-equipo-de-trabajo/{iEquipoTrabajoId?}', 'EquiposTrabajoController@quitarUsuarioProyecto')->name('configuracion.proyectos-por-cliente.equipos-trabajo.quitar');

                
                /******************************************************************************************
		    
			    	Grupo de rutas para la administracion de ENCUESTAS
			    
			    *******************************************************************************************/
			    // Rutas para administracion de portafolios y grupos de productos
                Route::group(['prefix' => 'encuestas'], function(){

                	// Rutas para ver la lista de encuestas por idClienteProyecto
					Route::match(['get','post'],'lista-de-encuestas/{sClienteProyectoId?}', 'ClienteProyectos\EncuestasController@index')->name('configuracion.proyectos-por-cliente.encuestas.list');

					// Rutas para agregar nuevas encuestas al sistema
					Route::get('/agregar-encuesta/{sClienteProyectoId?}', 'ClienteProyectos\EncuestasController@create')->name('configuracion.proyectos-por-cliente.encuestas.create');
					Route::post('/agregar-encuesta/{sClienteProyectoId?}', 'ClienteProyectos\EncuestasController@store')->name('configuracion.proyectos-por-cliente.encuestas.store');

					// Rutas para actualizar una encuesta en el sistema de acuerdo a su ID
					Route::get('/editar-encuesta/{sClienteProyectoId?}/{sEncuestaId?}', 'ClienteProyectos\EncuestasController@edit')->name('configuracion.proyectos-por-cliente.encuestas.edit');
					Route::match(['PUT', 'PATCH'], '/editar-encuesta/{sClienteProyectoId?}/{sEncuestaId?}', 'ClienteProyectos\EncuestasController@update')->name('configuracion.proyectos-por-cliente.encuestas.update');

					// Rutas para eliminar una encuesta en el sistema de acuerdo a su ID
					Route::get('/eliminar-encuesta/{sClienteProyectoId?}/{sEncuestaId?}', 'ClienteProyectos\EncuestasController@destroy')->name('configuracion.proyectos-por-cliente.encuestas.delete');

					// Rutas para eliminar una pregunta de la encuesta de acuerdo a su ID
					Route::get('/eliminar-pregunta/{sPreguntaId?}', 'ClienteProyectos\EncuestasController@destroyPregunta')->name('configuracion.proyectos-por-cliente.encuestas.delete-pregunta');
					
					// Rutas para filtrar y descargar el reporte de la encuesta
					Route::get('/filtrar-encuesta/{sClienteProyectoId?}/{sEncuestaId?}', 'ClienteProyectos\EncuestasController@filtros')->name('configuracion.proyectos-por-cliente.encuestas.filtros');
					Route::post('/filtrar-encuesta/{sClienteProyectoId?}/{sEncuestaId?}', 'ClienteProyectos\EncuestasController@descarga')->name('configuracion.proyectos-por-cliente.encuestas.descarga');

                });

                /******************************************************************************************
		    
			    	Grupo de rutas para la administracion de FAMILIAS, CATEGORIAS y PRESENTACIONES DE PRODUCTOS
			    
			    *******************************************************************************************/

                // Rutas para administracion de portafolios y grupos de productos
                Route::group(['prefix' => 'productos'], function(){
                	
                	/*************************************************************************************
		    
				    	Grupo de rutas para la administracion de GRUPOS DE PRODUCTOS
				    
				    **************************************************************************************/
                	// Rutas para ver la lista de grupos de producto por idClienteProyecto
					Route::match(['get','post'],'grupos-de-productos/{sClienteProyectoId?}', 'GrupoProductosController@index')->name('configuracion.proyectos-por-cliente.grupos-de-productos.list');

					// Rutas para agregar un nuevo grupo de producto por idClienteProyecto
					Route::get('agregar-grupo-de-producto/{sClienteProyectoId?}', 'GrupoProductosController@create')->name('configuracion.proyectos-por-cliente.grupos-de-productos.create');
					Route::post('agregar-grupo-de-producto/{sClienteProyectoId?}', 'GrupoProductosController@store')->name('configuracion.proyectos-por-cliente.grupos-de-productos.store');

					// Rutas para editar un grupo de producto
					Route::get('editar-grupo-de-producto/{sClienteProyectoId?}/{iId?}', 'GrupoProductosController@edit')->name('configuracion.proyectos-por-cliente.grupos-de-productos.edit');
					Route::post('editar-familia-de-producto/{iId?}', 'GrupoProductosController@update')->name('configuracion.proyectos-por-cliente.grupos-de-productos.update');

					// Ruta para eliminar un grupo de producto
					Route::get('eliminar-grupo-de-producto/{sClienteProyectoId?}/{iId?}', 'GrupoProductosController@destroy')->name('configuracion.proyectos-por-cliente.grupos-de-productos.destroy');

                	/*************************************************************************************
		    
				    	Grupo de rutas para la administracion de FAMILIAS DE PRODUCTOS
				    
				    **************************************************************************************/
                	// Rutas para ver la lista de familias de producto por idClienteProyecto
					Route::match(['get','post'],'familias-de-productos/{sClienteProyectoId?}', 'FamiliasProductosController@index')->name('configuracion.proyectos-por-cliente.familias-de-productos.list');

					// Rutas para agregar una nueva familia de producto por idClienteProyecto
					Route::get('agregar-familia-de-producto/{sClienteProyectoId?}', 'FamiliasProductosController@create')->name('configuracion.proyectos-por-cliente.familias-de-productos.create');
					Route::post('agregar-familia-de-producto/{sClienteProyectoId?}', 'FamiliasProductosController@store')->name('configuracion.proyectos-por-cliente.familias-de-productos.store');

					// Rutas para editar una familia de producto
					Route::get('editar-familia-de-producto/{sClienteProyectoId?}/{iId?}', 'FamiliasProductosController@edit')->name('configuracion.proyectos-por-cliente.familias-de-productos.edit');
					Route::post('editar-familia-de-producto/{iId?}', 'FamiliasProductosController@update')->name('configuracion.proyectos-por-cliente.familias-de-productos.update');

					// Ruta para eliminar una familia de producto
					Route::get('eliminar-familia-de-producto/{sClienteProyectoId?}/{iId?}', 'FamiliasProductosController@destroy')->name('configuracion.proyectos-por-cliente.familias-de-productos.destroy');


					/****************************************************************************************
		    
				    	Grupo de rutas para la administracion de CATEGORIAS DE PRODUCTOS
				    
				    ****************************************************************************************/
					// Rutas para ver la lista de categorias de producto por idFamiliaProducto
					Route::match(['get','post'],'categorias-de-productos/{iFamiliaProductoId?}', 'CategoriasProductosController@index')->name('configuracion.proyectos-por-cliente.categorias-de-productos.list');

					// Rutas para agregar una nueva categoria de producto por idFamiliaProducto
					Route::get('agregar-categoria-de-producto/{iFamiliaProductoId?}', 'CategoriasProductosController@create')->name('configuracion.proyectos-por-cliente.categorias-de-productos.create');
					Route::post('agregar-categoria-de-producto/{iFamiliaProductoId?}', 'CategoriasProductosController@store')->name('configuracion.proyectos-por-cliente.categorias-de-productos.store');

					// Rutas para editar una categoria de producto
					Route::get('editar-categoria-de-producto/{iFamiliaProductoId?}/{iId?}', 'CategoriasProductosController@edit')->name('configuracion.proyectos-por-cliente.categorias-de-productos.edit');
					Route::post('editar-categoria-de-producto/{iId?}', 'CategoriasProductosController@update')->name('configuracion.proyectos-por-cliente.categorias-de-productos.update');

					// Ruta para eliminar una categoria de producto
					Route::get('eliminar-categoria-de-producto/{iFamiliaProductoId?}/{iId?}', 'CategoriasProductosController@destroy')->name('configuracion.proyectos-por-cliente.categorias-de-productos.destroy');


					/************************************************************************************************
		    
				    	Grupo de rutas para la administracion de PRESENTACIONES DE PRODUCTOS
				    
				    *************************************************************************************************/
					// Rutas para ver la lista de presentaciones de producto por idCategoriaProducto
					Route::match(['get','post'],'presentaciones-de-productos/{iCategoriaProductoId?}', 'PresentacionesProductosController@index')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.list');

					// Rutas para agregar una nueva presentacion de producto por idCategoriaProducto
					Route::get('agregar-presentacion-de-producto/{iCategoriaProductoId?}', 'PresentacionesProductosController@create')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.create');
					Route::post('agregar-presentacion-de-producto/{iCategoriaProductoId?}', 'PresentacionesProductosController@store')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.store');

					// Rutas para editar una presentacion de producto
					Route::get('editar-presentacion-de-producto/{iCategoriaProductoId?}/{iId?}', 'PresentacionesProductosController@edit')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.edit');
					Route::post('editar-presentacion-de-producto/{iId?}', 'PresentacionesProductosController@update')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.update');

					// Ruta para eliminar una presentacion de producto
					Route::get('eliminar-presentacion-de-producto/{iCategoriaProductoId?}/{iId?}', 'PresentacionesProductosController@destroy')->name('configuracion.proyectos-por-cliente.presentaciones-de-productos.destroy');
                });

                // Rutas para administracion de rutas y hojas de ruta
                Route::group(['prefix' => 'rutas'], function(){
                	/************************************************************************************************
		    
				    	Grupo de rutas para la administracion de RUTAS DEL PROYECTO
				    
				    *************************************************************************************************/
				    // Rutas para ver la lista de rutas del proyecto por idClienteProyecto
					Route::match(['get','post'],'lista-de-rutas/{sClienteProyectoId?}', 'RutasController@index')->name('configuracion.proyectos-por-cliente.rutas.list');

					// Rutas para agregar una nueva ruta del proyecto por idClienteProyecto
					Route::get('agregar-ruta/{sClienteProyectoId?}', 'RutasController@create')->name('configuracion.proyectos-por-cliente.rutas.create');
					Route::post('agregar-ruta/{sClienteProyectoId?}', 'RutasController@store')->name('configuracion.proyectos-por-cliente.rutas.store');

					// Rutas para editar una ruta del proyecto
					Route::get('editar-ruta/{sClienteProyectoId?}/{iId?}', 'RutasController@edit')->name('configuracion.proyectos-por-cliente.rutas.edit');
					Route::post('editar-ruta/{iId?}', 'RutasController@update')->name('configuracion.proyectos-por-cliente.rutas.update');

					// Ruta para eliminar una ruta del proyecto
					Route::get('eliminar-ruta/{sClienteProyectoId?}/{iId?}', 'RutasController@destroy')->name('configuracion.proyectos-por-cliente.rutas.destroy');
					
                });

                /****************************************************************************************
		    
			    	Grupo de rutas para la administracion de ESTABLECIMIENTOS POR RUTA
			    
			    ****************************************************************************************/

                Route::group(['prefix' => 'establecimientos-por-ruta'], function(){
                	
                	// Ruta para mostrar la lista de PDV por ruta
                	Route::get('lista-de-establecimientos/{sRutaId?}', 'RutaEstablecimientosController@index')->name('configuracion.proyectos-por-cliente.establecimientos-por-ruta.list');

                	// Ruta para mostrar la lista de PDV por proyecto del cliente
                	Route::get('establecimientos-del-proyecto/{sRutaId?}', 'RutaEstablecimientosController@establecimientos')->name('configuracion.proyectos-por-cliente.establecimientos-por-ruta.shops');

                	// Ruta para quitar un establecimiento de la ruta
                	Route::get('quitar-establecimiento/{sRutaId?}/{sEstablecimientoId?}', 'RutaEstablecimientosController@delete')->name('configuracion.proyectos-por-cliente.establecimientos.delete');

                	// Ruta para agregar un establecimiento a la ruta
                	Route::get('agregar-establecimiento/{sRutaId?}/{sEstablecimientoId?}', 'RutaEstablecimientosController@store')->name('configuracion.proyectos-por-cliente.establecimientos.store');

                	// Ruta para actualizar el orden de visita de los establecimientos de la ruta
                	Route::post('actualizar-orden/{sRutaId?}', 'RutaEstablecimientosController@update')->name('configuracion.proyectos-por-cliente.establecimientos.update');
                });

                // Rutas para administracion de establecimientos
                Route::group(['prefix' => 'establecimientos'], function(){
                	/************************************************************************************************
		    
				    	Grupo de rutas para la administracion de ESTABLECIMIENTOS DEL PROYECTO
				    
				    *************************************************************************************************/
				    // Rutas para ver la lista de establecimientos del proyecto por idClienteProyecto
					Route::match(['get','post'],'lista-de-establecimientos/{sClienteProyectoId?}', 'ClienteProyectos\EstablecimientosController@index')->name('configuracion.proyectos-por-cliente.establecimientos.lista');

					Route::match(['get','post'],'mapa-de-establecimientos/{sClienteProyectoId?}', 'ClienteProyectos\EstablecimientosController@indexMapa')->name('configuracion.proyectos-por-cliente.establecimientos.mapa');

					Route::get('exportar-establecimientos/{sClienteProyectoId?}', 'ClienteProyectos\EstablecimientosController@exportExcel')->name('configuracion.proyectos-por-cliente.establecimientos.exportExcel');

					// Rutas para agregar un nuevo establecimiento al proyecto por idClienteProyecto
					Route::get('agregar-establecimiento/{sClienteProyectoId?}', 'ClienteProyectos\EstablecimientosController@create')->name('configuracion.proyectos-por-cliente.establecimientos.crear');
					Route::post('agregar-establecimiento', 'ClienteProyectos\EstablecimientosController@store')->name('configuracion.proyectos-por-cliente.establecimientos.guardar');

					// Rutas para editar un establecimiento del proyecto por idClienteProyecto
					Route::get('editar-establecimiento/{sClienteProyectoId?}/{sId?}', 'ClienteProyectos\EstablecimientosController@edit')->name('configuracion.proyectos-por-cliente.establecimientos.editar');
					Route::match(['PUT', 'PATCH'], 'editar-establecimiento/{sClienteProyectoId?}/{sId?}', 'ClienteProyectos\EstablecimientosController@update')->name('configuracion.proyectos-por-cliente.establecimientos.actualizar');

					// Ruta para ver el detalle del establecimiento por proyecto del cliente
					Route::get('detalle-establecimiento/{sClienteProyectoId?}/{sId?}', 'ClienteProyectos\EstablecimientosController@show')->name('configuracion.proyectos-por-cliente.establecimientos.detalle');

					// Ruta para eliminar un establecimiento del proyecto por idClienteProyecto
					Route::delete('quitar-establecimiento/{sClienteProyectoId?}/{sId?}', 'ClienteProyectos\EstablecimientosController@destroy')->name('configuracion.proyectos-por-cliente.establecimientos.quitar');

					// Rutas para asignar un establecimiento de la base general al proyecto seleccionado
					Route::match(['GET', 'POST'],'seleccionar-establecimiento/{sClienteProyectoId?}', 'ClienteProyectos\EstablecimientosController@assignList')->name('configuracion.proyectos-por-cliente.establecimientos.lista.asignar');
					Route::get('asignar-establecimiento/{sClienteProyectoId?}/{sEstablecimientoId?}', 'ClienteProyectos\EstablecimientosController@assign')->name('configuracion.proyectos-por-cliente.establecimientos.asignar');
                });
			});
		});

		/**********************************************************************************
    
	    	Grupo de rutas para la administracion de los catalogos del sistema
	    
	    ***********************************************************************************/
		Route::group(['prefix' => 'catalogos'], function(){
			
			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de provincias
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'provincias'], function(){
				// Rutas para ver la lista de provincias
				Route::match(['get','post'],'', 'ProvinciasController@index');
				Route::match(['get','post'],'lista-de-provincias', 'ProvinciasController@index')->name('catalogos.provincias.list');

				// Rutas para agregar una nueva provincia
				Route::get('agregar-provincia', 'ProvinciasController@create')->name('catalogos.provincias.create');
				Route::post('agregar-provincia', 'ProvinciasController@store')->name('catalogos.provincias.store');

				// Rutas para editar una provincia
				Route::get('editar-provincia/{iId?}', 'ProvinciasController@edit')->name('catalogos.provincias.edit');
				Route::post('editar-provincia/{iId?}', 'ProvinciasController@update')->name('catalogos.provincias.update');

				// Ruta para eliminar una provincia
				Route::get('eliminar-provincia/{iId?}', 'ProvinciasController@destroy')->name('catalogos.provincias.destroy');
			});


			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de ciudades
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'ciudades'], function(){
				// Rutas para ver la lista de ciudades
				Route::match(['get','post'],'', 'CiudadesController@index');
				Route::match(['get','post'],'lista-de-ciudades/{iProvinciaId?}', 'CiudadesController@index')->name('catalogos.ciudades.list');

				// Rutas para agregar una nueva ciudad
				Route::get('agregar-ciudad/{iProvinciaId?}', 'CiudadesController@create')->name('catalogos.ciudades.create');
				Route::post('agregar-ciudad/{iProvinciaId?}', 'CiudadesController@store')->name('catalogos.ciudades.store');

				// Rutas para editar una ciudad
				Route::get('editar-ciudad/{iId?}', 'CiudadesController@edit')->name('catalogos.ciudades.edit');
				Route::post('editar-ciudad/{iId?}', 'CiudadesController@update')->name('catalogos.ciudades.update');

				// Ruta para eliminar una ciudad
				Route::get('eliminar-ciudad/{iId?}', 'CiudadesController@destroy')->name('catalogos.ciudades.destroy');
			});

			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de zonas
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'zonas'], function(){
				// Rutas para ver la lista de zonas
				Route::match(['get','post'],'', 'ZonasController@index');
				Route::match(['get','post'],'lista-de-zonas/{ciudadId?}', 'ZonasController@index')->name('catalogos.zonas.list');

				// Rutas para agregar una nueva zona
				Route::get('agregar-zona/{ciudadId?}', 'ZonasController@create')->name('catalogos.zonas.create');
				Route::post('agregar-zona/{ciudadId?}', 'ZonasController@store')->name('catalogos.zonas.store');

				// Rutas para editar una zona
				Route::get('editar-zona/{iId?}', 'ZonasController@edit')->name('catalogos.zonas.edit');
				Route::post('editar-zona/{iId?}', 'ZonasController@update')->name('catalogos.zonas.update');

				// Ruta para eliminar una zona
				Route::get('eliminar-zona/{iId?}', 'ZonasController@destroy')->name('catalogos.zonas.destroy');
			});

			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de parroquias
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'parroquias'], function(){
				// Rutas para ver la lista de parroquias
				Route::match(['get','post'],'', 'ParroquiasController@index');
				Route::match(['get','post'],'lista-de-parroquias/{zonaId?}', 'ParroquiasController@index')->name('catalogos.parroquias.list');

				// Rutas para agregar un nuevo parroquia
				Route::get('agregar-parroquia/{zonaId?}', 'ParroquiasController@create')->name('catalogos.parroquias.create');
				Route::post('agregar-parroquia/{zonaId?}', 'ParroquiasController@store')->name('catalogos.parroquias.store');

				// Rutas para editar un parroquia
				Route::get('editar-parroquia/{iId?}', 'ParroquiasController@edit')->name('catalogos.parroquias.edit');
				Route::post('editar-parroquia/{iId?}', 'ParroquiasController@update')->name('catalogos.parroquias.update');

				// Ruta para eliminar un parroquia
				Route::get('eliminar-parroquia/{iId?}', 'ParroquiasController@destroy')->name('catalogos.parroquias.destroy');
			});


			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de barrios
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'barrios'], function(){
				// Rutas para ver la lista de barrios
				Route::match(['get','post'],'', 'BarriosController@index');
				Route::match(['get','post'],'lista-de-barrios/{sectorId?}', 'BarriosController@index')->name('catalogos.barrios.list');

				// Rutas para agregar un nuevo barrio
				Route::get('agregar-barrio/{sectorId?}', 'BarriosController@create')->name('catalogos.barrios.create');
				Route::post('agregar-barrio/{sectorId?}', 'BarriosController@store')->name('catalogos.barrios.store');

				// Rutas para editar un barrio
				Route::get('editar-barrio/{iId?}', 'BarriosController@edit')->name('catalogos.barrios.edit');
				Route::post('editar-barrio/{iId?}', 'BarriosController@update')->name('catalogos.barrios.update');

				// Ruta para eliminar un barrio
				Route::get('eliminar-barrio/{iId?}', 'BarriosController@destroy')->name('catalogos.barrios.destroy');
			});


			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de cadenas
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'cadenas'], function(){
				// Rutas para ver la lista de cadenas
				Route::match(['get','post'],'', 'CadenasController@index');
				Route::match(['get','post'],'lista-de-cadenas', 'CadenasController@index')->name('catalogos.cadenas.list');

				// Rutas para agregar una nueva cadena
				Route::get('agregar-cadena', 'CadenasController@create')->name('catalogos.cadenas.create');
				Route::post('agregar-cadena', 'CadenasController@store')->name('catalogos.cadenas.store');

				// Rutas para editar una cadena
				Route::get('editar-cadena/{iId?}', 'CadenasController@edit')->name('catalogos.cadenas.edit');
				Route::post('editar-cadena/{iId?}', 'CadenasController@update')->name('catalogos.cadenas.update');

				// Ruta para eliminar una cadena
				Route::get('eliminar-cadena/{iId?}', 'CadenasController@destroy')->name('catalogos.cadenas.destroy');
			});


			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de Canales
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'canales'], function(){
				// Rutas para ver la lista de canales
				Route::match(['get','post'],'', 'CanalesController@index');
				Route::match(['get','post'],'lista-de-canales', 'CanalesController@index')->name('catalogos.canales.list');

				// Rutas para agregar un nuevo canal
				Route::get('agregar-canal', 'CanalesController@create')->name('catalogos.canales.create');
				Route::post('agregar-canal', 'CanalesController@store')->name('catalogos.canales.store');

				// Rutas para editar un canal
				Route::get('editar-canal/{iId?}', 'CanalesController@edit')->name('catalogos.canales.edit');
				Route::post('editar-canal/{iId?}', 'CanalesController@update')->name('catalogos.canales.update');

				// Ruta para eliminar un canal
				Route::get('eliminar-canal/{iId?}', 'CanalesController@destroy')->name('catalogos.canales.destroy');
			});

			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de Subcanales
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'subcanales'], function(){
				// Rutas para ver la lista de subcanales
				Route::match(['get','post'],'', 'SubcanalesController@index');
				Route::match(['get','post'],'lista-de-subcanales/{canalId?}', 'SubcanalesController@index')->name('catalogos.subcanales.list');

				// Rutas para agregar un nuevo subcanal
				Route::get('agregar-subcanal/{canalId?}', 'SubcanalesController@create')->name('catalogos.subcanales.create');
				Route::post('agregar-subcanal/{canalId?}', 'SubcanalesController@store')->name('catalogos.subcanales.store');

				// Rutas para editar un subcanal
				Route::get('editar-subcanal/{iId?}', 'SubcanalesController@edit')->name('catalogos.subcanales.edit');
				Route::post('editar-subcanal/{iId?}', 'SubcanalesController@update')->name('catalogos.subcanales.update');

				// Ruta para eliminar un subcanal
				Route::get('eliminar-subcanal/{iId?}', 'SubcanalesController@destroy')->name('catalogos.subcanales.destroy');
			});

			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de Tipos de Negocio
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'tipos-de-negocio'], function(){
				// Rutas para ver la lista de tipos de negocio
				Route::match(['get','post'],'', 'TiposNegocioController@index');
				Route::match(['get','post'],'lista-de-tipos-de-negocio/{subcanalId?}', 'TiposNegocioController@index')->name('catalogos.tipos-de-negocio.list');

				// Rutas para agregar un nuevo tipo de negocio
				Route::get('agregar-tipo-de-negocio/{subcanalId?}', 'TiposNegocioController@create')->name('catalogos.tipos-de-negocio.create');
				Route::post('agregar-tipo-de-negocio/{subcanalId?}', 'TiposNegocioController@store')->name('catalogos.tipos-de-negocio.store');

				// Rutas para editar un tipo de negocio
				Route::get('editar-tipo-de-negocio/{iId?}', 'TiposNegocioController@edit')->name('catalogos.tipos-de-negocio.edit');
				Route::post('editar-tipo-de-negocio/{iId?}', 'TiposNegocioController@update')->name('catalogos.tipos-de-negocio.update');

				// Ruta para eliminar un tipo de negocio
				Route::get('eliminar-tipo-de-negocio/{iId?}', 'TiposNegocioController@destroy')->name('catalogos.tipos-de-negocio.destroy');
			});


			/**********************************************************************************
    
		    	Grupo de rutas para la administracion de Establecimientos
		    
		    ***********************************************************************************/
			Route::group(['prefix' => 'establecimientos'], function(){
				// Rutas para ver la lista de establecimientos
				Route::match(['get','post'],'', 'EstablecimientosController@index');
				Route::match(['get','post'],'lista-de-establecimientos', 'EstablecimientosController@index')->name('catalogos.establecimientos.list');

				// Rutas para agregar un nuevo establecimiento
				Route::get('agregar-establecimiento', 'EstablecimientosController@create')->name('catalogos.establecimientos.create');
				Route::post('agregar-establecimiento', 'EstablecimientosController@store')->name('catalogos.establecimientos.store');

				// Rutas para editar un establecimiento
				Route::get('editar-establecimiento/{iId?}', 'EstablecimientosController@edit')->name('catalogos.establecimientos.edit');
				Route::post('editar-establecimiento/{iId?}', 'EstablecimientosController@update')->name('catalogos.establecimientos.update');

				// Rutas para importar una lista de establecimientos
				Route::get('importar-establecimientos', 'EstablecimientosController@load')->name('catalogos.establecimientos.load');
				Route::post('importar-establecimientos', 'EstablecimientosController@save')->name('catalogos.establecimientos.save');

				// Ruta para ver el detalle de un establecimiento
				Route::get('detalle-de-establecimiento/{iId?}', 'EstablecimientosController@show')->name('catalogos.establecimientos.show');

				// Ruta para cambiar el estado de un establecimiento
				Route::get('cambiar-estado-establecimiento/{iId?}', 'EstablecimientosController@status')->name('catalogos.establecimientos.change-status');

				// Ruta para eliminar un establecimiento
				Route::get('eliminar-establecimiento/{iId?}', 'EstablecimientosController@destroy')->name('catalogos.establecimientos.destroy');
			});

		});
	});


	/************************************************************************************************
		    
    	Grupo de rutas para navegacion del cliente en el sistema 
    
    *************************************************************************************************/
	Route::group(['middleware' => 'estandar'], function(){
		
		Route::group(['prefix' => '/clientes'], function(){
			/************************************************************************************************
		    
		    	Grupo de rutas para navegacion del cliente en los proyectos que tiene asignados en el sistema 
		    
		    *************************************************************************************************/
			Route::group(['prefix' => '/proyectos'], function(){
				// Ruta para mostrar la lista de proyectos asignados al cliente que pertenece al usuario
				Route::get('/', 'Clientes\ProyectosController@index')
				     ->name('clientes.proyectos.list');

				// Ruta para ver el detalle del Proyecto con el cual se esta trabajando para el cliente seleccionado.
				Route::get('/detalle/{iIdProyectoCliente?}', 'Clientes\ProyectosController@show')
				      ->name('clientes.proyectos.show');

				/************************************************************************************************
		    
			    	Grupo de rutas para navegacion del cliente en los PDV que se visitan en este proyecto 
			    
			    *************************************************************************************************/
				Route::group(['prefix' => '/establecimientos'], function(){
					// Ruta para ver mapa de establecimientos
					Route::match(['GET', 'POST'], '/mapa/{sClienteProyectoId?}', 'Clientes\EstablecimientosController@mapa')
					     ->name('clientes.proyectos.establecimientos.mapa');

					// Ruta para exportar la lista de establecimientos a un archivo excel
					Route::get('/exportar/{sClienteProyectoId?}', 'Clientes\EstablecimientosController@exportExcel')
					     ->name('clientes.proyectos.establecimientos.exportar');

					// Ruta para ver el detalle de un establecimiento
					Route::get('/detalle/{sClienteProyectoId?}/{sId?}', 'Clientes\EstablecimientosController@show')
					     ->name('clientes.proyectos.establecimientos.detalle');
				});
				/************************************************************************************************
		    
			    	Ruta para ver el detalle del equipo de trabajo que se encuentra involucrado en este proyecto
			    
			    *************************************************************************************************/
				Route::get('/equipo-trabajo/{sClienteProyectoId?}', 'Clientes\EquiposTrabajoController@index')
					 ->name('clientes.proyectos.equipo-trabajo');
				
				/************************************************************************************************
		    
			    	Rutas para mostrar los reportes del sistema
			    
				*************************************************************************************************/
				// Ruta para mostrar reporte de productividad
				Route::match(['GET', 'POST'], 'reporte-productividad/{sClienteProyectoId?}', 'Clientes\ReportesController@productividad')
					 ->name('clientes.proyectos.reporte-productividad');
					 
				// Ruta para mostrar reporte de visibilidad
				Route::match(['GET', 'POST'], 'reporte-visibilidad/{sClienteProyectoId?}', 'Clientes\ReportesController@visibilidad')
					 ->name('clientes.proyectos.reporte-visibilidad');
					 Route::post('exportar-reporte-visibilidad/{sClienteProyectoId?}', 'Clientes\ReportesController@exportarVisibilidad')
					 ->name('clientes.proyectos.exportar-reporte-visibilidad');
					 
				// Ruta para mostrar reporte de timing
				Route::match(['GET', 'POST'], 'reporte-timig/{sClienteProyectoId?}', 'Clientes\ReportesController@timing')
					 ->name('clientes.proyectos.reporte-timing');
					 
				// Ruta para mostrar reporte de rendimiento de rutas
				Route::match(['GET', 'POST'], 'reporte-rendimiento-rutas/{sClienteProyectoId?}', 'Clientes\ReportesController@rendimientoRutas')
				     ->name('clientes.proyectos.reporte-rendimiento-rutas');


				// Ruta para mostrar reporte de faltantes de stock
				Route::match(['GET', 'POST'], 'reporte-faltantes-stock/{sClienteProyectoId?}', 'Clientes\ReportesController@faltantesSock')
				     ->name('clientes.proyectos.reporte-faltantes-stock');
				
			});

			/************************************************************************************************
		    
		    	Ruta para que el cliente pueda administrar sus usuarios.
		    
		    *************************************************************************************************/
			Route::group(['prefix' => '/usuarios'], function(){
				// Ruta para mostrar la lista de proyectos asignados al cliente que pertenece al usuario
				Route::get('/', 'Clientes\UsuariosController@index')
				     ->name('clientes.usuarios.list');
			});
		});


	});
	

});

/*************************************************************************************************************

	Grupo de rutas del api para utilizacion en el backend del sistema

**************************************************************************************************************/

Route::group(['prefix' => '/api'], function(){

	/*************************************************************************************************************

		Grupo de rutas de la Version 1.0 del API del sistema

	**************************************************************************************************************/
	Route::group(['prefix' => '/v1'], function(){
		
		// Ruta para validar login desde el dispositivo movil
		Route::post('login', 'Api\LoginController@jwtLogin')->name('api.v1.login.validate');

		// Ruta para obtener la lista de proyectos por cliente
		Route::get('proyectos-por-cliente', 'Api\ClienteProyectosController@index')->name('api.v1.proyectos-cliente.list');
		


		/*************************************************************************************************************

			Grupo de rutas para administracion de catalogos del sistema desde el API

		**************************************************************************************************************/
		Route::group(['prefix' => 'catalogos'], function(){
			// Ruta para obtener la lista de catalogos
			Route::get('lista', 'Api\CatalogosController@listaCatalogos')->name('api.v1.catalogos.list');

			// Ruta para obtener la lista de provincias
			Route::get('lista-de-provincias', 'Api\CatalogosController@listaProvincias')->name('api.v1.catalogos.provincias.list');

			// Rutas para ver la lista de ciudades
			Route::get('lista-de-ciudades/{provinciaId?}', 'Api\CatalogosController@listaCiudades')->name('api.v1.catalogos.ciudades.list');

			// Rutas para ver la lista de zonas
			Route::get('lista-de-zonas/{ciudadId?}', 'Api\CatalogosController@listaZonas')->name('api.v1.catalogos.zonas.list');

			// Rutas para ver la lista de parroquias
			Route::get('lista-de-parroquias/{zonaId?}', 'Api\CatalogosController@listaParroquias')->name('api.v1.catalogos.parroquias.list');

			// Rutas para ver la lista de barrios
			Route::get('lista-de-barrios/{parroquiaId?}', 'Api\CatalogosController@listaBarrios')->name('api.v1.catalogos.barrios.list');

			// Rutas para ver la lista de cadenas
			Route::get('lista-de-cadenas', 'Api\CatalogosController@listaCadenas')->name('api.v1.catalogos.cadenas.list');

			// Rutas para ver la lista de Canales
			Route::get('lista-de-canales', 'Api\CatalogosController@listaCanales')->name('api.v1.catalogos.canales.list');

			// Rutas para ver la lista de Subcanales
			Route::get('lista-de-subcanales/{canalId?}', 'Api\CatalogosController@listaSubcanales')->name('api.v1.catalogos.subcanales.list');

			// Rutas para ver la lista de Tipos de Negocio
			Route::get('lista-de-tipos-de-negocio/{subcanalId?}', 'Api\CatalogosController@listaTiposNegocio')->name('api.v1.catalogos.tipos-de-negocio.list');

			// Ruta para obtener uno de los catalogos por ID
			Route::get('catalogo/{id?}', 'Api\CatalogosController@obtenerCatalogo')->name('api.v1.catalogos.get');

			// Ruta para obtener uno de los catalogos por Codigo y Contexto
			Route::get('catalogo-por-codigo/{contexto}/{codigo1?}', 'Api\CatalogosController@obtenerCatalogoPorCodigo')->name('api.v1.catalogos.get-by-code');

			// Ruta para agregar un nuevo catalogo
			Route::post('agregar-catalogo', 'Api\CatalogosController@guardarCatalogo')->name('api.v1.catalogos.store');

			// Ruta para actualizar un nuevo catalogo
			Route::post('actualizar-catalogo/{contexto?}/{codigoCatalogo?}', 'Api\CatalogosController@actualizarCatalogo')->name('api.v1.catalogos.update');

			// Ruta para eliminar un catalogo
			Route::get('eliminar-catalogo/{contexto?}/{codigoCatalogo?}', 'Api\CatalogosController@eliminarCatalogo')->name('api.v1.catalogos.destroy');
		});


		/*************************************************************************************************************

			Grupo de rutas para administracion de establecimientos

		**************************************************************************************************************/
		Route::group(['prefix' => 'establecimientos'], function(){
			// Ruta para obtener la lista de establecimientos del proyecto 
			Route::get('lista-de-establecimientos/{clienteProyectoId?}', 'Api\EstablecimientosController@index')->name('api.v1.establecimientos.list');

			// Ruta para obtener la lista de establecimientos del proyecto cargados por usuario 
			Route::get('lista-de-establecimientos-por-usuario/{clienteProyectoId?}', 'Api\EstablecimientosController@listByUser')->name('api.v1.establecimientos.list-by-user');

			// Ruta para agregar un nuevo establecimiento al sistema y asignarlo al proyecto
			Route::post('agregar-establecimiento', 'Api\EstablecimientosController@store')->name('api.v1.establecimientos.create');

			// Ruta para actualizar los datos de un establecimiento
			Route::match(['PUT', 'PATCH'],'actualizar-establecimiento/{sId?}', 'Api\EstablecimientosController@update')->name('api.v1.establecimientos.update');

			// Ruta para obtener el detalle de un establecimiento
			Route::get('ver-establecimiento/{clienteProyectoId?}/{sId?}', 'Api\EstablecimientosController@show')->name('api.v1.establecimientos.show');
		});


		/*************************************************************************************************************

			Grupo de rutas para administracion de rutas

		**************************************************************************************************************/
		Route::group(['prefix' => 'rutas'], function(){
			// Ruta para obtener las rutas del usuario
			Route::get('lista-de-rutas/{sClienteProyectoId?}', 'Api\RutasController@index')->name('api.v1.rutas.list');
			Route::get('lista-de-rutas-json/{sClienteProyectoId?}/{sEquipoTrabajoId?}', 'RutasController@listaJSON')->name('api.v1.rutas.listJSON');

			// Ruta para generar una hoja de ruta
			Route::post('generar-hoja-de-ruta', 'Api\RutasController@generateRoadmap')->name('api.v1.rutas.roadmap.generate');

			// Ruta para obtener la lista de hojas de ruta de un usuario
			Route::get('lista-hojas-de-ruta/{sClienteProyectoId?}', 'Api\RutasController@roadmapsList')->name('api.v1.rutas.roadmap.list');

			// Ruta para obtener el detalle de una hoja de ruta de un usuario
			Route::get('detalle-hoja-de-ruta/{sClienteProyectoId?}/{sHojaRutaId?}', 'Api\RutasController@roadmapDetail')->name('api.v1.rutas.roadmap.detail');

			// Ruta para actualizar un detalle de la hoja de ruta
			Route::match(['PUT', 'PATCH', 'POST'], 'actualizar-detalle/{sClienteProyectoId?}/{sDetalleRutaId?}', 'Api\RutasController@roadmapUpdate')->name('api.v1.rutas.roadmap.update');
		});



		/*************************************************************************************************************

			Grupo de rutas para administracion de novedades en la ruta

		**************************************************************************************************************/
		Route::group(['prefix' => 'novedades'], function(){
			// Ruta para ver la lista de novedades de un detalle de hoja de ruta
			Route::get('lista-de-novedades/{sClienteProyectoId?}/{sDetalleRutaId?}', 'Api\NovedadesController@novelties')->name('api.v1.rutas.novelties.list');

			// Ruta para agregar una nueva novedad
			Route::post('agregar-novedad', 'Api\NovedadesController@addNovelty')->name('api.v1.rutas.novelties.add');

			// Ruta para actualizar una novedad
			Route::match(['PUT', 'PATCH', 'POST'], 'actualizar-novedad/{sNovedadId?}', 'Api\NovedadesController@updateNovelty')->name('api.v1.rutas.novelties.update');

			// Ruta para ver la lista de fotos de una novedad
			Route::get('lista-de-fotos/{sNovedadId?}', 'Api\NovedadesController@photosList')->name('api.v1.novelties.photos.list');

			// Ruta para agregar una foto de una novedad
			Route::post('agregar-foto/{sNovedadId?}', 'Api\NovedadesController@addPhoto')->name('api.v1.novelties.photos.add');

			// Ruta para eliminar una foto de una novedad
			Route::match(['DELETE', 'GET'], 'eliminar-foto/{sNovedadId?}/{sFotoId?}', 'Api\NovedadesController@deletePhoto')->name('api.v1.novelties.photos.delete');
		});


		/*************************************************************************************************************

			Grupo de rutas para administracion de productos

		**************************************************************************************************************/
        Route::group(['prefix' => 'productos'], function(){
            
            // Ruta que devuelve la lista de grupos de producto
            Route::get('lista-de-grupos-de-productos/{sClienteProyectoId?}', 'Api\ProductosController@gruposProducto')->name('api.v1.products.groups');

            // Ruta que devuelve la lista de productos
            Route::get('lista-de-productos/{sClienteProyectoId?}/{sCadenaId?}/{iCompetencia}/{sGrupoProductoId?}/{sClasificacion?}', 'Api\ProductosController@index')->name('api.v1.products.list');

            // Ruta que devuelve un producto por su codigo de barras
            Route::get('detalle-de-producto/{sClienteProyectoId?}/{sProductoCode?}', 'Api\ProductosController@show')->name('api.v1.products.detail');
        });


        //Ruta para obtencion de datos de Must Stock List (MSL)
        Route::group(['prefix' => 'msl'], function(){
            
            Route::post('agregar-msl', 'Api\MSLController@addMSL')->name('api.v1.msl.add');

        });

        //Ruta para obtencion de datos de Precios
        Route::group(['prefix' => 'precios'], function(){
            
            Route::post('agregar-precio', 'Api\MSLController@addMSL')->name('api.v1.msl.add');

        });


        /*************************************************************************************************************

			Grupo de rutas para administracion de encuestas

		**************************************************************************************************************/
        route::group(['prefix' => '/encuestas'], function(){
			// Ruta que devuelve las encuestas activas generadas en el sistema
			/* /api/v1/encuestas/lista-de-encuestas  */
			Route::get('/lista-de-encuestas/{sClienteProyectoId?}/{sItemFiltroId?}/{sTipoFiltro?}', 'Api\EncuestasController@index')->name('api.v1.polls.list');

			// Ruta que devuelve las encuestas de supervisores activas generadas en el sistema
			/* /api/v1/encuestas/lista-de-encuestas  */
			Route::get('/lista-de-encuestas-supervisores/{sClienteProyectoId?}', 'Api\EncuestasController@supervisores')->name('api.v1.polls.supervisors');

			// Ruta que devuelve las preguntas de la encuesta seleccionada
			/* /api/v1/encuestas/preguntas  */
			Route::get('/preguntas/{sEncuestaId?}', 'Api\EncuestasController@questions')->name('api.v1.polls.questions');

			// Ruta para guardar la encuesta realizada
			/* /api/v1/encuestas/guardar-encuesta */
			Route::post('/guardar-encuesta', 'Api\EncuestasController@store')->name('api.v1.polls.store');


			/*************************************************************************************************************

				Grupo de rutas para administracion de fotos de la encuesta

			**************************************************************************************************************/
			route::group(['prefix' => '/fotos'], function(){
				// Ruta para ver las fotos de una encuesta realizada
				/* /api/v1/encuestas/fotos/lista */
				Route::get('/lista/{sClienteProyectoId?}/{sEncuestaId?}/{$sEstablecimientoId?}', 'Api\EncuestasController@listaFotos')->name('api.v1.polls.photos');

				// Ruta para agregar una foto a la encuesta realizada
				/* /api/v1/encuestas/fotos/guardar */
				Route::post('/guardar', 'Api\EncuestasController@addFoto')->name('api.v1.polls.add-photo');

				// Ruta para editar una foto de la encuesta realizada
				/* /api/v1/encuestas/fotos/editar */
				Route::post('/editar/{sEncuestaFotoID?}', 'Api\EncuestasController@editFoto')->name('api.v1.polls.edit-photo');

				// Ruta para eliminar una foto de la encuesta realizada
				/* /api/v1/encuestas/fotos/eliminar */
				Route::get('/eliminar/{sEncuestaFotoID?}', 'Api\EncuestasController@deleteFoto')->name('api.v1.polls.delete-photo');
			});
		});

	});

});
