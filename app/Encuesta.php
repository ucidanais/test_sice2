<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de encuestas
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class Encuesta extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'encuestas';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'tipo',  
            /*
                N -> Normal, 
                CE -> Clasificacion Establecimiento
            */
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'canal_id',
        'subcanal_id',
        'cadena_id',
        'provincia_id',
        'ciudad_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Encuesta __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // Encuesta __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // Encuesta __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // Encuesta __belongs_to__ Catalogo
    public function canal(){
        return $this->belongsTo('SICE\Catalogo', 'canal_id');
    }

    // Encuesta __belongs_to__ Catalogo
    public function subcanal(){
        return $this->belongsTo('SICE\Catalogo', 'subcanal_id');
    }

    // Encuesta __belongs_to__ Catalogo
    public function cadena(){
        return $this->belongsTo('SICE\Catalogo', 'cadena_id');
    }

    // Encuesta __belongs_to_many__ Catalogo
    public function cadenas(){
        return $this->belongsToMany('SICE\Catalogo', 'cadena_encuesta', 'encuesta_id', 'cadena_id');
    }

    // Encuesta __belongs_to__ Catalogo
    public function provincia(){
        return $this->belongsTo('SICE\Catalogo', 'provincia_id');
    }

    // Encuesta __belongs_to__ Catalogo
    public function ciudad(){
        return $this->belongsTo('SICE\Catalogo', 'ciudad_id');
    }

    // Encuesta __has_many__ Pregunta
    public function preguntas() {
        return $this->hasMany('SICE\Pregunta');
    }

    // Encuesta __has_many__ EncuestaFoto
    public function fotos() {
        return $this->hasMany('SICE\EncuestaFoto');
    }

    // Encuesta __has_many__ RespuestaEncuesta
    public function respuestaEncuestas() {
        return $this->hasMany('SICE\RespuestaEncuesta');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
        EJ:
        $aEncuestasNormales = Encuesta::normal()->byCliente($idCliente)->byProyecto($idProyecto)->get();
    **************************************************************************************************/
    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

    public function scopeNormal($sQuery){
        return $sQuery->whereTipo('N');
    }

    public function scopeClasificacionEstablecimientos($sQuery){
        return $sQuery->whereTipo('CE');
    }
}
