<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Establecimientos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
*/

class Establecimiento extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'establecimientos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'nombre',
        'direccion_manzana',
        'direccion_calle_principal',
        'direccion_numero',
        'direccion_transversal',
        'direccion_referencia',
        'administrador',
        'telefonos_contacto',
        'email_contacto',
        'foto',
        'geolocalizacion',
        'uuid_cadena',
        'codigo_cadena',
        'nombre_cadena',
        'uuid_canal',
        'codigo_canal',
        'nombre_canal',
        'uuid_subcanal',
        'codigo_subcanal',
        'nombre_subcanal',
        'uuid_tipo_negocio',
        'codigo_tipo_negocio',
        'nombre_tipo_negocio',
        'uuid_provincia',
        'codigo_provincia',
        'nombre_provincia',
        'uuid_ciudad',
        'codigo_ciudad',
        'nombre_ciudad',
        'uuid_zona',
        'codigo_zona',
        'nombre_zona',
        'uuid_parroquia',
        'codigo_parroquia',
        'nombre_parroquia',
        'uuid_barrio',
        'codigo_barrio',
        'nombre_barrio',
        'creado_por',
        'ultima_actualizacion',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/27
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Establecimiento __has_many__ ClasificacionEstablecimiento
    public function clasificacionesEstablecimiento() {
        return $this->hasMany('SICE\ClasificacionEstablecimiento', 'establecimiento_id', 'id');
    }

    // Establecimiento __has_many__ ClienteEstablecimiento
    public function clienteEstablecimientos() {
        return $this->hasMany('SICE\ClienteEstablecimiento', 'establecimiento_id', 'id');
    }

    // Establecimiento __has_many__ DetalleRuta
    public function detallesRuta() {
        return $this->hasMany('SICE\DetalleRuta', 'establecimiento_id', 'id');
    }

    // Establecimiento __has_many__ Novedad
    public function novedades() {
        return $this->hasMany('SICE\Novedad', 'establecimiento_id', 'id');
    }

    // Establecimiento __has_many__ InformacionDetalle
    public function informacionDetalles() {
        return $this->hasMany('SICE\InformacionDetalle', 'establecimiento_id', 'id');
    }

    // Establecimiento __has_many__ RespuestaEncuesta
    public function respuestaEncuestas() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'establecimiento_id', 'id');
    }

    // Establecimiento __belongs_to_many__ Ruta
    public function rutas() {
        return $this->belongsToMany('SICE\Ruta', 'establecimiento_ruta', 'establecimiento_id', 'ruta_id')->withPivot('orden_visita');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/27
        EJ:
        $aEstablecimientosActivos = Establecimiento::activos()->get();
    **************************************************************************************************/

    public function scopeActivos($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeInactivos($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeByCodigo($sQuery,$sCodigo){
        return $sQuery->whereCodigo($sCodigo);
    }

    public function scopeByNombre($sQuery,$sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    public function scopeByCodigoCadena($sQuery,$sCodigoCadena){
        return $sQuery->whereCodigoCadena($sCodigoCadena);
    }

    public function scopeByNombreCadena($sQuery,$sNombreCadena){
        return $sQuery->whereNombreCadena($sNombreCadena);
    }

    public function scopeByCodigoActividadConsumidor($sQuery,$sCodigoActividadConsumidor){
        return $sQuery->whereCodigoActividadConsumidor($sCodigoActividadConsumidor);
    }

    public function scopeByNombreActividadConsumidor($sQuery,$sNombreActividadConsumidor){
        return $sQuery->whereNombreActividadConsumidor($sNombreActividadConsumidor);
    }

    public function scopeByCodigoCanal($sQuery,$sCodigoCanal){
        return $sQuery->whereCodigoCanal($sCodigoCanal);
    }

    public function scopeByNombreCanal($sQuery,$sNombreCanal){
        return $sQuery->whereNombreCanal($sNombreCanal);
    }

    public function scopeByCodigoSubcanal($sQuery,$sCodigoSubcanal){
        return $sQuery->whereCodigoSubcanal($sCodigoSubcanal);
    }

    public function scopeByNombreSubcanal($sQuery,$sNombreSubcanal){
        return $sQuery->whereNombreSubcanal($sNombreSubcanal);
    }

    public function scopeByCodigoProvincia($sQuery,$sCodigoProvincia){
        return $sQuery->whereCodigoProvincia($sCodigoProvincia);
    }

    public function scopeByNombreProvincia($sQuery,$sNombreProvincia){
        return $sQuery->whereNombreProvincia($sNombreProvincia);
    }

    public function scopeByCodigoCiudad($sQuery,$sCodigoCiudad){
        return $sQuery->whereCodigoCiudad($sCodigoCiudad);
    }

    public function scopeByNombreCiudad($sQuery,$sNombreCiudad){
        return $sQuery->whereNombreCiudad($sNombreCiudad);
    }

    
    /**
    * Metodo que devuelve un objeto Establecimiento de acuerdo a su código
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/05/08
    *
    * @param string sCodigo
    * @return Establecimiento 
    */
    public static function byCodigo($sCodigo = ''){
        $oEstablecimiento = null;
        if($sCodigo){
            $oEstablecimiento = Establecimiento::byCodigo($sCodigo)->first();
        }
        return $oEstablecimiento;
    }


    /**
    * Metodo que busca establecimientos de acuerdo a una cadena buscada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/28
    *
    * @param string sValorBusqueda
    * @param integer iPaginate
    * @return Establecimiento[] 
    */
    public static function buscarEstablecimiento($sValorBusqueda = '', $iPaginate = 50){
        if($sValorBusqueda){
            return Establecimiento::where('id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('direccion_manzana','like','%'.$sValorBusqueda.'%')
                        ->orWhere('direccion_calle_principal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('direccion_transversal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('direccion_referencia','like','%'.$sValorBusqueda.'%')
                        ->paginate($iPaginate);
        }else{
            return Establecimiento::paginate($iPaginate);
        }
    }


    /**
    * Metodo que busca establecimientos que no se encuentran en un proyecto de un cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/07/19
    *
    * @param string sValorBusqueda
    * @param integer iPaginate
    * @return Establecimiento[] 
    */
    public static function buscarEstablecimientoSinClienteProyecto($sValorBusqueda = '', $iPaginate = 50){
        
        $listaEstablecimientos = Establecimiento::leftJoin('cliente_establecimientos', 'establecimientos.id', '=', 'cliente_establecimientos.establecimiento_id')
                                                ->select('establecimientos.*', 'cliente_establecimientos.cliente_proyecto_id');
        
        if($sValorBusqueda){
            $listaEstablecimientos = $listaEstablecimientos->where('establecimientos.id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_manzana','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_calle_principal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_transversal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_referencia','like','%'.$sValorBusqueda.'%');
        }
        $listaEstablecimientos = $listaEstablecimientos->orderBy('cliente_establecimientos.created_at', 'desc');
        $listaEstablecimientos = $listaEstablecimientos->paginate($iPaginate);
        return $listaEstablecimientos;
    }


    /**
    * Metodo que busca establecimientos de acuerdo a una cadena buscada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/28
    *
    * @param string sValorBusqueda
    * @param string sClienteProyectoId
    * @param integer iPaginate
    * @return Establecimiento[] 
    */
    public static function buscarEstablecimientoPorClienteProyecto($sValorBusqueda = '', $sClienteProyectoId = '', $iPaginate = null){
        
        $listaEstablecimientos = null;
        
        if( ! $sClienteProyectoId){
            return $listaEstablecimientos;
        }

        $oClienteProyecto = ClienteProyecto::find($sClienteProyectoId);

        if( ! is_object($oClienteProyecto)){
            return $listaEstablecimientos;   
        }
        
        $listaEstablecimientos = Establecimiento::join('cliente_establecimientos', 'establecimientos.id', '=', 'cliente_establecimientos.establecimiento_id')
                                                ->select('establecimientos.*', 'cliente_establecimientos.en_ruta', 'cliente_establecimientos.estado')
                                                ->where('cliente_establecimientos.cliente_proyecto_id', $oClienteProyecto->id);
        
        if($sValorBusqueda){
            $listaEstablecimientos = $listaEstablecimientos->where(function ($sQuery) use ($sValorBusqueda){
                $sQuery->where('establecimientos.id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_manzana','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_calle_principal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_transversal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_referencia','like','%'.$sValorBusqueda.'%');
            });
        }
        $numEstab = $listaEstablecimientos->count();
        if($iPaginate){
            $listaEstablecimientos = $listaEstablecimientos->paginate($iPaginate);
        }else{
            $listaEstablecimientos = $listaEstablecimientos->get();
        }
        $aResp = [
            'numEstab' => $numEstab,
            'establecimientos' => $listaEstablecimientos,
        ];
        return $aResp;
    }


    /**
    * Metodo que genera una lista de establecimientos de acuerdo a los filtros ingresados para mostrarla en el reporte
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/11/25
    *
    * @param array(string) aCadenas
    * @param array(string) aProvincias
    * @param array(string) aCiudades
    * @param array(string) aCanales
    * @param array(string) aSubcanales
    * @param array(string) aClienteProyecto
    * @param int iPaginate
    * @param string sValorBusqueda
    * @return array aResultado[] 
    */
    public static function buscarEstablecimientoReporte(
            $aCadenas = [],
            $aProvincias = [],
            $aCiudades = [],
            $aCanales = [],
            $aSubcanales = [],
            $aClienteProyecto = [],
            $iPaginate = null,
            $sValorBusqueda = null
        ){

        $listaEstablecimientos = Establecimiento::rightJoin('cliente_establecimientos', 'establecimientos.id', '=', 'cliente_establecimientos.establecimiento_id')
                                                ->select('establecimientos.*', 'cliente_establecimientos.en_ruta', 'cliente_establecimientos.estado', 'cliente_establecimientos.cliente_proyecto_id');
        
        if($sValorBusqueda){
            $listaEstablecimientos = $listaEstablecimientos->where(function ($sQuery) use ($sValorBusqueda){
                $sQuery->where('establecimientos.id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_cadena','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_canal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_subcanal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_tipo_negocio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_provincia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_ciudad','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_zona','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_parroquia','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.codigo_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.nombre_barrio','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_manzana','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_calle_principal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_transversal','like','%'.$sValorBusqueda.'%')
                        ->orWhere('establecimientos.direccion_referencia','like','%'.$sValorBusqueda.'%');
            });
        }
        
        if(count($aClienteProyecto)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aClienteProyecto){
                $ban = 0;
                foreach($aClienteProyecto as $key => $val){
                    if($ban == 0){
                        $sQuery->where('cliente_establecimientos.cliente_proyecto_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('cliente_establecimientos.cliente_proyecto_id','=',$val);
                    }
                }
            });
        }

        if(count($aCadenas)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aCadenas){
                $ban = 0;
                foreach($aCadenas as $key => $val){
                    if($ban == 0){
                        $sQuery->where('establecimientos.uuid_cadena','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('establecimientos.uuid_cadena','=',$val);
                    }
                }
            });
        }

        if(count($aProvincias)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aProvincias){
                $ban = 0;
                foreach($aProvincias as $key => $val){
                    if($ban == 0){
                        $sQuery->where('establecimientos.uuid_provincia','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('establecimientos.uuid_provincia','=',$val);
                    }
                }
            });
        }

        if(count($aCiudades)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aCiudades){
                $ban = 0;
                foreach($aCiudades as $key => $val){
                    if($ban == 0){
                        $sQuery->where('establecimientos.uuid_ciudad','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('establecimientos.uuid_ciudad','=',$val);
                    }
                }
            });
        }

        if(count($aCanales)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aCanales){
                $ban = 0;
                foreach($aCanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('establecimientos.uuid_canal','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('establecimientos.uuid_canal','=',$val);
                    }
                }
            });
        }

        if(count($aSubcanales)){
            $listaEstablecimientos = $listaEstablecimientos->where(function($sQuery) use($aSubcanales){
                $ban = 0;
                foreach($aSubcanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('establecimientos.uuid_subcanal','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('establecimientos.uuid_subcanal','=',$val);
                    }
                }
            });
        }

        $numEstab = $listaEstablecimientos->count();
        if($iPaginate){
            $listaEstablecimientos = $listaEstablecimientos->paginate($iPaginate);
        }else{
            $listaEstablecimientos = $listaEstablecimientos->get();
        }

        $aResp = [
            'numEstab' => $numEstab,
            'establecimientos' => $listaEstablecimientos,
        ];
        return $aResp;

    }


    /**
    * Metodo que genera un nuevo codigo para el establecimiento que se va a insertar en la BD
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/04/12
    *
    * @param string sCodigoProvincia
    * @param string sCodigoCiudad
    * @param string sCodigoZona
    * @param string sCodigoParroquia
    * @param string sCodigoBarrio
    * @param string sCodigoCanal
    * @param string sCodigoSubcanal
    * @param string sCodigoTipoNegocio
    * @param string sCodigoCadena
    * @return String sCodigo 
    */
    public static function generaCodigo(
            $sCodigoProvincia = '',
            $sCodigoCiudad = '',
            $sCodigoZona = '',
            $sCodigoParroquia = '',
            $sCodigoBarrio = '',
            $sCodigoCanal = '',
            $sCodigoSubcanal = '',
            $sCodigoTipoNegocio = '',
            $sCodigoCadena = ''
        ){

        // Se inicializa la variable
        $nuevoCodigo = '';
        // Obtenemos unicamante los secuenciales de los codigos
        $sCodigoCiudad = strlen($sCodigoCiudad) > 2 ? substr($sCodigoCiudad, 3, 2) : $sCodigoCiudad;
        $sCodigoZona = strlen($sCodigoZona) > 1 ? substr($sCodigoZona, 6, 1) : $sCodigoZona;
        $sCodigoParroquia = strlen($sCodigoParroquia) > 3 ? substr($sCodigoParroquia, 8, 3) : $sCodigoParroquia;
        $sCodigoBarrio = strlen($sCodigoBarrio) > 3 ? substr($sCodigoBarrio, 12, 3) : $sCodigoBarrio;
        $sCodigoSubcanal = strlen($sCodigoSubcanal) > 2 ? substr($sCodigoSubcanal, 2, 2) : $sCodigoSubcanal;
        $sCodigoTipoNegocio = strlen($sCodigoTipoNegocio) > 3 ? substr($sCodigoTipoNegocio, 5, 3) : $sCodigoTipoNegocio;

        // Obtenemos la lista de establecimientos
        $listaEstablecimientos = Establecimiento::get();
        $numCaracteres = 6;
        $codigoMax = 0; // En esta variable se almacenara el codigo maximo de la lista de catalogos
        $codigoSig = 1; // En esta variable se almacenara
        if(count($listaEstablecimientos)){
          foreach($listaEstablecimientos as $establecimiento){
            // A continuacion obtenemos unicamente el secuencial de cada establecimiento, quitando el resto del codigo para poder obtener el maximo
            // En la variable $numCaracteres seteamos el numero de caracteres que debe tener el secuencial de cada establecimiento
            $secuencial = substr($establecimiento->codigo, 29, 6);
            $secInt = (int)$secuencial;  // Una vez obtenido el maximo secuencial, guardamos el numero entero en otra variable para incrementarlo
            if($secInt > $codigoMax){
              $codigoMax = $secInt;
            }
          }
          // Incrementamos en 1 el secuencial del codigo del establecimiento
          $codigoSig += $codigoMax;
        }else{
          // A continuacion obtenemos unicamente el secuencial de cada establecimiento, quitando el resto del codigo para poder obtener el maximo
          // En la variable $numCaracteres seteamos el numero de caracteres que debe tener el secuencial de cada establecimiento
          $secuencial = 0;
          $secInt = (int)$secuencial;  // Una vez obtenido el maximo secuencial, guardamos el numero entero en otra variable para incrementarlo
          if($secInt > $codigoMax){
            $codigoMax = $secInt;
          }
          // Incrementamos en 1 el secuencial del codigo del establecimiento
          $codigoSig += $codigoMax;
        }
        // De acuerdo al numero de caracteres que tiene cada secuencial, llenamos con ceros (0) a la izquierda el secuencial para completar el numero de caracteres que debe tener el secuencial del establecimiento
        $nuevoSecuencial = str_pad($codigoSig, $numCaracteres, "0", STR_PAD_LEFT);
        // Generamos el codigo del establecimiento combinando los codigos de: provincia, ciudad, zona, parroquia, barrio, canal, subcanal, 
        // tipo de negocio, cadena y el secuencial generado para el establecimiento.
        $nuevoCodigo = $sCodigoProvincia."-".$sCodigoCiudad."-".$sCodigoZona."-".$sCodigoParroquia."-".$sCodigoBarrio."-".$sCodigoCanal."-".$sCodigoSubcanal."-".$sCodigoTipoNegocio."-".$sCodigoCadena."-".$nuevoSecuencial;
        
        return $nuevoCodigo; // retornamos el codigo nuevo generado
    }
}
