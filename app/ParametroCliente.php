<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de parametrizacion de Clientes del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
*/

class ParametroCliente extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'parametro_clientes';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'parametro',
        'valor_cadena',
        'valor_numero',
        'valor_fecha',
        'observaciones',
        'vigencia_inicio',
        'vigencia_fin',
        'cliente_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // ParametroCliente __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
        EJ:
        $aParametroPorCodigo = ParametroCliente::byCodigo('01')->get();
    **************************************************************************************************/

    public function scopeByCodigo($sQuery, $sCodigo){
        return $sQuery->whereCodigo($sCodigo);
    }

    public function scopeByParametro($sQuery, $sParam){
        return $sQuery->whereParametro($sParam);
    }
}
