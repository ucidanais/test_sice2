<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administración de Proyectos del sistema (Portafolio de servicios de la empresa)
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/27
* @Parametrizacion
* @Configuracion
*/

class Proyecto extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'proyectos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'nombre',
        'estado',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/27
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Proyecto __has_many__ ParametroProyecto
    public function parametrosProyecto() {
        return $this->hasMany('SICE\ParametroProyecto');
    }

	// Proyecto __has_many__ ClienteProyecto
    public function clientesProyecto() {
        return $this->hasMany('SICE\ClienteProyecto');
    }

    // Proyecto __has_many__ EquipoTrabajo
    public function equiposTrabajo() {
        return $this->hasMany('SICE\EquipoTrabajo');
    }

    // Proyecto __has_many__ Encuesta
    public function encuestas() {
        return $this->hasMany('SICE\Encuesta');
    }

    // Proyecto __has_many__ EncuestaFoto
    public function encuestaFotos() {
        return $this->hasMany('SICE\EncuestaFoto');
    }

    // Proyecto __has_many__ FamiliaProducto
    public function familiasProducto() {
        return $this->hasMany('SICE\FamiliaProducto');
    }

    // Proyecto __has_many__ CategoriaProducto
    public function categoriasProducto() {
        return $this->hasMany('SICE\CategoriaProducto');
    }

    // Proyecto __has_many__ PresentacionProducto
    public function presentacionesProducto() {
        return $this->hasMany('SICE\PresentacionProducto');
    }

    // Proyecto __has_many__ ProductoCadena
    public function productosCadena() {
        return $this->hasMany('SICE\ProductoCadena');
    }

    // Proyecto __has_many__ Establecimiento
    public function establecimientos() {
        return $this->hasMany('SICE\Establecimiento', 'proyecto_id');
    }

    // Proyecto __has_many__ ClasificacionEstablecimiento
    public function clasificacionesEstablecimiento() {
        return $this->hasMany('SICE\ClasificacionEstablecimiento');
    }

    // Proyecto __has_many__ ClienteEstablecimiento
    public function clienteEstablecimientos() {
        return $this->hasMany('SICE\ClienteEstablecimiento', 'proyecto_id');
    }

    // Proyecto __has_many__ Ruta
    public function rutas() {
        return $this->hasMany('SICE\Ruta', 'proyecto_id');
    }

    // Proyecto __has_many__ GrupoProducto
    public function gruposProducto() {
        return $this->hasMany('SICE\GrupoProducto');
    }

    // Proyecto __has_many__ HojaRuta
    public function hojasRuta() {
        return $this->hasMany('SICE\HojaRuta', 'proyecto_id');
    }

    // Proyecto __has_many__ DetalleRuta
    public function detallesRuta() {
        return $this->hasMany('SICE\DetalleRuta', 'proyecto_id');
    }

    // Proyecto __has_many__ Novedad
    public function novedades() {
        return $this->hasMany('SICE\Novedad', 'proyecto_id');
    }

    // Proyecto __has_many__ InformacionDetalle
    public function informacionDetalles() {
        return $this->hasMany('SICE\InformacionDetalle', 'proyecto_id');
    }    

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/27
        EJ:
        $proyectosActivos = Proyecto::activos()->get();
    **************************************************************************************************/
    public function scopeByCodigo($sQuery,$sCodigo){
        return $sQuery->whereCodigo($sCodigo);
    }

    public function scopeByNombre($sQuery,$sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    public function scopeActivos($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeInactivos($sQuery){
        return $sQuery->whereEstado(0);
    }

    /**
    * Metodo que busca proyectos de acuerdo a una cadena buscada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/01/10
    *
    * @param string sValorBusqueda
    * @param int iPaginado
    * @return Proyecto[] 
    */
    public static function buscarProyecto($sValorBusqueda = '', $iPaginado = 50){
        if($sValorBusqueda){
            return Proyecto::where('id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo','like','%'.$sValorBusqueda.'%')
                        ->paginate($iPaginado);
        }else{
            return Proyecto::paginate($iPaginado);
        }
    }
}
