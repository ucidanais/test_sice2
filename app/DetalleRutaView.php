<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;

/**
* Clase para consultas en la vista vw_detalle_rutas
* @Autor Raúl Chauvin
* @FechaCreacion  2018/10/14
* @Reportes
*/

class DetalleRutaView extends Model
{
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'vw_detalle_rutas';

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/
    public function scopeVisitados($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeNoVisitados($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeDesde($sQuery, $dFecha){
        return $sQuery->where('created_at', '>=', $dFecha);   
    }

    public function scopeHasta($sQuery, $dFecha){
        return $sQuery->where('created_at', '<=', $dFecha);
    }


    /**
    * Metodo que genera una lista de registros de la vista de acuerdo a los filtros ingresados para mostrarla en el reporte
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @param array(string) aCadenas
    * @param array(string) aProvincias
    * @param array(string) aCiudades
    * @param array(string) aCanales
    * @param array(string) aSubcanales
    * @param string sClienteProyecto
    * @param int iPaginate
    * @return array(DetalleRutaView) aResultado
    */
    public static function buscarDetalleRuta(
        $aCadenas = [],
        $aProvincias = [],
        $aCiudades = [],
        $aCanales = [],
        $aSubcanales = [],
        $sClienteProyecto = '',
        $iPaginate = null,
        $resp = 'num'
    ){
        $aResultado = null;

        if( ! $sClienteProyecto){
            return $aResultado;
        }

        $aResultado = DetalleRutaView::where('cliente_proyecto_id', $sClienteProyecto);

        if(count($aCadenas)){
            $aResultado = $aResultado->where(function($sQuery) use($aCadenas){
                $ban = 0;
                foreach($aCadenas as $key => $val){
                    if($ban == 0){
                        $sQuery->where('uuid_cadena','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('uuid_cadena','=',$val);
                    }
                }
            });
        }

        if(count($aProvincias)){
            $aResultado = $aResultado->where(function($sQuery) use($aProvincias){
                $ban = 0;
                foreach($aProvincias as $key => $val){
                    if($ban == 0){
                        $sQuery->where('uuid_provincia','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('uuid_provincia','=',$val);
                    }
                }
            });
        }

        if(count($aCiudades)){
            $aResultado = $aResultado->where(function($sQuery) use($aCiudades){
                $ban = 0;
                foreach($aCiudades as $key => $val){
                    if($ban == 0){
                        $sQuery->where('uuid_ciudad','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('uuid_ciudad','=',$val);
                    }
                }
            });
        }

        if(count($aCanales)){
            $aResultado = $aResultado->where(function($sQuery) use($aCanales){
                $ban = 0;
                foreach($aCanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('uuid_canal','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('uuid_canal','=',$val);
                    }
                }
            });
        }

        if(count($aSubcanales)){
            $aResultado = $aResultado->where(function($sQuery) use($aSubcanales){
                $ban = 0;
                foreach($aSubcanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('uuid_subcanal','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('uuid_subcanal','=',$val);
                    }
                }
            });
        }

        if($resp == 'num'){
            return $aResultado->count();
        }else{
            return $iPaginate ? $aResultado->paginate($iPaginate) : $aResultado->get();
        }

    }
}
