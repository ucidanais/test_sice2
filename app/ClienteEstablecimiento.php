<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Establecimientos por cliente del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2018/05/08
* @Parametrizacion
* @Configuracion
*/

class ClienteEstablecimiento extends Model
{
    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'cliente_establecimientos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'en_ruta',
        'estado',
        'establecimiento_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2018/05/08
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // ClienteEstablecimiento __belongs_to__ Establecimiento
    public function establecimiento() {
        return $this->belongsTo('SICE\Establecimiento', 'establecimiento_id');
    }

    // ClienteEstablecimiento __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto', 'cliente_proyecto_id');
    }

    // ClienteEstablecimiento __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente', 'cliente_id');
    }

    // ClienteEstablecimiento __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto', 'proyecto_id');
    }


    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2018/05/08
        EJ:
        $aEstablecimientosSinRuta = Establecimiento::activos()->sinRuta()->get();
    **************************************************************************************************/

    public function scopeActivos($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeInactivos($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeEnRuta($sQuery){
        return $sQuery->whereEnRuta(1);
    }

    public function scopeSinRuta($sQuery){
        return $sQuery->whereEnRuta(0);
    }
}
