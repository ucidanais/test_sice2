<?php

namespace SICE;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administración de usuarios del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/15
* @AutenticacionAutorizacion
*/
class Usuario extends Authenticatable
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'usuarios';

    /**
     * Atributos que pueden ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'password',
        'tipo',
        'avatar',
        'estado',
        'cliente_id',
    ];


    /**
     * Atributos excluidos de la forma JSON del modelo.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];


    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/15
    Metodos para construir relaciones en ORM
    ******************************************************************/

    // Usuario __belongs_to_many__ Permiso
    public function permisos() {
        return $this->belongsToMany('SICE\Permiso','permiso_usuario','usuario_id','permiso_id');
    }

    // Usuario __has_many__ EquipoTrabajo
    public function equiposTrabajo() {
        return $this->hasMany('SICE\EquipoTrabajo','usuario_id');
    }

    // Usuario __has_many__ EquipoTrabajo
    public function equiposTrabajoReporta() {
        return $this->hasMany('SICE\EquipoTrabajo','usuario_reporta_id');
    }

    // Usuario __has_many__ Ruta
    public function rutas() {
        return $this->hasMany('SICE\Ruta','usuario_id');
    }

    // Usuario __has_many__ RespuestaEncuesta
    public function respuestasEncuesta() {
        return $this->hasMany('SICE\RespuestaEncuesta','usuario_id');
    }


    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/15
        EJ:
        $aUsuariosActivos = Usuario::activos()->get();

        Ej de como obtener los usuarios activos de un cliente
        $aUsuariosEmpresa = Usuario::cliente($idCliente)->activos()->estandar()->get();
    **************************************************************************************************/

    public function scopeActivos($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeInactivos($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeSuperadministradores($sQuery){
        return $sQuery->whereTipo('S');
    }

    public function scopeAdministradores($sQuery){
        return $sQuery->whereTipo('A');
    }

    public function scopeBackOffice($sQuery){
        return $sQuery->whereTipo('B');
    }

    public function scopeEstandar($sQuery){
        return $sQuery->whereTipo('E');
    }

    public function scopeCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeSinReferencia($sQuery){
        return $sQuery->whereNull('cliente_id');
    }


    /**
     * Método que genera el hash de las claves antes de guardarlas en la BD.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/15
     *
     * @param string password
     *
     */
    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }


    /**
     * Metodo que verifica si un usuario es superadministrador.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/15
     *
     * @param int iId?
     * @return bool
     */
    public static function isSuperAdmin($iId = ''){
        $iId = !$iId ? \Auth::user()->id : $iId;
        return Usuario::find($iId)->tipo == 'S' ? TRUE : FALSE;
    }

    /**
     * Metodo que verifica si un usuario es Administrador.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/15
     *
     * @param int iId?
     * @return bool
     */
    public static function isAdmin($iId = ''){
        $iId = !$iId ? \Auth::user()->id : $iId;
        return Usuario::find($iId)->tipo == 'A' ? TRUE : FALSE;
    }

    /**
     * Metodo que verifica si un usuario es back office.
     * @Autor Raúl Chauvin
     * @FechaCreacion  2016/06/15
     *
     * @param int iId?
     * @return bool
     */
    public static function isBackOffice($iId = ''){
        $iId = !$iId ? \Auth::user()->id : $iId;
        return Usuario::find($iId)->tipo == 'B' ? TRUE : FALSE;
    }


    /**
    * Metodo que verifica los permisos de acceso hacia una determinada seccion del sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    * EJ:
    * $bTienePermiso = Usuario::verificaPermiso('backend/usuarios/agregarUsuario');
    * El método devuelve TRUE o FALSE dependiendo de los permisos que tenga el usuario
    *
    * @param string sPath
    * @param int iId
    * @return bool
    */
    public static function verificaPermiso($sPath = '', $iId = ''){
        $iId = !$iId ? \Auth::user()->id : $iId;
        if(Usuario::isSuperAdmin($iId)){
            return TRUE;
        }
        $aPermisos = Usuario::find($iId)->permisos;
        foreach($aPermisos as $aPermiso){
            if($aPermiso->path == $sPath){
                return TRUE;
            }
        }
        return FALSE;
    }


    /**
    * Metodo que devuelve un modelo Usuario encontrado por email o falso en caso de no encontrarlo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    *
    * @param string sEmail
    * @return Usuario
    */
    public static function byEmail($sEmail = ''){
        if($sEmail){
            return Usuario::whereEmail($sEmail)->first();
        }else{
            return FALSE;
        }
    }

    /**
    * Metodo que verifica si un usuario pertenece a un cliente activo en el sistema
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    *
    * @return bool 
    */
    public static function verificaCliente(){
        $iId = \Auth::user()->cliente_id ? \Auth::user()->cliente_id : null;
        if($iId){
            $oCliente = Cliente::find($iId);
            if($oCliente){
                return TRUE;
            }
        }
        return FALSE;
    }


    /**
    * Metodo que devuelve el objeto Cliente al que pertenece el usuario
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    *
    * @return Usuario 
    */
    public static function getCliente(){
        if(Usuario::verificaCliente()){
            $oCliente = Cliente::find(\Auth::user()->cliente_id);
            return $oCliente;
        }
        return null;
    }

    /**
    * Metodo que busca usuarios de acuerdo a una cadena buscada y el id del cliente
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    *
    * @param string sValorBusqueda
    * @param int iIdCliente
    * @return Usuario[] 
    */
    public static function buscarUsuario($sValorBusqueda = '', $iIdCliente = ''){
        if($sValorBusqueda){
            if($iIdCliente){
                return Usuario::where('cliente_id', $iIdCliente)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('nombre','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('apellido','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('email','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
            }else{
                return Usuario::where('id','like','%'.$sValorBusqueda.'%')
                            ->orWhere('nombre','like','%'.$sValorBusqueda.'%')
                            ->orWhere('apellido','like','%'.$sValorBusqueda.'%')
                            ->orWhere('email','like','%'.$sValorBusqueda.'%')
                            ->paginate(50);
            }
        }else{
            if($iIdCliente){
                return Usuario::where('cliente_id', $iIdCliente)->paginate(50);
            }else{
                return Usuario::whereNull('cliente_id')->paginate(50);
            }
        }
    }
}
