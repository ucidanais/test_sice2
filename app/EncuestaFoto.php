<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de fotos de las encuestas
* @Autor Raúl Chauvin
* @FechaCreacion  2019/04/12
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class EncuestaFoto extends Model
{
    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'encuesta_fotos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'foto',
        'descripcion',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'establecimiento_id',
        'canal_id',
        'subcanal_id',
        'cadena_id',
        'provincia_id',
        'ciudad_id',
        'encuesta_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];


    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // EncuestaFoto __belongs_to__ Encuesta
    public function encuesta() {
        return $this->belongsTo('SICE\Encuesta');
    }

    // EncuestaFoto __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // EncuestaFoto __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // EncuestaFoto __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // EncuestaFoto __belongs_to__ Catalogo
    public function canal(){
        return $this->belongsTo('SICE\Catalogo', 'canal_id');
    }

    // EncuestaFoto __belongs_to__ Catalogo
    public function subcanal(){
        return $this->belongsTo('SICE\Catalogo', 'subcanal_id');
    }

    // EncuestaFoto __belongs_to__ Catalogo
    public function cadena(){
        return $this->belongsTo('SICE\Catalogo', 'cadena_id');
    }

    // EncuestaFoto __belongs_to__ Catalogo
    public function provincia(){
        return $this->belongsTo('SICE\Catalogo', 'provincia_id');
    }

    // EncuestaFoto __belongs_to__ Catalogo
    public function ciudad(){
        return $this->belongsTo('SICE\Catalogo', 'ciudad_id');
    }

}
