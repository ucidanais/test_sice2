<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Clientes del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/27
* @Parametrizacion
* @Configuracion
*/

class Cliente extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'clientes';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'nombre',
        'logotipo',
        'estado',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/27
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Cliente __has_many__ ParametroCliente
    public function parametrosCliente() {
        return $this->hasMany('SICE\ParametroCliente', 'cliente_id');
    }

    // Cliente __has_many__ ClienteProyecto
    public function clientesProyecto() {
        return $this->hasMany('SICE\ClienteProyecto', 'cliente_id');
    }

    // Cliente __has_many__ EquipoTrabajo
    public function equiposTrabajo() {
        return $this->hasMany('SICE\EquipoTrabajo', 'cliente_id');
    }

    // Cliente __has_many__ Encuesta
    public function encuestas() {
        return $this->hasMany('SICE\Encuesta', 'cliente_id');
    }

    // Cliente __has_many__ EncuestaFoto
    public function encuestaFotos() {
        return $this->hasMany('SICE\EncuestaFoto', 'cliente_id');
    }

    // Cliente __has_many__ FamiliaProducto
    public function familiasProducto() {
        return $this->hasMany('SICE\FamiliaProducto', 'cliente_id');
    }

    // Cliente __has_many__ CategoriaProducto
    public function categoriasProducto() {
        return $this->hasMany('SICE\CategoriaProducto', 'cliente_id');
    }

    // Cliente __has_many__ PresentacionProducto
    public function presentacionesProducto() {
        return $this->hasMany('SICE\PresentacionProducto', 'cliente_id');
    }

    // Cliente __has_many__ ProductoCadena
    public function productosCadena() {
        return $this->hasMany('SICE\ProductoCadena', 'cliente_id');
    }

    // Cliente __has_many__ Establecimiento
    public function establecimientos() {
        return $this->hasMany('SICE\Establecimientos', 'cliente_id');
    }

    // Cliente __has_many__ ClasificacionEstablecimiento
    public function clasificacionesEstablecimiento() {
        return $this->hasMany('SICE\ClasificacionEstablecimiento', 'cliente_id');
    }

    // Cliente __has_many__ ClienteEstablecimiento
    public function clienteEstablecimientos() {
        return $this->hasMany('SICE\ClienteEstablecimiento', 'cliente_id');
    }

    // Cliente __has_many__ Ruta
    public function rutas() {
        return $this->hasMany('SICE\Ruta', 'cliente_id');
    }

    // Cliente __has_many__ GrupoProducto
    public function gruposProducto() {
        return $this->hasMany('SICE\GrupoProducto', 'cliente_id');
    }

    // Cliente __has_many__ HojaRuta
    public function hojasRuta() {
        return $this->hasMany('SICE\HojaRuta', 'cliente_id');
    }

    // Cliente __has_many__ DetalleRuta
    public function detallesRuta() {
        return $this->hasMany('SICE\DetalleRuta', 'cliente_id');
    }

    // Cliente __has_many__ Novedad
    public function novedades() {
        return $this->hasMany('SICE\Novedad', 'cliente_id');
    }

    // Cliente __has_many__ InformacionDetalle
    public function informacionDetalles() {
        return $this->hasMany('SICE\InformacionDetalle', 'cliente_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/27
        EJ:
        $aClientesActivos = Usuario::activos()->get();
    **************************************************************************************************/

    public function scopeActivos($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeInactivos($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeByCodigo($sQuery,$sCodigo){
        return $sQuery->whereCodigo($sCodigo);
    }

    public function scopeByNombre($sQuery,$sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    /**
    * Metodo que busca clientes de acuerdo a una cadena buscada
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/27
    *
    * @param string sValorBusqueda
    * @param int iPaginado
    * @return Cliente[] 
    */
    public static function buscarCliente($sValorBusqueda = '', $iPaginado = 50){
        if($sValorBusqueda){
            return Cliente::where('id','like','%'.$sValorBusqueda.'%')
                        ->orWhere('nombre','like','%'.$sValorBusqueda.'%')
                        ->orWhere('codigo','like','%'.$sValorBusqueda.'%')
                        ->paginate($iPaginado);
        }else{
            return Cliente::paginate($iPaginado);
        }
    }
}
