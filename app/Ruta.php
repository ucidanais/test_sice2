<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de rutas por proyecto de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class Ruta extends Model
{

    use SoftDeletes, UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'rutas';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'lunes',
        'martes',
        'miercoles',
        'jueves',
        'viernes',
        'sabado',
        'domingo',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'equipo_trabajo_id',
        'usuario_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'deleted_at',
        'id',
    ];

    protected $dates = ['deleted_at'];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Ruta __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // Ruta __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // Ruta __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // Ruta __belongs_to__ EquipoTrabajo
    public function equipoTrabajo() {
        return $this->belongsTo('SICE\EquipoTrabajo','equipo_trabajo_id');
    }

    // Ruta __belongs_to__ Usuario
    public function usuario() {
        return $this->belongsTo('SICE\Usuario');
    }

    // Ruta __has_many__ HojaRuta
    public function hojasRuta() {
        return $this->hasMany('SICE\HojaRuta', 'ruta_id');
    }

    // Ruta __belongs_to_many__ Establecimiento
    public function establecimientos() {
        return $this->belongsToMany('SICE\Establecimiento', 'establecimiento_ruta', 'ruta_id', 'establecimiento_id')
                    ->withPivot('orden_visita');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
        EJ:
        $aRutasClienteProyecto = Ruta::byCliente($idCliente)->byProyecto($idProyecto)->byNombre($sNombre)->get();
    **************************************************************************************************/
    public function scopeByNombre($sQuery, $sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

    public function scopeLunes($sQuery){
        return $sQuery->whereLunes(1);
    }

    public function scopeMartes($sQuery){
        return $sQuery->whereMartes(1);
    }

    public function scopeMiercoles($sQuery){
        return $sQuery->whereMiercoles(1);
    }

    public function scopeJueves($sQuery){
        return $sQuery->whereJueves(1);
    }

    public function scopeViernes($sQuery){
        return $sQuery->whereViernes(1);
    }

    public function scopeSabado($sQuery){
        return $sQuery->whereSabado(1);
    }

    public function scopeDomingo($sQuery){
        return $sQuery->whereDomingo(1);
    }
}
