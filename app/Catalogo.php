<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administración de catalogos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/27
* @Parametrizacion
* @Configuracion
*/

class Catalogo extends Model
{
    
    use UuidModelTrait;

    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'catalogos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'contexto',
        'tipo_dato',
        'codigo1',
        'codigo2',
        'valor1',
        'valor2',
        'descripcion',
        'coordenadas_poligono',
        'catalogo_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/27
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Catalogo __belongs_to__ Catalogo
    public function catalogoPadre() {
        return $this->belongsTo('SICE\Catalogo','catalogo_id','id');
    }

    // Catalogo __has_many__ Catalogos
    public function catalogos() {
        return $this->hasMany('SICE\Catalogo', 'catalogo_id');
    }

    // Catalogo __has_many__ Novedades
    public function novedadesCanal() {
        return $this->hasMany('SICE\Novedad', 'canal_id');
    }

    // Catalogo __has_many__ Novedades
    public function novedadesSubcanal() {
        return $this->hasMany('SICE\Novedad', 'subcanal_id');
    }

    // Catalogo __has_many__ Novedades
    public function novedadesCadena() {
        return $this->hasMany('SICE\Novedad', 'cadena_id');
    }

    // Catalogo __has_many__ Novedades
    public function novedadesProvincia() {
        return $this->hasMany('SICE\Novedad', 'provincia_id');
    }

    // Catalogo __has_many__ Novedades
    public function novedadesCiudad() {
        return $this->hasMany('SICE\Novedad', 'ciudad_id');
    }

    // Catalogo __has_many__ Encuesta
    public function encuestaCanal() {
        return $this->hasMany('SICE\Encuesta', 'canal_id');
    }

    // Catalogo __has_many__ Encuesta
    public function encuestaSubcanal() {
        return $this->hasMany('SICE\Encuesta', 'subcanal_id');
    }

    // Catalogo __has_many__ Encuesta
    public function encuestaCadena() {
        return $this->hasMany('SICE\Encuesta', 'cadena_id');
    }

    // Catalogo __belongs_to_many__ Encuestas
    public function encuestasCadena(){
        return $this->belongsToMany('SICE\Encuesta', 'cadena_encuesta', 'cadena_id', 'encuesta_id');
    }

    // Catalogo __has_many__ Encuesta
    public function encuestaProvincia() {
        return $this->hasMany('SICE\Encuesta', 'provincia_id');
    }

    // Catalogo __has_many__ Encuesta
    public function encuestaCiudad() {
        return $this->hasMany('SICE\Encuesta', 'ciudad_id');
    }



    // Catalogo __has_many__ EncuestaFoto
    public function encuestaFotoCanal() {
        return $this->hasMany('SICE\EncuestaFoto', 'canal_id');
    }

    // Catalogo __has_many__ EncuestaFoto
    public function encuestaFotoSubcanal() {
        return $this->hasMany('SICE\EncuestaFoto', 'subcanal_id');
    }

    // Catalogo __has_many__ EncuestaFoto
    public function encuestaFotoCadena() {
        return $this->hasMany('SICE\EncuestaFoto', 'cadena_id');
    }

    // Catalogo __has_many__ EncuestaFoto
    public function encuestaFotoProvincia() {
        return $this->hasMany('SICE\EncuestaFoto', 'provincia_id');
    }

    // Catalogo __has_many__ EncuestaFoto
    public function encuestaFotoCiudad() {
        return $this->hasMany('SICE\EncuestaFoto', 'ciudad_id');
    }


    // Catalogo __has_many__ RespuestaEncuesta
    public function respuestaEncuestaCanal() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'canal_id');
    }

    // Catalogo __has_many__ RespuestaEncuesta
    public function respuestaEncuestaSubcanal() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'subcanal_id');
    }

    // Catalogo __has_many__ RespuestaEncuesta
    public function respuestaEncuestaCadena() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'cadena_id');
    }

    // Catalogo __has_many__ RespuestaEncuesta
    public function respuestaEncuestaProvincia() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'provincia_id');
    }

    // Catalogo __has_many__ RespuestaEncuesta
    public function respuestaEncuestaCiudad() {
        return $this->hasMany('SICE\RespuestaEncuesta', 'ciudad_id');
    }



    // Catalogo __has_many__ InformacionDetalle
    public function informacionDetallesCanal() {
        return $this->hasMany('SICE\InformacionDetalle', 'canal_id');
    }

    // Catalogo __has_many__ InformacionDetalle
    public function informacionDetallesSubcanal() {
        return $this->hasMany('SICE\InformacionDetalle', 'subcanal_id');
    }

    // Catalogo __has_many__ InformacionDetalle
    public function informacionDetallesCadena() {
        return $this->hasMany('SICE\InformacionDetalle', 'cadena_id');
    }

    // Catalogo __has_many__ InformacionDetalle
    public function informacionDetallesProvincia() {
        return $this->hasMany('SICE\InformacionDetalle', 'provincia_id');
    }

    // Catalogo __has_many__ InformacionDetalle
    public function informacionDetallesCiudad() {
        return $this->hasMany('SICE\InformacionDetalle', 'ciudad_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/27
        EJ:
        $catalogosContexto = Catalogo::byContexto('Ciudades')->get();
    **************************************************************************************************/
    public function scopeByContexto($sQuery,$sContexto){
        return $sQuery->where('contexto', $sContexto);
    }

    public function scopeByTipoDato($sQuery, $sTipoDato){
        return $sQuery->where('tipo_dato','=',$sTipoDato);
    }

    public function scopeByCodigo1($sQuery, $sCodigo1){
        return $sQuery->where('codigo1', $sCodigo1);
    }

    public function scopeByCodigo2($sQuery, $sCodigo2){
        return $sQuery->where('codigo2', $sCodigo2);
    }

    public function scopeByValor1($sQuery, $sValor1){
        return $sQuery->where('valor1', $sValor1);
    }

    public function scopeByValor2($sQuery, $sValor2){
        return $sQuery->where('valor2', $sValor2);
    }

    public function scopeByPadre($sQuery, $iIdPadre){
        return $sQuery->where('catalogo_id', $iIdPadre);
    }


    /**
    * Metodo que devuelve un catalogo de acuerdo al codigo ingresado.
    * @Autor Raúl Chauvin
    * @FechaCreacion  2017/03/25
    *
    * @param string sCodigo1
    * @return Catalogo 
    */
    public static function catalogoByCodigo($sCodigo1 = ''){
      $oCatalogo = null;
      if($sCodigo1){
        $oCatalogo = Catalogo::where('codigo1', $sCodigo1)->first();
      }
      return $oCatalogo;
    }

    /**
    * Metodo que busca catalogos de acuerdo a una cadena buscada y el contexto del catalogo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/08/04
    *
    * @param string sValorBusqueda
    * @param int iIdCliente
    * @return Catalogo[] 
    */
    public static function buscarCatalogo($sValorBusqueda = '', $sContexto = '', $iIdPadre = ''){
        if($sValorBusqueda){
            if($sContexto){
                if($iIdPadre){
                  return Catalogo::where('contexto', $sContexto)
                            ->where('catalogo_id', $iIdPadre)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('tipo_dato','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('codigo1','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('codigo2','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('valor1','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('valor2','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('descripcion','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
                }else{
                  return Catalogo::where('contexto', $sContexto)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('tipo_dato','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('codigo1','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('codigo2','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('valor1','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('valor2','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('descripcion','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
                }
            }else{
                return [];
            }
        }else{
            if($sContexto){
                if($iIdPadre){
                  return Catalogo::where('contexto', $sContexto)->where('catalogo_id', $iIdPadre)->paginate(50);
                }else{
                  return Catalogo::where('contexto', $sContexto)->paginate(50);
                }
            }else{
                return [];
            }
        }
    }


    /**
    * Metodo que genera un nuevo codigo para el catalogo que se va a insertar en la BD
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/12/19
    *
    * @param string sContexto
    * @param int iIdPadre
    * @return String sCodigo 
    */
    public static function generaCodigo($sContexto = '', $iIdPadre = ''){
      // Se inicializa la variable
      $nuevoCodigo = '';

      if($sContexto){  // Verificamos si esta llegando el parametro Contexto, de otra manera se devolvera el codigo en blanco
        // Obtenemos la lista de catalogos relacionados con el contexto 
        $listaCatalogos = Catalogo::where('contexto', $sContexto);
        if($iIdPadre){
          // Si llega el ID del catalogo padre, obtenemos unicamente los registros hijos de este catalogo
          $listaCatalogos = $listaCatalogos->where('catalogo_id', $iIdPadre);
        }
        $listaCatalogos = $listaCatalogos->get();
        $numCaracteres = 1;
        $codigoMax = 0; // En esta variable se almacenara el codigo maximo de la lista de catalogos
        $codigoSig = 1; // En esta variable se almacenara
        if(count($listaCatalogos)){
          foreach($listaCatalogos as $catalogo){
            // A continuacion obtenemos unicamente el secuencial de cada catalogo, quitando el resto del codigo para poder obtener el maximo
            // En la variable $numCaracteres seteamos el numero de caracteres que debe tener el secuencial de cada catalogo
            switch (strtolower($sContexto)) {
              case 'ciudades':
                $secuencial = substr($catalogo->codigo1, 3, 2);
                $numCaracteres = 2;
                break;
              case 'zonas':
                $secuencial = substr($catalogo->codigo1, 6, 1);
                $numCaracteres = 1;
                break;
              case 'parroquias':
                $secuencial = substr($catalogo->codigo1, 8, 3);
                $numCaracteres = 3;
                break;
              case 'barrios':
                $secuencial = substr($catalogo->codigo1, 12, 3);
                $numCaracteres = 3;
                break;
              case 'subcanales':
                $secuencial = substr($catalogo->codigo1, 2, 2);
                $numCaracteres = 2;
                break;
              case 'tipos de negocio':
                $secuencial = substr($catalogo->codigo1, 5, 3);
                $numCaracteres = 3;
                break;
              case 'cadenas':
                $secuencial = $catalogo->codigo1;
                $numCaracteres = 3;
                break;
              default:
                $secuencial = $catalogo->codigo1;
                $numCaracteres = 1;
                break;
            }
            $secInt = (int)$secuencial;  // Una vez obtenido el maximo secuencial, guardamos el numero entero en otra variable para incrementarlo
            if($secInt > $codigoMax){
              $codigoMax = $secInt;
            }
          }
          // Incrementamos en 1 el secuencial del codigo del catalogo
          $codigoSig += $codigoMax;
        }else{
          // A continuacion obtenemos unicamente el secuencial de cada catalogo, quitando el resto del codigo para poder obtener el maximo
          // En la variable $numCaracteres seteamos el numero de caracteres que debe tener el secuencial de cada catalogo
          $secuencial = 0;
          switch (strtolower($sContexto)) {
            case 'ciudades':
              $numCaracteres = 2;
              break;
            case 'zonas':
              $numCaracteres = 1;
              break;
            case 'parroquias':
              $numCaracteres = 3;
              break;
            case 'barrios':
              $numCaracteres = 3;
              break;
            case 'subcanales':
              $numCaracteres = 2;
              break;
            case 'tipos de negocio':
              $numCaracteres = 3;
              break;
            case 'cadenas':
              $numCaracteres = 3;
              break;
            default:
              $numCaracteres = 1;
              break;
          }
          $secInt = (int)$secuencial;  // Una vez obtenido el maximo secuencial, guardamos el numero entero en otra variable para incrementarlo
          if($secInt > $codigoMax){
            $codigoMax = $secInt;
          }
          // Incrementamos en 1 el secuencial del codigo del catalogo
          $codigoSig += $codigoMax;
        }
        // De acuerdo al numero de caracteres que tiene cada secuencial, llenamos con ceros (0) a la izquierda el secuencial para completar el numero de caracteres que debe tener el secuencial
        $nuevoCodigo = str_pad($codigoSig, $numCaracteres, "0", STR_PAD_LEFT);
        // Si el catalogo tiene un padre, obtenemos el padre y agregamos su codigo al codigo del secuencial generado para completar el codigo del catalogo
        if($iIdPadre){
          $objPadre = Catalogo::find($iIdPadre);
          if($objPadre){
            $nuevoCodigo = $objPadre->codigo1."-".$nuevoCodigo;
          }
        }
      }
      return $nuevoCodigo; // retornamos el codigo nuevo generado
    } 
}
