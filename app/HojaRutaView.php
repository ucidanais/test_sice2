<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;

/**
* Clase para consultas en la vista vw_hojas_ruta
* @Autor Raúl Chauvin
* @FechaCreacion  2018/11/26
* @Reportes
*/

class HojaRutaView extends Model
{
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'vw_hojas_ruta';

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/
    public function scopeVisitados($sQuery){
        return $sQuery->whereEstado(1);
    }

    public function scopeNoVisitados($sQuery){
        return $sQuery->whereEstado(0);
    }

    public function scopeDesde($sQuery, $dFecha){
        return $sQuery->where('created_at', '>=', $dFecha);   
    }

    public function scopeHasta($sQuery, $dFecha){
        return $sQuery->where('created_at', '<=', $dFecha);
    }

    public function scopeHasHoraEntrada($sQuery){
        return $sQuery->whereNotNull('hora_entrada');
    }

    public function scopeHasHoraSalida($sQuery){
        return $sQuery->whereNotNull('hora_salida');
    }
}