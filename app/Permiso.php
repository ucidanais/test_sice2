<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administración de permisos del sistema que luego serán asignados a los usuarios
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/15
* @AutenticacionAutorizacion
*/

class Permiso extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'permisos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'modulo_id',
        'nombre',
        'path'
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
	Autor Raúl Chauvin
    FechaCreacion  2016/06/15
    Metodos para construir relaciones en ORM
	******************************************************************/

	// Permiso __belongs_to_many__ Usuario
	public function usuarios() {
		return $this->belongsToMany('SICE\Usuario','permiso_usuario','permiso_id','usuario_id');
	}

	// Permiso __belongs_to__ Modulo
	public function modulo(){
		return $this->belongsTo('SICE\Modulo', 'modulo_id');
	}
}
