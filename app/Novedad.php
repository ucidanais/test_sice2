<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Novedades en Detalles de Hojas de Ruta en proyectos de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
* @Operacion
* @Gestion
* @Reportes
*/

class Novedad extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'novedades';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_novedad',
        'detalle',
        'producto',
        'competencia',
        'cliente',
        'cadena',
        'referente_a',
        'rotacion',
        'pvp',
        'precio_afiliado',
        'precio_no_afiliado',
        'fabricante',
        'distribuidor',
        'gestion',
        'establecimiento_id',
        'canal_id',
        'subcanal_id',
        'cadena_id',
        'provincia_id',
        'ciudad_id',
        'detalle_ruta_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/29
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Novedad __belongs_to__ DetalleRuta
    public function detalleRuta() {
        return $this->belongsTo('SICE\DetalleRuta', 'detalle_ruta_id');
    }

    // Novedad __belongs_to__ ClienteProyecto
    public function clienteProyecto(){
        return $this->belongsTo('SICE\ClienteProyecto', 'cliente_proyecto_id');
    }

    // Novedad __belongs_to__ Cliente
    public function objetoCliente(){
        return $this->belongsTo('SICE\Cliente', 'cliente_id');
    }

    // Novedad __belongs_to__ Proyecto
    public function proyecto(){
        return $this->belongsTo('SICE\Proyecto', 'proyecto_id');
    }

    // Novedad __belongs_to__ Establecimiento
    public function establecimiento(){
        return $this->belongsTo('SICE\Establecimiento', 'establecimiento_id');
    }

    // Novedad __belongs_to__ Catalogo
    public function canal(){
        return $this->belongsTo('SICE\Catalogo', 'canal_id');
    }

    // Novedad __belongs_to__ Catalogo
    public function subcanal(){
        return $this->belongsTo('SICE\Catalogo', 'subcanal_id');
    }

    // Novedad __belongs_to__ Catalogo
    public function cadenaObject(){
        return $this->belongsTo('SICE\Catalogo', 'cadena_id');
    }

    // Novedad __belongs_to__ Catalogo
    public function provincia(){
        return $this->belongsTo('SICE\Catalogo', 'provincia_id');
    }

    // Novedad __belongs_to__ Catalogo
    public function ciudad(){
        return $this->belongsTo('SICE\Catalogo', 'ciudad_id');
    }

    // Novedad __has_many__ Foto
    public function fotos() {
        return $this->hasMany('SICE\Foto', 'novedad_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/
    public function scopeByTipoNovedad($sQuery,$sTipoNovedad){
        return $sQuery->whereTipoNovedad($sTipoNovedad);
    }

    public function scopeVisibilidad($sQuery){
        return $sQuery->whereTipoNovedad('visibilidad');
    }

    public function scopeDesde($sQuery, $dFecha){
        return $sQuery->where('created_at', '>=', $dFecha);   
    }

    public function scopeHasta($sQuery, $dFecha){
        return $sQuery->where('created_at', '<=', $dFecha);
    }


    /**
    * Metodo que genera una lista de registros de la vista de acuerdo a los filtros ingresados para mostrarla en el reporte
    * @Autor Raúl Chauvin
    * @FechaCreacion  2018/10/14
    *
    * @param date $desde
    * @param date $hasta
    * @param array(string) $aCadenas
    * @param array(string) $aProvincias
    * @param array(string) $aCiudades
    * @param array(string) $aCanales
    * @param array(string) $aSubcanales
    * @param array(string) $aTipoNovedad
    * @param string $sClienteProyecto
    * @param int $iPaginate
    * @return array(Novedad) $aResultado
    */
    public static function buscarNovedades(
        $desde = '',
        $hasta = '',
        $aCadenas = [],
        $aProvincias = [],
        $aCiudades = [],
        $aCanales = [],
        $aSubcanales = [],
        $aTipoNovedad = [],
        $sClienteProyecto = '',
        $iPaginate = null
    ){
        $aResultado = null;

        if( ! $sClienteProyecto){
            return $aResultado;
        }

        $aResultado = Novedad::has('fotos')->where('cliente_proyecto_id', $sClienteProyecto)->desde($desde)->hasta($hasta);

        if(count($aCadenas)){
            $aResultado = $aResultado->where(function($sQuery) use($aCadenas){
                $ban = 0;
                foreach($aCadenas as $key => $val){
                    if($ban == 0){
                        $sQuery->where('cadena_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('cadena_id','=',$val);
                    }
                }
            });
        }

        if(count($aProvincias)){
            $aResultado = $aResultado->where(function($sQuery) use($aProvincias){
                $ban = 0;
                foreach($aProvincias as $key => $val){
                    if($ban == 0){
                        $sQuery->where('provincia_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('provincia_id','=',$val);
                    }
                }
            });
        }

        if(count($aCiudades)){
            $aResultado = $aResultado->where(function($sQuery) use($aCiudades){
                $ban = 0;
                foreach($aCiudades as $key => $val){
                    if($ban == 0){
                        $sQuery->where('ciudad_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('ciudad_id','=',$val);
                    }
                }
            });
        }

        if(count($aCanales)){
            $aResultado = $aResultado->where(function($sQuery) use($aCanales){
                $ban = 0;
                foreach($aCanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('canal_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('canal_id','=',$val);
                    }
                }
            });
        }

        if(count($aSubcanales)){
            $aResultado = $aResultado->where(function($sQuery) use($aSubcanales){
                $ban = 0;
                foreach($aSubcanales as $key => $val){
                    if($ban == 0){
                        $sQuery->where('subcanal_id','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('subcanal_id','=',$val);
                    }
                }
            });
        }

        if(count($aTipoNovedad)){
            $aResultado = $aResultado->where(function($sQuery) use($aTipoNovedad){
                $ban = 0;
                foreach($aTipoNovedad as $key => $val){
                    if($ban == 0){
                        $sQuery->where('tipo_novedad','=',$val);
                        $ban = 1;
                    }else{
                        $sQuery->orWhere('tipo_novedad','=',$val);
                    }
                }
            });
        }

        return $iPaginate ? $aResultado->orderBy('created_at', 'desc')->paginate($iPaginate) : $aResultado->orderBy('created_at', 'desc')->take(20)->get();

    }
}
