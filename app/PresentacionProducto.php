<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de presentaciones de productos
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class PresentacionProducto extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'presentacion_productos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'precio',
        'contexto',
        'es_competencia',
        'categoria_producto_id',
        'familia_producto_id',
        'presentacion_producto_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // PresentacionProducto __belongs_to__ CategoriaProducto
    public function categoriaProducto() {
        return $this->belongsTo('SICE\CategoriaProducto','categoria_producto_id');
    }

    // PresentacionProducto __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // PresentacionProducto __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // PresentacionProducto __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // PresentacionProducto __belongs_to__ PresentacionProducto
    public function presentacionProductoPadre() {
        return $this->belongsTo('SICE\PresentacionProducto','presentacion_producto_id');
    }

    // PresentacionProducto __has_many__ PresentacionProducto
    public function presentacionProductos() {
        return $this->hasMany('SICE\PresentacionProducto','presentacion_producto_id');
    }

    // PresentacionProducto __has_many__ ProductoCadena
    public function productosCadena() {
        return $this->hasMany('SICE\ProductoCadena','categoria_producto_id');
    }

    // PresentacionProducto __belongs_to_many__ GrupoProducto
    public function gruposProducto() {
        return $this->belongsToMany('SICE\GrupoProducto', 'grupo_presentacion', 'presentacion_id', 'grupo_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
    **************************************************************************************************/
    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

    public function scopeByContexto($sQuery, $sContexto){
        return $sQuery->whereContexto($sContexto);
    }

    public function scopeCompetencia($sQuery){
        return $sQuery->where('es_competencia', 1);
    }

    public function scopeMarca($sQuery){
        return $sQuery->where('es_competencia', 0);
    }

    /**
    * Metodo que busca presentaciones de producto de acuerdo a una cadena buscada y la categoria a la que pertenece
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/28
    *
    * @param string sValorBusqueda
    * @param int iIdCategoriaProducto
    * @param int iCompetencia
    * @return PresentacionProducto[] 
    */
    public static function buscarPresentacion($sValorBusqueda = '', $iIdCategoriaProducto = '', $iCompetencia = null){
        if($sValorBusqueda){
            if($iIdCategoriaProducto){
                if($iCompetencia == 1){
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)
                            ->where('es_competencia', 1)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('nombre','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
                }elseif($iCompetencia == 0){
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)
                            ->where('es_competencia', 0)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('nombre','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
                }else{
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)
                            ->where(function($sQuery) use($sValorBusqueda){
                                $sQuery->where('id','like','%'.$sValorBusqueda.'%')
                                      ->orWhere('nombre','like','%'.$sValorBusqueda.'%');
                            })
                            ->paginate(50);
                }
            }else{
                return [];
            }
        }else{
            if($iIdCategoriaProducto){
                if($iCompetencia == 1){
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)->where('es_competencia', 1)->paginate(50);
                }elseif($iCompetencia == 0){
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)->where('es_competencia', 0)->paginate(50);
                }else{
                    return PresentacionProducto::where('categoria_producto_id','=',$iIdCategoriaProducto)->paginate(50);
                }
            }else{
                return [];
            }
        }
    }
}
