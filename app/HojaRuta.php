<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Hojas de Ruta en proyectos de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
* @Operacion
* @Gestion
* @Reportes
*/

class HojaRuta extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'hojas_ruta';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'fecha',
        'visitas_por_hacer',
        'visitas_realizadas',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'equipo_trabajo_id',
        'usuario_id',
        'ruta_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/29
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // HojaRuta __belongs_to__ ClienteProyecto
    public function clienteProyecto(){
        return $this->belongsTo('SICE\ClienteProyecto', 'cliente_proyecto_id');
    }

    // HojaRuta __belongs_to__ Cliente
    public function cliente(){
        return $this->belongsTo('SICE\Cliente', 'cliente_id');
    }

    // HojaRuta __belongs_to__ Proyecto
    public function proyecto(){
        return $this->belongsTo('SICE\Proyecto', 'proyecto_id');
    }

    // HojaRuta __belongs_to__ EquipoTrabajo
    public function equipoTrabajo(){
        return $this->belongsTo('SICE\EquipoTrabajo', 'equipo_trabajo_id');
    }

    // HojaRuta __belongs_to__ Usuario
    public function usuario(){
        return $this->belongsTo('SICE\Usuario', 'usuario_id');
    }

    // HojaRuta __belongs_to__ Ruta
    public function ruta(){
        return $this->belongsTo('SICE\Ruta', 'ruta_id');
    }

    // HojaRuta __has_many__ DetalleRuta
    public function detallesRuta() {
        return $this->hasMany('SICE\DetalleRuta', 'hoja_ruta_id');
    }

    // HojaRuta __has_many_through__ Novedades
    public function novedades() {
        return $this->hasManyThrough('SICE\DetalleRuta','SICE\Novedad','hoja_ruta_id', 'detalle_ruta_id','id');
    }

    // HojaRuta __has_many_through__ InformacionDetalle
    public function informacionesDetalle() {
        return $this->hasManyThrough('SICE\DetalleRuta','SICE\InformacionDetalle','hoja_ruta_id', 'detalle_ruta_id','id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/29
    **************************************************************************************************/

    public function scopeByNombre($sQuery,$sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    public function scopeDesde($sQuery,$dFecha){
        return $sQuery->where('fecha', '>=', $dFecha);
    }

    public function scopeHasta($sQuery,$dFecha){
        return $sQuery->where('fecha', '<=', $dFecha);
    }
}
