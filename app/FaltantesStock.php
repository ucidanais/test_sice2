<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;

class FaltantesStock extends Model
{
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'faltantes_stocks';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'local_id',
        'user_id',
        'cliente_proyecto_id',
        'familia_producto_id',
        'categoria_producto_id',
        'producto_id',
        'ruta_id'

        ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at',
        'updated_at',
        'id',
    ];
}
