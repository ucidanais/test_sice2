<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de respuestas de las encuestas
* @Autor Raúl Chauvin
* @FechaCreacion  2019/03/31
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class RespuestaEncuesta extends Model
{
    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'respuesta_encuestas';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'valor',
        'valor_aux',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
        'establecimiento_id',
        'canal_id',
        'subcanal_id',
        'cadena_id',
        'provincia_id',
        'ciudad_id',
        'encuesta_id',
        'pregunta_id',
        'usuario_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2019/03/31
    Metodos para construir relaciones en ORM
    ******************************************************************/
    
    // RespuestaEncuesta __belongs_to__ Encuesta
    public function encuesta() {
        return $this->belongsTo('SICE\Encuesta', 'encuesta_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Pregunta
    public function pregunta() {
        return $this->belongsTo('SICE\Pregunta', 'pregunta_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto', 'cliente_proyecto_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente', 'cliente_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto', 'proyecto_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Establecimiento
    public function establecimiento() {
        return $this->belongsTo('SICE\Establecimiento', 'establecimiento_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Canal
    public function canal() {
        return $this->belongsTo('SICE\Catalogo', 'canal_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Subcanal
    public function subcanal() {
        return $this->belongsTo('SICE\Catalogo', 'subcanal_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Cadena
    public function cadena() {
        return $this->belongsTo('SICE\Catalogo', 'cadena_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Provincia
    public function provincia() {
        return $this->belongsTo('SICE\Catalogo', 'provincia_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Ciudad
    public function ciudad() {
        return $this->belongsTo('SICE\Catalogo', 'ciudad_id', 'id');
    }

    // RespuestaEncuesta __belongs_to__ Usuario
    public function usuario() {
        return $this->belongsTo('SICE\Usuario', 'usuario_id', 'id');
    }
}
