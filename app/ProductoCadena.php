<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de productos por cadena
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/

class ProductoCadena extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'producto_cadena';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'codigo_cadena',
        'nombre_cadena',
        'clasificacion_establecimiento',
        'presentacion_producto_id',
        'categoria_producto_id',
        'familia_producto_id',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // ProductoCadena __belongs_to__ FamiliaProducto
    public function familiaProducto() {
        return $this->belongsTo('SICE\FamiliaProducto','familia_producto_id');
    }

    // ProductoCadena __belongs_to__ CategoriaProducto
    public function categoriaProducto() {
        return $this->belongsTo('SICE\CategoriaProducto','categoria_producto_id');
    }

    // ProductoCadena __belongs_to__ PresentacionProducto
    public function presentacionProducto() {
        return $this->belongsTo('SICE\PresentacionProducto','presentacion_producto_id');
    }

    // ProductoCadena __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // ProductoCadena __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // ProductoCadena __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
    **************************************************************************************************/
    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }

    public function scopeByCodigoCadena($sQuery, $sCodigoCadena){
        return $sQuery->whereCodigoCadena($sCodigoCadena);
    }

    public function scopeByNombreCadena($sQuery, $sNombreCadena){
        return $sQuery->whereNombreCadena($sNombreCadena);
    }
}
