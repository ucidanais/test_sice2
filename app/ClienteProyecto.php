<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para gestion de Proyectos por Cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/28
* @Parametrizacion
* @Configuracion
*/

class ClienteProyecto extends Model
{

    use UuidModelTrait;
    
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'cliente_proyectos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'estado',
        'descripcion',
        'tomar_stock',
        'clasificaciones',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // ClienteProyecto __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente');
    }

    // ClienteProyecto __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto');
    }

    // ClienteProyecto __has_many__ EquipoTrabajo
    public function equiposTrabajo() {
        return $this->hasMany('SICE\EquipoTrabajo');
    }

    // ClienteProyecto __has_many__ Encuesta
    public function encuestas() {
        return $this->hasMany('SICE\Encuesta');
    }

    // ClienteProyecto __has_many__ EncuestaFoto
    public function encuestaFotos() {
        return $this->hasMany('SICE\EncuestaFoto');
    }

    // ClienteProyecto __has_many__ FamiliaProducto
    public function familiasProducto() {
        return $this->hasMany('SICE\FamiliaProducto','cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ CategoriaProducto
    public function categoriaProducto() {
        return $this->hasMany('SICE\CategoriaProducto','cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ PresentacionProducto
    public function presentacionProducto() {
        return $this->hasMany('SICE\PresentacionProducto','cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ ProductoCadena
    public function productoCadenas() {
        return $this->hasMany('SICE\ProductoCadena','cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ ClasificacionEstablecimiento
    public function clasificacionesEstablecimiento() {
        return $this->hasMany('SICE\ClasificacionEstablecimiento', 'cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ ClienteEstablecimiento
    public function clienteEstablecimientos() {
        return $this->hasMany('SICE\ClienteEstablecimiento', 'cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ Ruta
    public function rutas() {
        return $this->hasMany('SICE\Ruta');
    }

    // ClienteProyecto __has_many__ GrupoProducto
    public function gruposProducto() {
        return $this->hasMany('SICE\GrupoProducto');
    }

    // ClienteProyecto __has_many__ HojaRuta
    public function hojasRuta() {
        return $this->hasMany('SICE\HojaRuta', 'cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ DetalleRuta
    public function detalleRutas() {
        return $this->hasMany('SICE\DetalleRuta', 'cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ Novedad
    public function novedades() {
        return $this->hasMany('SICE\Novedad', 'cliente_proyecto_id');
    }

    // ClienteProyecto __has_many__ InformacionDetalle
    public function informacionDetalles() {
        return $this->hasMany('SICE\InformacionDetalle', 'cliente_proyecto_id');
    }
}
