<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administración de modulos del sistema
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/15
* @AutenticacionAutorizacion
*/
class Modulo extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'modulos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'tipo'
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/15
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // Modulo __has_many__ Permisos
    public function permisos() {
        return $this->hasMany('SICE\Permiso', 'modulo_id', 'id');
    }

    /**
    * Metodo que devuelve un modelo Modulo encontrado por nombre o falso en caso de no encontrarlo
    * @Autor Raúl Chauvin
    * @FechaCreacion  2016/06/15
    *
    * @param string sNombre
    * @return Modulo
    */
    public static function byNombre($sNombre = ''){
        if($sNombre){
            return Modulo::whereNombre($sNombre)->first();
        }else{
            return FALSE;
        }
    }


    /*************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/15
        EJ:
        $ModulosAdministracion = Modulo::admin()->get();
        $ModulosEstandar = Modulo::estandar()->get();
    **************************************************************************************************/
    public function scopeAdmin($sQuery){
        return $sQuery->whereTipo('A');
    }

    public function scopeEstandar($sQuery){
        return $sQuery->whereTipo('E');
    }
}
