<?php

namespace SICE;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;

/**
* Clase para administracion de grupos de productos por proyecto de cliente
* @Autor Raúl Chauvin
* @FechaCreacion  2016/06/29
* @Parametrizacion
* @Configuracion
* @Gestion
* @Reportes
*/


class GrupoProducto extends Model
{

    use UuidModelTrait;
    /**
     * Conexion utilizada en este modelo.
     *
     * @var string
     */
    protected $connection = 'mysql';
    
    /**
     * Tabla de la Base de Datos usada por el modelo.
     *
     * @var string
     */
    protected $table = 'grupo_productos';

    /**
     * Atributos que deben ser llenados del modelo.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'cliente_proyecto_id',
        'cliente_id',
        'proyecto_id',
    ];

    /**
     * Atributos generados automáticamente por el modelo.
     *
     * @var array
     */
    protected $guarded = [
        'created_at', 
        'updated_at', 
        'id',
    ];

    /*****************************************************************
    Autor Raúl Chauvin
    FechaCreacion  2016/06/28
    Metodos para construir relaciones en ORM
    ******************************************************************/
    // GrupoProducto __belongs_to__ ClienteProyecto
    public function clienteProyecto() {
        return $this->belongsTo('SICE\ClienteProyecto','cliente_proyecto_id');
    }

    // GrupoProducto __belongs_to__ Cliente
    public function cliente() {
        return $this->belongsTo('SICE\Cliente','cliente_id');
    }

    // GrupoProducto __belongs_to__ Proyecto
    public function proyecto() {
        return $this->belongsTo('SICE\Proyecto','proyecto_id');
    }

    // GrupoProducto __belongs_to_many__ PresentacionProducto
    public function presentacionesProducto() {
        return $this->belongsToMany('SICE\PresentacionProducto', 'grupo_presentacion', 'grupo_id', 'presentacion_id');
    }

    /****************************************************************************************************************************************
        Metodos scope para utilizar en el controlador
        Autor Raúl Chauvin
        FechaCreacion  2016/06/28
        EJ:
        $aGruposProductoClienteProyecto = GrupoProducto::byCliente($idCliente)->byProyecto($idProyecto)->byNombre($sNombre)->get();
    ******************************************************************************************************************************************/
    public function scopeByNombre($sQuery, $sNombre){
        return $sQuery->whereNombre($sNombre);
    }

    public function scopeByCliente($sQuery, $iIdCliente){
        return $sQuery->whereClienteId($iIdCliente);
    }

    public function scopeByProyecto($sQuery, $iIdProyecto){
        return $sQuery->whereProyectoId($iIdProyecto);
    }
}
